.class public Lcom/soula2/Conversation;
.super LX/0kA;
.source ""

# interfaces
.implements LX/0kP;
.implements LX/0kR;
.implements LX/0kS;
.implements LX/0kT;
.implements LX/0kU;
.implements LX/0kV;
.implements LX/0kW;
.implements LX/0kX;
.implements LX/0kY;
.implements LX/0kZ;
.implements LX/0ka;
.implements LX/0kb;
.implements LX/0kc;
.implements LX/0kd;
.implements LX/0ke;
.implements LX/0kf;


# static fields
.field public static A5B:Z

.field public static final A5C:LX/0mY;

.field public static final A5D:Ljava/lang/String;

.field public static final A5E:Ljava/lang/String;

.field public static final A5F:Ljava/util/ArrayList;

.field public static final A5G:Ljava/util/HashMap;

.field public static final A5H:Ljava/util/HashMap;

.field public static final A5I:Ljava/util/HashMap;


# instance fields
.field public A00:I

.field public A01:I

.field public A02:I

.field public A03:J

.field public A04:J

.field public A05:Landroid/animation/ObjectAnimator;

.field public A06:Landroid/content/BroadcastReceiver;

.field public A07:Landroid/os/Handler;

.field public A08:Landroid/view/View;

.field public A09:Landroid/view/View;

.field public A0A:Landroid/view/View;

.field public A0B:Landroid/view/View;

.field public A0C:Landroid/view/View;

.field public A0D:Landroid/view/View;

.field public A0E:Landroid/view/View;

.field public A0F:Landroid/view/View;

.field public A0G:Landroid/view/View;

.field public A0H:Landroid/view/View;

.field public A0I:Landroid/view/View;

.field public A0J:Landroid/view/View;

.field public A0K:Landroid/view/View;

.field public A0L:Landroid/view/ViewGroup;

.field public A0M:Landroid/view/ViewGroup;

.field public A0N:Landroid/view/ViewGroup;

.field public A0O:Landroid/view/ViewGroup;

.field public A0P:Landroid/view/ViewGroup;

.field public A0Q:Landroid/widget/FrameLayout;

.field public A0R:Landroid/widget/FrameLayout;

.field public A0S:Landroid/widget/ImageButton;

.field public A0T:Landroid/widget/ImageButton;

.field public A0U:Landroid/widget/ImageButton;

.field public A0V:Landroid/widget/ImageButton;

.field public A0W:Landroid/widget/ImageView;

.field public A0X:Landroid/widget/ImageView;

.field public A0Y:Landroid/widget/TextView;

.field public A0Z:Landroid/widget/TextView;

.field public A0a:LX/01b;

.field public A0b:LX/03w;

.field public A0c:LX/059;

.field public A0d:LX/18I;

.field public A0e:LX/10E;

.field public A0f:LX/15c;

.field public A0g:LX/2Du;

.field public A0h:LX/2Dt;

.field public A0i:LX/2Ds;

.field public A0j:LX/2Dr;

.field public A0k:LX/2Dq;

.field public A0l:LX/2Dp;

.field public A0m:LX/2Do;

.field public A0n:LX/2Dn;

.field public A0o:LX/2Dl;

.field public A0p:LX/2Dk;

.field public A0q:LX/2E0;

.field public A0r:LX/2Dj;

.field public A0s:LX/2Dz;

.field public A0t:LX/2Dw;

.field public A0u:LX/2Cj;

.field public A0v:LX/2Dv;

.field public A0w:Lcom/soula2/KeyboardPopupLayout;

.field public A0x:LX/15K;

.field public A0y:LX/0w7;

.field public A0z:LX/0vi;

.field public A10:LX/18O;

.field public A11:LX/0wg;

.field public A12:LX/1Fo;

.field public A13:Lcom/soula2/TextEmojiLabel;

.field public A14:Lcom/soula2/TextEmojiLabel;

.field public A15:LX/0yv;

.field public A16:Lcom/soula2/WaEditText;

.field public A17:LX/0sB;

.field public A18:LX/3MC;

.field public A19:LX/187;

.field public A1A:LX/0zc;

.field public A1B:LX/0yx;

.field public A1C:LX/19a;

.field public A1D:LX/18W;

.field public A1E:LX/18V;

.field public A1F:LX/18U;

.field public A1G:LX/15p;

.field public A1H:LX/1lD;

.field public A1I:LX/17i;

.field public A1J:LX/18H;

.field public A1K:LX/0wh;

.field public A1L:LX/1Dz;

.field public A1M:LX/0wO;

.field public A1N:LX/10b;

.field public A1O:LX/11o;

.field public A1P:LX/15M;

.field public A1Q:Lcom/soula2/community/ConversationCommunityViewModel;

.field public A1R:LX/18Y;

.field public A1S:LX/10Z;

.field public A1T:LX/11F;

.field public A1U:LX/0za;

.field public A1V:LX/1Hc;

.field public A1W:LX/14y;

.field public A1X:LX/11A;

.field public A1Y:LX/0vr;

.field public A1Z:LX/0mT;

.field public A1a:LX/1tq;

.field public A1b:LX/2FT;

.field public A1c:Lcom/soula2/conversation/ConversationListView;

.field public A1d:LX/17j;

.field public A1e:LX/194;

.field public A1f:LX/2w2;

.field public A1g:LX/0wi;

.field public A1h:LX/1NR;

.field public A1i:LX/0z4;

.field public A1j:LX/181;

.field public A1k:LX/18D;

.field public A1l:LX/18C;

.field public A1m:LX/20b;

.field public A1n:LX/18l;

.field public A1o:LX/18b;

.field public A1p:LX/2iK;

.field public A1q:LX/1ld;

.field public A1r:LX/1lY;

.field public A1s:LX/17o;

.field public A1t:LX/2Fe;

.field public A1u:LX/17y;

.field public A1v:LX/1kT;

.field public A1w:LX/2D2;

.field public A1x:LX/0mf;

.field public A1y:LX/0l4;

.field public A1z:LX/2iL;

.field public A20:LX/0qQ;

.field public A21:LX/0mH;

.field public A22:LX/0sE;

.field public A23:LX/00z;

.field public A24:Lcom/soula2/countrygating/viewmodel/CountryGatingViewModel;

.field public A25:LX/0yU;

.field public A26:LX/16R;

.field public A27:LX/0yY;

.field public A28:LX/0yR;

.field public A29:Lcom/soula2/ctwa/bizpreview/BusinessPreviewInitializer;

.field public A2A:LX/1be;

.field public A2B:LX/18A;

.field public A2C:LX/20a;

.field public A2D:LX/18J;

.field public A2E:LX/0rJ;

.field public A2F:LX/0rK;

.field public A2G:LX/183;

.field public A2H:LX/0yO;

.field public A2I:LX/17h;

.field public A2J:LX/0sl;

.field public A2K:LX/1Ds;

.field public A2L:LX/0vj;

.field public A2M:LX/0wa;

.field public A2N:LX/0uN;

.field public A2O:LX/0w1;

.field public A2P:LX/1C6;

.field public A2Q:LX/1Cs;

.field public A2R:LX/19M;

.field public A2S:LX/0mR;

.field public A2T:LX/0mS;

.field public A2U:LX/10g;

.field public A2V:LX/0p5;

.field public A2W:LX/0vR;

.field public A2X:LX/0uk;

.field public A2Y:LX/13P;

.field public A2Z:LX/0y5;

.field public A2a:LX/1Ea;

.field public A2b:LX/0mJ;

.field public A2c:LX/0zy;

.field public A2d:LX/0zz;

.field public A2e:LX/10p;

.field public A2f:LX/2yl;

.field public A2g:LX/147;

.field public A2h:LX/0zB;

.field public A2i:LX/1A2;

.field public A2j:LX/0mU;

.field public A2k:LX/0ma;

.field public A2l:LX/17Z;

.field public A2m:LX/35A;

.field public A2n:LX/17b;

.field public A2o:LX/11C;

.field public A2p:LX/1cK;

.field public A2q:LX/128;

.field public A2r:LX/0l8;

.field public A2s:LX/1BD;

.field public A2t:LX/0lL;

.field public A2u:LX/0lB;

.field public A2v:Lcom/soula2/mentions/MentionableEntry;

.field public A2w:LX/0yV;

.field public A2x:LX/14K;

.field public A2y:LX/0uB;

.field public A2z:LX/0ld;

.field public A30:LX/134;

.field public A31:Lcom/whatsapp/nativelibloader/WhatsAppLibLoader;

.field public A32:LX/0uh;

.field public A33:LX/0ui;

.field public A34:LX/16T;

.field public A35:LX/18k;

.field public A36:LX/152;

.field public A37:LX/0rd;

.field public A38:LX/16K;

.field public A39:LX/17m;

.field public A3A:LX/3MU;

.field public A3B:LX/17s;

.field public A3C:LX/150;

.field public A3D:LX/0xx;

.field public A3E:LX/0xw;

.field public A3F:LX/16j;

.field public A3G:LX/2FY;

.field public A3H:Lcom/soula2/polls/PollVoterViewModel;

.field public A3I:LX/0pe;

.field public A3J:LX/10W;

.field public A3K:LX/2yh;

.field public A3L:LX/0md;

.field public A3M:LX/0v9;

.field public A3N:LX/0zS;

.field public A3O:LX/105;

.field public A3P:LX/0ny;

.field public A3Q:LX/0nw;

.field public A3R:LX/11U;

.field public A3S:LX/0vz;

.field public A3T:LX/3tP;

.field public A3U:LX/184;

.field public A3V:LX/166;

.field public A3W:LX/122;

.field public A3X:LX/12d;

.field public A3Y:LX/13E;

.field public A3Z:LX/17g;

.field public A3a:LX/1A3;

.field public A3b:LX/17f;

.field public A3c:LX/1KQ;

.field public A3d:LX/17r;

.field public A3e:LX/1E8;

.field public A3f:LX/1JK;

.field public A3g:LX/121;

.field public A3h:LX/0zn;

.field public A3i:Lcom/soula2/tosgating/viewmodel/ToSGatingViewModel;

.field public A3j:LX/18B;

.field public A3k:LX/0y9;

.field public A3l:LX/17w;

.field public A3m:LX/17x;

.field public A3n:LX/15g;

.field public A3o:LX/18a;

.field public A3p:LX/1L4;

.field public A3q:LX/1L4;

.field public A3r:LX/0tc;

.field public A3s:LX/0l3;

.field public A3t:LX/0l3;

.field public A3u:LX/0l3;

.field public A3v:LX/0l3;

.field public A3w:LX/2Fb;

.field public A3x:LX/1mL;

.field public A3y:LX/2v5;

.field public A3z:LX/188;

.field public A40:LX/0qM;

.field public A41:LX/18m;

.field public A42:LX/0kz;

.field public A43:LX/2D4;

.field public A44:LX/1F5;

.field public A45:LX/0y6;

.field public A46:LX/0wm;

.field public A47:LX/0y8;

.field public A48:LX/15O;

.field public A49:LX/01K;

.field public A4A:LX/01K;

.field public A4B:LX/01K;

.field public A4C:LX/01K;

.field public A4D:LX/01K;

.field public A4E:Ljava/lang/Boolean;

.field public A4F:Ljava/lang/Runnable;

.field public A4G:Ljava/lang/String;

.field public A4H:Ljava/lang/String;

.field public A4I:Ljava/lang/String;

.field public A4J:Ljava/lang/String;

.field public A4K:Z

.field public A4L:Z

.field public A4M:Z

.field public A4N:Z

.field public A4O:Z

.field public A4P:Z

.field public A4Q:Z

.field public A4R:Z

.field public A4S:Z

.field public A4T:Z

.field public A4U:Z

.field public A4V:Z

.field public A4W:Z

.field public A4X:Z

.field public A4Y:Z

.field public A4Z:Z

.field public A4a:Z

.field public A4b:Z

.field public A4c:Z

.field public A4d:Z

.field public A4e:Z

.field public final A4f:Landroid/database/DataSetObserver;

.field public final A4g:Landroid/os/Handler;

.field public final A4h:Landroid/os/Handler;

.field public final A4i:Landroid/text/TextWatcher;

.field public final A4j:Landroid/text/TextWatcher;

.field public final A4k:Landroid/text/TextWatcher;

.field public final A4l:Landroid/view/View$OnClickListener;

.field public final A4m:Landroid/view/ViewTreeObserver$OnPreDrawListener;

.field public final A4n:Landroid/widget/AbsListView$OnScrollListener;

.field public final A4o:Landroid/widget/TextView$OnEditorActionListener;

.field public final A4p:LX/2M7;

.field public final A4q:LX/1zo;

.field public final A4r:LX/213;

.field public final A4s:LX/0ky;

.field public final A4t:LX/3xf;

.field public final A4u:LX/4uu;

.field public final A4v:LX/1W7;

.field public final A4w:LX/0t1;

.field public final A4x:LX/0kk;

.field public final A4y:LX/48s;

.field public final A4z:LX/2Fd;

.field public final A50:LX/1JJ;

.field public final A51:LX/2FZ;

.field public final A52:LX/2Ph;

.field public final A53:LX/1Ee;

.field public final A54:Ljava/lang/Runnable;

.field public final A55:Ljava/util/Set;

.field public final A56:Ljava/util/Set;

.field public final A57:Ljava/util/Set;

.field public final A58:Ljava/util/Set;

.field public final A59:Ljava/util/Stack;

.field public final A5A:[I


# direct methods
.method public static constructor <clinit>()V
    .locals 3

    .line 94815
    const-string v2, "com.soula2"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v0, ".intent.action.OPEN"

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/soula2/Conversation;->A5D:Ljava/lang/String;

    .line 94816
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v0, ".intent.action.PLAY"

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/soula2/Conversation;->A5E:Ljava/lang/String;

    .line 94817
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    sput-object v0, Lcom/soula2/Conversation;->A5F:Ljava/util/ArrayList;

    .line 94818
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lcom/soula2/Conversation;->A5I:Ljava/util/HashMap;

    .line 94819
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lcom/soula2/Conversation;->A5H:Ljava/util/HashMap;

    .line 94820
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lcom/soula2/Conversation;->A5G:Ljava/util/HashMap;

    .line 94821
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 94822
    new-instance v0, LX/0mY;

    invoke-direct {v0, v1, v2, v2}, LX/0mY;-><init>([LX/1KO;ZZ)V

    .line 94823
    sput-object v0, Lcom/soula2/Conversation;->A5C:LX/0mY;

    return-void
.end method

.method public constructor <init>()V
    .locals 6

    .line 94824
    invoke-direct {p0}, LX/0kA;-><init>()V

    const/4 v2, 0x0

    .line 94825
    iput-boolean v2, p0, Lcom/soula2/Conversation;->A4R:Z

    .line 94826
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/soula2/Conversation;->A56:Ljava/util/Set;

    .line 94827
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/soula2/Conversation;->A55:Ljava/util/Set;

    .line 94828
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/soula2/Conversation;->A57:Ljava/util/Set;

    .line 94829
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/soula2/Conversation;->A58:Ljava/util/Set;

    .line 94830
    iput v2, p0, Lcom/soula2/Conversation;->A00:I

    const-wide/16 v4, -0x1

    .line 94831
    iput-wide v4, p0, Lcom/soula2/Conversation;->A03:J

    .line 94832
    new-instance v0, LX/0kk;

    invoke-direct {v0}, LX/0kk;-><init>()V

    iput-object v0, p0, Lcom/soula2/Conversation;->A4x:LX/0kk;

    const/4 v3, 0x1

    .line 94833
    iput-boolean v3, p0, Lcom/soula2/Conversation;->A4Z:Z

    .line 94834
    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    iput-object v0, p0, Lcom/soula2/Conversation;->A4h:Landroid/os/Handler;

    const/4 v1, 0x0

    .line 94835
    iput-object v1, p0, Lcom/soula2/Conversation;->A3p:LX/1L4;

    .line 94836
    new-instance v0, Ljava/util/Stack;

    invoke-direct {v0}, Ljava/util/Stack;-><init>()V

    iput-object v0, p0, Lcom/soula2/Conversation;->A59:Ljava/util/Stack;

    .line 94837
    new-instance v0, Lcom/whatsapp/voipcalling/IDxCObserverShape112S0100000_1_I0;

    invoke-direct {v0, p0, v2}, Lcom/whatsapp/voipcalling/IDxCObserverShape112S0100000_1_I0;-><init>(Ljava/lang/Object;I)V

    iput-object v0, p0, Lcom/soula2/Conversation;->A53:LX/1Ee;

    .line 94838
    new-instance v0, LX/2we;

    invoke-direct {v0, p0}, LX/2we;-><init>(Lcom/soula2/Conversation;)V

    iput-object v0, p0, Lcom/soula2/Conversation;->A52:LX/2Ph;

    .line 94839
    new-instance v0, LX/2M7;

    invoke-direct {v0, p0}, LX/2M7;-><init>(Lcom/soula2/Conversation;)V

    iput-object v0, p0, Lcom/soula2/Conversation;->A4p:LX/2M7;

    .line 94840
    new-instance v0, LX/0ky;

    invoke-direct {v0, p0}, LX/0ky;-><init>(Lcom/soula2/Conversation;)V

    iput-object v0, p0, Lcom/soula2/Conversation;->A4s:LX/0ky;

    .line 94841
    new-instance v0, Lcom/soula2/payments/IDxAObserverShape93S0100000_2_I0;

    invoke-direct {v0, p0, v2}, Lcom/soula2/payments/IDxAObserverShape93S0100000_2_I0;-><init>(Ljava/lang/Object;I)V

    iput-object v0, p0, Lcom/soula2/Conversation;->A4y:LX/48s;

    .line 94842
    new-instance v0, Lcom/soula2/biz/IDxPObserverShape64S0100000_2_I0;

    invoke-direct {v0, p0, v2}, Lcom/soula2/biz/IDxPObserverShape64S0100000_2_I0;-><init>(Ljava/lang/Object;I)V

    iput-object v0, p0, Lcom/soula2/Conversation;->A4v:LX/1W7;

    .line 94843
    new-instance v0, Lcom/whatsapp/stickers/IDxSObserverShape102S0100000_2_I0;

    invoke-direct {v0, p0, v2}, Lcom/whatsapp/stickers/IDxSObserverShape102S0100000_2_I0;-><init>(Ljava/lang/Object;I)V

    iput-object v0, p0, Lcom/soula2/Conversation;->A50:LX/1JJ;

    .line 94844
    new-instance v0, LX/3xf;

    invoke-direct {v0, p0}, LX/3xf;-><init>(Lcom/soula2/Conversation;)V

    iput-object v0, p0, Lcom/soula2/Conversation;->A4t:LX/3xf;

    .line 94845
    iput-wide v4, p0, Lcom/soula2/Conversation;->A04:J

    .line 94846
    iput-object v1, p0, Lcom/soula2/Conversation;->A05:Landroid/animation/ObjectAnimator;

    .line 94847
    new-instance v0, LX/4Zr;

    invoke-direct {v0, p0}, LX/4Zr;-><init>(Lcom/soula2/Conversation;)V

    iput-object v0, p0, Lcom/soula2/Conversation;->A4z:LX/2Fd;

    .line 94848
    new-instance v0, LX/36e;

    invoke-direct {v0, p0}, LX/36e;-><init>(Lcom/soula2/Conversation;)V

    iput-object v0, p0, Lcom/soula2/Conversation;->A4o:Landroid/widget/TextView$OnEditorActionListener;

    .line 94849
    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    iput-object v0, p0, Lcom/soula2/Conversation;->A4g:Landroid/os/Handler;

    .line 94850
    const/16 v1, 0x2c

    new-instance v0, Lcom/facebook/redex/RunnableRunnableShape1S0100000_I0;

    invoke-direct {v0, p0, v1}, Lcom/facebook/redex/RunnableRunnableShape1S0100000_I0;-><init>(Lcom/soula2/Conversation;I)V

    iput-object v0, p0, Lcom/soula2/Conversation;->A54:Ljava/lang/Runnable;

    .line 94851
    new-instance v0, LX/1rP;

    invoke-direct {v0, p0}, LX/1rP;-><init>(Lcom/soula2/Conversation;)V

    iput-object v0, p0, Lcom/soula2/Conversation;->A4n:Landroid/widget/AbsListView$OnScrollListener;

    .line 94852
    new-instance v0, Lcom/soula2/text/IDxWAdapterShape105S0100000_2_I0;

    invoke-direct {v0, p0, v2}, Lcom/soula2/text/IDxWAdapterShape105S0100000_2_I0;-><init>(Ljava/lang/Object;I)V

    iput-object v0, p0, Lcom/soula2/Conversation;->A4i:Landroid/text/TextWatcher;

    .line 94853
    new-instance v0, Lcom/soula2/text/IDxWAdapterShape104S0100000_1_I0;

    invoke-direct {v0, p0, v2}, Lcom/soula2/text/IDxWAdapterShape104S0100000_1_I0;-><init>(Ljava/lang/Object;I)V

    iput-object v0, p0, Lcom/soula2/Conversation;->A4k:Landroid/text/TextWatcher;

    .line 94854
    new-instance v0, Lcom/facebook/redex/IDxDListenerShape187S0100000_2_I0;

    invoke-direct {v0, p0, v3}, Lcom/facebook/redex/IDxDListenerShape187S0100000_2_I0;-><init>(Ljava/lang/Object;I)V

    iput-object v0, p0, Lcom/soula2/Conversation;->A4m:Landroid/view/ViewTreeObserver$OnPreDrawListener;

    .line 94855
    const/4 v1, 0x5

    new-instance v0, Lcom/facebook/redex/ViewOnClickCListenerShape6S0100000_I0;

    invoke-direct {v0, p0, v1}, Lcom/facebook/redex/ViewOnClickCListenerShape6S0100000_I0;-><init>(Ljava/lang/Object;I)V

    iput-object v0, p0, Lcom/soula2/Conversation;->A4l:Landroid/view/View$OnClickListener;

    .line 94856
    new-instance v0, LX/2VY;

    invoke-direct {v0, p0}, LX/2VY;-><init>(Lcom/soula2/Conversation;)V

    iput-object v0, p0, Lcom/soula2/Conversation;->A4f:Landroid/database/DataSetObserver;

    .line 94857
    new-instance v0, Lcom/soula2/data/IDxMObserverShape79S0100000_1_I0;

    invoke-direct {v0, p0, v2}, Lcom/soula2/data/IDxMObserverShape79S0100000_1_I0;-><init>(Ljava/lang/Object;I)V

    iput-object v0, p0, Lcom/soula2/Conversation;->A4w:LX/0t1;

    const/4 v0, 0x2

    new-array v0, v0, [I

    .line 94858
    iput-object v0, p0, Lcom/soula2/Conversation;->A5A:[I

    .line 94859
    iput-boolean v2, p0, Lcom/soula2/Conversation;->A4d:Z

    .line 94860
    new-instance v0, Lcom/soula2/text/IDxWAdapterShape104S0100000_1_I0;

    invoke-direct {v0, p0, v3}, Lcom/soula2/text/IDxWAdapterShape104S0100000_1_I0;-><init>(Ljava/lang/Object;I)V

    iput-object v0, p0, Lcom/soula2/Conversation;->A4j:Landroid/text/TextWatcher;

    .line 94861
    new-instance v0, Lcom/facebook/redex/IDxCListenerShape208S0100000_1_I0;

    invoke-direct {v0, p0, v2}, Lcom/facebook/redex/IDxCListenerShape208S0100000_1_I0;-><init>(Ljava/lang/Object;I)V

    iput-object v0, p0, Lcom/soula2/Conversation;->A4u:LX/4uu;

    .line 94862
    new-instance v0, Lcom/facebook/redex/IDxSListenerShape280S0100000_1_I0;

    invoke-direct {v0, p0, v2}, Lcom/facebook/redex/IDxSListenerShape280S0100000_1_I0;-><init>(Ljava/lang/Object;I)V

    iput-object v0, p0, Lcom/soula2/Conversation;->A51:LX/2FZ;

    .line 94863
    new-instance v0, LX/1zo;

    invoke-direct {v0, p0}, LX/1zo;-><init>(Lcom/soula2/Conversation;)V

    iput-object v0, p0, Lcom/soula2/Conversation;->A4q:LX/1zo;

    .line 94864
    new-instance v0, LX/213;

    invoke-direct {v0, p0}, LX/213;-><init>(Lcom/soula2/Conversation;)V

    iput-object v0, p0, Lcom/soula2/Conversation;->A4r:LX/213;

    return-void
.end method

.method public static A02(ZZZ)Landroid/view/animation/Animation;
    .locals 11

    .line 94865
    const/4 v5, 0x1

    new-instance v3, Landroid/view/animation/AnimationSet;

    invoke-direct {v3, v5}, Landroid/view/animation/AnimationSet;-><init>(Z)V

    const-wide/16 v0, 0x64

    const/high16 v8, 0x3f800000    # 1.0f

    const/4 v10, 0x0

    if-eqz p2, :cond_0

    .line 94866
    new-instance v2, Landroid/view/animation/AlphaAnimation;

    if-eqz p1, :cond_3

    invoke-direct {v2, v10, v8}, Landroid/view/animation/AlphaAnimation;-><init>(FF)V

    .line 94867
    :goto_0
    invoke-virtual {v2, v0, v1}, Landroid/view/animation/Animation;->setDuration(J)V

    .line 94868
    invoke-virtual {v3, v2}, Landroid/view/animation/AnimationSet;->addAnimation(Landroid/view/animation/Animation;)V

    :cond_0
    if-nez p0, :cond_1

    const/high16 v8, -0x40800000    # -1.0f

    .line 94869
    :cond_1
    const/4 v6, 0x0

    if-eqz p1, :cond_2

    move v6, v8

    const/4 v8, 0x0

    .line 94870
    :cond_2
    new-instance v4, Landroid/view/animation/TranslateAnimation;

    move v7, v5

    move v9, v5

    move p0, v5

    move p1, v10

    invoke-direct/range {v4 .. v12}, Landroid/view/animation/TranslateAnimation;-><init>(IFIFIFIF)V

    .line 94871
    invoke-virtual {v4, v0, v1}, Landroid/view/animation/Animation;->setDuration(J)V

    .line 94872
    invoke-virtual {v3, v4}, Landroid/view/animation/AnimationSet;->addAnimation(Landroid/view/animation/Animation;)V

    .line 94873
    invoke-virtual {v3, v0, v1}, Landroid/view/animation/Animation;->setDuration(J)V

    return-object v3

    .line 94874
    :cond_3
    invoke-direct {v2, v8, v10}, Landroid/view/animation/AlphaAnimation;-><init>(FF)V

    goto :goto_0
.end method

.method public static A03(Landroid/content/Intent;LX/2JU;)V
    .locals 2

    if-eqz p1, :cond_1

    .line 94875
    iget-object v0, p1, LX/2JU;->A02:Ljava/util/List;

    if-eqz v0, :cond_1

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_1

    .line 94876
    iget-object v1, p1, LX/2JU;->A02:Ljava/util/List;

    if-nez v1, :cond_0

    .line 94877
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p1, LX/2JU;->A02:Ljava/util/List;

    .line 94878
    :cond_0
    const/4 v0, 0x0

    invoke-interface {v1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    const-string/jumbo v0, "preferred_label"

    .line 94879
    invoke-virtual {p0, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    :cond_1
    return-void
.end method

.method public static synthetic A09(Landroid/os/Bundle;Lcom/soula2/Conversation;)V
    .locals 3

    .line 94880
    const-string v0, "dialogAction"

    .line 94881
    invoke-virtual {p0, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    const/4 v2, 0x1

    if-ne v0, v2, :cond_0

    const-string v0, "parentGroupJid"

    .line 94882
    invoke-virtual {p0, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v1

    check-cast v1, LX/0nO;

    .line 94883
    invoke-static {v1}, LX/009;->A05(Ljava/lang/Object;)V

    .line 94884
    iget-object v0, p1, Lcom/soula2/Conversation;->A0s:LX/2Dz;

    .line 94885
    invoke-virtual {v0, p1, v1, v2}, LX/2Dz;->A00(LX/0kF;LX/0nO;I)LX/2zt;

    move-result-object v1

    .line 94886
    iget-object v0, p1, LX/0kF;->A01:LX/0nN;

    .line 94887
    invoke-virtual {v0}, LX/0nN;->A0B()V

    .line 94888
    iget-object v0, v0, LX/0nN;->A05:LX/1H4;

    .line 94889
    invoke-virtual {v1, v0}, LX/2zt;->A00(Lcom/whatsapp/jid/UserJid;)V

    .line 94890
    :cond_0
    return-void
.end method

.method public static synthetic A0A(Lcom/soula2/Conversation;I)V
    .locals 2

    .line 94891
    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v0, 0x15

    if-lt v1, v0, :cond_1

    .line 94892
    invoke-virtual {p0}, Landroid/app/Activity;->getWindow()Landroid/view/Window;

    move-result-object v1

    const v0, 0x7f060424

    if-nez p1, :cond_0

    .line 94893
    const v0, 0x7f0600db

    .line 94894
    :cond_0
    invoke-static {p0, v0}, LX/00S;->A00(Landroid/content/Context;I)I

    move-result v0

    .line 94895
    invoke-virtual {v1, v0}, Landroid/view/Window;->setStatusBarColor(I)V

    .line 94896
    :cond_1
    return-void
.end method

.method public static synthetic A0B(Lcom/soula2/Conversation;Z)V
    .locals 10

    .line 94897
    move-object v4, p0

    iget-object v0, p0, Lcom/soula2/Conversation;->A1x:LX/0mf;

    .line 94898
    iget-object v0, v0, LX/0mf;->A09:Ljava/lang/String;

    .line 94899
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 94900
    iget-object v1, p0, Lcom/soula2/Conversation;->A1x:LX/0mf;

    const/4 v0, 0x0

    .line 94901
    iput-object v0, v1, LX/0mf;->A09:Ljava/lang/String;

    .line 94902
    iput-object v0, v1, LX/0mf;->A0B:Ljava/util/ArrayList;

    .line 94903
    :cond_0
    return-void

    .line 94904
    :cond_1
    iget-object v1, p0, Lcom/soula2/Conversation;->A1x:LX/0mf;

    .line 94905
    iget-object v0, v1, LX/0mf;->A09:Ljava/lang/String;

    .line 94906
    iput-object v0, v1, LX/0mf;->A0A:Ljava/lang/String;

    .line 94907
    invoke-virtual {p0}, LX/0kB;->A2a()V

    .line 94908
    iget-object v0, p0, Lcom/soula2/Conversation;->A1c:Lcom/soula2/conversation/ConversationListView;

    invoke-virtual {v0}, Lcom/soula2/conversation/ConversationListView;->getFirstPosition()I

    move-result v3

    .line 94909
    const-string v0, "conversation/search/ first-visible:"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1, v0}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/soula2/Conversation;->A1c:Lcom/soula2/conversation/ConversationListView;

    .line 94910
    invoke-virtual {v0}, Landroid/widget/AdapterView;->getFirstVisiblePosition()I

    move-result v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v0, " header-count:"

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v0, p0, Lcom/soula2/Conversation;->A1c:Lcom/soula2/conversation/ConversationListView;

    .line 94911
    invoke-virtual {v0}, Landroid/widget/ListView;->getHeaderViewsCount()I

    move-result v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v0, " first-pos:"

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v0, " up:"

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    .line 94912
    invoke-static {v0}, Lcom/whatsapp/util/Log;->i(Ljava/lang/String;)V

    if-gez v3, :cond_2

    .line 94913
    iget-object v1, p0, LX/0kH;->A05:LX/0li;

    const v0, 0x7f120e24

    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, LX/0li;->A08(II)V

    .line 94914
    iget-object v1, p0, Lcom/soula2/Conversation;->A16:Lcom/soula2/WaEditText;

    const/16 v0, 0x8

    invoke-virtual {v1, v0}, Landroid/view/View;->sendAccessibilityEvent(I)V

    .line 94915
    iget-object v0, p0, Lcom/soula2/Conversation;->A16:Lcom/soula2/WaEditText;

    invoke-virtual {v0}, Landroid/view/View;->requestFocus()Z

    .line 94916
    iget-object v0, p0, Lcom/soula2/Conversation;->A16:Lcom/soula2/WaEditText;

    .line 94917
    invoke-virtual {v0, v2}, Lcom/soula2/WaEditText;->A05(Z)V

    .line 94918
    return-void

    .line 94919
    :cond_2
    iget-object v0, p0, Lcom/soula2/Conversation;->A1c:Lcom/soula2/conversation/ConversationListView;

    invoke-virtual {v0}, Lcom/soula2/conversation/ConversationListView;->getConversationCursorAdapter()LX/0me;

    move-result-object v2

    .line 94920
    invoke-virtual {v2, v3}, LX/0me;->A06(I)LX/0md;

    move-result-object p0

    if-eqz p0, :cond_0

    .line 94921
    iget-object v0, p0, LX/0md;->A0z:LX/1HF;

    .line 94922
    iget-object v0, v0, LX/1HF;->A00:LX/0l8;

    .line 94923
    const/4 v1, 0x1

    if-nez v0, :cond_3

    const-string v0, "conversation/search/divider-at-search-position"

    .line 94924
    invoke-static {v0}, Lcom/whatsapp/util/Log;->i(Ljava/lang/String;)V

    add-int/lit8 v0, v3, 0x1

    .line 94925
    invoke-virtual {v2, v0}, LX/0me;->A06(I)LX/0md;

    move-result-object p0

    if-nez p0, :cond_3

    .line 94926
    const-string v0, "conversation/search/first-message-is-null"

    .line 94927
    invoke-static {v0}, Lcom/whatsapp/util/Log;->w(Ljava/lang/String;)V

    return-void

    .line 94928
    :cond_3
    iget-object v0, v4, Lcom/soula2/Conversation;->A1f:LX/2w2;

    if-eqz v0, :cond_4

    .line 94929
    invoke-virtual {v0, v1}, LX/0ng;->A08(Z)V

    .line 94930
    :cond_4
    iget-object v8, v4, Lcom/soula2/Conversation;->A2T:LX/0mS;

    if-nez v8, :cond_5

    .line 94931
    iget-object v1, v4, Lcom/soula2/Conversation;->A2S:LX/0mR;

    iget-object v0, v4, Lcom/soula2/Conversation;->A2r:LX/0l8;

    invoke-virtual {v1, v0}, LX/0mR;->A09(LX/0l8;)LX/0mS;

    move-result-object v8

    iput-object v8, v4, Lcom/soula2/Conversation;->A2T:LX/0mS;

    .line 94932
    :cond_5
    iget-object v1, v4, Lcom/soula2/Conversation;->A1x:LX/0mf;

    .line 94933
    iget-object v0, v1, LX/0mf;->A09:Ljava/lang/String;

    .line 94934
    invoke-virtual {v8, v0}, LX/0mS;->A05(Ljava/lang/CharSequence;)V

    .line 94935
    iget-object v0, v1, LX/0mf;->A0B:Ljava/util/ArrayList;

    .line 94936
    invoke-virtual {v8, v0}, LX/0mS;->A06(Ljava/util/List;)V

    .line 94937
    iget-object v5, v4, LX/0kF;->A05:LX/0lm;

    iget-object v9, v4, Lcom/soula2/Conversation;->A2Y:LX/13P;

    iget-object v7, v4, Lcom/soula2/Conversation;->A2S:LX/0mR;

    iget-object v6, v4, LX/0kB;->A0M:LX/0nZ;

    new-instance v3, LX/2w2;

    invoke-direct/range {v3 .. v11}, LX/2w2;-><init>(LX/0kf;LX/0lm;LX/0nZ;LX/0mR;LX/0mS;LX/13P;LX/0md;Z)V

    iput-object v3, v4, Lcom/soula2/Conversation;->A1f:LX/2w2;

    .line 94938
    iget-object v1, v4, LX/0kJ;->A05:LX/0lO;

    const/4 v0, 0x0

    new-array v0, v0, [Ljava/lang/Void;

    invoke-interface {v1, v3, v0}, LX/0lO;->AZp(LX/0ng;[Ljava/lang/Object;)V

    return-void
.end method

.method public static A0D(LX/10E;LX/0l8;)V
    .locals 1

    .line 94939
    invoke-virtual {p0}, LX/10E;->A00()LX/1hf;

    move-result-object p0

    .line 94940
    iget-boolean v0, p0, LX/1hf;->A02:Z

    .line 94941
    if-eqz v0, :cond_3

    .line 94942
    if-eqz p1, :cond_0

    .line 94943
    invoke-virtual {p0}, LX/1hf;->A00()Lcom/soula2/Conversation;

    move-result-object v0

    iget-object v0, v0, Lcom/soula2/Conversation;->A2r:LX/0l8;

    invoke-virtual {p1, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 94944
    :cond_0
    invoke-virtual {p0}, LX/1hf;->A00()Lcom/soula2/Conversation;

    move-result-object p0

    .line 94945
    :cond_1
    :goto_0
    invoke-virtual {p0}, Landroid/app/Activity;->finish()V

    :cond_2
    return-void

    .line 94946
    :cond_3
    iget-object p0, p0, LX/1hf;->A00:Lcom/soula2/Conversation;

    if-eqz p0, :cond_2

    .line 94947
    invoke-virtual {p0}, Landroid/app/Activity;->isFinishing()Z

    move-result v0

    if-nez v0, :cond_2

    .line 94948
    if-eqz p1, :cond_1

    .line 94949
    iget-object v0, p0, Lcom/soula2/Conversation;->A2r:LX/0l8;

    invoke-virtual {p1, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    goto :goto_0
.end method


# virtual methods
.method public A1d()I
    .locals 1

    const v0, 0x29f511de

    return v0
.end method

.method public A1e()LX/1OF;
    .locals 2

    .line 94950
    invoke-super {p0}, LX/0kK;->A1e()LX/1OF;

    move-result-object v1

    .line 94951
    const/4 v0, 0x1

    .line 94952
    iput-boolean v0, v1, LX/1OF;->A01:Z

    .line 94953
    iput-boolean v0, v1, LX/1OF;->A03:Z

    .line 94954
    return-object v1
.end method

.method public A1i()V
    .locals 17

    .line 94955
    move-object/from16 v12, p0

    iget-boolean v0, v12, Lcom/soula2/Conversation;->A4K:Z

    if-nez v0, :cond_3

    .line 94956
    invoke-virtual {v12}, LX/0kK;->A1l()V

    .line 94957
    invoke-virtual {v12}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string/jumbo v3, "start_t"

    invoke-virtual {v0, v3}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 94958
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v9

    .line 94959
    invoke-virtual {v12}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    move-result-object v2

    const-wide/16 v0, 0x0

    invoke-virtual {v2, v3, v0, v1}, Landroid/content/Intent;->getLongExtra(Ljava/lang/String;J)J

    move-result-wide v4

    sub-long/2addr v9, v4

    .line 94960
    iget-object v2, v12, Lcom/soula2/Conversation;->A3E:LX/0xw;

    .line 94961
    iget-object v5, v2, LX/0xw;->A00:LX/0xf;

    const v4, 0x29f52e4a

    const/4 v2, 0x2

    invoke-interface {v5, v4, v2}, LX/0xf;->AJY(IS)V

    .line 94962
    iget-object v5, v12, Lcom/soula2/Conversation;->A2r:LX/0l8;

    .line 94963
    instance-of v2, v5, LX/0nQ;

    if-eqz v2, :cond_5

    .line 94964
    iget-object v2, v12, LX/0kJ;->A05:LX/0lO;

    const/4 v14, 0x0

    new-instance v11, Lcom/facebook/redex/RunnableRunnableShape0S0200100_I0;

    move-object v13, v5

    move-wide v15, v9

    invoke-direct/range {v11 .. v16}, Lcom/facebook/redex/RunnableRunnableShape0S0200100_I0;-><init>(Ljava/lang/Object;Ljava/lang/Object;IJ)V

    invoke-interface {v2, v11}, LX/0lO;->AZm(Ljava/lang/Runnable;)V

    .line 94965
    :goto_0
    iget-object v7, v12, Lcom/soula2/Conversation;->A2r:LX/0l8;

    instance-of v2, v7, Lcom/whatsapp/jid/UserJid;

    if-eqz v2, :cond_1

    .line 94966
    iget-object v4, v12, Lcom/soula2/Conversation;->A1X:LX/11A;

    .line 94967
    iget-object v6, v4, LX/11A;->A04:LX/0kj;

    const/16 v5, 0x478

    .line 94968
    sget-object v2, LX/0kl;->A02:LX/0kl;

    invoke-virtual {v6, v2, v5}, LX/0kj;->A01(LX/0kl;I)I

    move-result v5

    .line 94969
    const/4 v2, -0x1

    if-eq v5, v2, :cond_4

    iget-object v2, v4, LX/11A;->A01:LX/0nN;

    .line 94970
    invoke-virtual {v2}, LX/0nN;->A0G()Z

    move-result v2

    if-nez v2, :cond_4

    .line 94971
    :cond_0
    iget-object v6, v4, LX/11A;->A06:LX/0lO;

    const/16 v2, 0x25

    new-instance v5, Lcom/facebook/redex/RunnableRunnableShape4S0200000_I0_2;

    invoke-direct {v5, v4, v2, v7}, Lcom/facebook/redex/RunnableRunnableShape4S0200000_I0_2;-><init>(Ljava/lang/Object;ILjava/lang/Object;)V

    const-string v4, "LogOutContactChatOpen"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v6, v5, v2}, LX/0lO;->AZo(Ljava/lang/Runnable;Ljava/lang/String;)V

    .line 94972
    :cond_1
    :goto_1
    invoke-virtual {v12}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    move-result-object v2

    invoke-virtual {v2, v3}, Landroid/content/Intent;->removeExtra(Ljava/lang/String;)V

    .line 94973
    iget-object v4, v12, LX/0kH;->A0C:LX/0kj;

    const/16 v3, 0x60e

    .line 94974
    sget-object v2, LX/0kl;->A02:LX/0kl;

    invoke-virtual {v4, v2, v3}, LX/0kj;->A01(LX/0kl;I)I

    move-result v2

    .line 94975
    iget-object v6, v12, Lcom/soula2/Conversation;->A2g:LX/147;

    int-to-long v2, v2

    const-string v5, "chat_open"

    cmp-long v4, v2, v0

    if-lez v4, :cond_2

    cmp-long v0, v9, v2

    if-ltz v0, :cond_2

    .line 94976
    new-instance v1, LX/1dQ;

    invoke-direct {v1}, LX/1dQ;-><init>()V

    .line 94977
    iput-object v5, v1, LX/1dQ;->A02:Ljava/lang/String;

    .line 94978
    invoke-static {v9, v10}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, v1, LX/1dQ;->A00:Ljava/lang/Long;

    .line 94979
    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, v1, LX/1dQ;->A01:Ljava/lang/Long;

    .line 94980
    iget-object v0, v6, LX/147;->A0I:LX/0pG;

    invoke-virtual {v0, v1}, LX/0pG;->A05(LX/0p8;)V

    .line 94981
    :cond_2
    iget-object v4, v12, Lcom/soula2/Conversation;->A3g:LX/121;

    iget-object v3, v12, Lcom/soula2/Conversation;->A2r:LX/0l8;

    .line 94982
    iget-object v2, v4, LX/121;->A06:LX/1G8;

    const/16 v1, 0xb

    new-instance v0, Lcom/facebook/redex/RunnableRunnableShape10S0200000_I0_8;

    invoke-direct {v0, v4, v1, v3}, Lcom/facebook/redex/RunnableRunnableShape10S0200000_I0_8;-><init>(Ljava/lang/Object;ILjava/lang/Object;)V

    invoke-virtual {v2, v0}, LX/1G8;->execute(Ljava/lang/Runnable;)V

    .line 94983
    :cond_3
    return-void

    .line 94984
    :cond_4
    const/16 v2, 0x681

    invoke-virtual {v6, v2}, LX/0kj;->A07(I)Z

    move-result v2

    .line 94985
    if-nez v2, :cond_0

    goto :goto_1

    .line 94986
    :cond_5
    iget-object v4, v12, Lcom/soula2/Conversation;->A2g:LX/147;

    const/4 v6, 0x3

    const/4 v7, 0x0

    move v8, v7

    invoke-virtual/range {v4 .. v10}, LX/147;->A08(LX/0l8;IIIJ)V

    goto/16 :goto_0
.end method

.method public A1r()V
    .locals 1

    const-string v0, "Conversation/onActivityAsyncInit"

    .line 94987
    invoke-static {v0}, Lcom/whatsapp/util/Log;->d(Ljava/lang/String;)V

    .line 94988
    iget-object v0, p0, Lcom/soula2/Conversation;->A21:LX/0mH;

    invoke-virtual {v0}, LX/0mH;->A01()V

    return-void
.end method

.method public A1s()Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method public A21(I)V
    .locals 2

    .line 94989
    iget-object v0, p0, Lcom/soula2/Conversation;->A18:LX/3MC;

    if-eqz v0, :cond_0

    iget-object v1, p0, LX/0kH;->A0C:LX/0kj;

    const/16 v0, 0x6e6

    .line 94990
    invoke-virtual {v1, v0}, LX/0kj;->A07(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 94991
    iget-object v0, p0, Lcom/soula2/Conversation;->A18:LX/3MC;

    .line 94992
    iget-object v0, v0, LX/3MC;->A01:LX/1qk;

    invoke-virtual {v0}, LX/1qk;->A00()V

    .line 94993
    :cond_0
    return-void
.end method

.method public A2T()Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method public A2d(I)V
    .locals 3

    .line 94994
    invoke-super {p0, p1}, LX/0kB;->A2d(I)V

    .line 94995
    iget-object v0, p0, LX/0kB;->A0i:Lcom/soula2/reactions/ReactionsTrayViewModel;

    invoke-static {v0}, LX/009;->A05(Ljava/lang/Object;)V

    const/4 v1, 0x0

    const/4 v0, 0x2

    if-ne p1, v0, :cond_1

    .line 94996
    iget-object v0, p0, Lcom/soula2/Conversation;->A2j:LX/0mU;

    .line 94997
    invoke-virtual {v0, v1}, LX/0mU;->A0I(I)V

    .line 94998
    iget-object v1, p0, Lcom/soula2/Conversation;->A09:Landroid/view/View;

    const/16 v0, 0x8

    invoke-virtual {v1, v0}, Landroid/view/View;->setVisibility(I)V

    .line 94999
    iget-object v2, p0, LX/0kH;->A05:LX/0li;

    const/16 v1, 0x1c

    new-instance v0, Lcom/facebook/redex/RunnableRunnableShape1S0100000_I0;

    invoke-direct {v0, p0, v1}, Lcom/facebook/redex/RunnableRunnableShape1S0100000_I0;-><init>(Lcom/soula2/Conversation;I)V

    invoke-virtual {v2, v0}, LX/0li;->A0J(Ljava/lang/Runnable;)V

    .line 95000
    :cond_0
    return-void

    .line 95001
    :cond_1
    if-nez p1, :cond_0

    .line 95002
    iget-object v0, p0, Lcom/soula2/Conversation;->A1v:LX/1kT;

    invoke-virtual {v0}, LX/1kT;->A05()Z

    move-result v0

    .line 95003
    if-nez v0, :cond_0

    .line 95004
    iget-object v0, p0, Lcom/soula2/Conversation;->A09:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 95005
    iget-object v0, p0, Lcom/soula2/Conversation;->A2k:LX/0ma;

    invoke-virtual {v0}, LX/0mb;->A01()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 95006
    iget-object v0, p0, Lcom/soula2/Conversation;->A2k:LX/0ma;

    invoke-virtual {v0, v1}, LX/0mb;->A00(Z)V

    return-void
.end method

.method public final A2f()I
    .locals 4

    .line 95007
    new-instance v1, Landroid/graphics/Point;

    invoke-direct {v1}, Landroid/graphics/Point;-><init>()V

    .line 95008
    invoke-virtual {p0}, Landroid/app/Activity;->getWindowManager()Landroid/view/WindowManager;

    move-result-object v0

    invoke-interface {v0}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/view/Display;->getSize(Landroid/graphics/Point;)V

    .line 95009
    iget v0, v1, Landroid/graphics/Point;->y:I

    shl-int/lit8 v3, v0, 0x1

    .line 95010
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v0, 0x7f070236

    invoke-virtual {v1, v0}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    div-int/2addr v3, v0

    .line 95011
    iget-object v0, p0, Lcom/soula2/Conversation;->A1x:LX/0mf;

    .line 95012
    iget v2, v0, LX/0mf;->A03:I

    .line 95013
    add-int/lit8 v0, v3, -0xa

    if-le v2, v0, :cond_0

    .line 95014
    const-string v0, "conversation/page size (from unseen):"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1, v0}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 95015
    add-int/lit8 v0, v2, 0xa

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    .line 95016
    invoke-static {v0}, Lcom/whatsapp/util/Log;->i(Ljava/lang/String;)V

    .line 95017
    iget-object v0, p0, Lcom/soula2/Conversation;->A1x:LX/0mf;

    .line 95018
    iget v0, v0, LX/0mf;->A03:I

    .line 95019
    add-int/lit8 v3, v0, 0xa

    return v3

    .line 95020
    :cond_0
    const-string v1, "conversation/page size:"

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/whatsapp/util/Log;->i(Ljava/lang/String;)V

    return v3
.end method

.method public A2g(I)I
    .locals 1

    if-eqz p1, :cond_2

    const/4 v0, 0x1

    if-eq p1, v0, :cond_1

    .line 95021
    iget-object v0, p0, Lcom/soula2/Conversation;->A0M:Landroid/view/ViewGroup;

    .line 95022
    invoke-virtual {v0}, Landroid/view/View;->getVisibility()I

    move-result v0

    if-eqz v0, :cond_0

    :goto_0
    iget-object v0, p0, Lcom/soula2/Conversation;->A0P:Landroid/view/ViewGroup;

    .line 95023
    invoke-virtual {v0}, Landroid/view/View;->getVisibility()I

    move-result v0

    if-nez v0, :cond_3

    .line 95024
    :cond_0
    :goto_1
    const v0, 0x7f080374

    .line 95025
    return v0

    .line 95026
    :cond_1
    iget-object v0, p0, Lcom/soula2/Conversation;->A2v:Lcom/soula2/mentions/MentionableEntry;

    .line 95027
    iget-boolean v0, v0, Lcom/soula2/mentions/MentionableEntry;->A0H:Z

    .line 95028
    if-nez v0, :cond_4

    .line 95029
    iget-object v0, p0, Lcom/soula2/Conversation;->A0M:Landroid/view/ViewGroup;

    .line 95030
    invoke-virtual {v0}, Landroid/view/View;->getVisibility()I

    move-result v0

    if-nez v0, :cond_3

    goto :goto_0

    .line 95031
    :cond_2
    iget-object v0, p0, Lcom/soula2/Conversation;->A2v:Lcom/soula2/mentions/MentionableEntry;

    .line 95032
    iget-boolean v0, v0, Lcom/soula2/mentions/MentionableEntry;->A0H:Z

    .line 95033
    if-nez v0, :cond_4

    goto :goto_1

    .line 95034
    :cond_3
    const v0, 0x7f080377

    return v0

    .line 95035
    :cond_4
    const v0, 0x7f080375

    return v0
.end method

.method public final A2h()LX/0md;
    .locals 2

    .line 95036
    iget-object v1, p0, LX/0kB;->A0J:LX/1tx;

    if-eqz v1, :cond_0

    .line 95037
    iget-object v0, v1, LX/1tx;->A04:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/AbstractMap;->isEmpty()Z

    move-result v0

    .line 95038
    if-nez v0, :cond_0

    .line 95039
    iget-object v0, v1, LX/1tx;->A04:Ljava/util/HashMap;

    .line 95040
    invoke-virtual {v0}, Ljava/util/AbstractMap;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0md;

    return-object v0

    :cond_0
    const/4 v0, 0x0

    return-object v0
.end method

.method public A2i()V
    .locals 9

    .line 95041
    iget-object v0, p0, Lcom/soula2/Conversation;->A2C:LX/20a;

    invoke-virtual {v0}, LX/20a;->A01()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 95042
    const/16 v0, 0x22

    new-instance v6, Lcom/facebook/redex/RunnableRunnableShape1S0100000_I0;

    invoke-direct {v6, p0, v0}, Lcom/facebook/redex/RunnableRunnableShape1S0100000_I0;-><init>(Lcom/soula2/Conversation;I)V

    .line 95043
    const/16 v0, 0x23

    new-instance v7, Lcom/facebook/redex/RunnableRunnableShape1S0100000_I0;

    invoke-direct {v7, p0, v0}, Lcom/facebook/redex/RunnableRunnableShape1S0100000_I0;-><init>(Lcom/soula2/Conversation;I)V

    .line 95044
    iget-object v4, p0, Lcom/soula2/Conversation;->A2C:LX/20a;

    .line 95045
    iget-boolean v0, v4, LX/20a;->A0F:Z

    if-eqz v0, :cond_1

    iget-boolean v0, v4, LX/20a;->A0D:Z

    if-nez v0, :cond_1

    .line 95046
    iget v1, v4, LX/20a;->A0I:I

    const/4 v0, 0x2

    if-ne v1, v0, :cond_1

    const/4 v3, 0x1

    .line 95047
    iput-boolean v3, v4, LX/20a;->A0D:Z

    .line 95048
    iget-object v1, v4, LX/20a;->A05:Lcom/facebook/redex/RunnableRunnableShape1S0201000_I1;

    if-eqz v1, :cond_0

    .line 95049
    iget-object v0, v4, LX/20a;->A0L:LX/0li;

    invoke-virtual {v0, v1}, LX/0li;->A0I(Ljava/lang/Runnable;)V

    .line 95050
    :cond_0
    iget-object v5, v4, LX/20a;->A0A:LX/2RU;

    .line 95051
    invoke-static {v5}, LX/009;->A03(Landroid/view/View;)V

    .line 95052
    iget-object v0, v4, LX/20a;->A02:Landroid/view/ViewGroup;

    invoke-virtual {v0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    iget v8, v0, Landroid/view/ViewGroup$LayoutParams;->height:I

    .line 95053
    iget-object v0, v4, LX/20a;->A02:Landroid/view/ViewGroup;

    invoke-virtual {v0}, Landroid/view/View;->getHeight()I

    move-result v0

    .line 95054
    new-instance v2, Lcom/facebook/redex/IDxAnimationShape1S0101000_2_I0;

    invoke-direct {v2, v4, v0, v3}, Lcom/facebook/redex/IDxAnimationShape1S0101000_2_I0;-><init>(Ljava/lang/Object;II)V

    const-wide/16 v0, 0x190

    .line 95055
    invoke-virtual {v2, v0, v1}, Landroid/view/animation/Animation;->setDuration(J)V

    .line 95056
    new-instance v3, LX/3b9;

    invoke-direct/range {v3 .. v8}, LX/3b9;-><init>(LX/20a;LX/2RU;Ljava/lang/Runnable;Ljava/lang/Runnable;I)V

    invoke-virtual {v2, v3}, Landroid/view/animation/Animation;->setAnimationListener(Landroid/view/animation/Animation$AnimationListener;)V

    .line 95057
    iget-object v0, v4, LX/20a;->A02:Landroid/view/ViewGroup;

    invoke-virtual {v0, v2}, Landroid/view/View;->startAnimation(Landroid/view/animation/Animation;)V

    .line 95058
    :cond_1
    return-void

    .line 95059
    :cond_2
    iget-object v0, p0, Lcom/soula2/Conversation;->A0P:Landroid/view/ViewGroup;

    invoke-virtual {v0}, Landroid/view/View;->getVisibility()I

    move-result v0

    if-nez v0, :cond_1

    iget v2, p0, Lcom/soula2/Conversation;->A02:I

    if-ltz v2, :cond_1

    .line 95060
    const-string v1, "conversation/hidelinkpreview/start "

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/whatsapp/util/Log;->i(Ljava/lang/String;)V

    .line 95061
    iget-object v1, p0, Lcom/soula2/Conversation;->A0P:Landroid/view/ViewGroup;

    iget-object v0, p0, Lcom/soula2/Conversation;->A1m:LX/20b;

    .line 95062
    iget-object v0, v0, LX/20b;->A07:Lcom/soula2/webpagepreview/WebPagePreviewView;

    .line 95063
    invoke-virtual {p0, v0, v1}, Lcom/soula2/Conversation;->A30(Landroid/view/View;Landroid/view/ViewGroup;)V

    const/4 v0, 0x0

    .line 95064
    invoke-virtual {p0, v0}, Lcom/soula2/Conversation;->A3B(Z)V

    return-void
.end method

.method public A2j()V
    .locals 2

    .line 95065
    iget-object v1, p0, Lcom/soula2/Conversation;->A2v:Lcom/soula2/mentions/MentionableEntry;

    const v0, 0x7f120d15

    invoke-virtual {p0, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, LX/1l6;->setHint(Ljava/lang/String;)V

    return-void
.end method

.method public final A2k()V
    .locals 5

    .line 95066
    iget-object v1, p0, Lcom/soula2/Conversation;->A3y:LX/2v5;

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    .line 95067
    invoke-virtual {v1, v0}, LX/0ng;->A08(Z)V

    const/4 v0, 0x0

    .line 95068
    iput-object v0, p0, Lcom/soula2/Conversation;->A3y:LX/2v5;

    .line 95069
    :cond_0
    new-instance v4, LX/3Cw;

    invoke-direct {v4, p0}, LX/3Cw;-><init>(Lcom/soula2/Conversation;)V

    .line 95070
    iget-object v3, p0, LX/0kB;->A0M:LX/0nZ;

    iget-object v1, p0, Lcom/soula2/Conversation;->A40:LX/0qM;

    iget-object v0, p0, Lcom/soula2/Conversation;->A2r:LX/0l8;

    .line 95071
    invoke-static {v0}, LX/009;->A05(Ljava/lang/Object;)V

    new-instance v2, LX/2v5;

    invoke-direct {v2, v3, v0, v4, v1}, LX/2v5;-><init>(LX/0nZ;LX/0l8;LX/4uD;LX/0qM;)V

    iput-object v2, p0, Lcom/soula2/Conversation;->A3y:LX/2v5;

    .line 95072
    iget-object v1, p0, LX/0kJ;->A05:LX/0lO;

    const/4 v0, 0x0

    new-array v0, v0, [Ljava/lang/Void;

    invoke-interface {v1, v2, v0}, LX/0lO;->AZj(LX/0ng;[Ljava/lang/Object;)V

    return-void
.end method

.method public final A2l()V
    .locals 3

    .line 95073
    iget-object v0, p0, Lcom/soula2/Conversation;->A2v:Lcom/soula2/mentions/MentionableEntry;

    iget-object v2, p0, Lcom/soula2/Conversation;->A4i:Landroid/text/TextWatcher;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->removeTextChangedListener(Landroid/text/TextWatcher;)V

    const/4 v0, 0x0

    .line 95074
    iput-boolean v0, p0, Lcom/soula2/Conversation;->A4Q:Z

    .line 95075
    :try_start_0
    iget-object v1, p0, Lcom/soula2/Conversation;->A2v:Lcom/soula2/mentions/MentionableEntry;

    const-string v0, ""

    invoke-virtual {v1, v0}, Lcom/soula2/mentions/MentionableEntry;->setText(Ljava/lang/String;)V

    .line 95076
    invoke-virtual {p0}, Lcom/soula2/Conversation;->A2j()V

    .line 95077
    iget-object v0, p0, Lcom/soula2/Conversation;->A2v:Lcom/soula2/mentions/MentionableEntry;

    .line 95078
    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    .line 95079
    invoke-static {v0}, Landroid/text/method/TextKeyListener;->clear(Landroid/text/Editable;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 95080
    iget-object v0, p0, Lcom/soula2/Conversation;->A2v:Lcom/soula2/mentions/MentionableEntry;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 95081
    invoke-virtual {p0}, Lcom/soula2/Conversation;->A2k()V

    .line 95082
    invoke-virtual {p0}, Lcom/soula2/Conversation;->A2q()V

    return-void

    :catchall_0
    move-exception v1

    .line 95083
    iget-object v0, p0, Lcom/soula2/Conversation;->A2v:Lcom/soula2/mentions/MentionableEntry;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 95084
    throw v1
.end method

.method public final A2m()V
    .locals 57

    .line 95085
    move-object/from16 v13, p0

    iget-object v0, v13, Lcom/soula2/Conversation;->A42:LX/0kz;

    if-nez v0, :cond_1

    .line 95086
    invoke-virtual {v13}, Landroid/app/Activity;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v3

    const v2, 0x7f0d05c6

    const v0, 0x7f0a13c8

    invoke-virtual {v13, v0}, LX/00i;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/view/ViewGroup;

    const/4 v0, 0x1

    invoke-virtual {v3, v2, v1, v0}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    .line 95087
    iget-object v12, v13, Lcom/soula2/Conversation;->A43:LX/2D4;

    iget-object v11, v13, Lcom/soula2/Conversation;->A0w:Lcom/soula2/KeyboardPopupLayout;

    iget-object v0, v13, Lcom/soula2/Conversation;->A1y:LX/0l4;

    move-object/from16 v29, v0

    .line 95088
    invoke-static/range {v29 .. v29}, LX/009;->A05(Ljava/lang/Object;)V

    iget-object v0, v13, Lcom/soula2/Conversation;->A2j:LX/0mU;

    move-object/from16 v37, v0

    .line 95089
    invoke-static/range {v37 .. v37}, LX/009;->A05(Ljava/lang/Object;)V

    iget-object v14, v13, Lcom/soula2/Conversation;->A4x:LX/0kk;

    .line 95090
    iget-object v0, v12, LX/2D4;->A01:LX/2D1;

    .line 95091
    invoke-virtual {v0, v13, v11}, LX/2D1;->A00(Landroid/content/Context;Landroid/view/View;)LX/0l0;

    move-result-object v48

    .line 95092
    iget-object v0, v12, LX/2D4;->A0D:LX/0lm;

    move-object/from16 v31, v0

    iget-object v0, v12, LX/2D4;->A0H:LX/0kj;

    move-object/from16 v35, v0

    iget-object v0, v12, LX/2D4;->A04:LX/0li;

    move-object/from16 v56, v0

    iget-object v0, v12, LX/2D4;->A0O:LX/17Q;

    move-object/from16 v42, v0

    iget-object v0, v12, LX/2D4;->A00:LX/0nf;

    move-object/from16 v55, v0

    iget-object v0, v12, LX/2D4;->A0R:LX/0lO;

    move-object/from16 v45, v0

    iget-object v0, v12, LX/2D4;->A03:LX/0lC;

    move-object/from16 v54, v0

    iget-object v0, v12, LX/2D4;->A0P:LX/18n;

    move-object/from16 v43, v0

    iget-object v0, v12, LX/2D4;->A05:LX/0nB;

    move-object/from16 v53, v0

    iget-object v0, v12, LX/2D4;->A06:LX/0oS;

    move-object/from16 v26, v0

    iget-object v0, v12, LX/2D4;->A0J:LX/0lL;

    move-object/from16 v25, v0

    iget-object v0, v12, LX/2D4;->A0V:LX/0y6;

    move-object/from16 v23, v0

    iget-object v0, v12, LX/2D4;->A0T:LX/17P;

    move-object/from16 v22, v0

    iget-object v0, v12, LX/2D4;->A0C:LX/01e;

    move-object/from16 v21, v0

    iget-object v0, v12, LX/2D4;->A0F:LX/00z;

    move-object/from16 v20, v0

    iget-object v0, v12, LX/2D4;->A0N:LX/0v9;

    move-object/from16 v18, v0

    iget-object v0, v12, LX/2D4;->A0I:LX/147;

    move-object/from16 v16, v0

    iget-object v0, v12, LX/2D4;->A07:Lcom/soula2/audioRecording/AudioRecordFactory;

    move-object/from16 v24, v0

    iget-object v15, v12, LX/2D4;->A0G:LX/0yA;

    iget-object v10, v12, LX/2D4;->A0U:LX/18m;

    iget-object v9, v12, LX/2D4;->A0E:LX/0kh;

    iget-object v8, v12, LX/2D4;->A08:Lcom/soula2/audioRecording/OpusRecorderFactory;

    iget-object v7, v12, LX/2D4;->A09:LX/0s8;

    iget-object v6, v12, LX/2D4;->A0S:LX/0qM;

    iget-object v5, v12, LX/2D4;->A0K:LX/16f;

    iget-object v4, v12, LX/2D4;->A0L:LX/0pe;

    iget-object v3, v12, LX/2D4;->A0Q:LX/17w;

    iget-object v2, v12, LX/2D4;->A0A:LX/11Q;

    iget-object v1, v12, LX/2D4;->A0B:LX/2D2;

    iget-object v0, v12, LX/2D4;->A02:LX/2D3;

    .line 95093
    invoke-virtual {v0, v13, v14}, LX/2D3;->A00(Landroid/app/Activity;LX/0kk;)LX/2yq;

    move-result-object v50

    new-instance v0, LX/2wf;

    move-object/from16 v17, v13

    move-object/from16 v19, v13

    move-object/from16 v27, v2

    move-object/from16 v28, v1

    move-object/from16 v30, v21

    move-object/from16 v32, v9

    move-object/from16 v33, v20

    move-object/from16 v34, v15

    move-object/from16 v36, v16

    move-object/from16 v38, v25

    move-object/from16 v39, v5

    move-object/from16 v40, v4

    move-object/from16 v41, v18

    move-object/from16 v44, v3

    move-object/from16 v46, v6

    move-object/from16 v47, v22

    move-object/from16 v49, v10

    move-object/from16 v51, v12

    move-object/from16 v52, v23

    move-object v14, v0

    move-object v15, v11

    move-object/from16 v16, v13

    move-object/from16 v18, v55

    move-object/from16 v20, v54

    move-object/from16 v21, v56

    move-object/from16 v22, v53

    move-object/from16 v23, v26

    move-object/from16 v25, v8

    move-object/from16 v26, v7

    invoke-direct/range {v14 .. v52}, LX/2wf;-><init>(Landroid/view/View;LX/00j;Lcom/soula2/Conversation;LX/0nf;LX/0kM;LX/0lC;LX/0li;LX/0nB;LX/0oS;Lcom/soula2/audioRecording/AudioRecordFactory;Lcom/soula2/audioRecording/OpusRecorderFactory;LX/0s8;LX/11Q;LX/2D2;LX/0l4;LX/01e;LX/0lm;LX/0kh;LX/00z;LX/0yA;LX/0kj;LX/147;LX/0mU;LX/0lL;LX/16f;LX/0pe;LX/0v9;LX/17Q;LX/18n;LX/17w;LX/0lO;LX/0qM;LX/17P;LX/0l0;LX/18m;LX/2yq;LX/2D4;LX/0y6;)V

    .line 95094
    iput-object v0, v13, Lcom/soula2/Conversation;->A42:LX/0kz;

    .line 95095
    iget-object v3, v13, Lcom/soula2/Conversation;->A2r:LX/0l8;

    .line 95096
    iget-object v2, v0, LX/0kz;->A0P:LX/0l2;

    const/4 v1, 0x0

    if-eqz v2, :cond_0

    const/4 v1, 0x1

    .line 95097
    :cond_0
    xor-int/lit8 v2, v1, 0x1

    const-string v1, "Do not update the ptt receiver once the recording has started"

    .line 95098
    invoke-static {v1, v2}, LX/009;->A0C(Ljava/lang/String;Z)V

    .line 95099
    iput-object v3, v0, LX/0kz;->A0J:LX/0l8;

    .line 95100
    iget-object v1, v13, Lcom/soula2/Conversation;->A1x:LX/0mf;

    .line 95101
    iget-object v1, v1, LX/0mf;->A08:LX/0md;

    .line 95102
    iput-object v1, v0, LX/0kz;->A0L:LX/0md;

    .line 95103
    iget-object v1, v13, Lcom/soula2/Conversation;->A1y:LX/0l4;

    .line 95104
    iget-object v2, v1, LX/0l4;->A01:LX/0l5;

    .line 95105
    instance-of v1, v2, LX/2Ig;

    if-eqz v1, :cond_1

    .line 95106
    check-cast v2, LX/2Ig;

    iget-object v1, v13, Lcom/soula2/Conversation;->A2A:LX/1be;

    .line 95107
    iput-object v2, v0, LX/0kz;->A0H:LX/2Ig;

    .line 95108
    iput-object v1, v0, LX/0kz;->A0I:LX/1be;

    .line 95109
    :cond_1
    return-void
.end method

.method public final A2n()V
    .locals 2

    .line 95110
    iget-object v1, p0, Lcom/soula2/Conversation;->A13:Lcom/soula2/TextEmojiLabel;

    const/16 v0, 0x8

    invoke-virtual {v1, v0}, Landroid/view/View;->setVisibility(I)V

    .line 95111
    iget-object v0, p0, Lcom/soula2/Conversation;->A0a:LX/01b;

    if-eqz v0, :cond_0

    .line 95112
    invoke-virtual {v0}, Landroid/app/Dialog;->dismiss()V

    const/4 v0, 0x0

    .line 95113
    iput-object v0, p0, Lcom/soula2/Conversation;->A0a:LX/01b;

    :cond_0
    return-void
.end method

.method public final A2o()V
    .locals 1

    .line 95114
    iget-object v0, p0, Lcom/soula2/Conversation;->A2C:LX/20a;

    invoke-virtual {v0}, LX/20a;->A01()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    .line 95115
    invoke-virtual {p0, v0}, Lcom/soula2/Conversation;->A2g(I)I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/soula2/Conversation;->A2x(I)V

    .line 95116
    :cond_0
    iget-object v0, p0, Lcom/soula2/Conversation;->A2C:LX/20a;

    invoke-virtual {v0}, LX/20a;->A00()V

    return-void
.end method

.method public final A2p()V
    .locals 4

    .line 95117
    sget-object v2, Lcom/soula2/Conversation;->A5G:Ljava/util/HashMap;

    iget-object v1, p0, Lcom/soula2/Conversation;->A2b:LX/0mJ;

    const-class v0, LX/0l8;

    invoke-virtual {v1, v0}, LX/0mJ;->A0A(Ljava/lang/Class;)Lcom/whatsapp/jid/Jid;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/util/AbstractMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/23Q;

    if-eqz v0, :cond_0

    .line 95118
    iget-object v1, p0, Lcom/soula2/Conversation;->A2C:LX/20a;

    iget-boolean v3, v0, LX/23Q;->A03:Z

    .line 95119
    iget-object v0, v1, LX/20a;->A0B:LX/0l8;

    if-eqz v0, :cond_0

    .line 95120
    iget-object v2, v1, LX/20a;->A0R:LX/18A;

    invoke-virtual {v0}, Lcom/whatsapp/jid/Jid;->getRawString()Ljava/lang/String;

    move-result-object v1

    const/4 v0, 0x1

    .line 95121
    invoke-virtual {v2, v0, v1, v3}, LX/18A;->A00(ILjava/lang/String;Z)V

    .line 95122
    :cond_0
    return-void
.end method

.method public final A2q()V
    .locals 3

    .line 95123
    iget-object v2, p0, Lcom/soula2/Conversation;->A2C:LX/20a;

    .line 95124
    iget-boolean v0, v2, LX/20a;->A0F:Z

    if-eqz v0, :cond_0

    .line 95125
    iget v1, v2, LX/20a;->A0I:I

    const/4 v0, 0x2

    if-ne v1, v0, :cond_1

    .line 95126
    iget-object v0, v2, LX/20a;->A0A:LX/2RU;

    invoke-static {v0}, LX/009;->A03(Landroid/view/View;)V

    invoke-virtual {v0}, Landroid/view/View;->getVisibility()I

    move-result v0

    if-nez v0, :cond_0

    .line 95127
    :goto_0
    const/4 v0, 0x1

    .line 95128
    invoke-virtual {p0, v0}, Lcom/soula2/Conversation;->A3B(Z)V

    :cond_0
    return-void

    .line 95129
    :cond_1
    iget-object v0, v2, LX/20a;->A09:Lcom/soula2/ctwa/icebreaker/ui/IcebreakerBubbleView;

    invoke-static {v0}, LX/009;->A03(Landroid/view/View;)V

    invoke-virtual {v0}, Landroid/view/View;->getVisibility()I

    move-result v0

    if-nez v0, :cond_0

    goto :goto_0
.end method

.method public final A2r()V
    .locals 12

    .line 95130
    iget-object v0, p0, LX/0kB;->A01:LX/059;

    if-eqz v0, :cond_1

    iget-object v0, p0, LX/0kB;->A0J:LX/1tx;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/soula2/Conversation;->A1x:LX/0mf;

    .line 95131
    iget-object v0, v0, LX/0mf;->A08:LX/0md;

    .line 95132
    if-nez v0, :cond_1

    .line 95133
    invoke-virtual {p0}, Lcom/soula2/Conversation;->A2h()LX/0md;

    move-result-object v0

    if-eqz v0, :cond_1

    iget-object v1, p0, LX/0kB;->A0J:LX/1tx;

    .line 95134
    iget-object v0, v1, LX/1tx;->A04:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/AbstractMap;->size()I

    move-result v0

    .line 95135
    const/4 v3, 0x1

    if-ne v0, v3, :cond_1

    iget-object v7, p0, LX/0kH;->A0C:LX/0kj;

    iget-object v4, p0, LX/0kB;->A07:LX/0nL;

    iget-object v9, p0, LX/0kB;->A0X:LX/0vp;

    iget-object v11, p0, Lcom/soula2/Conversation;->A3h:LX/0zn;

    iget-object v5, p0, Lcom/soula2/Conversation;->A1U:LX/0za;

    iget-object v6, p0, LX/0kB;->A0N:LX/0nR;

    .line 95136
    iget-object v0, v1, LX/1tx;->A04:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/AbstractMap;->values()Ljava/util/Collection;

    move-result-object v0

    .line 95137
    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v10

    check-cast v10, LX/0md;

    iget-object v8, p0, Lcom/soula2/Conversation;->A2h:LX/0zB;

    .line 95138
    invoke-static/range {v4 .. v11}, LX/1Vf;->A0T(LX/0nL;LX/0za;LX/0nR;LX/0kj;LX/0zB;LX/0vp;LX/0md;LX/0zn;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 95139
    iget-object v1, p0, Lcom/soula2/Conversation;->A1c:Lcom/soula2/conversation/ConversationListView;

    invoke-virtual {p0}, Lcom/soula2/Conversation;->A2h()LX/0md;

    move-result-object v0

    iget-object v0, v0, LX/0md;->A0z:LX/1HF;

    invoke-virtual {v1, v0}, Lcom/soula2/conversation/ConversationListView;->A00(LX/1HF;)LX/1Mr;

    move-result-object v5

    if-eqz v5, :cond_1

    .line 95140
    invoke-virtual {v5}, LX/1Mt;->A0X()Z

    move-result v0

    if-eqz v0, :cond_0

    iget v0, v5, LX/1Mt;->A06:I

    if-nez v0, :cond_0

    .line 95141
    iget-object v7, v5, LX/1Mt;->A0D:Landroid/view/View;

    new-array v1, v3, [F

    .line 95142
    invoke-virtual {v5}, Landroid/view/View;->getWidth()I

    move-result v0

    div-int/lit8 v0, v0, 0x6

    .line 95143
    int-to-float v0, v0

    const/4 v4, 0x0

    aput v0, v1, v4

    const-string/jumbo v2, "translationX"

    invoke-static {v7, v2, v1}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Ljava/lang/String;[F)Landroid/animation/ObjectAnimator;

    move-result-object v6

    const-wide/16 v0, 0x258

    .line 95144
    invoke-virtual {v6, v0, v1}, Landroid/animation/ObjectAnimator;->setDuration(J)Landroid/animation/ObjectAnimator;

    move-result-object v1

    new-instance v0, LX/07d;

    invoke-direct {v0}, LX/07d;-><init>()V

    invoke-virtual {v1, v0}, Landroid/animation/Animator;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    .line 95145
    const/16 v1, 0xf

    new-instance v0, Lcom/facebook/redex/IDxLAdapterShape4S0100000_2_I0;

    invoke-direct {v0, v5, v1}, Lcom/facebook/redex/IDxLAdapterShape4S0100000_2_I0;-><init>(Ljava/lang/Object;I)V

    invoke-virtual {v6, v0}, Landroid/animation/Animator;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    .line 95146
    new-array v1, v3, [F

    const/4 v0, 0x0

    aput v0, v1, v4

    invoke-static {v7, v2, v1}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Ljava/lang/String;[F)Landroid/animation/ObjectAnimator;

    move-result-object v2

    const-wide/16 v0, 0xc8

    .line 95147
    invoke-virtual {v2, v0, v1}, Landroid/animation/ObjectAnimator;->setDuration(J)Landroid/animation/ObjectAnimator;

    move-result-object v1

    new-instance v0, Landroid/view/animation/DecelerateInterpolator;

    invoke-direct {v0}, Landroid/view/animation/DecelerateInterpolator;-><init>()V

    invoke-virtual {v1, v0}, Landroid/animation/Animator;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    .line 95148
    const/16 v1, 0x10

    new-instance v0, Lcom/facebook/redex/IDxLAdapterShape4S0100000_2_I0;

    invoke-direct {v0, v5, v1}, Lcom/facebook/redex/IDxLAdapterShape4S0100000_2_I0;-><init>(Ljava/lang/Object;I)V

    invoke-virtual {v2, v0}, Landroid/animation/Animator;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    .line 95149
    new-instance v4, Landroid/animation/AnimatorSet;

    invoke-direct {v4}, Landroid/animation/AnimatorSet;-><init>()V

    .line 95150
    invoke-virtual {v4, v6}, Landroid/animation/AnimatorSet;->play(Landroid/animation/Animator;)Landroid/animation/AnimatorSet$Builder;

    .line 95151
    invoke-virtual {v4, v2}, Landroid/animation/AnimatorSet;->play(Landroid/animation/Animator;)Landroid/animation/AnimatorSet$Builder;

    move-result-object v2

    const-wide/16 v0, 0x384

    invoke-virtual {v2, v0, v1}, Landroid/animation/AnimatorSet$Builder;->after(J)Landroid/animation/AnimatorSet$Builder;

    .line 95152
    invoke-virtual {v4}, Landroid/animation/Animator;->start()V

    const/4 v0, 0x2

    .line 95153
    iput v0, v5, LX/1Mt;->A06:I

    .line 95154
    invoke-virtual {v5}, Landroid/view/View;->invalidate()V

    .line 95155
    :cond_0
    iget-object v1, p0, LX/0kH;->A05:LX/0li;

    const v0, 0x7f1217f2

    invoke-virtual {v1, v0, v3}, LX/0li;->A08(II)V

    .line 95156
    invoke-virtual {p0}, LX/0kB;->A2Z()V

    :cond_1
    return-void
.end method

.method public final A2s()V
    .locals 4

    .line 95157
    const-string v0, "conversation/session/active/jid "

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1, v0}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/soula2/Conversation;->A2r:LX/0l8;

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/whatsapp/util/Log;->d(Ljava/lang/String;)V

    .line 95158
    iget-object v3, p0, Lcom/soula2/Conversation;->A0e:LX/10E;

    iget-object v2, p0, LX/0kF;->A0A:LX/0z3;

    const/4 v0, 0x1

    new-instance v1, LX/1hf;

    invoke-direct {v1, p0, v2, v0}, LX/1hf;-><init>(Lcom/soula2/Conversation;LX/0z3;Z)V

    .line 95159
    iget-object v0, v3, LX/10E;->A00:Ljava/util/concurrent/atomic/AtomicReference;

    invoke-virtual {v0, v1}, Ljava/util/concurrent/atomic/AtomicReference;->set(Ljava/lang/Object;)V

    .line 95160
    return-void
.end method

.method public final A2t()V
    .locals 5

    .line 95161
    iget-object v0, p0, Lcom/soula2/Conversation;->A0e:LX/10E;

    .line 95162
    invoke-virtual {v0}, LX/10E;->A00()LX/1hf;

    move-result-object v4

    .line 95163
    invoke-static {v4}, LX/009;->A05(Ljava/lang/Object;)V

    .line 95164
    iget-object v0, v4, LX/1hf;->A00:Lcom/soula2/Conversation;

    if-ne v0, p0, :cond_0

    .line 95165
    const-string v0, "conversation/session/inactive/jid "

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1, v0}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/soula2/Conversation;->A2r:LX/0l8;

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/whatsapp/util/Log;->d(Ljava/lang/String;)V

    .line 95166
    iget-object v3, p0, Lcom/soula2/Conversation;->A0e:LX/10E;

    const/4 v2, 0x0

    iget-object v0, p0, LX/0kF;->A0A:LX/0z3;

    new-instance v1, LX/1hf;

    invoke-direct {v1, p0, v0, v2}, LX/1hf;-><init>(Lcom/soula2/Conversation;LX/0z3;Z)V

    .line 95167
    iget-object v0, v3, LX/10E;->A00:Ljava/util/concurrent/atomic/AtomicReference;

    invoke-virtual {v0, v4, v1}, Ljava/util/concurrent/atomic/AtomicReference;->compareAndSet(Ljava/lang/Object;Ljava/lang/Object;)Z

    .line 95168
    :cond_0
    return-void
.end method

.method public final A2u()V
    .locals 31

    .line 95169
    move-object/from16 v1, p0

    iget-object v0, v1, Lcom/soula2/Conversation;->A1q:LX/1ld;

    iget-object v9, v1, Lcom/soula2/Conversation;->A2b:LX/0mJ;

    iget-boolean v8, v1, Lcom/soula2/Conversation;->A4M:Z

    iget-boolean v7, v1, Lcom/soula2/Conversation;->A4b:Z

    .line 95170
    iget-object v4, v9, LX/0mJ;->A0C:LX/1NU;

    const/4 v3, 0x1

    const/4 v2, 0x0

    const/4 v6, 0x0

    if-eqz v4, :cond_0

    const/4 v6, 0x1

    .line 95171
    :cond_0
    invoke-virtual {v9}, LX/0mJ;->A0J()Z

    move-result v4

    if-eqz v4, :cond_c

    .line 95172
    const-class v10, LX/0nO;

    .line 95173
    invoke-virtual {v9, v10}, LX/0mJ;->A0A(Ljava/lang/Class;)Lcom/whatsapp/jid/Jid;

    move-result-object v6

    invoke-static {v6}, LX/009;->A05(Ljava/lang/Object;)V

    check-cast v6, LX/0nO;

    .line 95174
    iget-object v5, v0, LX/1ld;->A0K:LX/0y5;

    iget-object v4, v0, LX/1ld;->A0O:LX/0vp;

    invoke-virtual {v5, v4, v6}, LX/0y5;->A03(LX/0vp;LX/0nO;)Z

    move-result v4

    if-eqz v4, :cond_b

    .line 95175
    invoke-virtual {v0, v9, v2}, LX/1ld;->A00(LX/0mJ;Z)V

    .line 95176
    :goto_0
    iget-object v4, v0, LX/1ld;->A03:LX/2FS;

    if-eqz v4, :cond_1

    .line 95177
    invoke-interface {v4}, LX/2FS;->getType()I

    move-result v8

    .line 95178
    iget-object v6, v0, LX/1ld;->A0I:LX/2S0;

    .line 95179
    if-nez v3, :cond_3

    .line 95180
    invoke-interface {v4}, LX/2FS;->AGn()V

    .line 95181
    :cond_1
    :goto_1
    move v6, v3

    .line 95182
    :goto_2
    iput-boolean v2, v0, LX/1ld;->A08:Z

    .line 95183
    iget-boolean v2, v0, LX/1ld;->A07:Z

    if-eq v6, v2, :cond_2

    .line 95184
    const-string v3, "conversation/spam/"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v6}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/whatsapp/util/Log;->i(Ljava/lang/String;)V

    .line 95185
    iput-boolean v6, v0, LX/1ld;->A07:Z

    .line 95186
    invoke-static {v6}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    .line 95187
    if-eqz v0, :cond_2

    .line 95188
    iget-object v4, v1, Lcom/soula2/Conversation;->A1z:LX/2iL;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_31

    const/4 v0, 0x0

    .line 95189
    invoke-virtual {v4, v0}, LX/2iL;->A00(Landroid/graphics/drawable/Drawable;)V

    .line 95190
    iget-object v1, v4, LX/2iL;->A01:Landroid/view/ViewGroup;

    const v0, 0x7f08076d

    invoke-virtual {v1, v0}, Landroid/view/View;->setBackgroundResource(I)V

    .line 95191
    :cond_2
    return-void

    .line 95192
    :cond_3
    iget-object v4, v6, LX/2S0;->A00:LX/0mJ;

    const/4 v7, 0x0

    if-nez v4, :cond_a

    const-string v4, "Contact is unexpected null"

    .line 95193
    invoke-static {v4}, Lcom/whatsapp/util/Log;->e(Ljava/lang/String;)V

    .line 95194
    :cond_4
    :goto_3
    invoke-virtual {v6, v8}, LX/2S0;->A01(I)Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x0

    if-eqz v4, :cond_5

    const/4 v5, 0x1

    .line 95195
    :cond_5
    const/4 v4, 0x1

    const v16, 0x7f120a36

    const v19, 0x7f120ae6

    if-ne v8, v4, :cond_6

    .line 95196
    const v16, 0x7f120a37

    .line 95197
    const v19, 0x7f120ae7

    .line 95198
    :cond_6
    const/4 v8, 0x0

    .line 95199
    const/16 v14, 0x8

    const/16 v9, 0x8

    if-eqz v5, :cond_7

    const/4 v9, 0x0

    .line 95200
    :cond_7
    const/16 v13, 0x8

    if-eqz v7, :cond_8

    const/4 v13, 0x0

    .line 95201
    :cond_8
    iget-object v4, v6, LX/2S0;->A09:LX/0kh;

    .line 95202
    iget-object v5, v4, LX/0kh;->A00:Landroid/content/SharedPreferences;

    const-string/jumbo v4, "privacy_groupadd"

    invoke-interface {v5, v4, v2}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v4

    .line 95203
    if-nez v4, :cond_9

    const/4 v14, 0x0

    .line 95204
    :cond_9
    const/16 v11, 0x8

    const/16 v17, -0x1

    .line 95205
    new-instance v7, LX/48S;

    move v12, v11

    move v15, v11

    move/from16 v18, v17

    move/from16 v20, v2

    move v10, v2

    invoke-direct/range {v7 .. v20}, LX/48S;-><init>(Ljava/lang/String;IIIIIIIIIIII)V

    .line 95206
    iget-object v4, v0, LX/1ld;->A03:LX/2FS;

    invoke-interface {v4, v7}, LX/2FS;->AZD(LX/48S;)V

    goto/16 :goto_1

    .line 95207
    :cond_a
    invoke-virtual {v4, v10}, LX/0mJ;->A0A(Ljava/lang/Class;)Lcom/whatsapp/jid/Jid;

    move-result-object v5

    check-cast v5, Lcom/whatsapp/jid/GroupJid;

    if-eqz v5, :cond_4

    .line 95208
    iget-object v4, v6, LX/2S0;->A0B:LX/0nR;

    invoke-virtual {v4, v5}, LX/0nR;->A0B(Lcom/whatsapp/jid/GroupJid;)Z

    move-result v7

    goto :goto_3

    .line 95209
    :cond_b
    const/4 v3, 0x0

    goto/16 :goto_0

    .line 95210
    :cond_c
    iget-object v4, v0, LX/1ld;->A0N:LX/0zB;

    iget-object v5, v0, LX/1ld;->A0P:LX/0l8;

    invoke-static {v4, v5}, LX/1bj;->A01(LX/0zB;Lcom/whatsapp/jid/Jid;)Z

    move-result v4

    if-nez v4, :cond_17

    iget-object v4, v0, LX/1ld;->A0M:LX/0kj;

    .line 95211
    invoke-static {v4, v5}, LX/1hm;->A00(LX/0kj;Lcom/whatsapp/jid/Jid;)Z

    move-result v4

    if-nez v4, :cond_17

    .line 95212
    iget-object v4, v9, LX/0mJ;->A0D:Lcom/whatsapp/jid/Jid;

    invoke-static {v4}, LX/0mQ;->A0E(Lcom/whatsapp/jid/Jid;)Z

    move-result v4

    .line 95213
    if-nez v4, :cond_10

    if-nez v8, :cond_e

    if-eqz v6, :cond_d

    iget-object v4, v0, LX/1ld;->A0K:LX/0y5;

    .line 95214
    invoke-virtual {v4, v5}, LX/0y5;->A05(LX/0l8;)Z

    move-result v4

    if-eqz v4, :cond_10

    .line 95215
    :cond_d
    iget-object v4, v0, LX/1ld;->A06:Ljava/util/ArrayList;

    if-nez v4, :cond_14

    .line 95216
    :try_start_0
    iget-object v6, v0, LX/1ld;->A0Q:LX/2yy;

    const-class v4, Lcom/whatsapp/jid/UserJid;

    invoke-virtual {v9, v4}, LX/0mJ;->A0A(Ljava/lang/Class;)Lcom/whatsapp/jid/Jid;

    move-result-object v4

    check-cast v4, Lcom/whatsapp/jid/UserJid;

    invoke-virtual {v6, v4}, LX/2yy;->A00(Lcom/whatsapp/jid/UserJid;)Ljava/util/ArrayList;

    move-result-object v4

    iput-object v4, v0, LX/1ld;->A06:Ljava/util/ArrayList;

    goto :goto_4
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 95217
    :cond_e
    iget-object v3, v0, LX/1ld;->A04:LX/2oy;

    if-nez v3, :cond_f

    .line 95218
    iget-object v3, v0, LX/1ld;->A0C:LX/00i;

    new-instance v4, LX/2oy;

    invoke-direct {v4, v3}, LX/2oy;-><init>(Landroid/content/Context;)V

    iput-object v4, v0, LX/1ld;->A04:LX/2oy;

    .line 95219
    iget-object v3, v0, LX/1ld;->A0B:Landroid/widget/ListView;

    invoke-virtual {v3, v4}, Landroid/widget/ListView;->addFooterView(Landroid/view/View;)V

    .line 95220
    :cond_f
    iget-object v8, v0, LX/1ld;->A04:LX/2oy;

    invoke-static {v8}, LX/009;->A03(Landroid/view/View;)V

    iget-object v4, v0, LX/1ld;->A0D:LX/0mI;

    .line 95221
    const/4 v3, 0x3

    new-instance v7, Lcom/facebook/redex/RunnableRunnableShape7S0100000_I0_6;

    invoke-direct {v7, v4, v3}, Lcom/facebook/redex/RunnableRunnableShape7S0100000_I0_6;-><init>(Ljava/lang/Object;I)V

    .line 95222
    iget-object v6, v8, LX/3g9;->A01:Landroid/view/ViewGroup;

    const/16 v4, 0xc

    new-instance v3, Lcom/whatsapp/util/ViewOnClickCListenerShape5S0100000_I1_1;

    invoke-direct {v3, v8, v4}, Lcom/whatsapp/util/ViewOnClickCListenerShape5S0100000_I1_1;-><init>(Ljava/lang/Object;I)V

    invoke-virtual {v6, v3}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 95223
    iget-object v6, v8, LX/3g9;->A02:Landroid/view/ViewGroup;

    const/16 v4, 0x8

    new-instance v3, Lcom/whatsapp/util/ViewOnClickCListenerShape1S0300000_I1;

    invoke-direct {v3, v8, v5, v7, v4}, Lcom/whatsapp/util/ViewOnClickCListenerShape1S0300000_I1;-><init>(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;I)V

    invoke-virtual {v6, v3}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 95224
    iget-object v4, v8, LX/3g9;->A03:Landroid/widget/TextView;

    const v3, 0x7f12056a

    invoke-virtual {v4, v3}, Landroid/widget/TextView;->setText(I)V

    goto/16 :goto_6

    .line 95225
    :cond_10
    iget-object v3, v0, LX/1ld;->A01:LX/2FR;

    const/16 v4, 0x8

    if-eqz v3, :cond_11

    .line 95226
    iget-object v3, v3, LX/2FR;->A00:Landroid/view/View;

    invoke-virtual {v3, v4}, Landroid/view/View;->setVisibility(I)V

    .line 95227
    :cond_11
    iget-object v3, v0, LX/1ld;->A03:LX/2FS;

    if-eqz v3, :cond_12

    .line 95228
    invoke-interface {v3}, LX/2FS;->AGn()V

    .line 95229
    :cond_12
    iget-object v3, v0, LX/1ld;->A05:LX/2oz;

    if-eqz v3, :cond_13

    .line 95230
    iget-object v3, v3, LX/3g9;->A00:Landroid/view/ViewGroup;

    invoke-virtual {v3, v4}, Landroid/view/View;->setVisibility(I)V

    .line 95231
    :cond_13
    iget-object v3, v0, LX/1ld;->A04:LX/2oy;

    if-eqz v3, :cond_17

    .line 95232
    iget-object v3, v3, LX/3g9;->A00:Landroid/view/ViewGroup;

    invoke-virtual {v3, v4}, Landroid/view/View;->setVisibility(I)V

    goto :goto_6

    .line 95233
    :cond_14
    :goto_4
    invoke-virtual {v4}, Ljava/util/AbstractCollection;->isEmpty()Z

    move-result v4

    if-nez v4, :cond_19

    .line 95234
    iget-object v7, v0, LX/1ld;->A0L:LX/0vO;

    .line 95235
    iget-object v4, v7, LX/0vO;->A07:LX/0uN;

    invoke-virtual {v4, v5}, LX/0uN;->A06(LX/0l8;)LX/1Ni;

    move-result-object v4

    if-eqz v4, :cond_18

    .line 95236
    iget v4, v4, LX/1Ni;->A09:I

    .line 95237
    if-ne v4, v3, :cond_18

    const/4 v3, 0x0

    .line 95238
    :cond_15
    :goto_5
    iget-object v7, v0, LX/1ld;->A05:LX/2oz;

    if-eqz v7, :cond_17

    .line 95239
    iget-object v8, v0, LX/1ld;->A0C:LX/00i;

    iget-object v10, v0, LX/1ld;->A06:Ljava/util/ArrayList;

    .line 95240
    iget-object v6, v7, LX/3g9;->A00:Landroid/view/ViewGroup;

    const/16 v4, 0x8

    if-eqz v3, :cond_16

    const/4 v4, 0x0

    :cond_16
    invoke-virtual {v6, v4}, Landroid/view/View;->setVisibility(I)V

    if-eqz v3, :cond_17

    .line 95241
    iget-object v6, v7, LX/3g9;->A01:Landroid/view/ViewGroup;

    const/16 v4, 0x27

    new-instance v3, Lcom/whatsapp/util/ViewOnClickCListenerShape1S0200000_I1;

    invoke-direct {v3, v7, v4, v5}, Lcom/whatsapp/util/ViewOnClickCListenerShape1S0200000_I1;-><init>(Ljava/lang/Object;ILjava/lang/Object;)V

    invoke-virtual {v6, v3}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 95242
    iget-object v6, v7, LX/3g9;->A02:Landroid/view/ViewGroup;

    const/16 v4, 0x9

    new-instance v3, Lcom/whatsapp/util/ViewOnClickCListenerShape1S0300000_I1;

    invoke-direct {v3, v7, v8, v5, v4}, Lcom/whatsapp/util/ViewOnClickCListenerShape1S0300000_I1;-><init>(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;I)V

    invoke-virtual {v6, v3}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 95243
    iget-object v9, v7, LX/3g9;->A03:Landroid/widget/TextView;

    iget-object v8, v7, LX/3g9;->A04:LX/00z;

    const v7, 0x7f10009c

    .line 95244
    invoke-virtual {v10}, Ljava/util/AbstractCollection;->size()I

    move-result v3

    int-to-long v4, v3

    const/4 v3, 0x1

    new-array v6, v3, [Ljava/lang/Object;

    invoke-virtual {v10}, Ljava/util/AbstractCollection;->size()I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v6, v2

    .line 95245
    invoke-virtual {v8, v6, v7, v4, v5}, LX/00z;->A0H([Ljava/lang/Object;IJ)Ljava/lang/String;

    move-result-object v3

    .line 95246
    invoke-virtual {v9, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 95247
    :cond_17
    :goto_6
    const/4 v6, 0x0

    goto/16 :goto_2

    .line 95248
    :cond_18
    iget-object v4, v0, LX/1ld;->A05:LX/2oz;

    if-nez v4, :cond_15

    .line 95249
    iget-object v4, v0, LX/1ld;->A0C:LX/00i;

    new-instance v6, LX/2oz;

    invoke-direct {v6, v4}, LX/2oz;-><init>(Landroid/content/Context;)V

    iput-object v6, v0, LX/1ld;->A05:LX/2oz;

    .line 95250
    iget-object v4, v0, LX/1ld;->A0Q:LX/2yy;

    invoke-virtual {v6, v7, v4}, LX/2oz;->setup(LX/0vO;LX/2yy;)V

    .line 95251
    iget-object v4, v0, LX/1ld;->A0B:Landroid/widget/ListView;

    invoke-virtual {v4, v6}, Landroid/widget/ListView;->addFooterView(Landroid/view/View;)V

    goto :goto_5

    .line 95252
    :cond_19
    if-eqz v7, :cond_1a

    .line 95253
    invoke-virtual {v0, v9, v2}, LX/1ld;->A00(LX/0mJ;Z)V

    .line 95254
    iget-object v4, v0, LX/1ld;->A03:LX/2FS;

    if-eqz v4, :cond_17

    .line 95255
    iget-object v5, v0, LX/1ld;->A0I:LX/2S0;

    .line 95256
    iget-object v4, v9, LX/0mJ;->A0D:Lcom/whatsapp/jid/Jid;

    .line 95257
    if-eqz v4, :cond_17

    .line 95258
    const-class v4, LX/0l8;

    invoke-virtual {v9, v4}, LX/0mJ;->A0A(Ljava/lang/Class;)Lcom/whatsapp/jid/Jid;

    move-result-object v4

    check-cast v4, LX/0l8;

    if-eqz v4, :cond_17

    .line 95259
    invoke-static {v4}, LX/176;->A04(LX/0l8;)Ljava/lang/String;

    move-result-object v6

    .line 95260
    iget-object v4, v5, LX/2S0;->A04:LX/00i;

    .line 95261
    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v4, 0x7f121a1d

    new-array v3, v3, [Ljava/lang/Object;

    aput-object v6, v3, v2

    invoke-virtual {v5, v4, v3}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    .line 95262
    const/16 v6, 0x8

    const/4 v13, -0x1

    .line 95263
    new-instance v4, LX/48S;

    move v7, v6

    move v8, v6

    move v9, v2

    move v10, v6

    move v11, v6

    move v12, v2

    move v14, v13

    move v15, v13

    move/from16 v16, v13

    move/from16 v17, v2

    invoke-direct/range {v4 .. v17}, LX/48S;-><init>(Ljava/lang/String;IIIIIIIIIIII)V

    .line 95264
    iget-object v3, v0, LX/1ld;->A03:LX/2FS;

    invoke-interface {v3, v4}, LX/2FS;->AZD(LX/48S;)V

    goto :goto_6

    .line 95265
    :cond_1a
    iget-object v4, v0, LX/1ld;->A0F:LX/0nN;

    invoke-virtual {v4}, LX/0nN;->A0G()Z

    move-result v4

    if-nez v4, :cond_17

    .line 95266
    iget-object v4, v0, LX/1ld;->A0K:LX/0y5;

    const-class v6, LX/0l8;

    .line 95267
    invoke-virtual {v9, v6}, LX/0mJ;->A0A(Ljava/lang/Class;)Lcom/whatsapp/jid/Jid;

    move-result-object v6

    invoke-static {v6}, LX/009;->A05(Ljava/lang/Object;)V

    check-cast v6, LX/0l8;

    .line 95268
    invoke-virtual {v4, v6}, LX/0y5;->A00(LX/0l8;)I

    move-result v6

    const/4 v7, -0x1

    if-ne v6, v7, :cond_2a

    .line 95269
    invoke-virtual {v0, v9, v2}, LX/1ld;->A00(LX/0mJ;Z)V

    const/4 v6, 0x1

    :goto_7
    const/16 v16, 0x0

    .line 95270
    if-nez v6, :cond_2b

    .line 95271
    invoke-virtual {v9}, LX/0mJ;->A0K()Z

    move-result v8

    if-nez v8, :cond_2b

    .line 95272
    iget-object v8, v0, LX/1ld;->A01:LX/2FR;

    if-nez v8, :cond_1b

    .line 95273
    iget-object v13, v0, LX/1ld;->A0C:LX/00i;

    new-instance v12, LX/2FR;

    invoke-direct {v12, v13}, LX/2FR;-><init>(Landroid/content/Context;)V

    iput-object v12, v0, LX/1ld;->A01:LX/2FR;

    .line 95274
    iget-object v8, v0, LX/1ld;->A0J:LX/0uN;

    move-object/from16 v19, v8

    iget-object v8, v0, LX/1ld;->A0E:LX/0kM;

    move-object/from16 v17, v8

    iget-object v15, v0, LX/1ld;->A0G:LX/0nB;

    iget-object v14, v0, LX/1ld;->A0H:LX/0wh;

    iget-object v11, v0, LX/1ld;->A0D:LX/0mI;

    .line 95275
    const/16 v10, 0x29

    new-instance v8, Lcom/facebook/redex/RunnableRunnableShape1S0100000_I0;

    invoke-direct {v8, v11, v10}, Lcom/facebook/redex/RunnableRunnableShape1S0100000_I0;-><init>(Ljava/lang/Object;I)V

    .line 95276
    move-object/from16 v18, v13

    move-object/from16 v20, v17

    move-object/from16 v21, v15

    move-object/from16 v22, v14

    move-object/from16 v23, v8

    move-object/from16 v24, v9

    move-object/from16 v17, v12

    invoke-virtual/range {v17 .. v24}, LX/2FR;->setup(LX/00i;LX/0uN;LX/0kM;LX/0nB;LX/0wh;Ljava/lang/Runnable;LX/0mJ;)V

    .line 95277
    iget-object v10, v0, LX/1ld;->A09:Landroid/view/ViewGroup;

    iget-object v8, v0, LX/1ld;->A01:LX/2FR;

    invoke-virtual {v10, v8, v2}, Landroid/view/ViewGroup;->addView(Landroid/view/View;I)V

    .line 95278
    :cond_1b
    :goto_8
    iget-object v11, v0, LX/1ld;->A01:LX/2FR;

    if-eqz v11, :cond_1e

    .line 95279
    iget-object v12, v11, LX/2FR;->A00:Landroid/view/View;

    const/16 v10, 0x8

    const/16 v8, 0x8

    if-eqz v3, :cond_1c

    const/4 v8, 0x0

    :cond_1c
    invoke-virtual {v12, v8}, Landroid/view/View;->setVisibility(I)V

    if-eqz v3, :cond_1e

    .line 95280
    iget-object v8, v11, LX/2FR;->A04:LX/0wh;

    invoke-static {v5}, Lcom/whatsapp/jid/UserJid;->of(Lcom/whatsapp/jid/Jid;)Lcom/whatsapp/jid/UserJid;

    move-result-object v3

    invoke-virtual {v8, v3}, LX/0wh;->A0I(Lcom/whatsapp/jid/UserJid;)Z

    move-result v12

    .line 95281
    invoke-virtual {v11}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v8

    const v3, 0x7f12021f

    if-eqz v12, :cond_1d

    const v3, 0x7f12192d

    :cond_1d
    invoke-virtual {v8, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 95282
    iget-object v8, v11, LX/2FR;->A03:Landroid/widget/TextView;

    invoke-virtual {v8, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    if-nez v12, :cond_1e

    .line 95283
    iget-object v3, v11, LX/2FR;->A05:LX/0uN;

    invoke-virtual {v3, v5}, LX/0uN;->A09(LX/0l8;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_1e

    .line 95284
    invoke-virtual {v8, v10}, Landroid/view/View;->setVisibility(I)V

    .line 95285
    iget-object v8, v11, LX/2FR;->A02:Landroid/widget/TextView;

    const v3, 0x7f12009a

    invoke-virtual {v8, v3}, Landroid/widget/TextView;->setText(I)V

    .line 95286
    iget-object v3, v11, LX/2FR;->A01:Landroid/view/View;

    invoke-virtual {v3, v10}, Landroid/view/View;->setVisibility(I)V

    .line 95287
    :cond_1e
    iget-object v3, v0, LX/1ld;->A03:LX/2FS;

    if-eqz v3, :cond_29

    .line 95288
    invoke-interface {v3}, LX/2FS;->getType()I

    move-result v12

    .line 95289
    iget-object v10, v0, LX/1ld;->A0I:LX/2S0;

    .line 95290
    iget-object v3, v9, LX/0mJ;->A0D:Lcom/whatsapp/jid/Jid;

    .line 95291
    invoke-static {v3}, Lcom/whatsapp/jid/UserJid;->of(Lcom/whatsapp/jid/Jid;)Lcom/whatsapp/jid/UserJid;

    move-result-object v8

    if-eqz v8, :cond_27

    .line 95292
    invoke-virtual {v9}, LX/0mJ;->A0I()Z

    move-result v3

    if-eqz v3, :cond_27

    .line 95293
    iget-object v3, v10, LX/2S0;->A08:LX/0za;

    invoke-virtual {v3, v8}, LX/0za;->A02(Lcom/whatsapp/jid/UserJid;)Z

    move-result v11

    .line 95294
    :goto_9
    iget-object v8, v10, LX/2S0;->A06:LX/0wh;

    const-class v3, Lcom/whatsapp/jid/UserJid;

    invoke-virtual {v9, v3}, LX/0mJ;->A0A(Ljava/lang/Class;)Lcom/whatsapp/jid/Jid;

    move-result-object v3

    check-cast v3, Lcom/whatsapp/jid/UserJid;

    invoke-virtual {v8, v3}, LX/0wh;->A0I(Lcom/whatsapp/jid/UserJid;)Z

    move-result v3

    const/4 v8, 0x1

    const/16 v22, 0x0

    if-eqz v6, :cond_28

    if-eqz v11, :cond_24

    if-nez v3, :cond_28

    .line 95295
    :goto_a
    const v27, 0x7f12021f

    if-eqz v11, :cond_25

    .line 95296
    const v26, 0x7f1202fd

    .line 95297
    :cond_1f
    :goto_b
    iget-boolean v3, v10, LX/2S0;->A01:Z

    if-eqz v3, :cond_20

    .line 95298
    const v26, 0x7f1218ec

    :cond_20
    if-eqz v12, :cond_21

    const/4 v8, 0x0

    .line 95299
    :cond_21
    const/16 v18, 0x0

    .line 95300
    const/16 v19, 0x8

    if-eqz v8, :cond_22

    const/16 v19, 0x0

    .line 95301
    :cond_22
    const/16 v20, 0x8

    if-eqz v11, :cond_23

    const/16 v20, 0x0

    .line 95302
    const/16 v22, 0x8

    .line 95303
    :cond_23
    const/16 v23, 0x8

    const v28, 0x7f1202a4

    const v30, 0x7f120e28

    .line 95304
    new-instance v3, LX/48S;

    move-object/from16 v17, v3

    move/from16 v21, v2

    move/from16 v24, v23

    move/from16 v25, v23

    move/from16 v29, v7

    invoke-direct/range {v17 .. v30}, LX/48S;-><init>(Ljava/lang/String;IIIIIIIIIIII)V

    .line 95305
    iget-object v2, v0, LX/1ld;->A03:LX/2FS;

    invoke-interface {v2, v3}, LX/2FS;->AZD(LX/48S;)V

    .line 95306
    :goto_c
    iget-boolean v2, v0, LX/1ld;->A07:Z

    if-nez v2, :cond_30

    .line 95307
    iget-object v13, v4, LX/0y5;->A08:LX/0qV;

    iget-object v2, v4, LX/0y5;->A02:LX/0lm;

    .line 95308
    invoke-virtual {v2}, LX/0lm;->A00()J

    move-result-wide v2

    const/4 v14, 0x0

    goto :goto_d

    .line 95309
    :cond_24
    const v27, 0x7f12192d

    if-nez v3, :cond_25

    goto :goto_a

    .line 95310
    :cond_25
    iget v9, v9, LX/0mJ;->A06:I

    if-eq v9, v8, :cond_26

    const/4 v3, 0x2

    if-eq v9, v3, :cond_26

    const/4 v3, 0x3

    const v26, 0x7f1215b7

    if-eq v9, v3, :cond_1f

    .line 95311
    const v26, 0x7f1215b6

    goto :goto_b

    .line 95312
    :cond_26
    const v26, 0x7f1215b8

    goto :goto_b

    .line 95313
    :cond_27
    const/4 v11, 0x0

    goto :goto_9

    .line 95314
    :cond_28
    iget-object v2, v0, LX/1ld;->A03:LX/2FS;

    if-eqz v2, :cond_29

    .line 95315
    invoke-interface {v2}, LX/2FS;->AGn()V

    .line 95316
    :cond_29
    if-eqz v6, :cond_30

    goto :goto_c

    .line 95317
    :cond_2a
    invoke-virtual {v4, v5}, LX/0y5;->A05(LX/0l8;)Z

    move-result v6

    if-eqz v6, :cond_2c

    .line 95318
    invoke-virtual {v0, v9, v3}, LX/1ld;->A00(LX/0mJ;Z)V

    const/4 v6, 0x1

    const/16 v16, 0x1

    .line 95319
    :cond_2b
    const/4 v3, 0x0

    goto/16 :goto_8

    .line 95320
    :cond_2c
    const/4 v6, 0x0

    goto/16 :goto_7

    .line 95321
    :goto_d
    :try_start_1
    invoke-virtual {v13, v5}, LX/0qV;->A01(LX/0l8;)Lorg/json/JSONObject;

    move-result-object v12

    if-eqz v12, :cond_2e

    .line 95322
    const-string/jumbo v9, "tb_last_shown_ts"

    .line 95323
    invoke-virtual {v12, v9}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_2d

    const-string/jumbo v8, "tb_last_action_ts"

    .line 95324
    invoke-virtual {v12, v8}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_2f

    .line 95325
    invoke-virtual {v12, v9}, Lorg/json/JSONObject;->getLong(Ljava/lang/String;)J

    move-result-wide v10

    invoke-virtual {v12, v8}, Lorg/json/JSONObject;->getLong(Ljava/lang/String;)J

    move-result-wide v8

    cmp-long v7, v10, v8

    if-gez v7, :cond_2f

    .line 95326
    :cond_2d
    :goto_e
    const-string/jumbo v7, "tb_last_shown_ts"

    .line 95327
    invoke-virtual {v12, v7, v2, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;J)Lorg/json/JSONObject;

    .line 95328
    invoke-virtual {v13, v5, v12}, LX/0qV;->A02(LX/0l8;Lorg/json/JSONObject;)V

    goto :goto_f

    .line 95329
    :cond_2e
    new-instance v12, Lorg/json/JSONObject;

    invoke-direct {v12}, Lorg/json/JSONObject;-><init>()V

    goto :goto_e

    .line 95330
    :goto_f
    const/4 v14, 0x1
    :try_end_1
    .catch Lorg/json/JSONException; {:try_start_1 .. :try_end_1} :catch_0

    .line 95331
    :catch_0
    :cond_2f
    if-eqz v16, :cond_30

    if-eqz v14, :cond_30

    const/4 v2, 0x1

    .line 95332
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    .line 95333
    new-instance v3, LX/1fQ;

    invoke-direct {v3}, LX/1fQ;-><init>()V

    .line 95334
    iput-object v2, v3, LX/1fQ;->A00:Ljava/lang/Integer;

    .line 95335
    iput-object v2, v3, LX/1fQ;->A01:Ljava/lang/Integer;

    .line 95336
    invoke-virtual {v5}, Lcom/whatsapp/jid/Jid;->getRawString()Ljava/lang/String;

    move-result-object v2

    iput-object v2, v3, LX/1fQ;->A02:Ljava/lang/String;

    .line 95337
    iget-object v2, v4, LX/0y5;->A06:LX/0pG;

    invoke-virtual {v2, v3}, LX/0pG;->A07(LX/0p8;)V

    .line 95338
    invoke-virtual {v2}, LX/0pG;->A01()V

    .line 95339
    :cond_30
    move/from16 v2, v16

    goto/16 :goto_2

    .line 95340
    :cond_31
    iget-object v3, v4, LX/2iL;->A04:LX/0nw;

    iget-object v2, v4, LX/2iL;->A03:LX/0l8;

    iget-object v0, v4, LX/2iL;->A00:Landroid/app/Activity;

    invoke-virtual {v3, v0, v2}, LX/0nw;->A06(Landroid/content/Context;LX/0l8;)LX/2EA;

    move-result-object v0

    .line 95341
    invoke-virtual {v3, v0}, LX/0nw;->A03(LX/2EA;)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    if-eqz v0, :cond_32

    .line 95342
    invoke-virtual {v4, v0}, LX/2iL;->A00(Landroid/graphics/drawable/Drawable;)V

    .line 95343
    :cond_32
    iget-object v4, v1, Lcom/soula2/Conversation;->A2Z:LX/0y5;

    iget-object v0, v1, Lcom/soula2/Conversation;->A2b:LX/0mJ;

    const-class v3, LX/0l8;

    .line 95344
    invoke-virtual {v0, v3}, LX/0mJ;->A0A(Ljava/lang/Class;)Lcom/whatsapp/jid/Jid;

    move-result-object v2

    invoke-static {v2}, LX/009;->A05(Ljava/lang/Object;)V

    check-cast v2, LX/0l8;

    const/4 v0, 0x1

    .line 95345
    invoke-virtual {v4, v2, v0}, LX/0y5;->A06(LX/0l8;I)Z

    .line 95346
    iget-object v2, v1, Lcom/soula2/Conversation;->A2N:LX/0uN;

    iget-object v0, v1, Lcom/soula2/Conversation;->A2r:LX/0l8;

    invoke-static {v0}, LX/009;->A05(Ljava/lang/Object;)V

    invoke-virtual {v2, v0}, LX/0uN;->A0D(LX/0l8;)Z

    move-result v0

    if-eqz v0, :cond_33

    .line 95347
    iget-object v2, v1, Lcom/soula2/Conversation;->A30:LX/134;

    const/16 v4, 0x9

    iget-object v0, v1, Lcom/soula2/Conversation;->A2b:LX/0mJ;

    .line 95348
    invoke-virtual {v0, v3}, LX/0mJ;->A0A(Ljava/lang/Class;)Lcom/whatsapp/jid/Jid;

    move-result-object v3

    check-cast v3, LX/0l8;

    const-wide/16 v6, 0x0

    const/4 v5, 0x0

    .line 95349
    invoke-virtual/range {v2 .. v7}, LX/134;->A04(LX/0l8;IIJ)V

    .line 95350
    :cond_33
    iget-object v0, v1, Lcom/soula2/Conversation;->A1c:Lcom/soula2/conversation/ConversationListView;

    invoke-virtual {v0}, Lcom/soula2/conversation/ConversationListView;->getConversationCursorAdapter()LX/0me;

    move-result-object v2

    .line 95351
    iget v0, v2, LX/0me;->A01:I

    add-int/lit8 v0, v0, 0x1

    iput v0, v2, LX/0me;->A01:I

    .line 95352
    iget-object v0, v1, Lcom/soula2/Conversation;->A1c:Lcom/soula2/conversation/ConversationListView;

    invoke-virtual {v0}, Lcom/soula2/conversation/ConversationListView;->A02()V

    return-void

    .line 95353
    :catchall_0
    move-exception v0

    .line 95354
    throw v0
.end method

.method public final A2v()V
    .locals 3

    .line 95355
    iget-object v0, p0, Lcom/soula2/Conversation;->A1b:LX/2FT;

    const-class v1, LX/2nK;

    .line 95356
    iget-object v0, v0, LX/2FT;->A00:Ljava/util/Map;

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/1uG;

    .line 95357
    invoke-static {v2}, LX/009;->A05(Ljava/lang/Object;)V

    check-cast v2, LX/2nK;

    iget-object v0, p0, Lcom/soula2/Conversation;->A2b:LX/0mJ;

    .line 95358
    iput-object v0, v2, LX/2nK;->A00:LX/0mJ;

    .line 95359
    invoke-virtual {v2}, LX/1uG;->A03()Z

    move-result v1

    .line 95360
    invoke-virtual {v2}, LX/1uG;->A04()Z

    move-result v0

    if-eqz v1, :cond_1

    .line 95361
    if-eqz v0, :cond_2

    .line 95362
    invoke-virtual {v2}, LX/2nK;->A05()V

    .line 95363
    :cond_0
    return-void

    .line 95364
    :cond_1
    if-eqz v0, :cond_0

    const/4 v0, 0x0

    .line 95365
    invoke-virtual {v2, v0}, LX/1uG;->A02(Z)V

    return-void

    .line 95366
    :cond_2
    invoke-virtual {v2}, LX/1uG;->A00()V

    return-void
.end method

.method public final A2w()V
    .locals 8

    .line 95367
    iget-object v0, p0, Lcom/soula2/Conversation;->A1b:LX/2FT;

    const-class v1, LX/2FU;

    .line 95368
    iget-object v0, v0, LX/2FT;->A00:Ljava/util/Map;

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, LX/1uG;

    .line 95369
    check-cast v6, LX/2FU;

    if-eqz v6, :cond_0

    .line 95370
    iget-object v7, p0, Lcom/soula2/Conversation;->A2b:LX/0mJ;

    .line 95371
    iput-object v7, v6, LX/2FU;->A01:LX/0mJ;

    .line 95372
    iget-object v5, v6, LX/2FU;->A09:LX/1Ds;

    const-class v0, Lcom/whatsapp/jid/UserJid;

    .line 95373
    invoke-virtual {v7, v0}, LX/0mJ;->A0A(Ljava/lang/Class;)Lcom/whatsapp/jid/Jid;

    move-result-object v1

    invoke-static {v1}, LX/009;->A05(Ljava/lang/Object;)V

    check-cast v1, LX/0l8;

    .line 95374
    iget-object v0, v5, LX/1Ds;->A03:LX/0uN;

    invoke-virtual {v0, v1}, LX/0uN;->A06(LX/0l8;)LX/1Ni;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 95375
    iget-wide v3, v0, LX/1Ni;->A0B:J

    const-wide/16 v1, -0x1

    cmp-long v0, v3, v1

    if-eqz v0, :cond_1

    .line 95376
    iget-object v2, v6, LX/2FU;->A0C:LX/0lO;

    new-instance v1, LX/2uj;

    .line 95377
    invoke-direct {v1, v6, v5, v7}, LX/2uj;-><init>(LX/2FU;LX/1Ds;LX/0mJ;)V

    .line 95378
    const/4 v0, 0x0

    new-array v0, v0, [Ljava/lang/Void;

    invoke-interface {v2, v1, v0}, LX/0lO;->AZj(LX/0ng;[Ljava/lang/Object;)V

    .line 95379
    :cond_0
    return-void

    .line 95380
    :cond_1
    invoke-virtual {v6}, LX/1uG;->A04()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    .line 95381
    invoke-virtual {v6, v0}, LX/1uG;->A02(Z)V

    return-void
.end method

.method public A2x(I)V
    .locals 3

    .line 95382
    iget-object v2, p0, Lcom/soula2/Conversation;->A23:LX/00z;

    .line 95383
    invoke-static {p0, p1}, LX/00S;->A04(Landroid/content/Context;I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    new-instance v1, LX/1qq;

    invoke-direct {v1, v0, v2}, LX/1qq;-><init>(Landroid/graphics/drawable/Drawable;LX/00z;)V

    .line 95384
    iget-object v0, p0, Lcom/soula2/Conversation;->A0A:Landroid/view/View;

    invoke-static {v1, v0}, LX/2Vn;->A00(Landroid/graphics/drawable/Drawable;Landroid/view/View;)V

    .line 95385
    return-void
.end method

.method public final A2y(I)V
    .locals 4

    .line 95386
    iget-object v1, p0, LX/0kH;->A0C:LX/0kj;

    const/16 v0, 0x342

    invoke-virtual {v1, v0}, LX/0kj;->A07(I)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 95387
    invoke-virtual {p0, p1}, Lcom/soula2/Conversation;->A3F(I)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 95388
    iget-object v0, p0, Lcom/soula2/Conversation;->A0C:Landroid/view/View;

    if-eqz v0, :cond_1

    invoke-virtual {v0}, Landroid/view/View;->getVisibility()I

    move-result v0

    const/16 v3, 0x8

    if-eq v0, v3, :cond_1

    .line 95389
    iget-object v0, p0, LX/0kH;->A09:LX/0kh;

    .line 95390
    invoke-virtual {v0}, LX/0kh;->A0D()Ljava/lang/String;

    move-result-object v2

    .line 95391
    iget-object v0, v0, LX/0kh;->A00:Landroid/content/SharedPreferences;

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    .line 95392
    const-string v0, "payment_chat_composer_entry_nux_shown"

    invoke-interface {v1, v0, v2}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 95393
    iget-object v0, p0, Lcom/soula2/Conversation;->A05:Landroid/animation/ObjectAnimator;

    if-eqz v0, :cond_0

    .line 95394
    invoke-virtual {v0}, Landroid/animation/Animator;->cancel()V

    .line 95395
    :cond_0
    iget-object v0, p0, Lcom/soula2/Conversation;->A0C:Landroid/view/View;

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    .line 95396
    :cond_1
    return-void
.end method

.method public final A2z(I)V
    .locals 7

    .line 95397
    iget-object v1, p0, LX/0kH;->A0C:LX/0kj;

    const/16 v0, 0x342

    invoke-virtual {v1, v0}, LX/0kj;->A07(I)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 95398
    invoke-virtual {p0, p1}, Lcom/soula2/Conversation;->A3F(I)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 95399
    iget-object v0, p0, LX/0kH;->A09:LX/0kh;

    .line 95400
    invoke-virtual {v0}, LX/0kh;->A0D()Ljava/lang/String;

    move-result-object v3

    .line 95401
    iget-object v2, v0, LX/0kh;->A00:Landroid/content/SharedPreferences;

    const-string v1, "payment_chat_composer_entry_nux_shown"

    const-string v0, ""

    .line 95402
    invoke-interface {v2, v1, v0}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 95403
    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    .line 95404
    if-nez v0, :cond_3

    .line 95405
    iget-object v1, p0, Lcom/soula2/Conversation;->A0C:Landroid/view/View;

    if-eqz v1, :cond_0

    const/4 v0, 0x0

    .line 95406
    invoke-virtual {v1, v0}, Landroid/view/View;->setVisibility(I)V

    .line 95407
    :cond_0
    iget-object v2, p0, Lcom/soula2/Conversation;->A05:Landroid/animation/ObjectAnimator;

    const/4 v3, 0x1

    if-nez v2, :cond_1

    .line 95408
    iget-object v6, p0, Lcom/soula2/Conversation;->A0C:Landroid/view/View;

    const/4 v0, 0x3

    new-array v5, v0, [Landroid/animation/PropertyValuesHolder;

    new-array v1, v3, [F

    const/4 v4, 0x0

    const v2, 0x3fb33333    # 1.4f

    aput v2, v1, v4

    const-string/jumbo v0, "scaleX"

    .line 95409
    invoke-static {v0, v1}, Landroid/animation/PropertyValuesHolder;->ofFloat(Ljava/lang/String;[F)Landroid/animation/PropertyValuesHolder;

    move-result-object v0

    aput-object v0, v5, v4

    new-array v1, v3, [F

    aput v2, v1, v4

    const-string/jumbo v0, "scaleY"

    .line 95410
    invoke-static {v0, v1}, Landroid/animation/PropertyValuesHolder;->ofFloat(Ljava/lang/String;[F)Landroid/animation/PropertyValuesHolder;

    move-result-object v0

    aput-object v0, v5, v3

    const/4 v2, 0x2

    new-array v1, v2, [F

    fill-array-data v1, :array_0

    const-string v0, "alpha"

    .line 95411
    invoke-static {v0, v1}, Landroid/animation/PropertyValuesHolder;->ofFloat(Ljava/lang/String;[F)Landroid/animation/PropertyValuesHolder;

    move-result-object v0

    aput-object v0, v5, v2

    .line 95412
    invoke-static {v6, v5}, Landroid/animation/ObjectAnimator;->ofPropertyValuesHolder(Ljava/lang/Object;[Landroid/animation/PropertyValuesHolder;)Landroid/animation/ObjectAnimator;

    move-result-object v2

    iput-object v2, p0, Lcom/soula2/Conversation;->A05:Landroid/animation/ObjectAnimator;

    .line 95413
    :cond_1
    const-wide/16 v0, 0x5dc

    invoke-virtual {v2, v0, v1}, Landroid/animation/ObjectAnimator;->setDuration(J)Landroid/animation/ObjectAnimator;

    .line 95414
    iget-object v1, p0, Lcom/soula2/Conversation;->A05:Landroid/animation/ObjectAnimator;

    const/4 v0, -0x1

    invoke-virtual {v1, v0}, Landroid/animation/ValueAnimator;->setRepeatCount(I)V

    .line 95415
    iget-object v0, p0, Lcom/soula2/Conversation;->A05:Landroid/animation/ObjectAnimator;

    invoke-virtual {v0, v3}, Landroid/animation/ValueAnimator;->setRepeatMode(I)V

    .line 95416
    iget-object v0, p0, Lcom/soula2/Conversation;->A05:Landroid/animation/ObjectAnimator;

    invoke-virtual {v0}, Landroid/animation/Animator;->start()V

    .line 95417
    :cond_2
    return-void

    .line 95418
    :cond_3
    invoke-virtual {p0, p1}, Lcom/soula2/Conversation;->A2y(I)V

    return-void

    :array_0
    .array-data 4
        0x3ecccccd    # 0.4f
        0x0
    .end array-data
.end method

.method public final A30(Landroid/view/View;Landroid/view/ViewGroup;)V
    .locals 12

    const/4 v0, 0x1

    .line 95419
    move-object v9, p0

    invoke-virtual {p0, v0}, Lcom/soula2/Conversation;->A2g(I)I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/soula2/Conversation;->A2x(I)V

    .line 95420
    iget-object v0, p0, Lcom/soula2/Conversation;->A1c:Lcom/soula2/conversation/ConversationListView;

    if-nez v0, :cond_4

    const/4 v11, 0x0

    .line 95421
    :goto_0
    const/4 v0, -0x1

    .line 95422
    iput v0, p0, Lcom/soula2/Conversation;->A02:I

    .line 95423
    iget-object v0, p0, Lcom/soula2/Conversation;->A0w:Lcom/soula2/KeyboardPopupLayout;

    const/4 v6, 0x0

    invoke-virtual {v0, v6}, Landroid/view/ViewGroup;->setClipChildren(Z)V

    .line 95424
    iget-object v0, p0, Lcom/soula2/Conversation;->A1c:Lcom/soula2/conversation/ConversationListView;

    invoke-virtual {v0}, Landroid/widget/AbsListView;->getTranscriptMode()I

    move-result v10

    .line 95425
    move-object v8, p2

    invoke-virtual {p2}, Landroid/view/View;->getHeight()I

    move-result v5

    .line 95426
    int-to-float v1, v5

    const/4 v0, 0x0

    new-instance v4, Landroid/view/animation/TranslateAnimation;

    invoke-direct {v4, v0, v0, v0, v1}, Landroid/view/animation/TranslateAnimation;-><init>(FFFF)V

    const-wide/16 v0, 0xfa

    .line 95427
    invoke-virtual {v4, v0, v1}, Landroid/view/animation/Animation;->setDuration(J)V

    .line 95428
    move-object v7, p1

    invoke-virtual {p1, v4}, Landroid/view/View;->startAnimation(Landroid/view/animation/Animation;)V

    .line 95429
    iget-object v2, p0, Lcom/soula2/Conversation;->A0P:Landroid/view/ViewGroup;

    if-ne p2, v2, :cond_0

    iget-object v2, p0, Lcom/soula2/Conversation;->A0M:Landroid/view/ViewGroup;

    .line 95430
    invoke-virtual {v2}, Landroid/view/View;->getVisibility()I

    move-result v2

    if-nez v2, :cond_0

    .line 95431
    iget-object v2, p0, Lcom/soula2/Conversation;->A0M:Landroid/view/ViewGroup;

    invoke-virtual {v2, v4}, Landroid/view/View;->startAnimation(Landroid/view/animation/Animation;)V

    .line 95432
    :cond_0
    iget-object v2, p0, Lcom/soula2/Conversation;->A2v:Lcom/soula2/mentions/MentionableEntry;

    .line 95433
    iget-boolean v2, v2, Lcom/soula2/mentions/MentionableEntry;->A0H:Z

    .line 95434
    if-eqz v2, :cond_1

    .line 95435
    iget-object v2, p0, Lcom/soula2/Conversation;->A0Q:Landroid/widget/FrameLayout;

    invoke-virtual {v2, v4}, Landroid/view/View;->startAnimation(Landroid/view/animation/Animation;)V

    .line 95436
    :cond_1
    if-eqz v11, :cond_2

    .line 95437
    iget-object v3, p0, Lcom/soula2/Conversation;->A1c:Lcom/soula2/conversation/ConversationListView;

    const/4 v2, 0x2

    invoke-virtual {v3, v2}, Landroid/widget/AbsListView;->setTranscriptMode(I)V

    .line 95438
    iget-object v2, p0, Lcom/soula2/Conversation;->A1c:Lcom/soula2/conversation/ConversationListView;

    invoke-virtual {v2, v4}, Landroid/view/View;->startAnimation(Landroid/view/animation/Animation;)V

    .line 95439
    :cond_2
    iget-object v2, p0, Lcom/soula2/Conversation;->A0A:Landroid/view/View;

    invoke-virtual {v2}, Landroid/view/View;->getBackground()Landroid/graphics/drawable/Drawable;

    move-result-object v4

    .line 95440
    instance-of v2, v4, LX/2Vn;

    if-nez v2, :cond_3

    .line 95441
    new-instance v3, LX/2Vn;

    invoke-direct {v3, v4}, LX/2Vn;-><init>(Landroid/graphics/drawable/Drawable;)V

    .line 95442
    iget-object v2, p0, Lcom/soula2/Conversation;->A0A:Landroid/view/View;

    invoke-static {v3, v2}, LX/2Vn;->A00(Landroid/graphics/drawable/Drawable;Landroid/view/View;)V

    .line 95443
    :cond_3
    new-instance v2, Lcom/facebook/redex/IDxAnimationShape1S0101000_2_I0;

    invoke-direct {v2, p0, v5, v6}, Lcom/facebook/redex/IDxAnimationShape1S0101000_2_I0;-><init>(Ljava/lang/Object;II)V

    .line 95444
    invoke-virtual {v2, v0, v1}, Landroid/view/animation/Animation;->setDuration(J)V

    .line 95445
    iget-object v0, p0, Lcom/soula2/Conversation;->A0A:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->startAnimation(Landroid/view/animation/Animation;)V

    .line 95446
    new-instance v6, LX/2fj;

    invoke-direct/range {v6 .. v11}, LX/2fj;-><init>(Landroid/view/View;Landroid/view/ViewGroup;Lcom/soula2/Conversation;IZ)V

    invoke-virtual {v2, v6}, Landroid/view/animation/Animation;->setAnimationListener(Landroid/view/animation/Animation$AnimationListener;)V

    .line 95447
    return-void

    .line 95448
    :cond_4
    invoke-virtual {v0}, Lcom/soula2/conversation/ConversationListView;->A0A()Z

    move-result v11

    goto :goto_0
.end method

.method public final A31(Landroid/view/View;Landroid/view/ViewGroup;)V
    .locals 7

    .line 95449
    move-object v4, p0

    iget-object v0, p0, Lcom/soula2/Conversation;->A1c:Lcom/soula2/conversation/ConversationListView;

    if-nez v0, :cond_1

    const/4 v6, 0x0

    .line 95450
    :goto_0
    const/4 v0, 0x1

    .line 95451
    iput v0, p0, Lcom/soula2/Conversation;->A02:I

    .line 95452
    iget-object v0, p0, Lcom/soula2/Conversation;->A0w:Lcom/soula2/KeyboardPopupLayout;

    const/4 v2, 0x0

    invoke-virtual {v0, v2}, Landroid/view/ViewGroup;->setClipChildren(Z)V

    .line 95453
    iget-object v0, p0, Lcom/soula2/Conversation;->A1c:Lcom/soula2/conversation/ConversationListView;

    invoke-virtual {v0}, Landroid/widget/AbsListView;->getTranscriptMode()I

    move-result v5

    .line 95454
    iget-object v1, p0, Lcom/soula2/Conversation;->A1c:Lcom/soula2/conversation/ConversationListView;

    if-eqz v6, :cond_0

    .line 95455
    const/4 v0, 0x2

    invoke-virtual {v1, v0}, Landroid/widget/AbsListView;->setTranscriptMode(I)V

    .line 95456
    :goto_1
    invoke-virtual {p0, v2}, Lcom/soula2/Conversation;->A2g(I)I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/soula2/Conversation;->A2x(I)V

    .line 95457
    move-object v3, p2

    invoke-virtual {p2, v2}, Landroid/view/View;->setVisibility(I)V

    .line 95458
    invoke-virtual {p2}, Landroid/view/View;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    move-result-object v0

    new-instance v1, LX/36H;

    move-object v2, p1

    invoke-direct/range {v1 .. v6}, LX/36H;-><init>(Landroid/view/View;Landroid/view/ViewGroup;Lcom/soula2/Conversation;IZ)V

    .line 95459
    invoke-virtual {v0, v1}, Landroid/view/ViewTreeObserver;->addOnGlobalLayoutListener(Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;)V

    return-void

    .line 95460
    :cond_0
    invoke-virtual {v1, v2}, Landroid/widget/AbsListView;->setTranscriptMode(I)V

    goto :goto_1

    .line 95461
    :cond_1
    invoke-virtual {v0}, Lcom/soula2/conversation/ConversationListView;->A0A()Z

    move-result v6

    goto :goto_0
.end method

.method public final A32(LX/0l5;)V
    .locals 15

    .line 95462
    move-object v7, p0

    invoke-static {p0}, LX/1ix;->A03(Landroid/app/Activity;)Z

    move-result v0

    if-nez v0, :cond_a

    .line 95463
    iget-object v2, p0, Lcom/soula2/Conversation;->A1y:LX/0l4;

    .line 95464
    iget v0, v2, LX/0l4;->A00:I

    if-nez v0, :cond_0

    move-object/from16 v3, p1

    if-eqz p1, :cond_0

    iget-object v1, v3, LX/0l5;->A0M:Ljava/lang/String;

    iget-object v0, v2, LX/0l4;->A05:Ljava/lang/String;

    .line 95465
    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    .line 95466
    iput v0, v2, LX/0l4;->A00:I

    .line 95467
    iput-object v3, v2, LX/0l4;->A01:LX/0l5;

    const/4 v0, 0x0

    .line 95468
    iput-object v0, v2, LX/0l4;->A03:LX/0lG;

    .line 95469
    :cond_0
    iget-object v0, v2, LX/0l4;->A01:LX/0l5;

    if-eqz v0, :cond_d

    iget-object v1, v2, LX/0l4;->A05:Ljava/lang/String;

    iget-object v0, v0, LX/0l5;->A0M:Ljava/lang/String;

    .line 95470
    invoke-static {v1, v0}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_d

    iget-object v0, v2, LX/0l4;->A01:LX/0l5;

    .line 95471
    invoke-virtual {v0}, LX/0l5;->A0B()Z

    move-result v0

    if-eqz v0, :cond_d

    iget-boolean v0, v2, LX/0l4;->A06:Z

    if-eqz v0, :cond_d

    .line 95472
    const-string v0, "conversation/showLinkPreviewShell"

    .line 95473
    invoke-static {v0}, Lcom/whatsapp/util/Log;->i(Ljava/lang/String;)V

    .line 95474
    iget-object v0, p0, Lcom/soula2/Conversation;->A1m:LX/20b;

    if-nez v0, :cond_4

    .line 95475
    new-instance v8, LX/3xe;

    invoke-direct {v8, p0}, LX/3xe;-><init>(Lcom/soula2/Conversation;)V

    .line 95476
    iget-object v11, p0, Lcom/soula2/Conversation;->A23:LX/00z;

    iget-object v10, p0, Lcom/soula2/Conversation;->A1y:LX/0l4;

    iget-object v12, p0, LX/0kJ;->A05:LX/0lO;

    iget-object v9, p0, LX/0kH;->A04:LX/0lC;

    iget-object v0, p0, LX/0kB;->A0X:LX/0vp;

    .line 95477
    invoke-virtual {v0}, LX/0vp;->A0X()Z

    move-result v13

    new-instance v6, LX/20b;

    invoke-direct/range {v6 .. v13}, LX/20b;-><init>(Landroid/content/Context;LX/3xe;LX/0lC;LX/0l4;LX/00z;LX/0lO;Z)V

    iput-object v6, p0, Lcom/soula2/Conversation;->A1m:LX/20b;

    .line 95478
    iget-object v1, p0, Lcom/soula2/Conversation;->A0P:Landroid/view/ViewGroup;

    .line 95479
    iget-object v0, v6, LX/20b;->A07:Lcom/soula2/webpagepreview/WebPagePreviewView;

    .line 95480
    invoke-virtual {v1, v0}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    .line 95481
    new-instance v4, LX/2yI;

    invoke-direct {v4, p0}, LX/2yI;-><init>(Lcom/soula2/Conversation;)V

    .line 95482
    iget-object v3, p0, Lcom/soula2/Conversation;->A2C:LX/20a;

    iget-object v2, p0, Lcom/soula2/Conversation;->A2r:LX/0l8;

    iget-object v1, p0, Lcom/soula2/Conversation;->A1c:Lcom/soula2/conversation/ConversationListView;

    iget-object v12, p0, Lcom/soula2/Conversation;->A2v:Lcom/soula2/mentions/MentionableEntry;

    iget-object v11, p0, Lcom/soula2/Conversation;->A0P:Landroid/view/ViewGroup;

    iget-object v5, p0, Lcom/soula2/Conversation;->A1m:LX/20b;

    iget-object v10, p0, Lcom/soula2/Conversation;->A0w:Lcom/soula2/KeyboardPopupLayout;

    .line 95483
    iget-boolean v0, v3, LX/20a;->A0S:Z

    if-eqz v0, :cond_2

    iget-boolean v0, v3, LX/20a;->A0F:Z

    if-nez v0, :cond_2

    .line 95484
    iput-object v2, v3, LX/20a;->A0B:LX/0l8;

    .line 95485
    iput-object v4, v3, LX/20a;->A07:LX/2yI;

    .line 95486
    iput-object v1, v3, LX/20a;->A03:Landroid/widget/ListView;

    .line 95487
    iput-object v12, v3, LX/20a;->A04:Landroid/widget/TextView;

    .line 95488
    iput-object v11, v3, LX/20a;->A02:Landroid/view/ViewGroup;

    .line 95489
    iput-object v5, v3, LX/20a;->A08:LX/20b;

    .line 95490
    iput-object v10, v3, LX/20a;->A00:Landroid/view/View;

    .line 95491
    iget-object v9, v3, LX/20a;->A0J:Landroid/app/Activity;

    invoke-virtual {v9}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    iget v0, v0, Landroid/content/res/Configuration;->orientation:I

    const/4 v6, 0x2

    const/4 v2, 0x1

    const/4 v4, 0x0

    const/4 v1, 0x0

    if-ne v0, v6, :cond_1

    const/4 v1, 0x1

    .line 95492
    :cond_1
    iget v0, v3, LX/20a;->A0I:I

    if-ne v0, v6, :cond_c

    if-nez v1, :cond_2

    .line 95493
    invoke-static {v9}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v1

    const v0, 0x7f0d015b

    .line 95494
    invoke-virtual {v1, v0, v11, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    iput-object v1, v3, LX/20a;->A01:Landroid/view/View;

    .line 95495
    invoke-static {v1}, LX/009;->A03(Landroid/view/View;)V

    const v0, 0x7f0a10d3

    .line 95496
    invoke-static {v1, v0}, LX/01U;->A0E(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/shimmer/ShimmerFrameLayout;

    iput-object v0, v3, LX/20a;->A06:Lcom/facebook/shimmer/ShimmerFrameLayout;

    .line 95497
    iget-object v0, v3, LX/20a;->A01:Landroid/view/View;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 95498
    iget-object v0, v3, LX/20a;->A01:Landroid/view/View;

    invoke-virtual {v11, v0}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    .line 95499
    iget-object v14, v3, LX/20a;->A0C:LX/15n;

    new-instance v13, LX/42B;

    invoke-direct {v13, v5, v3}, LX/42B;-><init>(LX/20b;LX/20a;)V

    new-instance v8, LX/2RU;

    invoke-direct/range {v8 .. v14}, LX/2RU;-><init>(Landroid/content/Context;Landroid/view/View;Landroid/view/ViewGroup;Landroid/widget/TextView;LX/42B;LX/15n;)V

    iput-object v8, v3, LX/20a;->A0A:LX/2RU;

    .line 95500
    invoke-virtual {v8, v1}, Landroid/view/View;->setVisibility(I)V

    .line 95501
    iput-boolean v4, v3, LX/20a;->A0G:Z

    .line 95502
    iget-object v0, v3, LX/20a;->A0A:LX/2RU;

    invoke-virtual {v11, v0}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    .line 95503
    iget-object v0, v3, LX/20a;->A0K:Landroid/text/TextWatcher;

    invoke-virtual {v12, v0}, Landroid/widget/TextView;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 95504
    :goto_0
    iput-boolean v2, v3, LX/20a;->A0F:Z

    .line 95505
    :cond_2
    sget-object v2, Lcom/soula2/Conversation;->A5G:Ljava/util/HashMap;

    iget-object v1, p0, Lcom/soula2/Conversation;->A2b:LX/0mJ;

    const-class v0, LX/0l8;

    .line 95506
    invoke-virtual {v1, v0}, LX/0mJ;->A0A(Ljava/lang/Class;)Lcom/whatsapp/jid/Jid;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/util/AbstractMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/23Q;

    if-eqz v1, :cond_4

    .line 95507
    iget-boolean v0, v1, LX/23Q;->A03:Z

    if-nez v0, :cond_3

    iget-boolean v0, v1, LX/23Q;->A04:Z

    if-eqz v0, :cond_4

    .line 95508
    :cond_3
    iget-object v2, p0, Lcom/soula2/Conversation;->A2C:LX/20a;

    .line 95509
    iget-boolean v0, v2, LX/20a;->A0F:Z

    if-eqz v0, :cond_4

    .line 95510
    iget v1, v2, LX/20a;->A0I:I

    const/4 v0, 0x2

    if-ne v1, v0, :cond_b

    .line 95511
    iget-object v0, v2, LX/20a;->A08:LX/20b;

    .line 95512
    iget-object v1, v0, LX/20b;->A07:Lcom/soula2/webpagepreview/WebPagePreviewView;

    .line 95513
    const/16 v0, 0x8

    invoke-virtual {v1, v0}, Landroid/view/View;->setVisibility(I)V

    .line 95514
    iget-object v1, v2, LX/20a;->A01:Landroid/view/View;

    invoke-static {v1}, LX/009;->A03(Landroid/view/View;)V

    const/4 v0, 0x0

    invoke-virtual {v1, v0}, Landroid/view/View;->setVisibility(I)V

    .line 95515
    iget-object v0, v2, LX/20a;->A06:Lcom/facebook/shimmer/ShimmerFrameLayout;

    invoke-static {v0}, LX/009;->A03(Landroid/view/View;)V

    invoke-virtual {v0}, Lcom/facebook/shimmer/ShimmerFrameLayout;->A02()V

    .line 95516
    :cond_4
    :goto_1
    iget-object v0, p0, Lcom/soula2/Conversation;->A0P:Landroid/view/ViewGroup;

    invoke-virtual {v0}, Landroid/view/View;->getVisibility()I

    move-result v0

    if-nez v0, :cond_5

    iget v0, p0, Lcom/soula2/Conversation;->A02:I

    if-gez v0, :cond_6

    :cond_5
    const-string v0, "conversation/showLinkPreviewShell/start"

    .line 95517
    invoke-static {v0}, Lcom/whatsapp/util/Log;->i(Ljava/lang/String;)V

    .line 95518
    iget-object v1, p0, Lcom/soula2/Conversation;->A0P:Landroid/view/ViewGroup;

    iget-object v0, p0, Lcom/soula2/Conversation;->A1m:LX/20b;

    .line 95519
    iget-object v0, v0, LX/20b;->A07:Lcom/soula2/webpagepreview/WebPagePreviewView;

    .line 95520
    invoke-virtual {p0, v0, v1}, Lcom/soula2/Conversation;->A31(Landroid/view/View;Landroid/view/ViewGroup;)V

    .line 95521
    :cond_6
    iget-object v0, p0, Lcom/soula2/Conversation;->A1y:LX/0l4;

    .line 95522
    iget-object v1, v0, LX/0l4;->A01:LX/0l5;

    .line 95523
    instance-of v0, v1, LX/2Ig;

    if-eqz v0, :cond_8

    .line 95524
    check-cast v1, LX/2Ig;

    .line 95525
    iget-object v3, v1, LX/2Ig;->A00:LX/1b5;

    .line 95526
    iget-object v2, p0, Lcom/soula2/Conversation;->A2E:LX/0rJ;

    iget-object v0, p0, Lcom/soula2/Conversation;->A2r:LX/0l8;

    invoke-static {v0}, Lcom/whatsapp/jid/UserJid;->of(Lcom/whatsapp/jid/Jid;)Lcom/whatsapp/jid/UserJid;

    move-result-object v1

    const/4 v0, 0x4

    if-eqz v3, :cond_7

    .line 95527
    const/16 v0, 0x11

    .line 95528
    :cond_7
    invoke-virtual {v2, v1, v0}, LX/0rJ;->A01(Lcom/whatsapp/jid/UserJid;I)V

    .line 95529
    :cond_8
    iget-object v1, p0, Lcom/soula2/Conversation;->A1m:LX/20b;

    iget-object v0, p0, Lcom/soula2/Conversation;->A1y:LX/0l4;

    .line 95530
    iget-object v4, v0, LX/0l4;->A01:LX/0l5;

    .line 95531
    iget-object v3, v1, LX/20b;->A07:Lcom/soula2/webpagepreview/WebPagePreviewView;

    iget-boolean v2, v1, LX/20b;->A00:Z

    const/4 v1, 0x0

    const/4 v0, 0x0

    .line 95532
    invoke-virtual {v3, v4, v0, v1, v2}, Lcom/soula2/webpagepreview/WebPagePreviewView;->A0A(LX/0l5;Ljava/util/List;ZZ)V

    .line 95533
    iget-object v2, p0, Lcom/soula2/Conversation;->A42:LX/0kz;

    if-eqz v2, :cond_9

    iget-object v0, p0, Lcom/soula2/Conversation;->A1y:LX/0l4;

    .line 95534
    iget-object v1, v0, LX/0l4;->A01:LX/0l5;

    .line 95535
    instance-of v0, v1, LX/2Ig;

    if-eqz v0, :cond_9

    .line 95536
    check-cast v1, LX/2Ig;

    iget-object v0, p0, Lcom/soula2/Conversation;->A2A:LX/1be;

    .line 95537
    iput-object v1, v2, LX/0kz;->A0H:LX/2Ig;

    .line 95538
    iput-object v0, v2, LX/0kz;->A0I:LX/1be;

    .line 95539
    :cond_9
    iget-object v2, p0, Lcom/soula2/Conversation;->A1y:LX/0l4;

    .line 95540
    iget v1, v2, LX/0l4;->A00:I

    const/4 v0, 0x1

    if-ne v1, v0, :cond_a

    iget-object v1, v2, LX/0l4;->A01:LX/0l5;

    if-eqz v1, :cond_a

    .line 95541
    iget-object v0, v1, LX/0l5;->A0K:[B

    .line 95542
    if-eqz v0, :cond_a

    iget-boolean v0, v2, LX/0l4;->A06:Z

    if-eqz v0, :cond_a

    instance-of v0, v1, LX/2Ig;

    if-nez v0, :cond_a

    .line 95543
    iget-object v2, p0, LX/0kJ;->A05:LX/0lO;

    const/16 v1, 0x1e

    new-instance v0, Lcom/facebook/redex/RunnableRunnableShape1S0100000_I0;

    invoke-direct {v0, p0, v1}, Lcom/facebook/redex/RunnableRunnableShape1S0100000_I0;-><init>(Lcom/soula2/Conversation;I)V

    invoke-interface {v2, v0}, LX/0lO;->AZm(Ljava/lang/Runnable;)V

    .line 95544
    :cond_a
    return-void

    .line 95545
    :cond_b
    iget-object v3, v2, LX/20a;->A09:Lcom/soula2/ctwa/icebreaker/ui/IcebreakerBubbleView;

    invoke-static {v3}, LX/009;->A03(Landroid/view/View;)V

    .line 95546
    invoke-virtual {v3}, Landroid/view/ViewGroup;->removeAllViews()V

    .line 95547
    invoke-virtual {v3}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v2

    const v1, 0x7f0d015b

    const/4 v0, 0x1

    .line 95548
    invoke-virtual {v2, v1, v3, v0}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    .line 95549
    const v0, 0x7f0a10d3

    invoke-static {v1, v0}, LX/01U;->A0E(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/shimmer/ShimmerFrameLayout;

    .line 95550
    invoke-virtual {v0}, Lcom/facebook/shimmer/ShimmerFrameLayout;->A02()V

    const/4 v0, 0x0

    .line 95551
    invoke-virtual {v3, v0}, Landroid/view/View;->setVisibility(I)V

    goto/16 :goto_1

    .line 95552
    :cond_c
    iget v0, v3, LX/20a;->A0H:I

    invoke-static {v9, v0}, LX/00S;->A05(Landroid/app/Activity;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/soula2/ctwa/icebreaker/ui/IcebreakerBubbleView;

    iput-object v0, v3, LX/20a;->A09:Lcom/soula2/ctwa/icebreaker/ui/IcebreakerBubbleView;

    goto/16 :goto_0

    .line 95553
    :cond_d
    invoke-virtual {p0}, Lcom/soula2/Conversation;->A2i()V

    return-void
.end method

.method public final A33(LX/2B6;Z)V
    .locals 2

    .line 95554
    iget-object v0, p0, Lcom/soula2/Conversation;->A1c:Lcom/soula2/conversation/ConversationListView;

    invoke-virtual {v0}, Lcom/soula2/conversation/ConversationListView;->getConversationCursorAdapter()LX/0me;

    move-result-object v1

    .line 95555
    iget v0, p1, LX/2B6;->A00:I

    .line 95556
    iput v0, v1, LX/0me;->A02:I

    .line 95557
    iget v0, p1, LX/2B6;->A01:I

    .line 95558
    iput v0, v1, LX/0me;->A03:I

    .line 95559
    iget v0, p1, LX/2B6;->A02:I

    .line 95560
    iput v0, v1, LX/0me;->A04:I

    const-string/jumbo v0, "updateUnseenCount"

    .line 95561
    invoke-virtual {v1, v0}, LX/0me;->A08(Ljava/lang/String;)V

    .line 95562
    if-eqz p2, :cond_0

    .line 95563
    iget-object v0, p0, Lcom/soula2/Conversation;->A1c:Lcom/soula2/conversation/ConversationListView;

    invoke-virtual {v0}, Lcom/soula2/conversation/ConversationListView;->getConversationCursorAdapter()LX/0me;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/BaseAdapter;->notifyDataSetChanged()V

    :cond_0
    return-void
.end method

.method public final A34(LX/0mJ;LX/0l8;Z)V
    .locals 2

    .line 95564
    :try_start_0
    iget-object v0, p0, Lcom/soula2/Conversation;->A3j:LX/18B;

    .line 95565
    invoke-virtual {v0, p1, p2, p3}, LX/18B;->A00(LX/0mJ;LX/0l8;Z)Landroid/content/Intent;

    move-result-object v1

    const/16 v0, 0xd

    .line 95566
    invoke-virtual {p0, v1, v0}, Landroid/app/Activity;->startActivityForResult(Landroid/content/Intent;I)V

    .line 95567
    iget-object v1, p0, LX/0kB;->A0p:LX/18r;

    const/4 v0, 0x5

    invoke-virtual {v1, p3, v0}, LX/18r;->A02(ZI)V

    return-void
    :try_end_0
    .catch Landroid/content/ActivityNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    :catch_0
    const/16 v0, 0xc

    .line 95568
    invoke-static {p0, v0}, LX/1ix;->A01(Landroid/app/Activity;I)V

    return-void
.end method

.method public final A35(LX/35A;)V
    .locals 30

    .line 95569
    move-object/from16 v5, p0

    iget-object v2, v5, Lcom/soula2/Conversation;->A1K:LX/0wh;

    iget-object v1, v5, Lcom/soula2/Conversation;->A2b:LX/0mJ;

    const-class v0, Lcom/whatsapp/jid/UserJid;

    invoke-virtual {v1, v0}, LX/0mJ;->A0A(Ljava/lang/Class;)Lcom/whatsapp/jid/Jid;

    move-result-object v0

    check-cast v0, Lcom/whatsapp/jid/UserJid;

    invoke-virtual {v2, v0}, LX/0wh;->A0I(Lcom/whatsapp/jid/UserJid;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/16 v0, 0x6a

    .line 95570
    invoke-static {v5, v0}, LX/1ix;->A01(Landroid/app/Activity;I)V

    return-void

    .line 95571
    :cond_0
    new-instance v20, Ljava/util/ArrayList;

    invoke-direct/range {v20 .. v20}, Ljava/util/ArrayList;-><init>()V

    .line 95572
    iget-object v1, v5, Lcom/soula2/Conversation;->A2r:LX/0l8;

    move-object/from16 v0, v20

    invoke-virtual {v0, v1}, Ljava/util/AbstractCollection;->add(Ljava/lang/Object;)Z

    .line 95573
    move-object/from16 v1, p1

    iget-object v2, v1, LX/35A;->A01:LX/359;

    iget v11, v2, LX/359;->A01:I

    if-gtz v11, :cond_1

    iget-object v0, v1, LX/35A;->A02:LX/359;

    iget v11, v0, LX/359;->A01:I

    .line 95574
    :cond_1
    iget v12, v2, LX/359;->A00:I

    if-gtz v12, :cond_2

    iget-object v0, v1, LX/35A;->A02:LX/359;

    iget v12, v0, LX/359;->A00:I

    .line 95575
    :cond_2
    iget-object v0, v5, Lcom/soula2/Conversation;->A2v:Lcom/soula2/mentions/MentionableEntry;

    invoke-virtual {v0}, Lcom/soula2/mentions/MentionableEntry;->getStringText()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/1bk;->A04(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v19

    .line 95576
    iget-object v0, v5, Lcom/soula2/Conversation;->A1x:LX/0mf;

    .line 95577
    iget-object v0, v0, LX/0mf;->A08:LX/0md;

    .line 95578
    const/4 v8, 0x0

    if-eqz v0, :cond_3

    const/4 v8, 0x1

    .line 95579
    :cond_3
    iget-boolean v0, v1, LX/35A;->A05:Z

    const-string/jumbo v18, "usage_quote"

    const-string v17, "clear_message_after_send"

    const-string v16, "mentions"

    const-string v10, "caption"

    const-string v9, "jid"

    const-string v7, "media_height"

    const-string v6, "media_width"

    if-eqz v0, :cond_4

    .line 95580
    iget-object v14, v2, LX/359;->A02:Ljava/lang/String;

    iget v4, v1, LX/35A;->A00:I

    iget-boolean v3, v5, Lcom/soula2/Conversation;->A4P:Z

    .line 95581
    const/4 v13, 0x0

    const/16 v2, 0x16

    .line 95582
    new-instance v15, Landroid/content/Intent;

    invoke-direct {v15}, Landroid/content/Intent;-><init>()V

    .line 95583
    invoke-virtual {v5}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v1

    const-string v0, "com.soula2.gifvideopreview.GifVideoPreviewActivity"

    invoke-virtual {v15, v1, v0}, Landroid/content/Intent;->setClassName(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v1

    const-string v0, "file_path"

    .line 95584
    invoke-virtual {v1, v0, v14}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v14

    .line 95585
    invoke-static/range {v20 .. v20}, LX/0mQ;->A06(Ljava/util/Collection;)Ljava/util/ArrayList;

    move-result-object v1

    const-string v0, "jids"

    invoke-virtual {v14, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    move-result-object v1

    const-string/jumbo v0, "send"

    .line 95586
    invoke-virtual {v1, v0, v13}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    move-result-object v1

    const-string/jumbo v0, "provider"

    .line 95587
    invoke-virtual {v1, v0, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    move-result-object v1

    const-string v0, "number_from_url"

    .line 95588
    invoke-virtual {v1, v0, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    move-result-object v1

    const-string v0, "origin"

    .line 95589
    invoke-virtual {v1, v0, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    move-result-object v0

    .line 95590
    :goto_0
    invoke-virtual {v0, v6, v11}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    move-result-object v0

    .line 95591
    invoke-virtual {v0, v7, v12}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    move-result-object v1

    iget-object v0, v5, Lcom/soula2/Conversation;->A2r:LX/0l8;

    .line 95592
    invoke-static {v0}, LX/0mQ;->A03(Lcom/whatsapp/jid/Jid;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v9, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v1

    .line 95593
    move-object/from16 v0, v19

    invoke-virtual {v1, v10, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v2

    iget-object v0, v5, Lcom/soula2/Conversation;->A2v:Lcom/soula2/mentions/MentionableEntry;

    .line 95594
    invoke-virtual {v0}, Lcom/soula2/mentions/MentionableEntry;->getMentions()Ljava/util/List;

    move-result-object v0

    invoke-static {v0}, LX/0mQ;->A06(Ljava/util/Collection;)Ljava/util/ArrayList;

    move-result-object v1

    .line 95595
    move-object/from16 v0, v16

    invoke-virtual {v2, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    move-result-object v2

    .line 95596
    invoke-static/range {v19 .. v19}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    xor-int/lit8 v1, v0, 0x1

    .line 95597
    move-object/from16 v0, v17

    invoke-virtual {v2, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    move-result-object v1

    .line 95598
    move-object/from16 v0, v18

    invoke-virtual {v1, v0, v8}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    move-result-object v1

    .line 95599
    const/16 v0, 0x19

    .line 95600
    invoke-virtual {v5, v1, v0}, Landroid/app/Activity;->startActivityForResult(Landroid/content/Intent;I)V

    return-void

    .line 95601
    :cond_4
    iget-object v0, v1, LX/35A;->A02:LX/359;

    iget-object v4, v0, LX/359;->A02:Ljava/lang/String;

    iget-object v3, v2, LX/359;->A02:Ljava/lang/String;

    iget-object v0, v1, LX/35A;->A03:LX/359;

    iget-object v2, v0, LX/359;->A02:Ljava/lang/String;

    iget v1, v1, LX/35A;->A00:I

    iget-boolean v0, v5, Lcom/soula2/Conversation;->A4P:Z

    const/16 v29, 0x0

    const/16 v27, 0x16

    .line 95602
    move-object/from16 v21, v5

    move-object/from16 v22, v4

    move-object/from16 v23, v3

    move-object/from16 v24, v2

    move-object/from16 v25, v20

    move/from16 v26, v1

    move/from16 v28, v0

    invoke-static/range {v21 .. v29}, LX/0kk;->A0c(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/List;IIZZ)Landroid/content/Intent;

    move-result-object v0

    goto :goto_0
.end method

.method public A36(LX/0md;)V
    .locals 8

    .line 95603
    iget-object v0, p0, Lcom/soula2/Conversation;->A1v:LX/1kT;

    invoke-virtual {v0}, LX/1kT;->A05()Z

    move-result v0

    .line 95604
    if-nez v0, :cond_5

    iget-object v0, p0, Lcom/soula2/Conversation;->A2v:Lcom/soula2/mentions/MentionableEntry;

    if-eqz v0, :cond_5

    .line 95605
    iget-object v0, p0, Lcom/soula2/Conversation;->A42:LX/0kz;

    move-object v5, p1

    if-eqz v0, :cond_0

    .line 95606
    iput-object p1, v0, LX/0kz;->A0L:LX/0md;

    .line 95607
    :cond_0
    if-eqz p1, :cond_5

    .line 95608
    iget-object v0, p0, Lcom/soula2/Conversation;->A0c:LX/059;

    if-eqz v0, :cond_1

    .line 95609
    invoke-virtual {v0}, LX/059;->A05()V

    .line 95610
    :cond_1
    iget-object v0, p0, Lcom/soula2/Conversation;->A0D:Landroid/view/View;

    const/4 v7, 0x0

    if-nez v0, :cond_2

    .line 95611
    invoke-virtual {p0}, Landroid/app/Activity;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v2

    const v1, 0x7f0d04de

    const/4 v0, 0x0

    invoke-virtual {v2, v1, v0, v7}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    iput-object v1, p0, Lcom/soula2/Conversation;->A0D:Landroid/view/View;

    .line 95612
    const v0, 0x7f0a0e68

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/FrameLayout;

    .line 95613
    const v1, 0x7f0801bd

    const v0, 0x7f06017b

    .line 95614
    invoke-static {p0, v1, v0}, LX/2AP;->A01(Landroid/content/Context;II)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 95615
    invoke-virtual {v2, v0}, Landroid/widget/FrameLayout;->setForeground(Landroid/graphics/drawable/Drawable;)V

    .line 95616
    iget-object v1, p0, Lcom/soula2/Conversation;->A0M:Landroid/view/ViewGroup;

    iget-object v0, p0, Lcom/soula2/Conversation;->A0D:Landroid/view/View;

    invoke-virtual {v1, v0}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    .line 95617
    iget-object v3, p0, Lcom/soula2/Conversation;->A23:LX/00z;

    iget-object v1, p0, Lcom/soula2/Conversation;->A0D:Landroid/view/View;

    const v0, 0x7f0a0e75

    .line 95618
    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    .line 95619
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v0, 0x7f07025b

    .line 95620
    invoke-virtual {v1, v0}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    .line 95621
    invoke-static {v2, v3, v7, v0}, LX/1up;->A08(Landroid/view/View;LX/00z;II)V

    .line 95622
    iget-object v0, p0, Lcom/soula2/Conversation;->A0D:Landroid/view/View;

    const v1, 0x7f0a02e0

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v7}, Landroid/view/View;->setVisibility(I)V

    .line 95623
    iget-object v0, p0, Lcom/soula2/Conversation;->A0D:Landroid/view/View;

    .line 95624
    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    const/4 v1, 0x1

    new-instance v0, Lcom/whatsapp/util/ViewOnClickCListenerShape0S0100000_I0;

    invoke-direct {v0, p0, v1}, Lcom/whatsapp/util/ViewOnClickCListenerShape0S0100000_I0;-><init>(Ljava/lang/Object;I)V

    .line 95625
    invoke-virtual {v2, v0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 95626
    iget-object v1, p0, Lcom/soula2/Conversation;->A0D:Landroid/view/View;

    const v0, 0x7f0a0e74

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    .line 95627
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    iget-object v0, p0, Lcom/soula2/Conversation;->A23:LX/00z;

    invoke-static {v1, v0}, LX/1Mr;->A01(Landroid/content/res/Resources;LX/00z;)F

    move-result v0

    .line 95628
    invoke-virtual {v2, v0}, Landroid/widget/TextView;->setTextSize(F)V

    .line 95629
    invoke-static {v2}, LX/1GU;->A06(Landroid/widget/TextView;)V

    .line 95630
    :cond_2
    iget-object v1, p0, Lcom/soula2/Conversation;->A1o:LX/18b;

    iget-object v2, p0, Lcom/soula2/Conversation;->A0D:Landroid/view/View;

    iget-object v4, p0, Lcom/soula2/Conversation;->A2r:LX/0l8;

    iget-object v6, p0, LX/0kB;->A0n:LX/17e;

    .line 95631
    iget-object v0, p0, LX/0kB;->A0F:LX/1Mq;

    invoke-virtual {v0, p0}, LX/1Mq;->A01(Landroid/content/Context;)LX/1Hc;

    move-result-object v3

    .line 95632
    invoke-virtual/range {v1 .. v7}, LX/18b;->A00(Landroid/view/View;LX/1Hc;LX/0l8;LX/0md;LX/17e;Z)V

    .line 95633
    iget-object v1, p0, Lcom/soula2/Conversation;->A0D:Landroid/view/View;

    const v0, 0x7f0a0e73

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/View;->getVisibility()I

    move-result v2

    .line 95634
    iget-object v1, p0, Lcom/soula2/Conversation;->A0D:Landroid/view/View;

    const v0, 0x7f0a02e9

    .line 95635
    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    if-nez v2, :cond_6

    const v0, 0x7f0807c6

    .line 95636
    invoke-virtual {v1, v0}, Landroid/view/View;->setBackgroundResource(I)V

    .line 95637
    :goto_0
    iget-object v0, p0, Lcom/soula2/Conversation;->A0M:Landroid/view/ViewGroup;

    invoke-virtual {v0}, Landroid/view/View;->getVisibility()I

    move-result v0

    if-nez v0, :cond_3

    iget v0, p0, Lcom/soula2/Conversation;->A02:I

    if-gez v0, :cond_4

    :cond_3
    const-string v0, "conversation/replypreview/start"

    .line 95638
    invoke-static {v0}, Lcom/whatsapp/util/Log;->i(Ljava/lang/String;)V

    .line 95639
    iget-object v1, p0, Lcom/soula2/Conversation;->A0M:Landroid/view/ViewGroup;

    iget-object v0, p0, Lcom/soula2/Conversation;->A0D:Landroid/view/View;

    invoke-virtual {p0, v0, v1}, Lcom/soula2/Conversation;->A31(Landroid/view/View;Landroid/view/ViewGroup;)V

    .line 95640
    :cond_4
    iget-object v0, p0, Lcom/soula2/Conversation;->A2j:LX/0mU;

    if-eqz v0, :cond_5

    invoke-virtual {v0}, Landroid/widget/PopupWindow;->isShowing()Z

    move-result v0

    if-nez v0, :cond_5

    invoke-virtual {p0}, LX/0kB;->AII()Z

    move-result v0

    if-nez v0, :cond_5

    .line 95641
    iget-object v0, p0, Lcom/soula2/Conversation;->A2v:Lcom/soula2/mentions/MentionableEntry;

    .line 95642
    invoke-virtual {v0, v7}, Lcom/soula2/WaEditText;->A05(Z)V

    .line 95643
    :cond_5
    return-void

    .line 95644
    :cond_6
    invoke-virtual {v1, v7}, Landroid/view/View;->setBackgroundColor(I)V

    goto :goto_0
.end method

.method public final A37(LX/0md;)V
    .locals 6

    .line 95645
    iget-object v5, p0, Lcom/soula2/Conversation;->A1c:Lcom/soula2/conversation/ConversationListView;

    .line 95646
    invoke-virtual {v5}, Landroid/widget/AdapterView;->getFirstVisiblePosition()I

    move-result v2

    .line 95647
    invoke-virtual {v5}, Landroid/widget/AdapterView;->getLastVisiblePosition()I

    move-result v1

    .line 95648
    invoke-virtual {v5}, Lcom/soula2/conversation/ConversationListView;->getConversationCursorAdapter()LX/0me;

    move-result-object v0

    invoke-virtual {v0, p1}, LX/0me;->A04(LX/0md;)I

    move-result v0

    const/4 v4, 0x0

    if-ltz v0, :cond_0

    add-int/lit8 v3, v0, 0x1

    if-lt v3, v2, :cond_0

    if-gt v3, v1, :cond_1

    sub-int v0, v3, v2

    .line 95649
    invoke-virtual {v5, v0}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v2

    if-eqz v2, :cond_0

    .line 95650
    invoke-virtual {v5}, Landroid/view/View;->getHeight()I

    move-result v0

    shr-int/lit8 v1, v0, 0x1

    invoke-virtual {v2}, Landroid/view/View;->getHeight()I

    move-result v0

    shr-int/lit8 v0, v0, 0x1

    sub-int/2addr v1, v0

    .line 95651
    invoke-virtual {v2}, Landroid/view/View;->getTop()I

    move-result v0

    if-le v0, v1, :cond_0

    .line 95652
    :goto_0
    invoke-virtual {v5, v3, v1}, Landroid/widget/AbsListView;->smoothScrollToPositionFromTop(II)V

    .line 95653
    iget-boolean v0, v5, Lcom/soula2/conversation/ConversationListView;->A0A:Z

    if-nez v0, :cond_0

    .line 95654
    invoke-virtual {v5, v4}, Landroid/widget/AbsListView;->setTranscriptMode(I)V

    .line 95655
    invoke-virtual {v5, v4}, Landroid/view/View;->setVisibility(I)V

    .line 95656
    iget-object v0, p0, Lcom/soula2/Conversation;->A3u:LX/0l3;

    invoke-virtual {v0, v4}, LX/0l3;->A02(I)V

    .line 95657
    invoke-virtual {p0, v4}, Lcom/soula2/Conversation;->A3C(Z)V

    :cond_0
    return-void

    .line 95658
    :cond_1
    invoke-virtual {v5}, Landroid/view/View;->getHeight()I

    move-result v0

    shr-int/lit8 v1, v0, 0x1

    goto :goto_0
.end method

.method public A38(LX/0md;I)V
    .locals 12

    .line 95659
    iget-object v0, p0, Lcom/soula2/Conversation;->A1c:Lcom/soula2/conversation/ConversationListView;

    invoke-virtual {v0}, Lcom/soula2/conversation/ConversationListView;->getConversationCursorAdapter()LX/0me;

    move-result-object v1

    .line 95660
    iget-object v2, p0, Lcom/soula2/Conversation;->A1x:LX/0mf;

    .line 95661
    invoke-virtual {v1}, LX/0me;->A02()I

    move-result v6

    .line 95662
    iget-wide v9, p0, Lcom/soula2/Conversation;->A03:J

    iget-object v0, p0, Lcom/soula2/Conversation;->A1c:Lcom/soula2/conversation/ConversationListView;

    .line 95663
    invoke-virtual {v0}, Landroid/widget/AdapterView;->getFirstVisiblePosition()I

    move-result v7

    .line 95664
    invoke-virtual {v1}, Landroid/widget/CursorAdapter;->getCount()I

    move-result v8

    .line 95665
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    .line 95666
    move-object v3, p1

    iget-object v0, p1, LX/0md;->A0z:LX/1HF;

    invoke-virtual {v4, v0}, Ljava/util/AbstractCollection;->add(Ljava/lang/Object;)Z

    const/4 v11, 0x1

    .line 95667
    move v5, p2

    invoke-virtual/range {v2 .. v11}, LX/0mf;->A0C(LX/0md;Ljava/util/List;IIIIJZ)V

    .line 95668
    return-void
.end method

.method public A39(LX/1HF;LX/4A4;Ljava/lang/String;Ljava/lang/String;[Landroid/graphics/Bitmap;I)V
    .locals 11

    const/4 v0, 0x3

    new-array v1, v0, [Ljava/lang/Object;

    const/4 v0, 0x0

    move-object v5, p1

    aput-object p1, v1, v0

    const/4 v0, 0x1

    move-object v7, p3

    aput-object p3, v1, v0

    const/4 v0, 0x2

    move-object v8, p4

    aput-object p4, v1, v0

    const-string v0, "conversation/initInlineVideoPlayback rowKey:%s fullUrl:%s canonicalUrl:%s"

    .line 95669
    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    .line 95670
    move-object v4, p0

    invoke-virtual {p0}, Lcom/soula2/Conversation;->AGh()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 95671
    iget-object v0, p0, Lcom/soula2/Conversation;->A42:LX/0kz;

    invoke-static {v0}, LX/009;->A05(Ljava/lang/Object;)V

    invoke-virtual {v0}, LX/0kz;->A03()V

    .line 95672
    :cond_0
    iget-object v1, p0, Lcom/soula2/Conversation;->A3x:LX/1mL;

    move-object v6, p2

    move-object/from16 v9, p5

    move/from16 v10, p6

    if-eqz v1, :cond_1

    iget-object v0, p0, Lcom/soula2/Conversation;->A3w:LX/2Fb;

    if-eqz v0, :cond_1

    .line 95673
    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move-object v6, v9

    move v7, v10

    invoke-interface/range {v1 .. v7}, LX/1mL;->A7y(LX/1HF;LX/4A4;Ljava/lang/String;Ljava/lang/String;[Landroid/graphics/Bitmap;I)V

    return-void

    .line 95674
    :cond_1
    iget-object v0, p0, Lcom/soula2/Conversation;->A1c:Lcom/soula2/conversation/ConversationListView;

    new-instance v2, LX/2Fb;

    invoke-direct {v2, p0, v0}, LX/2Fb;-><init>(Landroid/content/Context;Landroid/view/View;)V

    iput-object v2, p0, Lcom/soula2/Conversation;->A3w:LX/2Fb;

    .line 95675
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v0, 0x7f070412

    invoke-virtual {v1, v0}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    .line 95676
    iput v0, v2, LX/2Fb;->A01:I

    .line 95677
    iget-object v0, p0, Lcom/soula2/Conversation;->A1c:Lcom/soula2/conversation/ConversationListView;

    new-instance v2, LX/3xd;

    invoke-direct {v2, p0}, LX/3xd;-><init>(Lcom/soula2/Conversation;)V

    .line 95678
    iget-object v0, v0, Lcom/soula2/conversation/ConversationListView;->A03:LX/4As;

    .line 95679
    iget-object v1, v0, LX/4As;->A00:Ljava/util/Set;

    monitor-enter v1

    .line 95680
    :try_start_0
    invoke-interface {v1, v2}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 95681
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 95682
    new-instance v3, LX/3xc;

    invoke-direct {v3, p0}, LX/3xc;-><init>(Lcom/soula2/Conversation;)V

    .line 95683
    iget-object v0, p0, Lcom/soula2/Conversation;->A3w:LX/2Fb;

    .line 95684
    invoke-virtual {v0}, Landroid/view/View;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    move-result-object v0

    new-instance v2, LX/4Ni;

    invoke-direct/range {v2 .. v10}, LX/4Ni;-><init>(LX/3xc;Lcom/soula2/Conversation;LX/1HF;LX/4A4;Ljava/lang/String;Ljava/lang/String;[Landroid/graphics/Bitmap;I)V

    .line 95685
    invoke-virtual {v0, v2}, Landroid/view/ViewTreeObserver;->addOnGlobalLayoutListener(Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;)V

    .line 95686
    invoke-virtual {p0}, Landroid/app/Activity;->getWindow()Landroid/view/Window;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/view/ViewGroup;

    iget-object v2, p0, Lcom/soula2/Conversation;->A3w:LX/2Fb;

    const/4 v1, -0x1

    new-instance v0, Landroid/view/ViewGroup$LayoutParams;

    invoke-direct {v0, v1, v1}, Landroid/view/ViewGroup$LayoutParams;-><init>(II)V

    .line 95687
    invoke-virtual {v3, v2, v0}, Landroid/view/ViewGroup;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    return-void

    .line 95688
    :catchall_0
    :try_start_1
    move-exception v0

    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method public A3A(Z)V
    .locals 19

    .line 95689
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/soula2/Conversation;->A1K:LX/0wh;

    iget-object v2, v0, Lcom/soula2/Conversation;->A2b:LX/0mJ;

    const-class v1, Lcom/whatsapp/jid/UserJid;

    invoke-virtual {v2, v1}, LX/0mJ;->A0A(Ljava/lang/Class;)Lcom/whatsapp/jid/Jid;

    move-result-object v1

    check-cast v1, Lcom/whatsapp/jid/UserJid;

    invoke-virtual {v3, v1}, LX/0wh;->A0I(Lcom/whatsapp/jid/UserJid;)Z

    move-result v1

    if-eqz v1, :cond_1

    const/16 v1, 0x6a

    .line 95690
    :goto_0
    invoke-static {v0, v1}, LX/1ix;->A01(Landroid/app/Activity;I)V

    .line 95691
    :cond_0
    return-void

    .line 95692
    :cond_1
    iget-object v2, v0, Lcom/soula2/Conversation;->A42:LX/0kz;

    const/16 v5, 0x8

    const/4 v7, 0x1

    if-eqz v2, :cond_3

    .line 95693
    iget-object v1, v2, LX/0kz;->A0P:LX/0l2;

    if-eqz v1, :cond_2

    .line 95694
    iget-boolean v1, v0, Lcom/soula2/Conversation;->A4P:Z

    invoke-virtual {v2, v7, v1}, LX/0kz;->A0X(ZZ)V

    .line 95695
    iget-object v1, v0, Lcom/soula2/Conversation;->A0c:LX/059;

    if-eqz v1, :cond_0

    .line 95696
    iget-object v0, v0, Lcom/soula2/Conversation;->A09:Landroid/view/View;

    invoke-virtual {v0, v5}, Landroid/view/View;->setVisibility(I)V

    return-void

    .line 95697
    :cond_2
    iget-object v1, v2, LX/0kz;->A1G:LX/0l0;

    .line 95698
    iget-object v1, v1, LX/0l0;->A0B:Landroid/view/ViewGroup;

    invoke-virtual {v1}, Landroid/view/View;->getVisibility()I

    move-result v1

    if-nez v1, :cond_3

    .line 95699
    iget-object v2, v0, Lcom/soula2/Conversation;->A42:LX/0kz;

    .line 95700
    iget-object v1, v2, LX/0kz;->A0Q:Ljava/io/File;

    if-eqz v1, :cond_3

    .line 95701
    iget-boolean v1, v0, Lcom/soula2/Conversation;->A4P:Z

    invoke-virtual {v2, v1}, LX/0kz;->A0U(Z)V

    .line 95702
    iget-object v0, v0, Lcom/soula2/Conversation;->A42:LX/0kz;

    invoke-virtual {v0, v7}, LX/0kz;->A0R(Z)V

    return-void

    .line 95703
    :cond_3
    iget-object v1, v0, Lcom/soula2/Conversation;->A2v:Lcom/soula2/mentions/MentionableEntry;

    invoke-virtual {v1}, Lcom/soula2/mentions/MentionableEntry;->getStringText()Ljava/lang/String;

    move-result-object v3

    const-string/jumbo v2, "\u2800"

    .line 95704
    invoke-virtual {v3, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_4

    const-string v1, " "

    .line 95705
    invoke-virtual {v3, v2, v1}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v3

    .line 95706
    :cond_4
    invoke-virtual {v3}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v2

    .line 95707
    iget-boolean v1, v0, Lcom/soula2/Conversation;->A4Q:Z

    if-eqz v1, :cond_5

    .line 95708
    invoke-static {}, LX/1he;->A04()Z

    move-result v1

    if-eqz v1, :cond_5

    const-string/jumbo v3, "\u00ad"

    .line 95709
    invoke-virtual {v2, v3}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_5

    const-string v1, ""

    .line 95710
    invoke-virtual {v2, v3, v1}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v2

    .line 95711
    :cond_5
    iget-object v3, v0, LX/0kH;->A08:LX/01e;

    iget-object v1, v0, Lcom/soula2/Conversation;->A3I:LX/0pe;

    invoke-static {v3, v1, v2}, LX/1us;->A0C(LX/01e;LX/0pe;Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_6

    .line 95712
    iget-object v1, v0, LX/0kH;->A05:LX/0li;

    const v0, 0x7f12037f

    invoke-virtual {v1, v0, v7}, LX/0li;->A08(II)V

    return-void

    :cond_6
    const/high16 v4, 0x10000

    const/4 v3, 0x0

    if-eqz p1, :cond_f

    .line 95713
    invoke-static {v4, v2}, LX/1HG;->A04(ILjava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 95714
    :cond_7
    iget-object v1, v0, Lcom/soula2/Conversation;->A1y:LX/0l4;

    const/4 v10, 0x0

    invoke-virtual {v1, v10}, LX/0l4;->A08(Ljava/lang/String;)V

    .line 95715
    iget-object v1, v0, Lcom/soula2/Conversation;->A0P:Landroid/view/ViewGroup;

    invoke-virtual {v1, v5}, Landroid/view/View;->setVisibility(I)V

    .line 95716
    iget-object v1, v0, Lcom/soula2/Conversation;->A0M:Landroid/view/ViewGroup;

    invoke-virtual {v1, v5}, Landroid/view/View;->setVisibility(I)V

    .line 95717
    const v1, 0x7f080377

    invoke-virtual {v0, v1}, Lcom/soula2/Conversation;->A2x(I)V

    .line 95718
    iget-object v1, v0, Lcom/soula2/Conversation;->A0N:Landroid/view/ViewGroup;

    invoke-virtual {v1, v5}, Landroid/view/View;->setVisibility(I)V

    .line 95719
    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v1

    if-lez v1, :cond_a

    .line 95720
    sget-object v4, Lcom/soula2/Conversation;->A5G:Ljava/util/HashMap;

    iget-object v1, v0, Lcom/soula2/Conversation;->A2b:LX/0mJ;

    const-class v5, LX/0l8;

    invoke-virtual {v1, v5}, LX/0mJ;->A0A(Ljava/lang/Class;)Lcom/whatsapp/jid/Jid;

    move-result-object v1

    invoke-virtual {v4, v1}, Ljava/util/AbstractMap;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_e

    .line 95721
    invoke-virtual {v0}, Lcom/soula2/Conversation;->A2p()V

    .line 95722
    iget-object v1, v0, Lcom/soula2/Conversation;->A1y:LX/0l4;

    .line 95723
    iget-object v6, v1, LX/0l4;->A01:LX/0l5;

    .line 95724
    instance-of v1, v6, LX/2Ig;

    if-eqz v1, :cond_d

    .line 95725
    iget-object v4, v0, Lcom/soula2/Conversation;->A1w:LX/2D2;

    check-cast v6, LX/2Ig;

    iget-object v1, v0, Lcom/soula2/Conversation;->A2A:LX/1be;

    .line 95726
    invoke-virtual {v4, v6, v1}, LX/2D2;->A00(LX/2Ig;LX/1be;)LX/1b5;

    move-result-object v11

    .line 95727
    iget-object v1, v0, Lcom/soula2/Conversation;->A1y:LX/0l4;

    invoke-virtual {v1, v10}, LX/0l4;->A07(Ljava/lang/String;)V

    .line 95728
    invoke-virtual {v0, v7}, Lcom/soula2/Conversation;->A3B(Z)V

    .line 95729
    :goto_1
    iget-object v1, v0, Lcom/soula2/Conversation;->A1x:LX/0mf;

    .line 95730
    iget-object v9, v1, LX/0mf;->A08:LX/0md;

    .line 95731
    instance-of v1, v9, LX/1X6;

    if-eqz v1, :cond_8

    .line 95732
    check-cast v9, LX/1X6;

    .line 95733
    iget-object v8, v0, Lcom/soula2/Conversation;->A1E:LX/18V;

    const/16 v7, 0x24

    const/16 v1, 0x30

    .line 95734
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    .line 95735
    iget-object v4, v9, LX/1X6;->A06:Ljava/lang/String;

    .line 95736
    iget-object v1, v9, LX/1X6;->A01:Lcom/whatsapp/jid/UserJid;

    .line 95737
    invoke-virtual {v8, v1, v6, v4, v7}, LX/18V;->A03(Lcom/whatsapp/jid/UserJid;Ljava/lang/Integer;Ljava/lang/String;I)V

    .line 95738
    :cond_8
    iget-object v1, v0, Lcom/soula2/Conversation;->A1y:LX/0l4;

    .line 95739
    iget-object v8, v1, LX/0l4;->A01:LX/0l5;

    .line 95740
    instance-of v1, v8, LX/3be;

    if-eqz v1, :cond_c

    .line 95741
    check-cast v8, LX/3be;

    .line 95742
    iget-object v3, v8, LX/3be;->A00:LX/35D;

    .line 95743
    if-eqz v3, :cond_9

    .line 95744
    iget-object v4, v0, LX/0kB;->A03:LX/0oS;

    iget-object v1, v0, Lcom/soula2/Conversation;->A2b:LX/0mJ;

    .line 95745
    invoke-virtual {v1, v5}, LX/0mJ;->A0A(Ljava/lang/Class;)Lcom/whatsapp/jid/Jid;

    move-result-object v1

    invoke-static {v1}, Ljava/util/Collections;->singletonList(Ljava/lang/Object;)Ljava/util/List;

    move-result-object v10

    .line 95746
    iget-object v6, v3, LX/35D;->A00:Ljava/lang/String;

    .line 95747
    iget-object v7, v3, LX/35D;->A04:Ljava/lang/String;

    .line 95748
    iget-object v8, v3, LX/35D;->A02:Ljava/lang/String;

    .line 95749
    invoke-static {v2}, LX/1bk;->A04(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    .line 95750
    invoke-virtual {v3}, LX/35D;->A00()[B

    move-result-object v11

    iget-object v1, v0, Lcom/soula2/Conversation;->A1x:LX/0mf;

    .line 95751
    iget-object v5, v1, LX/0mf;->A08:LX/0md;

    .line 95752
    invoke-virtual/range {v4 .. v11}, LX/0oS;->A0R(LX/0md;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/List;[B)V

    .line 95753
    :cond_9
    :goto_2
    invoke-virtual {v0}, Lcom/soula2/Conversation;->A2l()V

    .line 95754
    invoke-virtual {v0}, Lcom/soula2/Conversation;->A2o()V

    .line 95755
    :cond_a
    iget-object v1, v0, LX/0kH;->A08:LX/01e;

    .line 95756
    invoke-virtual {v1}, LX/01e;->A0R()Landroid/view/inputmethod/InputMethodManager;

    move-result-object v1

    invoke-static {v1}, LX/009;->A05(Ljava/lang/Object;)V

    .line 95757
    invoke-virtual {v1}, Landroid/view/inputmethod/InputMethodManager;->isFullscreenMode()Z

    move-result v1

    if-nez v1, :cond_b

    iget-object v1, v0, Lcom/soula2/Conversation;->A2v:Lcom/soula2/mentions/MentionableEntry;

    .line 95758
    invoke-virtual {v1}, Landroid/view/View;->getHeight()I

    move-result v2

    iget-object v1, v0, Lcom/soula2/Conversation;->A1c:Lcom/soula2/conversation/ConversationListView;

    invoke-virtual {v1}, Landroid/view/View;->getHeight()I

    move-result v1

    add-int/2addr v2, v1

    shl-int/lit8 v2, v2, 0x2

    invoke-virtual {v0}, Landroid/app/Activity;->getWindow()Landroid/view/Window;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/View;->getHeight()I

    move-result v1

    if-ge v2, v1, :cond_0

    .line 95759
    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v1

    iget v2, v1, Landroid/content/res/Configuration;->orientation:I

    const/4 v1, 0x2

    if-ne v2, v1, :cond_0

    .line 95760
    :cond_b
    invoke-virtual {v0}, LX/0kB;->A2a()V

    .line 95761
    invoke-virtual {v0}, Lcom/soula2/Conversation;->A3E()Z

    return-void

    .line 95762
    :cond_c
    iget-object v7, v0, LX/0kB;->A03:LX/0oS;

    iget-object v1, v0, Lcom/soula2/Conversation;->A2b:LX/0mJ;

    .line 95763
    invoke-virtual {v1, v5}, LX/0mJ;->A0A(Ljava/lang/Class;)Lcom/whatsapp/jid/Jid;

    move-result-object v1

    invoke-static {v1}, Ljava/util/Collections;->singletonList(Ljava/lang/Object;)Ljava/util/List;

    move-result-object v14

    .line 95764
    invoke-static {v2}, LX/1bk;->A04(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v13

    iget-object v9, v0, Lcom/soula2/Conversation;->A1y:LX/0l4;

    iget-object v1, v0, Lcom/soula2/Conversation;->A1x:LX/0mf;

    .line 95765
    iget-object v12, v1, LX/0mf;->A08:LX/0md;

    .line 95766
    iget-object v1, v0, Lcom/soula2/Conversation;->A2v:Lcom/soula2/mentions/MentionableEntry;

    .line 95767
    invoke-virtual {v1}, Lcom/soula2/mentions/MentionableEntry;->getMentions()Ljava/util/List;

    move-result-object v15

    iget-boolean v2, v0, Lcom/soula2/Conversation;->A4P:Z

    iget-boolean v1, v0, Lcom/soula2/Conversation;->A4Q:Z

    .line 95768
    move/from16 v18, v3

    move/from16 v16, v2

    move/from16 v17, v1

    invoke-virtual/range {v7 .. v18}, LX/0oS;->A07(LX/0l5;LX/0l4;LX/1VG;LX/1b5;LX/0md;Ljava/lang/String;Ljava/util/List;Ljava/util/List;ZZZ)V

    goto :goto_2

    .line 95769
    :cond_d
    invoke-virtual {v0}, Lcom/soula2/Conversation;->A2q()V

    :cond_e
    move-object v11, v10

    goto/16 :goto_1

    .line 95770
    :cond_f
    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v1

    invoke-virtual {v2, v3, v1}, Ljava/lang/String;->codePointCount(II)I

    move-result v1

    if-le v1, v4, :cond_7

    const/16 v1, 0x11

    goto/16 :goto_0
.end method

.method public final A3B(Z)V
    .locals 4

    .line 95771
    iget-object v1, p0, Lcom/soula2/Conversation;->A2b:LX/0mJ;

    const-class v0, LX/0l8;

    invoke-virtual {v1, v0}, LX/0mJ;->A0A(Ljava/lang/Class;)Lcom/whatsapp/jid/Jid;

    move-result-object v1

    .line 95772
    sget-object v0, Lcom/soula2/Conversation;->A5G:Ljava/util/HashMap;

    invoke-virtual {v0, v1}, Ljava/util/AbstractMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 95773
    iget-object v0, p0, Lcom/soula2/Conversation;->A2E:LX/0rJ;

    invoke-static {v1}, Lcom/whatsapp/jid/UserJid;->of(Lcom/whatsapp/jid/Jid;)Lcom/whatsapp/jid/UserJid;

    move-result-object v3

    const/4 v2, 0x0

    if-eqz v3, :cond_0

    .line 95774
    invoke-virtual {v0, v3}, LX/0rJ;->A00(Lcom/whatsapp/jid/UserJid;)LX/204;

    move-result-object v0

    .line 95775
    iput-boolean v2, v0, LX/204;->A01:Z

    .line 95776
    :cond_0
    iget-object v1, p0, Lcom/soula2/Conversation;->A42:LX/0kz;

    if-eqz v1, :cond_1

    const/4 v0, 0x0

    .line 95777
    iput-object v0, v1, LX/0kz;->A0H:LX/2Ig;

    .line 95778
    iput-object v0, v1, LX/0kz;->A0I:LX/1be;

    .line 95779
    :cond_1
    iget-object v0, p0, Lcom/soula2/Conversation;->A2E:LX/0rJ;

    if-eqz v3, :cond_2

    .line 95780
    invoke-virtual {v0, v3}, LX/0rJ;->A00(Lcom/whatsapp/jid/UserJid;)LX/204;

    move-result-object v0

    .line 95781
    iput-boolean v2, v0, LX/204;->A02:Z

    .line 95782
    :cond_2
    if-eqz p1, :cond_4

    .line 95783
    iget-object v0, p0, Lcom/soula2/Conversation;->A2C:LX/20a;

    .line 95784
    iget-object v1, v0, LX/20a;->A0B:LX/0l8;

    if-eqz v1, :cond_3

    .line 95785
    iget-object v0, v0, LX/20a;->A0R:LX/18A;

    invoke-virtual {v1}, Lcom/whatsapp/jid/Jid;->getRawString()Ljava/lang/String;

    move-result-object v1

    .line 95786
    iget-object v0, v0, LX/18A;->A02:Ljava/util/Map;

    invoke-interface {v0, v1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 95787
    :cond_3
    invoke-virtual {p0}, Lcom/soula2/Conversation;->A2o()V

    :cond_4
    return-void
.end method

.method public final A3C(Z)V
    .locals 4

    .line 95788
    iget-object v0, p0, Lcom/soula2/Conversation;->A08:Landroid/view/View;

    if-eqz v0, :cond_1

    .line 95789
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v0, 0x7f07005f

    invoke-virtual {v1, v0}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v3

    .line 95790
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v0, 0x7f07005e

    invoke-virtual {v1, v0}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    .line 95791
    iget-object v1, p0, Lcom/soula2/Conversation;->A08:Landroid/view/View;

    const/4 v0, 0x0

    if-nez p1, :cond_0

    move v0, v2

    const/4 v2, 0x0

    :cond_0
    invoke-virtual {v1, v3, v0, v3, v2}, Landroid/view/View;->setPadding(IIII)V

    :cond_1
    return-void
.end method

.method public final A3D(Z)V
    .locals 5

    if-nez p1, :cond_0

    .line 95792
    iget-object v0, p0, Lcom/soula2/Conversation;->A3i:Lcom/soula2/tosgating/viewmodel/ToSGatingViewModel;

    iget-boolean v0, v0, Lcom/soula2/tosgating/viewmodel/ToSGatingViewModel;->A00:Z

    if-eqz v0, :cond_0

    return-void

    .line 95793
    :cond_0
    iget-object v0, p0, Lcom/soula2/Conversation;->A0a:LX/01b;

    if-eqz v0, :cond_1

    .line 95794
    invoke-virtual {v0}, Landroid/app/Dialog;->dismiss()V

    .line 95795
    :cond_1
    iget-object v2, p0, Lcom/soula2/Conversation;->A3i:Lcom/soula2/tosgating/viewmodel/ToSGatingViewModel;

    iget-object v0, p0, Lcom/soula2/Conversation;->A2r:LX/0l8;

    invoke-static {v0}, Lcom/whatsapp/jid/UserJid;->of(Lcom/whatsapp/jid/Jid;)Lcom/whatsapp/jid/UserJid;

    move-result-object v1

    .line 95796
    iget-object v0, v2, Lcom/soula2/tosgating/viewmodel/ToSGatingViewModel;->A04:LX/10g;

    invoke-virtual {v0, v1}, LX/10g;->A01(LX/0l8;)LX/0md;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 95797
    iget-object v0, v0, LX/0md;->A0z:LX/1HF;

    iget-boolean v0, v0, LX/1HF;->A02:Z

    const/4 v4, 0x1

    if-eqz v0, :cond_3

    :cond_2
    const/4 v4, 0x0

    .line 95798
    :cond_3
    invoke-virtual {p0}, Landroid/app/Activity;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v1

    const v0, 0x7f0d01f0

    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    .line 95799
    const v0, 0x7f121879

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(I)V

    .line 95800
    new-instance v3, LX/2Hs;

    invoke-direct {v3, p0}, LX/2Hs;-><init>(Landroid/content/Context;)V

    .line 95801
    iget-object v0, v3, LX/01Z;->A01:LX/0Nu;

    iput-object v1, v0, LX/0Nu;->A0B:Landroid/view/View;

    .line 95802
    iget-object v0, p0, Lcom/soula2/Conversation;->A3i:Lcom/soula2/tosgating/viewmodel/ToSGatingViewModel;

    .line 95803
    iget-object v0, v0, Lcom/soula2/tosgating/viewmodel/ToSGatingViewModel;->A02:LX/0nN;

    invoke-virtual {v0}, LX/0nN;->A0G()Z

    move-result v0

    if-eqz v0, :cond_6

    const v0, 0x7f121878

    if-eqz v4, :cond_4

    .line 95804
    const v0, 0x7f121872

    .line 95805
    :cond_4
    :goto_0
    invoke-virtual {v3, v0}, LX/01Z;->A01(I)V

    .line 95806
    iget-object v0, p0, LX/0kF;->A01:LX/0nN;

    invoke-virtual {v0}, LX/0nN;->A0G()Z

    move-result v0

    if-eqz v0, :cond_5

    .line 95807
    const v0, 0x7f120fbb

    invoke-virtual {v3, v0, v2}, LX/01Z;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)LX/01Z;

    .line 95808
    :goto_1
    invoke-virtual {v3}, LX/01Z;->create()LX/01b;

    move-result-object v0

    iput-object v0, p0, Lcom/soula2/Conversation;->A0a:LX/01b;

    .line 95809
    invoke-virtual {v0}, Landroid/app/Dialog;->show()V

    .line 95810
    iget-object v1, p0, Lcom/soula2/Conversation;->A3i:Lcom/soula2/tosgating/viewmodel/ToSGatingViewModel;

    const/4 v0, 0x1

    iput-boolean v0, v1, Lcom/soula2/tosgating/viewmodel/ToSGatingViewModel;->A00:Z

    return-void

    .line 95811
    :cond_5
    const v2, 0x7f121876

    const/4 v1, 0x0

    new-instance v0, Lcom/facebook/redex/IDxCListenerShape129S0100000_2_I0;

    invoke-direct {v0, p0, v1}, Lcom/facebook/redex/IDxCListenerShape129S0100000_2_I0;-><init>(Ljava/lang/Object;I)V

    .line 95812
    invoke-virtual {v3, v2, v0}, LX/01Z;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)LX/01Z;

    const v2, 0x7f121875

    const/4 v1, 0x1

    new-instance v0, Lcom/facebook/redex/IDxCListenerShape129S0100000_2_I0;

    invoke-direct {v0, p0, v1}, Lcom/facebook/redex/IDxCListenerShape129S0100000_2_I0;-><init>(Ljava/lang/Object;I)V

    .line 95813
    invoke-virtual {v3, v2, v0}, LX/01Z;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)LX/01Z;

    goto :goto_1

    .line 95814
    :cond_6
    const v0, 0x7f121877

    if-eqz v4, :cond_4

    .line 95815
    const v0, 0x7f121871

    goto :goto_0
.end method

.method public final A3E()Z
    .locals 2

    .line 95816
    iget-object v0, p0, Lcom/soula2/Conversation;->A3G:LX/2FY;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, LX/2FY;->A03(Z)V

    .line 95817
    iget-object v0, p0, Lcom/soula2/Conversation;->A2j:LX/0mU;

    invoke-virtual {v0}, Landroid/widget/PopupWindow;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 95818
    iget-object v0, p0, Lcom/soula2/Conversation;->A2j:LX/0mU;

    invoke-virtual {v0}, Landroid/widget/PopupWindow;->dismiss()V

    const/4 v0, 0x1

    return v0

    :cond_0
    return v1
.end method

.method public final A3F(I)Z
    .locals 3

    .line 95819
    iget-object v1, p0, LX/0kH;->A0C:LX/0kj;

    const/16 v0, 0x2e7

    invoke-virtual {v1, v0}, LX/0kj;->A07(I)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v2, p0, Lcom/soula2/Conversation;->A3C:LX/150;

    iget-object v1, p0, Lcom/soula2/Conversation;->A2b:LX/0mJ;

    const-class v0, Lcom/whatsapp/jid/UserJid;

    .line 95820
    invoke-virtual {v1, v0}, LX/0mJ;->A0A(Ljava/lang/Class;)Lcom/whatsapp/jid/Jid;

    move-result-object v0

    check-cast v0, Lcom/whatsapp/jid/UserJid;

    .line 95821
    invoke-virtual {v2, p0, v0, p1}, LX/150;->A0a(Landroid/content/Context;Lcom/whatsapp/jid/UserJid;I)Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x2

    if-eq p1, v0, :cond_0

    const/4 v0, 0x4

    if-ne p1, v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    return v0

    :cond_1
    const/4 v0, 0x0

    return v0
.end method

.method public A3G(LX/0l8;I)Z
    .locals 5

    .line 95822
    const v1, 0x7f0a0a42

    const/4 v4, 0x0

    const/4 v3, 0x1

    if-eq p2, v1, :cond_7

    const v0, 0x7f0a0a43

    if-eq p2, v0, :cond_7

    .line 95823
    const v0, 0x7f0a0aa9

    if-ne p2, v0, :cond_0

    if-eqz p1, :cond_4

    .line 95824
    iget-object v0, p0, LX/0kB;->A07:LX/0nL;

    invoke-virtual {v0, p1}, LX/0nL;->A0B(LX/0l8;)LX/0mJ;

    move-result-object v4

    .line 95825
    const v0, 0x7f0a12de

    .line 95826
    invoke-virtual {p0, v0}, LX/00i;->findViewById(I)Landroid/view/View;

    move-result-object v2

    new-instance v1, LX/2Fu;

    invoke-direct {v1, p0}, LX/2Fu;-><init>(Landroid/content/Context;)V

    const v0, 0x7f1218ea

    .line 95827
    invoke-virtual {v1, v0}, LX/2Fu;->A00(I)Ljava/lang/String;

    move-result-object v0

    .line 95828
    invoke-static {p0, v2, v0}, LX/1jw;->A05(Landroid/app/Activity;Landroid/view/View;Ljava/lang/String;)Landroid/os/Bundle;

    .line 95829
    new-instance v1, LX/0kk;

    invoke-direct {v1}, LX/0kk;-><init>()V

    const/4 v0, 0x3

    .line 95830
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v1, p0, v4, v0}, LX/0kk;->A0l(Landroid/content/Context;LX/0mJ;Ljava/lang/Integer;)Landroid/content/Intent;

    .line 95831
    :goto_0
    invoke-virtual {p0}, Landroid/app/Activity;->finish()V

    .line 95832
    return v3

    .line 95833
    :cond_0
    const v0, 0x7f0a0a75

    if-ne p2, v0, :cond_2

    if-eqz p1, :cond_1

    .line 95834
    iget-object v0, p0, LX/0kB;->A07:LX/0nL;

    invoke-virtual {v0, p1}, LX/0nL;->A0B(LX/0l8;)LX/0mJ;

    move-result-object v1

    .line 95835
    iget-object v2, p0, LX/0kF;->A00:LX/154;

    iget-object v0, p0, Lcom/soula2/Conversation;->A4x:LX/0kk;

    .line 95836
    invoke-virtual {v0, p0, v1}, LX/0kk;->A0k(Landroid/content/Context;LX/0mJ;)Landroid/content/Intent;

    move-result-object v1

    const-string v0, "Conversation:messageContact"

    .line 95837
    invoke-virtual {v2, p0, v1, v0}, LX/154;->A08(Landroid/content/Context;Landroid/content/Intent;Ljava/lang/String;)V

    goto :goto_0

    .line 95838
    :cond_1
    const-string v1, "conversation/message-contact/error no-resource"

    goto :goto_1

    .line 95839
    :cond_2
    const v0, 0x7f0a0aab

    const/16 v2, 0x8

    const-string v1, "conversation/call-contact/error no-resource"

    if-ne p2, v0, :cond_3

    if-eqz p1, :cond_5

    .line 95840
    iget-object v0, p0, LX/0kB;->A07:LX/0nL;

    invoke-virtual {v0, p1}, LX/0nL;->A0B(LX/0l8;)LX/0mJ;

    move-result-object v1

    .line 95841
    iget-object v0, p0, Lcom/soula2/Conversation;->A46:LX/0wm;

    invoke-virtual {v0, p0, v1, v2, v4}, LX/0wm;->A01(Landroid/content/Context;LX/0mJ;IZ)I

    return v3

    .line 95842
    :cond_3
    const v0, 0x7f0a0aa8

    if-ne p2, v0, :cond_6

    if-eqz p1, :cond_5

    .line 95843
    iget-object v0, p0, LX/0kB;->A07:LX/0nL;

    invoke-virtual {v0, p1}, LX/0nL;->A0B(LX/0l8;)LX/0mJ;

    move-result-object v1

    .line 95844
    iget-object v0, p0, Lcom/soula2/Conversation;->A46:LX/0wm;

    invoke-virtual {v0, p0, v1, v2, v3}, LX/0wm;->A01(Landroid/content/Context;LX/0mJ;IZ)I

    return v3

    .line 95845
    :cond_4
    const-string v1, "conversation/view-business-profile/error no-resource"

    .line 95846
    :cond_5
    :goto_1
    invoke-static {v1}, Lcom/whatsapp/util/Log;->e(Ljava/lang/String;)V

    return v3

    .line 95847
    :cond_6
    return v4

    :cond_7
    if-eqz p1, :cond_9

    .line 95848
    iget-object v0, p0, LX/0kB;->A07:LX/0nL;

    invoke-virtual {v0, p1}, LX/0nL;->A0A(LX/0l8;)LX/0mJ;

    move-result-object v0

    if-ne p2, v1, :cond_8

    const/4 v4, 0x1

    .line 95849
    :cond_8
    invoke-virtual {p0, v0, p1, v4}, Lcom/soula2/Conversation;->A34(LX/0mJ;LX/0l8;Z)V

    return v3

    :cond_9
    const-string v0, "conversation/add-contact-failed"

    .line 95850
    invoke-static {v0}, Lcom/whatsapp/util/Log;->w(Ljava/lang/String;)V

    .line 95851
    iget-object v1, p0, LX/0kH;->A05:LX/0li;

    const v0, 0x7f120a32

    invoke-virtual {v1, v0, v4}, LX/0li;->A08(II)V

    return v3
.end method

.method public A4W()V
    .locals 2

    .line 95852
    iget-object v1, p0, Lcom/soula2/Conversation;->A1c:Lcom/soula2/conversation/ConversationListView;

    const/4 v0, 0x1

    .line 95853
    iput-boolean v0, v1, Lcom/soula2/conversation/ConversationListView;->A0F:Z

    .line 95854
    return-void
.end method

.method public synthetic A4X(I)V
    .locals 0

    return-void
.end method

.method public A5v(LX/1Wm;IZZ)Z
    .locals 10

    .line 95855
    move-object v3, p0

    iget-object v0, p0, LX/0kH;->A0C:LX/0kj;

    invoke-static {v0}, LX/1Mc;->A01(LX/0kj;)Z

    move-result v9

    .line 95856
    iget-object v0, p0, Lcom/soula2/Conversation;->A1c:Lcom/soula2/conversation/ConversationListView;

    .line 95857
    invoke-virtual {v0}, Lcom/soula2/conversation/ConversationListView;->getConversationCursorAdapter()LX/0me;

    move-result-object v0

    .line 95858
    move-object v7, p1

    invoke-static {v0, p1}, LX/31E;->A00(LX/0me;LX/0md;)LX/0md;

    move-result-object v6

    const/4 v2, 0x0

    const/4 v8, 0x1

    if-eqz v6, :cond_5

    .line 95859
    iget-object v0, p0, LX/0kH;->A0C:LX/0kj;

    .line 95860
    invoke-static {v0, v6, p1, p3}, LX/31E;->A01(LX/0kj;LX/0md;LX/1Wm;Z)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 95861
    iput-boolean v8, p0, Lcom/soula2/Conversation;->A4N:Z

    .line 95862
    move-object v5, v6

    check-cast v5, LX/1Wm;

    .line 95863
    iget-object v1, p0, LX/0kB;->A0H:LX/18q;

    iget-boolean v0, p0, Lcom/soula2/Conversation;->A4W:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    if-eqz p4, :cond_1

    :cond_0
    const/4 v0, 0x1

    .line 95864
    :cond_1
    invoke-virtual {v1, p0, v0, v8}, LX/18q;->A01(Landroid/app/Activity;ZZ)LX/1gq;

    move-result-object v4

    .line 95865
    iput-object v5, v4, LX/1gq;->A0O:LX/1Wm;

    .line 95866
    iput v8, v4, LX/1gq;->A0A:I

    .line 95867
    iget v1, v5, LX/0md;->A0C:I

    .line 95868
    const/16 v0, 0x9

    if-eq v1, v0, :cond_2

    .line 95869
    const/16 v0, 0xa

    if-ne v1, v0, :cond_3

    :cond_2
    const/4 v2, 0x1

    .line 95870
    :cond_3
    iput-boolean v2, v4, LX/1gq;->A0T:Z

    .line 95871
    new-instance v0, LX/4Y7;

    invoke-direct {v0, p0, v4, v5}, LX/4Y7;-><init>(Lcom/soula2/Conversation;LX/1gq;LX/1Wm;)V

    .line 95872
    iput-object v0, v4, LX/1gq;->A0J:LX/4t9;

    .line 95873
    iget-object v0, p0, LX/0kH;->A05:LX/0li;

    new-instance v2, Lcom/facebook/redex/RunnableRunnableShape0S0510000_I0;

    invoke-direct/range {v2 .. v9}, Lcom/facebook/redex/RunnableRunnableShape0S0510000_I0;-><init>(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;IZ)V

    invoke-static {p0, v0, v2, p2}, LX/31F;->A01(Landroid/content/Context;LX/0li;Ljava/lang/Runnable;I)V

    return v8

    .line 95874
    :cond_4
    iget-object v0, p0, LX/0kH;->A0C:LX/0kj;

    .line 95875
    invoke-static {v0, v6, p1, p3}, LX/31E;->A01(LX/0kj;LX/0md;LX/1Wm;Z)Z

    move-result v0

    if-nez v0, :cond_6

    :cond_5
    if-eqz v9, :cond_6

    .line 95876
    iget-object v1, p0, LX/0kH;->A06:LX/0nB;

    sget-object v0, LX/0nC;->A24:LX/0pR;

    .line 95877
    invoke-virtual {v1, v0}, LX/0nC;->A02(LX/0pR;)I

    move-result v0

    int-to-long v0, v0

    .line 95878
    invoke-virtual {p0, p1, v0, v1, v8}, Lcom/soula2/Conversation;->AeL(LX/1Wm;JZ)V

    .line 95879
    :cond_6
    invoke-static {p0, p2}, LX/31F;->A00(Landroid/content/Context;I)V

    .line 95880
    iput-boolean v2, p0, Lcom/soula2/Conversation;->A4O:Z

    const/4 v0, 0x0

    .line 95881
    iput-object v0, p0, Lcom/soula2/Conversation;->A3L:LX/0md;

    .line 95882
    iput-boolean v2, p0, Lcom/soula2/Conversation;->A4N:Z

    return v2
.end method

.method public A9o()LX/1Hc;
    .locals 1

    .line 95883
    iget-object v0, p0, LX/0kB;->A0F:LX/1Mq;

    invoke-virtual {v0, p0}, LX/1Mq;->A01(Landroid/content/Context;)LX/1Hc;

    move-result-object v0

    return-object v0
.end method

.method public AEG()LX/0md;
    .locals 1

    .line 95884
    iget-object v0, p0, Lcom/soula2/Conversation;->A1x:LX/0mf;

    .line 95885
    iget-object v0, v0, LX/0mf;->A08:LX/0md;

    .line 95886
    return-object v0
.end method

.method public AEc()LX/00E;
    .locals 1

    .line 95887
    sget-object v0, LX/01l;->A01:LX/00E;

    return-object v0
.end method

.method public AGh()Z
    .locals 2

    .line 95888
    iget-object v0, p0, Lcom/soula2/Conversation;->A42:LX/0kz;

    if-eqz v0, :cond_0

    .line 95889
    iget-object v1, v0, LX/0kz;->A0P:LX/0l2;

    const/4 v0, 0x1

    if-nez v1, :cond_1

    .line 95890
    :cond_0
    const/4 v0, 0x0

    :cond_1
    return v0
.end method

.method public AKV(J)V
    .locals 3

    .line 95891
    const/4 v2, 0x0

    .line 95892
    iget-object v0, p0, LX/0kB;->A07:LX/0nL;

    invoke-virtual {v0, p1, p2}, LX/0nL;->A06(J)LX/0mJ;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 95893
    const-class v0, LX/0l8;

    .line 95894
    invoke-virtual {v1, v0}, LX/0mJ;->A0A(Ljava/lang/Class;)Lcom/whatsapp/jid/Jid;

    move-result-object v0

    invoke-static {v0}, LX/009;->A05(Ljava/lang/Object;)V

    check-cast v0, LX/0l8;

    .line 95895
    invoke-virtual {p0, v1, v0, v2}, Lcom/soula2/Conversation;->A34(LX/0mJ;LX/0l8;Z)V

    .line 95896
    :cond_0
    return-void
.end method

.method public AKw()V
    .locals 2

    .line 95897
    iget-object v1, p0, Lcom/soula2/Conversation;->A2b:LX/0mJ;

    const/4 v0, 0x0

    invoke-virtual {p0, v1, v0}, Lcom/soula2/Conversation;->AY9(LX/0mJ;Z)V

    return-void
.end method

.method public ANE(J)V
    .locals 3

    .line 95898
    const/4 v2, 0x1

    .line 95899
    iget-object v0, p0, LX/0kB;->A07:LX/0nL;

    invoke-virtual {v0, p1, p2}, LX/0nL;->A06(J)LX/0mJ;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 95900
    const-class v0, LX/0l8;

    .line 95901
    invoke-virtual {v1, v0}, LX/0mJ;->A0A(Ljava/lang/Class;)Lcom/whatsapp/jid/Jid;

    move-result-object v0

    invoke-static {v0}, LX/009;->A05(Ljava/lang/Object;)V

    check-cast v0, LX/0l8;

    .line 95902
    invoke-virtual {p0, v1, v0, v2}, Lcom/soula2/Conversation;->A34(LX/0mJ;LX/0l8;Z)V

    .line 95903
    :cond_0
    return-void
.end method

.method public AOE(LX/1KO;)V
    .locals 2

    .line 95904
    iget-object v1, p0, Lcom/soula2/Conversation;->A4u:LX/4uu;

    iget-object v0, p1, LX/1KO;->A00:[I

    invoke-interface {v1, v0}, LX/4uu;->AOD([I)V

    return-void
.end method

.method public AOt(Lcom/whatsapp/jid/UserJid;I)V
    .locals 1

    const/4 v0, 0x0

    .line 95905
    invoke-virtual {p0, v0}, Lcom/soula2/Conversation;->A32(LX/0l5;)V

    return-void
.end method

.method public AOu(Lcom/whatsapp/jid/UserJid;ZZ)V
    .locals 8

    .line 95906
    iget-object v3, p0, Lcom/soula2/Conversation;->A1y:LX/0l4;

    .line 95907
    iget-object v2, p0, Lcom/soula2/Conversation;->A1H:LX/1lD;

    if-nez v2, :cond_0

    .line 95908
    iget-object v1, p0, Lcom/soula2/Conversation;->A1G:LX/15p;

    iget-object v0, p0, Lcom/soula2/Conversation;->A3K:LX/2yh;

    new-instance v2, LX/1lD;

    invoke-direct {v2, v1, v0}, LX/1lD;-><init>(LX/15p;LX/2yh;)V

    iput-object v2, p0, Lcom/soula2/Conversation;->A1H:LX/1lD;

    .line 95909
    :cond_0
    iget-object v0, v3, LX/0l4;->A0J:LX/0u7;

    invoke-virtual {v0, p1}, LX/0u7;->A06(Lcom/whatsapp/jid/UserJid;)LX/1VX;

    move-result-object v4

    if-eqz v4, :cond_1

    .line 95910
    new-instance v6, LX/4Wu;

    invoke-direct {v6, v3, p1}, LX/4Wu;-><init>(LX/0l4;Lcom/whatsapp/jid/UserJid;)V

    const/4 v7, 0x2

    const/4 v3, 0x0

    move-object v5, v3

    invoke-virtual/range {v2 .. v7}, LX/1lD;->A02(Landroid/widget/ImageView;LX/1VX;LX/4sd;LX/28U;I)V

    .line 95911
    return-void

    .line 95912
    :cond_1
    iget-object v2, v3, LX/0l4;->A0T:LX/0lO;

    const/16 v1, 0xb

    new-instance v0, Lcom/facebook/redex/RunnableRunnableShape5S0200000_I0_3;

    invoke-direct {v0, v3, v1, p1}, Lcom/facebook/redex/RunnableRunnableShape5S0200000_I0_3;-><init>(Ljava/lang/Object;ILjava/lang/Object;)V

    invoke-interface {v2, v0}, LX/0lO;->AZm(Ljava/lang/Runnable;)V

    return-void
.end method

.method public APc()V
    .locals 0

    return-void
.end method

.method public APd()V
    .locals 3

    .line 95913
    iget-object v2, p0, LX/0kJ;->A05:LX/0lO;

    const/16 v1, 0x1d

    new-instance v0, Lcom/facebook/redex/RunnableRunnableShape1S0100000_I0;

    invoke-direct {v0, p0, v1}, Lcom/facebook/redex/RunnableRunnableShape1S0100000_I0;-><init>(Lcom/soula2/Conversation;I)V

    invoke-interface {v2, v0}, LX/0lO;->AZm(Ljava/lang/Runnable;)V

    return-void
.end method

.method public APg(LX/35A;)V
    .locals 4

    .line 95914
    iget-object v0, p0, LX/0kB;->A0L:LX/0o2;

    invoke-virtual {v0}, LX/0o2;->A07()Z

    move-result v0

    if-nez v0, :cond_1

    .line 95915
    iput-object p1, p0, Lcom/soula2/Conversation;->A2m:LX/35A;

    .line 95916
    const v3, 0x7f121347

    .line 95917
    sget v2, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v0, 0x1e

    const v1, 0x7f121348

    if-ge v2, v0, :cond_0

    .line 95918
    const v1, 0x7f121346

    .line 95919
    :cond_0
    const/16 v0, 0x32b

    .line 95920
    invoke-static {p0, v3, v1, v0}, Lcom/soula2/RequestPermissionActivity;->A0L(Landroid/app/Activity;III)V

    return-void

    .line 95921
    :cond_1
    invoke-virtual {p0, p1}, Lcom/soula2/Conversation;->A35(LX/35A;)V

    return-void
.end method

.method public ASQ(Lcom/soula2/picker/search/PickerSearchDialogFragment;)V
    .locals 1

    .line 95922
    iget-object v0, p0, Lcom/soula2/Conversation;->A3G:LX/2FY;

    invoke-virtual {v0, p1}, LX/2FY;->A01(Lcom/soula2/picker/search/PickerSearchDialogFragment;)V

    .line 95923
    invoke-virtual {p0}, Lcom/soula2/Conversation;->AGh()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 95924
    iget-object v0, p0, Lcom/soula2/Conversation;->A42:LX/0kz;

    invoke-static {v0}, LX/009;->A05(Ljava/lang/Object;)V

    invoke-virtual {v0}, LX/0kz;->A03()V

    :cond_0
    return-void
.end method

.method public ATQ()V
    .locals 1

    .line 95925
    iget-object v0, p0, Lcom/soula2/Conversation;->A1a:LX/1tq;

    invoke-virtual {v0}, LX/1tq;->A01()V

    return-void
.end method

.method public AVu(LX/059;)V
    .locals 2

    .line 95926
    invoke-super {p0, p1}, LX/0kB;->AVu(LX/059;)V

    .line 95927
    iget-object v0, p0, Lcom/soula2/Conversation;->A4A:LX/01K;

    invoke-interface {v0}, LX/01K;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/0xM;

    const-class v0, LX/2Dm;

    invoke-virtual {v1, v0}, LX/0xM;->A00(Ljava/lang/Class;)LX/0zC;

    move-result-object v0

    check-cast v0, LX/2Dm;

    .line 95928
    const/4 v1, 0x0

    .line 95929
    iget-object v0, v0, LX/2Dm;->A00:LX/1w2;

    invoke-virtual {v0, v1}, LX/1w2;->setShouldHideBanner(Z)V

    .line 95930
    return-void
.end method

.method public AVv(LX/059;)V
    .locals 2

    .line 95931
    invoke-super {p0, p1}, LX/0kB;->AVv(LX/059;)V

    .line 95932
    iget-object v0, p0, Lcom/soula2/Conversation;->A4A:LX/01K;

    invoke-interface {v0}, LX/01K;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/0xM;

    const-class v0, LX/2Dm;

    invoke-virtual {v1, v0}, LX/0xM;->A00(Ljava/lang/Class;)LX/0zC;

    move-result-object v0

    check-cast v0, LX/2Dm;

    .line 95933
    const/4 v1, 0x1

    .line 95934
    iget-object v0, v0, LX/2Dm;->A00:LX/1w2;

    invoke-virtual {v0, v1}, LX/1w2;->setShouldHideBanner(Z)V

    .line 95935
    return-void
.end method

.method public AW8()V
    .locals 1

    .line 95936
    iget-object v0, p0, Lcom/soula2/Conversation;->A1a:LX/1tq;

    invoke-virtual {v0}, LX/1tq;->A00()V

    return-void
.end method

.method public AWv()V
    .locals 2

    .line 95937
    iget-object v1, p0, Lcom/soula2/Conversation;->A2b:LX/0mJ;

    const/4 v0, 0x1

    invoke-virtual {p0, v1, v0}, Lcom/soula2/Conversation;->AY9(LX/0mJ;Z)V

    return-void
.end method

.method public AY9(LX/0mJ;Z)V
    .locals 9

    .line 95938
    move-object v4, p0

    iget-object v2, p0, LX/0kH;->A09:LX/0kh;

    .line 95939
    iget-boolean v1, p0, Lcom/soula2/Conversation;->A4S:Z

    const/16 v0, 0x8

    if-eqz v1, :cond_0

    const/16 v0, 0x19

    :cond_0
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    .line 95940
    iget-object v2, v2, LX/0kh;->A00:Landroid/content/SharedPreferences;

    const-string v1, "call_confirmation_dialog_count"

    const/4 v0, 0x0

    invoke-interface {v2, v1, v0}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v1

    .line 95941
    const/4 v0, 0x5

    move v8, p2

    if-lt v1, v0, :cond_2

    .line 95942
    invoke-virtual {p1}, LX/0mJ;->A0J()Z

    move-result v0

    if-nez v0, :cond_2

    .line 95943
    invoke-virtual {p1}, LX/0mJ;->A0J()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 95944
    const-class v0, LX/0nO;

    .line 95945
    invoke-virtual {p1, v0}, LX/0mJ;->A0A(Ljava/lang/Class;)Lcom/whatsapp/jid/Jid;

    move-result-object v5

    invoke-static {v5}, LX/009;->A05(Ljava/lang/Object;)V

    check-cast v5, Lcom/whatsapp/jid/GroupJid;

    .line 95946
    iget-object v3, p0, Lcom/soula2/Conversation;->A46:LX/0wm;

    iget-object v2, p0, LX/0kB;->A0N:LX/0nR;

    iget-object v1, p0, LX/0kF;->A01:LX/0nN;

    iget-object v0, p0, LX/0kB;->A07:LX/0nL;

    .line 95947
    invoke-static {v1, v0, v2, p1}, LX/1Px;->A0F(LX/0nN;LX/0nL;LX/0nR;LX/0mJ;)Ljava/util/List;

    move-result-object v6

    const/16 v7, 0x19

    .line 95948
    invoke-virtual/range {v3 .. v8}, LX/0wm;->A03(Landroid/content/Context;Lcom/whatsapp/jid/GroupJid;Ljava/util/List;IZ)I

    .line 95949
    return-void

    .line 95950
    :cond_1
    iget-object v1, p0, Lcom/soula2/Conversation;->A46:LX/0wm;

    const/16 v0, 0x8

    invoke-virtual {v1, p0, p1, v0, p2}, LX/0wm;->A01(Landroid/content/Context;LX/0mJ;IZ)I

    return-void

    .line 95951
    :cond_2
    invoke-static {p0, p1, v3, p2}, LX/1Px;->A0J(LX/0kH;LX/0mJ;Ljava/lang/Integer;Z)V

    return-void
.end method

.method public AZR()V
    .locals 3

    .line 95952
    iget-object v0, p0, Lcom/soula2/Conversation;->A0M:Landroid/view/ViewGroup;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 95953
    iget-object v0, p0, Lcom/soula2/Conversation;->A0N:Landroid/view/ViewGroup;

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 95954
    const v0, 0x7f080377

    invoke-virtual {p0, v0}, Lcom/soula2/Conversation;->A2x(I)V

    .line 95955
    iget-object v0, p0, Lcom/soula2/Conversation;->A1e:LX/194;

    .line 95956
    iget-object v2, v0, LX/194;->A00:Ljava/util/HashMap;

    .line 95957
    iget-object v1, p0, Lcom/soula2/Conversation;->A2b:LX/0mJ;

    const-class v0, LX/0l8;

    .line 95958
    invoke-virtual {v1, v0}, LX/0mJ;->A0A(Ljava/lang/Class;)Lcom/whatsapp/jid/Jid;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/util/AbstractMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 95959
    iget-object v1, p0, Lcom/soula2/Conversation;->A1x:LX/0mf;

    const/4 v0, 0x0

    invoke-virtual {v1, v0}, LX/0mf;->A0B(LX/0md;)V

    .line 95960
    invoke-virtual {p0}, Lcom/soula2/Conversation;->A2l()V

    return-void
.end method

.method public AcS(Landroidx/fragment/app/DialogFragment;)V
    .locals 0

    .line 95961
    invoke-virtual {p0, p1}, LX/0kH;->AcU(Landroidx/fragment/app/DialogFragment;)V

    return-void
.end method

.method public AeL(LX/1Wm;JZ)V
    .locals 3

    if-eqz p4, :cond_0

    .line 95962
    iget-wide v0, p1, LX/0md;->A11:J

    iput-wide v0, p0, Lcom/soula2/Conversation;->A04:J

    :cond_0
    const-wide/16 v1, 0x0

    cmp-long v0, p2, v1

    if-nez v0, :cond_1

    .line 95963
    iget-object v1, p0, Lcom/soula2/Conversation;->A1c:Lcom/soula2/conversation/ConversationListView;

    iget-object v0, p0, Lcom/soula2/Conversation;->A4F:Ljava/lang/Runnable;

    invoke-virtual {v1, v0}, Landroid/view/View;->removeCallbacks(Ljava/lang/Runnable;)Z

    .line 95964
    :cond_1
    const/16 v0, 0xc

    new-instance v1, Lcom/facebook/redex/RunnableRunnableShape2S0200000_I0;

    invoke-direct {v1, p1, v0, p0}, Lcom/facebook/redex/RunnableRunnableShape2S0200000_I0;-><init>(Ljava/lang/Object;ILjava/lang/Object;)V

    iput-object v1, p0, Lcom/soula2/Conversation;->A4F:Ljava/lang/Runnable;

    .line 95965
    iget-object v0, p0, Lcom/soula2/Conversation;->A1c:Lcom/soula2/conversation/ConversationListView;

    invoke-virtual {v0, v1, p2, p3}, Landroid/view/View;->postDelayed(Ljava/lang/Runnable;J)Z

    return-void
.end method

.method public dispatchTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 7

    const-string v3, "conversation/dispatch-touch-event "

    .line 95966
    iget-object v0, p0, Lcom/soula2/Conversation;->A1c:Lcom/soula2/conversation/ConversationListView;

    invoke-virtual {v0}, Lcom/soula2/conversation/ConversationListView;->getConversationCursorAdapter()LX/0me;

    move-result-object v0

    .line 95967
    iget-boolean v0, v0, LX/0me;->A0D:Z

    .line 95968
    const/4 v2, 0x0

    if-eqz v0, :cond_0

    .line 95969
    iget-object v0, p0, LX/0kB;->A0I:LX/11Q;

    .line 95970
    iget-object v0, v0, LX/11Q;->A00:LX/1gq;

    if-eqz v0, :cond_1

    .line 95971
    iget-boolean v0, v0, LX/1gq;->A0R:Z

    .line 95972
    if-eqz v0, :cond_1

    .line 95973
    :cond_0
    return v2

    .line 95974
    :cond_1
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    if-nez v0, :cond_2

    .line 95975
    iput-boolean v2, p0, Lcom/soula2/Conversation;->A4O:Z

    .line 95976
    :cond_2
    iget-object v0, p0, Lcom/soula2/Conversation;->A2k:LX/0ma;

    invoke-virtual {v0}, LX/0mb;->A01()Z

    move-result v0

    if-eqz v0, :cond_5

    iget-object v0, p0, Lcom/soula2/Conversation;->A09:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->isShown()Z

    move-result v0

    if-eqz v0, :cond_5

    .line 95977
    iget-object v0, p0, Lcom/soula2/Conversation;->A09:Landroid/view/View;

    iget-object v6, p0, Lcom/soula2/Conversation;->A5A:[I

    invoke-virtual {v0, v6}, Landroid/view/View;->getLocationOnScreen([I)V

    .line 95978
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getRawY()F

    move-result v1

    const/4 v5, 0x1

    aget v0, v6, v5

    int-to-float v0, v0

    cmpl-float v0, v1, v0

    if-ltz v0, :cond_5

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getRawY()F

    move-result v4

    aget v1, v6, v5

    iget-object v0, p0, Lcom/soula2/Conversation;->A09:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getHeight()I

    move-result v0

    add-int/2addr v1, v0

    int-to-float v0, v1

    cmpg-float v0, v4, v0

    if-gez v0, :cond_5

    .line 95979
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    if-nez v0, :cond_3

    .line 95980
    iput-boolean v5, p0, Lcom/soula2/Conversation;->A4d:Z

    goto :goto_0

    .line 95981
    :cond_3
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    if-ne v0, v5, :cond_5

    .line 95982
    iget-boolean v0, p0, Lcom/soula2/Conversation;->A4d:Z

    if-eqz v0, :cond_4

    .line 95983
    iget-object v0, p0, Lcom/soula2/Conversation;->A2k:LX/0ma;

    invoke-virtual {v0, v5}, LX/0mb;->A00(Z)V

    .line 95984
    iget-object v0, p0, Lcom/soula2/Conversation;->A09:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->requestFocus()Z

    .line 95985
    :cond_4
    iput-boolean v2, p0, Lcom/soula2/Conversation;->A4d:Z

    .line 95986
    :cond_5
    :goto_0
    :try_start_0
    invoke-super {p0, p1}, LX/0kH;->dispatchTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v2

    return v2
    :try_end_0
    .catch Landroid/content/ActivityNotFoundException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/ArrayIndexOutOfBoundsException; {:try_start_0 .. :try_end_0} :catch_0

    :catch_0
    move-exception v0

    .line 95987
    invoke-static {v3, v0}, Lcom/whatsapp/util/Log;->e(Ljava/lang/String;Ljava/lang/Throwable;)V

    return v2

    .line 95988
    :catch_1
    move-exception v0

    .line 95989
    invoke-static {v3, v0}, Lcom/whatsapp/util/Log;->e(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 95990
    iget-object v1, p0, LX/0kH;->A05:LX/0li;

    const v0, 0x7f120092

    invoke-virtual {v1, v0, v2}, LX/0li;->A08(II)V

    return v2
.end method

.method public onActivityResult(IILandroid/content/Intent;)V
    .locals 24

    .line 95991
    move-object/from16 v6, p0

    move/from16 v9, p1

    move/from16 v1, p2

    move-object/from16 v5, p3

    invoke-super {v6, v9, v1, v5}, LX/0kB;->onActivityResult(IILandroid/content/Intent;)V

    invoke-static {v9, v1, v5}, Lcom/ApxSAMods/core/Izumi;->onActivityResult(IILandroid/content/Intent;)V

    const/4 v4, 0x0

    if-nez p2, :cond_2

    if-eqz p3, :cond_2

    const-string v0, "oom"

    .line 95992
    invoke-virtual {v5, v0, v4}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "conversation/activityres/oom-error"

    .line 95993
    invoke-static {v0}, Lcom/whatsapp/util/Log;->e(Ljava/lang/String;)V

    .line 95994
    const v0, 0x7f120813

    .line 95995
    invoke-interface {v6, v0}, LX/0kM;->AcX(I)V

    .line 95996
    :cond_0
    const-string v0, "no-space"

    .line 95997
    invoke-virtual {v5, v0, v4}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    if-eqz v0, :cond_1

    const-string v0, "conversation/activityres/no-space"

    .line 95998
    invoke-static {v0}, Lcom/whatsapp/util/Log;->e(Ljava/lang/String;)V

    .line 95999
    const v0, 0x7f120810

    .line 96000
    invoke-interface {v6, v0}, LX/0kM;->AcX(I)V

    .line 96001
    :cond_1
    const-string v0, "io-error"

    .line 96002
    invoke-virtual {v5, v0, v4}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    if-eqz v0, :cond_2

    const-string v0, "conversation/activityres/fail/load-image"

    .line 96003
    invoke-static {v0}, Lcom/whatsapp/util/Log;->e(Ljava/lang/String;)V

    .line 96004
    const v0, 0x7f120807

    .line 96005
    invoke-interface {v6, v0}, LX/0kM;->AcX(I)V

    .line 96006
    :cond_2
    iget-object v0, v6, Lcom/soula2/Conversation;->A55:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_3
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1tr;

    .line 96007
    invoke-interface {v0, v5, v9, v1}, LX/1tr;->AKO(Landroid/content/Intent;II)Z

    move-result v0

    if-eqz v0, :cond_3

    :cond_4
    :sswitch_0
    return-void

    :cond_5
    const-string/jumbo v2, "status_distribution"

    const-string v10, "jids"

    const/4 v8, 0x0

    const/4 v3, 0x1

    const/4 v7, -0x1

    sparse-switch p1, :sswitch_data_0

    .line 96008
    :cond_6
    :goto_0
    iget-object v7, v6, LX/0kJ;->A05:LX/0lO;

    iget-object v4, v6, LX/0kB;->A0V:LX/0pG;

    iget-object v6, v6, LX/0kB;->A0M:LX/0nZ;

    .line 96009
    sget-object v0, LX/0qK;->A04:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v8

    :cond_7
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/30q;

    .line 96010
    instance-of v2, v3, LX/2ot;

    if-nez v2, :cond_9

    instance-of v0, v3, LX/2os;

    if-nez v0, :cond_8

    .line 96011
    instance-of v0, v3, LX/2or;

    if-eqz v0, :cond_7

    const/16 v0, 0x384

    .line 96012
    :goto_1
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    .line 96013
    if-eqz v0, :cond_7

    .line 96014
    invoke-virtual {v0}, Ljava/lang/Number;->intValue()I

    move-result v0

    if-ne v0, v9, :cond_7

    .line 96015
    if-nez v2, :cond_12

    instance-of v0, v3, LX/2os;

    if-nez v0, :cond_12

    .line 96016
    instance-of v0, v3, LX/2or;

    if-nez v0, :cond_12

    return-void

    .line 96017
    :cond_8
    const/16 v0, 0x385

    goto :goto_1

    .line 96018
    :cond_9
    const/16 v0, 0x387

    goto :goto_1

    .line 96019
    :sswitch_1
    if-ne v1, v7, :cond_6

    .line 96020
    invoke-virtual {v6}, Landroid/app/Activity;->finish()V

    goto :goto_0

    .line 96021
    :sswitch_2
    if-ne v1, v7, :cond_4

    .line 96022
    invoke-virtual {v6}, LX/0kB;->A2Y()Ljava/util/Collection;

    move-result-object v9

    .line 96023
    invoke-interface {v9}, Ljava/util/Collection;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_15

    .line 96024
    const-class v1, LX/0l8;

    .line 96025
    invoke-virtual {v5, v10}, Landroid/content/Intent;->getStringArrayListExtra(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v0

    .line 96026
    invoke-static {v1, v0}, LX/0mQ;->A07(Ljava/lang/Class;Ljava/lang/Iterable;)Ljava/util/List;

    move-result-object v7

    .line 96027
    iget-object v0, v6, LX/0kH;->A0C:LX/0kj;

    invoke-static {v0, v7}, LX/4EJ;->A00(LX/0kj;Ljava/util/List;)Z

    move-result v0

    if-eqz v0, :cond_a

    .line 96028
    invoke-static {v5}, LX/009;->A05(Ljava/lang/Object;)V

    .line 96029
    invoke-virtual {v5, v2}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v8

    check-cast v8, LX/1VG;

    .line 96030
    :cond_a
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2, v9}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    .line 96031
    const/16 v1, 0x1f

    new-instance v0, Lcom/facebook/redex/IDxComparatorShape21S0000000_2_I0;

    invoke-direct {v0, v1}, Lcom/facebook/redex/IDxComparatorShape21S0000000_2_I0;-><init>(I)V

    invoke-static {v2, v0}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    .line 96032
    invoke-virtual {v2}, Ljava/util/AbstractCollection;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :goto_2
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_16

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/0md;

    .line 96033
    iget-object v1, v6, LX/0kB;->A03:LX/0oS;

    iget-object v0, v6, Lcom/soula2/Conversation;->A11:LX/0wg;

    invoke-virtual {v1, v0, v8, v2, v7}, LX/0oS;->A06(LX/0wg;LX/1VG;LX/0md;Ljava/util/List;)V

    goto :goto_2

    .line 96034
    :sswitch_3
    if-ne v1, v7, :cond_4

    .line 96035
    iget-object v0, v6, Lcom/soula2/Conversation;->A1c:Lcom/soula2/conversation/ConversationListView;

    .line 96036
    iput-boolean v3, v0, Lcom/soula2/conversation/ConversationListView;->A0F:Z

    return-void

    .line 96037
    :sswitch_4
    if-ne v1, v7, :cond_4

    goto/16 :goto_5

    .line 96038
    :sswitch_5
    if-ne v1, v7, :cond_4

    const-string v0, "file_path"

    .line 96039
    invoke-virtual {v5, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    const-string v0, "media_url"

    .line 96040
    invoke-virtual {v5, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v11

    .line 96041
    const-class v1, LX/0l8;

    .line 96042
    invoke-virtual {v5, v10}, Landroid/content/Intent;->getStringArrayListExtra(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v0

    .line 96043
    invoke-static {v1, v0}, LX/0mQ;->A07(Ljava/lang/Class;Ljava/lang/Iterable;)Ljava/util/List;

    move-result-object v10

    .line 96044
    iget-object v0, v6, LX/0kH;->A0C:LX/0kj;

    invoke-static {v0, v10}, LX/4EJ;->A00(LX/0kj;Ljava/util/List;)Z

    move-result v0

    if-eqz v0, :cond_11

    .line 96045
    invoke-static {v5}, LX/009;->A05(Ljava/lang/Object;)V

    .line 96046
    invoke-virtual {v5, v2}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v14

    check-cast v14, LX/1VG;

    const-string v0, "audience_clicked"

    .line 96047
    invoke-virtual {v5, v0, v4}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v2

    const-string v0, "audience_updated"

    .line 96048
    invoke-virtual {v5, v0, v4}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v1

    .line 96049
    :goto_3
    invoke-static {v9}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_b

    invoke-static {v11}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_e

    .line 96050
    :cond_b
    new-instance v13, LX/0oR;

    invoke-direct {v13}, LX/0oR;-><init>()V

    .line 96051
    invoke-static {v9}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_f

    .line 96052
    new-instance v0, Ljava/io/File;

    invoke-direct {v0, v9}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 96053
    iput-object v0, v13, LX/0oR;->A0F:Ljava/io/File;

    .line 96054
    invoke-static {v0}, LX/1CH;->A01(Ljava/io/File;)Landroid/graphics/Bitmap;

    move-result-object v7

    const/16 v0, 0x64

    .line 96055
    invoke-static {v7, v0}, LX/1CH;->A03(Landroid/graphics/Bitmap;I)[B

    move-result-object v9

    .line 96056
    move-object v12, v8

    .line 96057
    :goto_4
    iget-object v0, v13, LX/0oR;->A0F:Ljava/io/File;

    .line 96058
    if-eqz v0, :cond_e

    :cond_c
    const-string/jumbo v0, "provider"

    .line 96059
    invoke-virtual {v5, v0, v4}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v7

    .line 96060
    const/4 v0, 0x1

    if-eq v7, v3, :cond_d

    const/4 v0, 0x2

    if-eq v7, v0, :cond_d

    const/4 v0, 0x0

    :cond_d
    iput v0, v13, LX/0oR;->A05:I

    .line 96061
    iget-object v0, v6, LX/0kB;->A03:LX/0oS;

    iget-object v11, v6, Lcom/soula2/Conversation;->A2t:LX/0lL;

    const/16 v20, 0xd

    const-string v7, "caption"

    .line 96062
    invoke-virtual {v5, v7}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v16

    iget-object v7, v6, Lcom/soula2/Conversation;->A1x:LX/0mf;

    .line 96063
    iget-object v15, v7, LX/0mf;->A08:LX/0md;

    .line 96064
    const-class v8, Lcom/whatsapp/jid/UserJid;

    const-string v7, "mentions"

    .line 96065
    invoke-virtual {v5, v7}, Landroid/content/Intent;->getStringArrayListExtra(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v7

    .line 96066
    invoke-static {v8, v7}, LX/0mQ;->A07(Ljava/lang/Class;Ljava/lang/Iterable;)Ljava/util/List;

    move-result-object v18

    iget-boolean v7, v6, Lcom/soula2/Conversation;->A4P:Z

    const/16 v19, 0x0

    .line 96067
    move/from16 v21, v4

    move/from16 v22, v4

    move/from16 v23, v7

    move-object/from16 v17, v10

    invoke-virtual/range {v11 .. v23}, LX/0lL;->A00(Landroid/net/Uri;LX/0oR;LX/1VG;LX/0md;Ljava/lang/String;Ljava/util/List;Ljava/util/List;Ljava/util/List;BIIZ)LX/1hy;

    move-result-object v7

    .line 96068
    invoke-virtual {v0, v7, v9, v2, v1}, LX/0oS;->A05(LX/1hy;[BZZ)V

    .line 96069
    iget-object v0, v6, Lcom/soula2/Conversation;->A1c:Lcom/soula2/conversation/ConversationListView;

    .line 96070
    iput-boolean v3, v0, Lcom/soula2/conversation/ConversationListView;->A0F:Z

    .line 96071
    iget-object v0, v6, Lcom/soula2/Conversation;->A2k:LX/0ma;

    invoke-virtual {v0, v4}, LX/0mb;->A00(Z)V

    .line 96072
    iget-object v0, v6, Lcom/soula2/Conversation;->A3G:LX/2FY;

    invoke-virtual {v0, v4}, LX/2FY;->A03(Z)V

    const-string v0, "clear_message_after_send"

    .line 96073
    invoke-virtual {v5, v0, v4}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    if-eqz v0, :cond_e

    .line 96074
    invoke-virtual {v6}, Lcom/soula2/Conversation;->A2p()V

    .line 96075
    invoke-virtual {v6}, Lcom/soula2/Conversation;->A2l()V

    .line 96076
    :cond_e
    invoke-virtual {v6}, Lcom/soula2/Conversation;->A3E()Z

    return-void

    .line 96077
    :cond_f
    invoke-static {v11}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v12

    const-string v0, "media_width"

    .line 96078
    invoke-virtual {v5, v0, v7}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    iput v0, v13, LX/0oR;->A08:I

    const-string v0, "media_height"

    .line 96079
    invoke-virtual {v5, v0, v7}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    iput v0, v13, LX/0oR;->A06:I

    const-string/jumbo v0, "preview_media_url"

    .line 96080
    invoke-virtual {v5, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    if-eqz v7, :cond_10

    .line 96081
    iget-object v0, v6, Lcom/soula2/Conversation;->A2l:LX/17Z;

    invoke-virtual {v0, v7}, LX/17Z;->A02(Ljava/lang/String;)[B

    move-result-object v8

    :cond_10
    move-object v9, v8

    if-nez v12, :cond_c

    goto/16 :goto_4

    .line 96082
    :cond_11
    move-object v14, v8

    const/4 v2, 0x0

    const/4 v1, 0x0

    goto/16 :goto_3

    .line 96083
    :sswitch_6
    if-ne v1, v7, :cond_4

    .line 96084
    iget-object v0, v6, Lcom/soula2/Conversation;->A1c:Lcom/soula2/conversation/ConversationListView;

    .line 96085
    iput-boolean v3, v0, Lcom/soula2/conversation/ConversationListView;->A0F:Z

    .line 96086
    :goto_5
    invoke-virtual {v6}, Lcom/soula2/Conversation;->A2p()V

    .line 96087
    invoke-virtual {v6}, Lcom/soula2/Conversation;->A2l()V

    return-void

    .line 96088
    :sswitch_7
    if-ne v1, v7, :cond_4

    if-eqz p3, :cond_4

    const-string v0, "contact"

    .line 96089
    invoke-virtual {v5, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 96090
    invoke-static {v0}, Lcom/whatsapp/jid/UserJid;->getNullable(Ljava/lang/String;)Lcom/whatsapp/jid/UserJid;

    move-result-object v1

    if-eqz v1, :cond_4

    .line 96091
    const-class v0, Lcom/soula2/Conversation;

    new-instance v2, Landroid/content/Intent;

    invoke-direct {v2, v6, v0}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 96092
    invoke-virtual {v1}, Lcom/whatsapp/jid/Jid;->getRawString()Ljava/lang/String;

    move-result-object v1

    const-string v0, "jid"

    invoke-virtual {v2, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 96093
    iget-object v0, v6, Lcom/soula2/Conversation;->A2r:LX/0l8;

    invoke-static {v0}, LX/0mQ;->A03(Lcom/whatsapp/jid/Jid;)Ljava/lang/String;

    move-result-object v1

    const-string v0, "group_reply_jid"

    invoke-virtual {v2, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 96094
    invoke-virtual {v6, v2}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    .line 96095
    invoke-virtual {v6}, Landroid/app/Activity;->finish()V

    return-void

    .line 96096
    :sswitch_8
    if-nez p2, :cond_4

    .line 96097
    iget-object v1, v6, LX/0kH;->A00:Landroid/view/View;

    .line 96098
    const/16 v0, 0x35

    .line 96099
    invoke-static {v6, v5, v1, v0}, LX/31C;->A00(Landroid/app/Activity;Landroid/content/Intent;Landroid/view/View;I)LX/0o8;

    move-result-object v0

    .line 96100
    invoke-virtual {v0}, LX/0mn;->A02()V

    return-void

    .line 96101
    :sswitch_9
    if-eqz p3, :cond_4

    const-string v0, "error_code"

    .line 96102
    invoke-virtual {v5, v0, v7}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v1

    const/16 v0, 0x1db

    if-ne v1, v0, :cond_4

    .line 96103
    iget-object v0, v6, Lcom/soula2/Conversation;->A3r:LX/0tc;

    .line 96104
    invoke-virtual {v0, v6}, LX/0tc;->A01(Landroid/content/Context;)LX/2Ln;

    move-result-object v1

    new-instance v0, LX/2Lo;

    invoke-direct {v0, v4}, LX/2Lo;-><init>(I)V

    .line 96105
    invoke-virtual {v1, v0}, LX/2Ln;->A01(LX/2Lp;)V

    return-void

    .line 96106
    :cond_12
    move-object v8, v3

    move-object v9, v5

    move-object v10, v6

    move-object v11, v4

    move-object v12, v7

    move v13, v1

    invoke-virtual/range {v8 .. v13}, LX/30q;->A04(Landroid/content/Intent;LX/0nZ;LX/0pG;LX/0lO;I)V

    return-void

    .line 96107
    :sswitch_a
    iget-object v0, v6, Lcom/soula2/Conversation;->A1Y:LX/0vr;

    invoke-virtual {v0}, LX/0vr;->A08()V

    .line 96108
    iget-object v1, v6, Lcom/soula2/Conversation;->A2b:LX/0mJ;

    invoke-virtual {v1}, LX/0mJ;->A0I()Z

    move-result v0

    if-eqz v0, :cond_13

    .line 96109
    const-class v0, Lcom/whatsapp/jid/UserJid;

    invoke-virtual {v1, v0}, LX/0mJ;->A0A(Ljava/lang/Class;)Lcom/whatsapp/jid/Jid;

    move-result-object v1

    check-cast v1, Lcom/whatsapp/jid/UserJid;

    if-eqz v1, :cond_13

    .line 96110
    iget-object v0, v6, LX/0kB;->A04:LX/0lA;

    invoke-virtual {v0, v6, v1, v8}, LX/0lA;->A04(LX/0kV;Lcom/whatsapp/jid/UserJid;Ljava/lang/String;)V

    .line 96111
    :cond_13
    iget-object v0, v6, LX/0kB;->A0p:LX/18r;

    invoke-virtual {v0}, LX/18r;->A00()V

    return-void

    .line 96112
    :sswitch_b
    if-ne v1, v7, :cond_14

    .line 96113
    iget-object v0, v6, Lcom/soula2/Conversation;->A2m:LX/35A;

    if-eqz v0, :cond_14

    .line 96114
    invoke-virtual {v6, v0}, Lcom/soula2/Conversation;->A35(LX/35A;)V

    .line 96115
    :cond_14
    iput-object v8, v6, Lcom/soula2/Conversation;->A2m:LX/35A;

    return-void

    .line 96116
    :cond_15
    const-string v0, "conversation/forward/failed"

    .line 96117
    invoke-static {v0}, Lcom/whatsapp/util/Log;->w(Ljava/lang/String;)V

    .line 96118
    iget-object v1, v6, LX/0kH;->A05:LX/0li;

    const v0, 0x7f120d20

    invoke-virtual {v1, v0, v4}, LX/0li;->A08(II)V

    goto :goto_6

    .line 96119
    :cond_16
    iget-object v1, v6, LX/0kH;->A0C:LX/0kj;

    const/16 v0, 0x32f

    .line 96120
    invoke-virtual {v1, v0}, LX/0kj;->A07(I)Z

    move-result v1

    const-string v5, "Conversation:forwardMessage"

    .line 96121
    invoke-interface {v7}, Ljava/util/List;->size()I

    move-result v0

    if-eqz v1, :cond_18

    .line 96122
    if-ne v0, v3, :cond_1a

    invoke-static {v7}, LX/0mQ;->A0P(Ljava/util/List;)Z

    move-result v0

    if-eqz v0, :cond_1a

    .line 96123
    new-instance v2, Landroid/content/Intent;

    invoke-direct {v2}, Landroid/content/Intent;-><init>()V

    .line 96124
    invoke-virtual {v6}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v1

    const-string v0, "com.soula2.HomeActivity"

    invoke-virtual {v2, v1, v0}, Landroid/content/Intent;->setClassName(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v1

    const-string v0, "com.soula2.intent.action.STATUSES"

    .line 96125
    invoke-virtual {v1, v0}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    .line 96126
    invoke-virtual {v6, v0}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    .line 96127
    :cond_17
    :goto_6
    # Steins;Zapp
	# Old: invoke-virtual {v6}, LX/0kB;->A2Z()V

    return-void

    .line 96128
    :cond_18
    if-ne v0, v3, :cond_1b

    invoke-interface {v7, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/whatsapp/jid/Jid;

    invoke-static {v0}, LX/0mQ;->A0N(Lcom/whatsapp/jid/Jid;)Z

    move-result v0

    if-nez v0, :cond_1b

    .line 96129
    :cond_19
    iget-object v1, v6, Lcom/soula2/Conversation;->A2r:LX/0l8;

    invoke-static {v1}, LX/009;->A05(Ljava/lang/Object;)V

    invoke-interface {v7, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_17

    .line 96130
    iget-object v3, v6, LX/0kF;->A00:LX/154;

    iget-object v2, v6, Lcom/soula2/Conversation;->A4x:LX/0kk;

    iget-object v1, v6, LX/0kB;->A07:LX/0nL;

    .line 96131
    invoke-interface {v7, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0l8;

    invoke-virtual {v1, v0}, LX/0nL;->A0B(LX/0l8;)LX/0mJ;

    move-result-object v0

    .line 96132
    invoke-virtual {v2, v6, v0}, LX/0kk;->A0k(Landroid/content/Context;LX/0mJ;)Landroid/content/Intent;

    move-result-object v0

    .line 96133
    invoke-virtual {v3, v6, v0, v5}, LX/154;->A08(Landroid/content/Context;Landroid/content/Intent;Ljava/lang/String;)V

    goto :goto_6

    .line 96134
    :cond_1a
    invoke-interface {v7}, Ljava/util/List;->size()I

    move-result v0

    if-eq v0, v3, :cond_19

    .line 96135
    invoke-static {v6}, LX/0kk;->A05(Landroid/content/Context;)Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v6, v0}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    .line 96136
    :cond_1b
    invoke-virtual {v6, v7}, LX/0kF;->A2R(Ljava/util/List;)V

    goto :goto_6

    :sswitch_data_0
    .sparse-switch
        0x2 -> :sswitch_2
        0x8 -> :sswitch_3
        0xb -> :sswitch_a
        0xd -> :sswitch_a
        0x16 -> :sswitch_4
        0x19 -> :sswitch_5
        0x1b -> :sswitch_6
        0x2a -> :sswitch_7
        0x33 -> :sswitch_0
        0x34 -> :sswitch_0
        0x35 -> :sswitch_8
        0x1db -> :sswitch_9
        0x326 -> :sswitch_4
        0x32b -> :sswitch_b
        0x386 -> :sswitch_1
    .end sparse-switch
.end method

.method public onBackPressed()V
    .locals 4

    .line 96137
    iget-object v0, p0, Lcom/soula2/Conversation;->A2k:LX/0ma;

    invoke-virtual {v0}, LX/0mb;->A02()Z

    move-result v0

    if-nez v0, :cond_0

    .line 96138
    iget-object v1, p0, Lcom/soula2/Conversation;->A3x:LX/1mL;

    if-eqz v1, :cond_1

    invoke-interface {v1}, LX/1mL;->ABj()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 96139
    invoke-interface {v1}, LX/1mL;->ABr()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 96140
    const/4 v0, 0x0

    invoke-interface {v1, v0}, LX/1mL;->A7u(Z)V

    .line 96141
    :cond_0
    return-void

    .line 96142
    :cond_1
    sget-object v1, Lcom/soula2/Conversation;->A5G:Ljava/util/HashMap;

    iget-object v0, p0, Lcom/soula2/Conversation;->A2b:LX/0mJ;

    const-class v3, LX/0l8;

    invoke-virtual {v0, v3}, LX/0mJ;->A0A(Ljava/lang/Class;)Lcom/whatsapp/jid/Jid;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/util/AbstractMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    if-nez v0, :cond_2

    .line 96143
    iget-object v0, p0, Lcom/soula2/Conversation;->A4G:Ljava/lang/String;

    if-eqz v0, :cond_3

    .line 96144
    :cond_2
    iget-object v2, p0, Lcom/soula2/Conversation;->A2E:LX/0rJ;

    iget-object v0, p0, Lcom/soula2/Conversation;->A2r:LX/0l8;

    invoke-static {v0}, Lcom/whatsapp/jid/UserJid;->of(Lcom/whatsapp/jid/Jid;)Lcom/whatsapp/jid/UserJid;

    move-result-object v1

    const/4 v0, 0x7

    .line 96145
    invoke-virtual {v2, v1, v0}, LX/0rJ;->A01(Lcom/whatsapp/jid/UserJid;I)V

    .line 96146
    :cond_3
    invoke-virtual {p0}, Landroid/app/Activity;->isTaskRoot()Z

    move-result v0

    if-eqz v0, :cond_5

    .line 96147
    invoke-static {p0}, LX/0kk;->A04(Landroid/content/Context;)Landroid/content/Intent;

    move-result-object v2

    .line 96148
    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v0, 0x15

    if-lt v1, v0, :cond_4

    .line 96149
    invoke-virtual {p0}, Landroid/app/Activity;->finishAndRemoveTask()V

    .line 96150
    invoke-virtual {p0, v2}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    return-void

    .line 96151
    :cond_4
    invoke-virtual {p0, v2}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    .line 96152
    :cond_5
    iget-object v2, p0, Lcom/soula2/Conversation;->A2N:LX/0uN;

    iget-object v0, p0, Lcom/soula2/Conversation;->A2b:LX/0mJ;

    invoke-virtual {v0, v3}, LX/0mJ;->A0A(Ljava/lang/Class;)Lcom/whatsapp/jid/Jid;

    move-result-object v1

    .line 96153
    invoke-virtual {v2}, LX/0uN;->A0B()Ljava/util/concurrent/ConcurrentHashMap;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/util/AbstractMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1Ni;

    if-eqz v0, :cond_6

    .line 96154
    iget-boolean v0, v0, LX/1Ni;->A0h:Z

    if-eqz v0, :cond_6

    .line 96155
    iget-object v1, p0, Lcom/soula2/Conversation;->A2L:LX/0vj;

    iget-object v0, p0, Lcom/soula2/Conversation;->A2b:LX/0mJ;

    .line 96156
    invoke-virtual {v0, v3}, LX/0mJ;->A0A(Ljava/lang/Class;)Lcom/whatsapp/jid/Jid;

    move-result-object v0

    invoke-static {v0}, LX/009;->A05(Ljava/lang/Object;)V

    check-cast v0, LX/0l8;

    .line 96157
    invoke-virtual {v1, v0}, LX/0vj;->A02(LX/0l8;)V

    .line 96158
    :cond_6
    iget-boolean v0, p0, Lcom/soula2/Conversation;->A4W:Z

    if-eqz v0, :cond_7

    .line 96159
    iget-object v0, p0, Lcom/soula2/Conversation;->A4B:LX/01K;

    invoke-interface {v0}, LX/01K;->get()Ljava/lang/Object;

    .line 96160
    :cond_7
    invoke-super {p0}, LX/0kH;->onBackPressed()V

    return-void
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 5

    .line 96161
    invoke-super {p0, p1}, LX/0kB;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 96162
    iget-object v1, p0, Lcom/soula2/Conversation;->A1c:Lcom/soula2/conversation/ConversationListView;

    .line 96163
    iget-boolean v0, v1, Lcom/soula2/conversation/ConversationListView;->A0A:Z

    if-eqz v0, :cond_7

    const/4 v0, 0x2

    .line 96164
    invoke-virtual {v1, v0}, Landroid/widget/AbsListView;->setTranscriptMode(I)V

    .line 96165
    iget-object v3, v1, Lcom/soula2/conversation/ConversationListView;->A0H:Landroid/os/Handler;

    const/4 v2, 0x0

    const-wide/16 v0, 0x3e8

    invoke-virtual {v3, v2, v0, v1}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    .line 96166
    :goto_0
    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v0, 0x13

    if-lt v1, v0, :cond_0

    iget-object v2, p0, Lcom/soula2/Conversation;->A3x:LX/1mL;

    if-eqz v2, :cond_0

    .line 96167
    iget v1, p1, Landroid/content/res/Configuration;->orientation:I

    invoke-interface {v2, v1}, LX/1mL;->Aai(I)V

    .line 96168
    invoke-interface {v2}, LX/1mL;->ABr()Z

    move-result v0

    if-nez v0, :cond_0

    .line 96169
    invoke-interface {v2, v1}, LX/1mL;->Ab8(I)V

    .line 96170
    :cond_0
    iget-object v1, p0, Lcom/soula2/Conversation;->A42:LX/0kz;

    if-eqz v1, :cond_1

    .line 96171
    iget-object v4, v1, LX/0kz;->A1G:LX/0l0;

    .line 96172
    iget-object v0, v4, LX/0l0;->A0J:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 96173
    iget-object v3, v1, LX/0kz;->A0R:Ljava/io/File;

    if-eqz v3, :cond_1

    .line 96174
    iget-object v2, v1, LX/0kz;->A0N:LX/1Mc;

    const/4 v1, 0x0

    const/4 v0, 0x1

    invoke-virtual {v4, v2, v3, v1, v0}, LX/0l0;->A03(LX/1Mc;Ljava/io/File;ZZ)V

    .line 96175
    :cond_1
    iget v0, p1, Landroid/content/res/Configuration;->orientation:I

    const/4 v2, 0x1

    .line 96176
    iget-object v1, p0, Lcom/soula2/Conversation;->A2v:Lcom/soula2/mentions/MentionableEntry;

    if-ne v0, v2, :cond_5

    .line 96177
    invoke-static {v1}, LX/1GU;->A05(Landroid/widget/EditText;)V

    .line 96178
    :cond_2
    :goto_1
    iget-object v3, p0, Lcom/soula2/Conversation;->A2C:LX/20a;

    .line 96179
    iget-boolean v0, v3, LX/20a;->A0F:Z

    if-eqz v0, :cond_4

    .line 96180
    iget v0, p1, Landroid/content/res/Configuration;->orientation:I

    const/4 v2, 0x2

    const/4 v1, 0x0

    if-ne v0, v2, :cond_3

    const/4 v1, 0x1

    :cond_3
    iput-boolean v1, v3, LX/20a;->A0E:Z

    .line 96181
    iget v0, v3, LX/20a;->A0I:I

    if-ne v0, v2, :cond_4

    if-eqz v1, :cond_4

    .line 96182
    invoke-virtual {v3}, LX/20a;->A00()V

    .line 96183
    :cond_4
    return-void

    .line 96184
    :cond_5
    const/4 v0, 0x2

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setMaxLines(I)V

    .line 96185
    iget-object v1, p0, Lcom/soula2/Conversation;->A3x:LX/1mL;

    if-eqz v1, :cond_6

    .line 96186
    invoke-interface {v1}, LX/1mL;->ABj()Z

    move-result v0

    if-eqz v0, :cond_6

    .line 96187
    invoke-interface {v1}, LX/1mL;->ABr()Z

    move-result v0

    if-nez v0, :cond_6

    .line 96188
    invoke-interface {v1}, LX/1mL;->A7f()V

    .line 96189
    iget-object v0, p0, Lcom/soula2/Conversation;->A3x:LX/1mL;

    invoke-interface {v0, v2}, LX/1mL;->Ab8(I)V

    .line 96190
    :cond_6
    iget-object v0, p0, Lcom/soula2/Conversation;->A42:LX/0kz;

    if-eqz v0, :cond_2

    .line 96191
    iget-object v0, v0, LX/0kz;->A1G:LX/0l0;

    .line 96192
    iget-object v0, v0, LX/0l0;->A0B:Landroid/view/ViewGroup;

    invoke-virtual {v0}, Landroid/view/View;->getVisibility()I

    move-result v0

    if-nez v0, :cond_2

    .line 96193
    invoke-virtual {p0}, LX/0kB;->A2a()V

    goto :goto_1

    .line 96194
    :cond_7
    iget v0, v1, Lcom/soula2/conversation/ConversationListView;->A00:I

    .line 96195
    invoke-virtual {v1, v0}, Landroid/widget/AdapterView;->setSelection(I)V

    goto/16 :goto_0
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 116

    const-string v31, "composer_has_text"

    const-string v29, "lifecycle_callbacks"

    const-string v27, "on_create_conversation"

    const-string v18, "inflateViews"

    const-string v17, "Conversation"

    const-string v34, "on_create"

    .line 96196
    const/4 v3, 0x0

    .line 96197
    :try_start_0
    move-object/from16 v1, p0

    iget-object v2, v1, LX/0kJ;->A02:LX/0kg;

    move-object/from16 v0, v17

    invoke-virtual {v2, v0}, LX/0kg;->A09(Ljava/lang/String;)V

    .line 96198
    const-string v2, "conversation/create"

    new-instance v33, LX/1L4;

    move-object/from16 v0, v33

    invoke-direct {v0, v2}, LX/1L4;-><init>(Ljava/lang/String;)V

    .line 96199
    invoke-virtual {v1}, Landroid/app/Activity;->getWindow()Landroid/view/Window;

    move-result-object v2

    .line 96200
    new-instance v0, Landroid/graphics/drawable/ColorDrawable;

    invoke-direct {v0, v3}, Landroid/graphics/drawable/ColorDrawable;-><init>(I)V

    invoke-virtual {v2, v0}, Landroid/view/Window;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 96201
    sget-boolean v0, LX/1jw;->A00:Z

    if-eqz v0, :cond_0

    const/16 v0, 0xd

    .line 96202
    invoke-virtual {v2, v0}, Landroid/view/Window;->requestFeature(I)Z

    const/16 v0, 0xc

    .line 96203
    invoke-virtual {v2, v0}, Landroid/view/Window;->requestFeature(I)Z

    const/high16 v0, -0x80000000

    .line 96204
    invoke-virtual {v2, v0}, Landroid/view/Window;->addFlags(I)V

    .line 96205
    :cond_0
    move-object/from16 v16, p1

    move-object/from16 v0, v16

    invoke-super {v1, v0}, LX/0kB;->onCreate(Landroid/os/Bundle;)V

    .line 96206
    move-object/from16 v0, v34

    invoke-virtual {v1, v0}, LX/0kK;->A1n(Ljava/lang/String;)V

    .line 96207
    iget-object v2, v1, Lcom/soula2/Conversation;->A0q:LX/2E0;

    .line 96208
    iget-object v0, v1, LX/0kK;->A00:LX/241;

    .line 96209
    invoke-virtual {v2, v0}, LX/2E0;->A00(LX/241;)LX/2Fe;

    move-result-object v0

    iput-object v0, v1, Lcom/soula2/Conversation;->A1t:LX/2Fe;

    .line 96210
    iget-object v5, v1, LX/0kJ;->A05:LX/0lO;

    iget-object v4, v1, Lcom/soula2/Conversation;->A21:LX/0mH;

    const/16 v2, 0x2b

    new-instance v0, Lcom/facebook/redex/RunnableRunnableShape1S0100000_I0;

    invoke-direct {v0, v4, v2}, Lcom/facebook/redex/RunnableRunnableShape1S0100000_I0;-><init>(Ljava/lang/Object;I)V

    invoke-interface {v5, v0}, LX/0lO;->AZm(Ljava/lang/Runnable;)V

    .line 96211
    invoke-virtual {v1}, Landroid/app/Activity;->getWindow()Landroid/view/Window;

    move-result-object v0

    invoke-static {v0}, LX/154;->A05(Landroid/view/Window;)V

    .line 96212
    new-instance v2, LX/01S;

    invoke-direct {v2, v1}, LX/01S;-><init>(LX/00o;)V

    const-class v0, Lcom/soula2/tosgating/viewmodel/ToSGatingViewModel;

    invoke-virtual {v2, v0}, LX/01S;->A00(Ljava/lang/Class;)LX/011;

    move-result-object v0

    check-cast v0, Lcom/soula2/tosgating/viewmodel/ToSGatingViewModel;

    iput-object v0, v1, Lcom/soula2/Conversation;->A3i:Lcom/soula2/tosgating/viewmodel/ToSGatingViewModel;

    .line 96213
    new-instance v2, LX/01S;

    invoke-direct {v2, v1}, LX/01S;-><init>(LX/00o;)V

    const-class v0, Lcom/soula2/countrygating/viewmodel/CountryGatingViewModel;

    invoke-virtual {v2, v0}, LX/01S;->A00(Ljava/lang/Class;)LX/011;

    move-result-object v0

    check-cast v0, Lcom/soula2/countrygating/viewmodel/CountryGatingViewModel;

    iput-object v0, v1, Lcom/soula2/Conversation;->A24:Lcom/soula2/countrygating/viewmodel/CountryGatingViewModel;

    .line 96214
    iget-object v4, v1, Lcom/soula2/Conversation;->A0m:LX/2Do;

    iget-object v2, v1, Lcom/soula2/Conversation;->A4h:Landroid/os/Handler;

    .line 96215
    new-instance v0, Lcom/facebook/redex/IDxFactoryShape36S0300000_1_I0;

    invoke-direct {v0, v2, v4, v1, v3}, Lcom/facebook/redex/IDxFactoryShape36S0300000_1_I0;-><init>(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;I)V

    .line 96216
    new-instance v2, LX/01S;

    invoke-direct {v2, v0, v1}, LX/01S;-><init>(LX/058;LX/00o;)V

    const-class v0, LX/0l4;

    .line 96217
    invoke-virtual {v2, v0}, LX/01S;->A00(Ljava/lang/Class;)LX/011;

    move-result-object v0

    check-cast v0, LX/0l4;

    iput-object v0, v1, Lcom/soula2/Conversation;->A1y:LX/0l4;

    .line 96218
    new-instance v2, LX/01S;

    invoke-direct {v2, v1}, LX/01S;-><init>(LX/00o;)V

    const-class v32, LX/3MU;

    move-object/from16 v0, v32

    invoke-virtual {v2, v0}, LX/01S;->A00(Ljava/lang/Class;)LX/011;

    move-result-object v0

    check-cast v0, LX/3MU;

    iput-object v0, v1, Lcom/soula2/Conversation;->A3A:LX/3MU;

    .line 96219
    new-instance v2, LX/01S;

    invoke-direct {v2, v1}, LX/01S;-><init>(LX/00o;)V

    const-class v0, Lcom/soula2/community/ConversationCommunityViewModel;

    .line 96220
    invoke-virtual {v2, v0}, LX/01S;->A00(Ljava/lang/Class;)LX/011;

    move-result-object v0

    check-cast v0, Lcom/soula2/community/ConversationCommunityViewModel;

    iput-object v0, v1, Lcom/soula2/Conversation;->A1Q:Lcom/soula2/community/ConversationCommunityViewModel;

    .line 96221
    iget-object v4, v0, Lcom/soula2/community/ConversationCommunityViewModel;->A02:LX/012;

    .line 96222
    const/16 v20, 0x5

    new-instance v2, Lcom/facebook/redex/IDxObserverShape118S0100000_2_I0;

    move/from16 v0, v20

    invoke-direct {v2, v1, v0}, Lcom/facebook/redex/IDxObserverShape118S0100000_2_I0;-><init>(Ljava/lang/Object;I)V

    .line 96223
    invoke-virtual {v4, v1, v2}, LX/013;->A05(LX/00m;LX/01R;)V

    .line 96224
    iget-object v0, v1, Lcom/soula2/Conversation;->A1Q:Lcom/soula2/community/ConversationCommunityViewModel;

    .line 96225
    iget-object v4, v0, Lcom/soula2/community/ConversationCommunityViewModel;->A01:LX/012;

    .line 96226
    const/16 v21, 0x4

    new-instance v2, Lcom/facebook/redex/IDxObserverShape118S0100000_2_I0;

    move/from16 v0, v21

    invoke-direct {v2, v1, v0}, Lcom/facebook/redex/IDxObserverShape118S0100000_2_I0;-><init>(Ljava/lang/Object;I)V

    .line 96227
    invoke-virtual {v4, v1, v2}, LX/013;->A05(LX/00m;LX/01R;)V

    .line 96228
    invoke-virtual {v1}, LX/00j;->A0V()LX/01G;

    move-result-object v4

    sget-object v2, Lcom/soula2/community/NewCommunityAdminBottomSheetFragment;->A03:Ljava/lang/String;

    new-instance v0, Lcom/facebook/redex/IDxRListenerShape359S0100000_2_I0;

    invoke-direct {v0, v1, v3}, Lcom/facebook/redex/IDxRListenerShape359S0100000_2_I0;-><init>(Ljava/lang/Object;I)V

    .line 96229
    invoke-virtual {v4, v0, v1, v2}, LX/01G;->A0e(LX/07L;LX/00m;Ljava/lang/String;)V

    .line 96230
    iget-object v0, v1, Lcom/soula2/Conversation;->A1y:LX/0l4;

    .line 96231
    iget-object v2, v0, LX/0l4;->A09:LX/013;

    .line 96232
    const/4 v4, 0x1

    new-instance v0, Lcom/facebook/redex/IDxObserverShape118S0100000_2_I0;

    invoke-direct {v0, v1, v4}, Lcom/facebook/redex/IDxObserverShape118S0100000_2_I0;-><init>(Ljava/lang/Object;I)V

    invoke-virtual {v2, v1, v0}, LX/013;->A05(LX/00m;LX/01R;)V

    .line 96233
    iget-object v0, v1, Lcom/soula2/Conversation;->A1y:LX/0l4;

    .line 96234
    iget-object v2, v0, LX/0l4;->A0C:LX/012;

    .line 96235
    const/16 v30, 0x6

    new-instance v0, Lcom/facebook/redex/IDxObserverShape118S0100000_2_I0;

    move/from16 v5, v30

    invoke-direct {v0, v1, v5}, Lcom/facebook/redex/IDxObserverShape118S0100000_2_I0;-><init>(Ljava/lang/Object;I)V

    .line 96236
    invoke-virtual {v2, v1, v0}, LX/013;->A05(LX/00m;LX/01R;)V

    .line 96237
    iget-object v0, v1, Lcom/soula2/Conversation;->A1y:LX/0l4;

    .line 96238
    iget-object v2, v0, LX/0l4;->A0B:LX/012;

    .line 96239
    new-instance v0, Lcom/facebook/redex/IDxObserverShape118S0100000_2_I0;

    invoke-direct {v0, v1, v3}, Lcom/facebook/redex/IDxObserverShape118S0100000_2_I0;-><init>(Ljava/lang/Object;I)V

    .line 96240
    invoke-virtual {v2, v1, v0}, LX/013;->A05(LX/00m;LX/01R;)V

    .line 96241
    iget-object v9, v1, Lcom/soula2/Conversation;->A3Y:LX/13E;

    iget-object v10, v1, LX/0kJ;->A05:LX/0lO;

    iget-object v7, v1, Lcom/soula2/Conversation;->A3V:LX/166;

    iget-object v8, v1, Lcom/soula2/Conversation;->A3X:LX/12d;

    iget-object v6, v1, LX/0kH;->A09:LX/0kh;

    new-instance v5, LX/1JK;

    invoke-direct/range {v5 .. v10}, LX/1JK;-><init>(LX/0kh;LX/166;LX/12d;LX/13E;LX/0lO;)V

    iput-object v5, v1, Lcom/soula2/Conversation;->A3f:LX/1JK;

    .line 96242
    new-instance v0, LX/2FY;

    invoke-direct {v0, v5}, LX/2FY;-><init>(LX/1JK;)V

    iput-object v0, v1, Lcom/soula2/Conversation;->A3G:LX/2FY;

    const/4 v0, 0x0

    if-eqz p1, :cond_1

    const/4 v0, 0x1

    .line 96243
    :cond_1
    iput-boolean v0, v1, Lcom/soula2/Conversation;->A4K:Z

    .line 96244
    iget-object v2, v1, LX/0kH;->A0C:LX/0kj;

    const/16 v0, 0x482

    .line 96245
    invoke-virtual {v2, v0}, LX/0kj;->A07(I)Z

    move-result v0

    iput-boolean v0, v1, Lcom/soula2/Conversation;->A4Y:Z

    .line 96246
    iget-object v2, v1, LX/0kH;->A0C:LX/0kj;

    .line 96247
    const/16 v0, 0x3a3

    .line 96248
    invoke-virtual {v2, v0}, LX/0kj;->A07(I)Z

    move-result v0

    .line 96249
    iput-boolean v0, v1, Lcom/soula2/Conversation;->A4W:Z

    .line 96250
    iget-object v2, v1, LX/0kH;->A0C:LX/0kj;

    const/16 v0, 0x573

    invoke-virtual {v2, v0}, LX/0kj;->A07(I)Z

    move-result v0

    iput-boolean v0, v1, Lcom/soula2/Conversation;->A4X:Z

    .line 96251
    iget-boolean v0, v1, Lcom/soula2/Conversation;->A4K:Z

    if-nez v0, :cond_2

    .line 96252
    sget-object v0, LX/1gq;->A0y:LX/13d;

    invoke-virtual {v0}, Ljava/util/AbstractMap;->clear()V

    .line 96253
    :cond_2
    iget-object v0, v1, Lcom/soula2/Conversation;->A31:Lcom/whatsapp/nativelibloader/WhatsAppLibLoader;

    invoke-virtual {v0}, Lcom/whatsapp/nativelibloader/WhatsAppLibLoader;->A03()Z

    move-result v0

    const/16 v28, 0x3

    if-nez v0, :cond_3

    const-string v0, "conversation/aborting due to native libraries missing"

    .line 96254
    invoke-static {v0}, Lcom/whatsapp/util/Log;->i(Ljava/lang/String;)V

    const-string v0, "on_create_whatsapp_lib_failure"

    .line 96255
    invoke-virtual {v1, v0}, LX/0kK;->A1o(Ljava/lang/String;)V

    .line 96256
    move/from16 v0, v28

    invoke-virtual {v1, v0}, LX/0kK;->A1q(S)V

    .line 96257
    invoke-virtual {v1}, Landroid/app/Activity;->finish()V

    goto/16 :goto_20

    .line 96258
    :cond_3
    iget-object v0, v1, LX/0kF;->A01:LX/0nN;

    .line 96259
    invoke-virtual {v0}, LX/0nN;->A0B()V

    .line 96260
    iget-object v0, v0, LX/0nN;->A00:Lcom/soula2/Me;

    .line 96261
    if-eqz v0, :cond_71

    iget-object v0, v1, Lcom/soula2/Conversation;->A2V:LX/0p5;

    .line 96262
    invoke-virtual {v0}, LX/0p5;->A04()V

    .line 96263
    iget-boolean v0, v0, LX/0p5;->A01:Z

    .line 96264
    if-eqz v0, :cond_71

    iget-object v0, v1, LX/0kF;->A09:LX/0nH;

    .line 96265
    invoke-virtual {v0}, LX/0nH;->A02()Z

    move-result v0

    if-eqz v0, :cond_71

    .line 96266
    iget-object v14, v1, LX/0kH;->A05:LX/0li;

    iget-object v7, v1, LX/0kB;->A07:LX/0nL;

    iget-object v15, v1, Lcom/soula2/Conversation;->A2O:LX/0w1;

    .line 96267
    invoke-virtual {v1}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    move-result-object v2

    iget-boolean v6, v1, Lcom/soula2/Conversation;->A4K:Z

    .line 96268
    const/16 v70, 0x0

    const/16 v69, 0x0

    const/16 v43, 0x0

    const/16 v61, 0x0

    const/4 v11, 0x0

    const/16 v53, 0x0

    const/16 v55, 0x0

    const/16 v54, 0x0

    const/16 v60, 0x0

    const/16 v65, 0x0

    const/16 v46, 0x0

    const/16 v42, 0x0

    const/16 v52, 0x0

    const/16 v51, 0x0

    const/16 v66, 0x0

    const/16 v62, 0x0

    const/16 v64, 0x0

    const/16 v37, 0x0

    const/4 v12, 0x0

    const/4 v13, 0x0

    const/16 v45, 0x0

    const/16 v59, 0x0

    const/16 v49, 0x0

    const/16 v57, 0x0

    const/16 v50, 0x0

    const/16 v36, 0x0

    const/16 v56, 0x0

    const/16 v47, 0x0

    const/16 v48, 0x0

    const/16 v58, 0x0

    .line 96269
    invoke-static {}, LX/1be;->A00()LX/1be;

    move-result-object v39

    .line 96270
    const-string v8, "authentication_token"

    .line 96271
    invoke-virtual {v2, v8}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v0

    const/16 v63, 0x0

    if-eqz v0, :cond_4
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_4

    .line 96272
    :try_start_1
    new-instance v5, Landroid/content/Intent;

    invoke-direct {v5}, Landroid/content/Intent;-><init>()V

    .line 96273
    invoke-virtual {v1}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v10

    const-string v9, "FakeClass"

    new-instance v0, Landroid/content/ComponentName;

    invoke-direct {v0, v10, v9}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v5, v0}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;

    .line 96274
    invoke-virtual {v1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    .line 96275
    invoke-static {v0, v3, v5, v3}, LX/1TE;->A00(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v5

    .line 96276
    invoke-virtual {v2, v8}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/app/PendingIntent;

    .line 96277
    invoke-virtual {v5, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v63

    goto :goto_0
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_4

    :catch_0
    :try_start_2
    move-exception v5

    const-string v0, "conversation/unable to verify intent"

    .line 96278
    invoke-static {v0, v5}, Lcom/whatsapp/util/Log;->e(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 96279
    :cond_4
    :goto_0
    const-string v19, "fromNotification"

    .line 96280
    move-object/from16 v0, v19

    invoke-virtual {v2, v0, v3}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v67

    .line 96281
    const-string v0, "fromCallNotification"

    .line 96282
    invoke-virtual {v2, v0, v3}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v68

    .line 96283
    const-string v0, "jid"

    .line 96284
    invoke-virtual {v2, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/0l8;->A01(Ljava/lang/String;)LX/0l8;

    move-result-object v5

    .line 96285
    const/4 v9, 0x2

    if-nez v5, :cond_c

    .line 96286
    invoke-virtual {v2}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v36

    .line 96287
    if-eqz v36, :cond_b

    .line 96288
    invoke-static/range {v36 .. v36}, LX/1ur;->A00(Landroid/net/Uri;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 96289
    invoke-static/range {v36 .. v36}, LX/1ur;->A00(Landroid/net/Uri;)Z

    move-result v0

    invoke-static {v0}, LX/009;->A0E(Z)V

    .line 96290
    invoke-static/range {v36 .. v36}, Landroid/content/ContentUris;->parseId(Landroid/net/Uri;)J

    move-result-wide v8

    .line 96291
    invoke-virtual {v7, v8, v9}, LX/0nL;->A06(J)LX/0mJ;

    move-result-object v11

    .line 96292
    if-eqz v11, :cond_8

    .line 96293
    const-class v0, LX/0l8;

    invoke-virtual {v11, v0}, LX/0mJ;->A0A(Ljava/lang/Class;)Lcom/whatsapp/jid/Jid;

    move-result-object v5

    check-cast v5, LX/0l8;

    .line 96294
    goto :goto_1

    .line 96295
    :cond_5
    invoke-virtual/range {v36 .. v36}, Landroid/net/Uri;->getScheme()Ljava/lang/String;

    move-result-object v8

    const-string/jumbo v0, "smsto"

    invoke-virtual {v0, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_6

    .line 96296
    invoke-virtual/range {v36 .. v36}, Landroid/net/Uri;->getScheme()Ljava/lang/String;

    move-result-object v8

    const-string/jumbo v0, "sms"

    invoke-virtual {v0, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_b

    .line 96297
    :cond_6
    invoke-virtual {v2}, Landroid/content/Intent;->getDataString()Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_7

    const-string v0, "conversation/sms/no uri"

    goto/16 :goto_5

    .line 96298
    :cond_7
    const-string v8, ":"

    .line 96299
    invoke-virtual {v0, v8}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    .line 96300
    array-length v8, v0

    if-ne v8, v9, :cond_a

    .line 96301
    const-string v9, "conversation/sms-jid/raw-number "

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8, v9}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    aget-object v9, v0, v4

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v8}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v8}, Lcom/whatsapp/util/Log;->i(Ljava/lang/String;)V

    .line 96302
    aget-object v0, v0, v4

    invoke-static {v0}, Ljava/net/URLDecoder;->decode(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 96303
    invoke-virtual {v7, v0, v4}, LX/0nL;->A0C(Ljava/lang/String;Z)LX/0mJ;

    move-result-object v0

    .line 96304
    if-eqz v0, :cond_9

    .line 96305
    move-object v11, v0

    .line 96306
    const-class v5, LX/0l8;

    invoke-virtual {v0, v5}, LX/0mJ;->A0A(Ljava/lang/Class;)Lcom/whatsapp/jid/Jid;

    move-result-object v5

    check-cast v5, LX/0l8;

    .line 96307
    const-string v8, "conversation/sms-jid:"

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/whatsapp/util/Log;->i(Ljava/lang/String;)V

    .line 96308
    :cond_8
    :goto_1
    const/4 v9, 0x0

    goto :goto_2

    .line 96309
    :cond_9
    const-string v0, "conversation/tell-a-friend"

    .line 96310
    invoke-static {v0}, Lcom/whatsapp/util/Log;->i(Ljava/lang/String;)V

    const-string/jumbo v0, "sms_body"

    .line 96311
    invoke-virtual {v2, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v50

    .line 96312
    const/16 v59, 0x3

    goto/16 :goto_7

    .line 96313
    :cond_a
    const-string v0, "conversation/sms/no jid"

    goto/16 :goto_5

    .line 96314
    :cond_b
    const-string v0, "conversation/start no jid"

    goto/16 :goto_5

    .line 96315
    :cond_c
    const/4 v9, 0x1

    .line 96316
    :goto_2
    if-eqz v5, :cond_11

    .line 96317
    invoke-virtual {v7, v5}, LX/0nL;->A0A(LX/0l8;)LX/0mJ;

    move-result-object v0

    if-nez v0, :cond_d

    .line 96318
    new-instance v0, LX/0mJ;

    invoke-direct {v0, v5}, LX/0mJ;-><init>(Lcom/whatsapp/jid/Jid;)V

    .line 96319
    :cond_d
    invoke-virtual {v0}, LX/0mJ;->A0J()Z

    move-result v8

    if-nez v8, :cond_e

    .line 96320
    iget-object v8, v0, LX/0mJ;->A0D:Lcom/whatsapp/jid/Jid;

    .line 96321
    invoke-static {v8}, LX/0mQ;->A0O(Lcom/whatsapp/jid/Jid;)Z

    move-result v8

    if-eqz v8, :cond_10

    :cond_e
    iget-object v0, v0, LX/0mJ;->A0K:Ljava/lang/String;

    if-nez v0, :cond_10

    const-string v0, "displayname"

    .line 96322
    invoke-virtual {v2, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    if-eqz v8, :cond_f

    const-string v0, "conversation/create/group-shortcut-removed"

    .line 96323
    invoke-static {v0}, Lcom/whatsapp/util/Log;->w(Ljava/lang/String;)V

    .line 96324
    const v2, 0x7f120acd

    new-array v0, v4, [Ljava/lang/Object;

    aput-object v8, v0, v3

    .line 96325
    invoke-virtual {v1, v2, v0}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 96326
    invoke-virtual {v14, v0, v4}, LX/0li;->A0G(Ljava/lang/CharSequence;I)V

    .line 96327
    const/16 v59, 0x2

    goto/16 :goto_7

    .line 96328
    :cond_f
    invoke-virtual {v7, v5}, LX/0nL;->A0B(LX/0l8;)LX/0mJ;

    .line 96329
    :cond_10
    invoke-virtual {v5}, Lcom/whatsapp/jid/Jid;->getType()I

    move-result v7

    if-eqz v7, :cond_11

    .line 96330
    if-eq v7, v4, :cond_11

    .line 96331
    const/16 v0, 0x12

    if-eq v7, v0, :cond_11

    .line 96332
    move/from16 v0, v28

    if-eq v7, v0, :cond_11

    .line 96333
    const/4 v0, 0x7

    if-eq v7, v0, :cond_11

    goto/16 :goto_4

    .line 96334
    :cond_11
    if-eqz v9, :cond_12

    .line 96335
    invoke-virtual {v15, v5}, LX/0w1;->A01(LX/0l8;)LX/0mJ;

    move-result-object v11

    .line 96336
    :cond_12
    if-nez v11, :cond_13

    .line 96337
    const-string v2, "conversation/start no contact for "

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_5

    .line 96338
    :cond_13
    const-string v0, "has_share"

    .line 96339
    invoke-virtual {v2, v0, v3}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v8

    .line 96340
    move/from16 v62, v8

    .line 96341
    const-string v0, "number_from_url"

    .line 96342
    invoke-virtual {v2, v0, v3}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    if-eqz v0, :cond_15

    if-eqz v6, :cond_14

    .line 96343
    invoke-static/range {v16 .. v16}, LX/009;->A05(Ljava/lang/Object;)V

    const-string v7, "has_number_from_url"

    .line 96344
    move-object/from16 v0, v16

    invoke-virtual {v0, v7}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_15

    :cond_14
    const/16 v66, 0x1

    .line 96345
    :cond_15
    const-string/jumbo v0, "text_from_url"

    .line 96346
    invoke-virtual {v2, v0, v3}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    if-eqz v0, :cond_17

    if-eqz v6, :cond_16

    .line 96347
    invoke-static/range {v16 .. v16}, LX/009;->A05(Ljava/lang/Object;)V

    const-string v7, "has_text_from_url"

    .line 96348
    move-object/from16 v0, v16

    invoke-virtual {v0, v7}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_17

    :cond_16
    const/16 v64, 0x1

    .line 96349
    :cond_17
    const-string v7, "added_by_qr_code"

    .line 96350
    invoke-virtual {v2, v7, v3}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    if-eqz v0, :cond_19

    if-eqz v6, :cond_18

    .line 96351
    invoke-static/range {v16 .. v16}, LX/009;->A05(Ljava/lang/Object;)V

    .line 96352
    move-object/from16 v0, v16

    invoke-virtual {v0, v7}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_19

    :cond_18
    const/16 v69, 0x1

    .line 96353
    :cond_19
    const-string v0, "contact_out_address_book"

    .line 96354
    invoke-virtual {v2, v0, v3}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    if-eqz v0, :cond_1b

    if-eqz v6, :cond_1a

    .line 96355
    invoke-static/range {v16 .. v16}, LX/009;->A05(Ljava/lang/Object;)V

    const-string v7, "added_by_number_search"

    .line 96356
    move-object/from16 v0, v16

    invoke-virtual {v0, v7}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1b

    :cond_1a
    const/16 v70, 0x1

    .line 96357
    :cond_1b
    if-eqz v8, :cond_1d

    if-nez v6, :cond_1d

    .line 96358
    if-eqz v63, :cond_1d

    const-string v0, "android.intent.extra.STREAM"

    .line 96359
    invoke-virtual {v2, v0}, Landroid/content/Intent;->getParcelableArrayListExtra(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v57

    const-string v0, "android.intent.extra.TEXT"

    .line 96360
    invoke-virtual {v2, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v46

    const-string v0, "origin"

    .line 96361
    invoke-virtual {v2, v0, v3}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v60

    .line 96362
    const-string/jumbo v0, "skip_preview"

    .line 96363
    invoke-virtual {v2, v0, v3}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v65

    .line 96364
    const-string/jumbo v0, "vcard_name"

    .line 96365
    invoke-virtual {v2, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v47

    .line 96366
    const-string/jumbo v0, "vcard_str"

    .line 96367
    invoke-virtual {v2, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v48

    .line 96368
    const-string/jumbo v0, "vcard_array_str"

    .line 96369
    invoke-virtual {v2, v0}, Landroid/content/Intent;->getStringArrayListExtra(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v56

    .line 96370
    invoke-virtual {v2}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    if-eqz v0, :cond_1c

    .line 96371
    invoke-virtual {v2}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v7

    const-string/jumbo v0, "wa_type"

    invoke-virtual {v7, v0}, Landroid/os/Bundle;->getByte(Ljava/lang/String;)B

    move-result v58

    .line 96372
    :cond_1c
    const-string/jumbo v0, "share_msg"

    .line 96373
    invoke-virtual {v2, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v49

    const-string v0, "iq_code"

    .line 96374
    invoke-virtual {v2, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v53

    const-string v0, "confirm"

    .line 96375
    invoke-virtual {v2, v0, v3}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v61

    :cond_1d
    const-string v0, "invite_bundle"

    .line 96376
    invoke-virtual {v2, v0}, Landroid/content/Intent;->getBundleExtra(Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v7

    if-eqz v7, :cond_1e

    if-nez v6, :cond_1e

    .line 96377
    invoke-virtual {v2, v0}, Landroid/content/Intent;->getBundleExtra(Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v37

    .line 96378
    :cond_1e
    const-string/jumbo v0, "product"

    .line 96379
    invoke-virtual {v2, v0}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v6

    if-eqz v6, :cond_1f

    .line 96380
    invoke-virtual {v2, v0}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v12

    check-cast v12, LX/1VY;

    .line 96381
    :cond_1f
    const-string v0, "business_jid"

    .line 96382
    invoke-virtual {v2, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    if-eqz v6, :cond_20
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_4

    .line 96383
    :try_start_3
    invoke-virtual {v2, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/whatsapp/jid/UserJid;->get(Ljava/lang/String;)Lcom/whatsapp/jid/UserJid;

    move-result-object v43

    .line 96384
    goto :goto_3
    :try_end_3
    .catch LX/1L0; {:try_start_3 .. :try_end_3} :catch_1
    .catchall {:try_start_3 .. :try_end_3} :catchall_4

    .line 96385
    :catch_1
    :try_start_4
    move-exception v6

    const-string v0, "ConversationIntentParser/businessJid is not a user jid"

    .line 96386
    invoke-static {v0, v6}, Lcom/whatsapp/util/Log;->e(Ljava/lang/String;Ljava/lang/Throwable;)V

    :cond_20
    :goto_3
    const-string/jumbo v0, "product_file"

    .line 96387
    invoke-virtual {v2, v0}, Landroid/content/Intent;->getSerializableExtra(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v6

    if-eqz v6, :cond_21

    .line 96388
    invoke-virtual {v2, v0}, Landroid/content/Intent;->getSerializableExtra(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v13

    check-cast v13, Ljava/io/File;

    .line 96389
    :cond_21
    const-string v0, "group_reply_jid"

    .line 96390
    invoke-virtual {v2, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    if-eqz v6, :cond_23

    .line 96391
    if-eqz v12, :cond_22

    const-string v0, "conversationIntentParser/groupReplyAndProductShouldNotBothExist"

    goto :goto_5

    .line 96392
    :goto_4
    const-string v2, "conversation/create/cannot-start-conversation-with-jid: "

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 96393
    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    .line 96394
    :goto_5
    invoke-static {v0}, Lcom/whatsapp/util/Log;->e(Ljava/lang/String;)V

    .line 96395
    const/16 v59, 0x1

    goto :goto_7
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_4

    .line 96396
    :cond_22
    :try_start_5
    invoke-virtual {v2, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 96397
    invoke-static {v0}, LX/0nO;->A03(Ljava/lang/String;)LX/0nO;

    move-result-object v42

    .line 96398
    const-string v0, "group_reply_subject"

    .line 96399
    invoke-virtual {v2, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v51

    .line 96400
    const-string v0, "group_reply_parent_group_jid"

    .line 96401
    invoke-virtual {v2, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v52

    .line 96402
    goto :goto_6
    :try_end_5
    .catch LX/1L0; {:try_start_5 .. :try_end_5} :catch_2
    .catchall {:try_start_5 .. :try_end_5} :catchall_4

    .line 96403
    :catch_2
    :try_start_6
    const-string v0, "ConversationIntentParser/groupReplyJid is not a permanent group jid"

    .line 96404
    invoke-static {v0}, Lcom/whatsapp/util/Log;->e(Ljava/lang/String;)V

    :cond_23
    :goto_6
    const-string v0, "entry_point_conversion_source"

    .line 96405
    invoke-virtual {v2, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    if-eqz v6, :cond_24

    .line 96406
    invoke-virtual {v2, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v54

    .line 96407
    :cond_24
    const-string v0, "entry_point_conversion_app"

    .line 96408
    invoke-virtual {v2, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    if-eqz v6, :cond_25

    .line 96409
    invoke-virtual {v2, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v55

    .line 96410
    :cond_25
    const-string v0, "extra_quoted_message_row_id"

    .line 96411
    invoke-virtual {v2, v0}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_26

    const-wide/16 v6, -0x1

    .line 96412
    invoke-virtual {v2, v0, v6, v7}, Landroid/content/Intent;->getLongExtra(Ljava/lang/String;J)J

    move-result-wide v6

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v45

    .line 96413
    :cond_26
    const-string v0, "ctwa_deeplink_content"

    .line 96414
    invoke-virtual {v2, v0}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_27

    .line 96415
    invoke-virtual {v2, v0}, Landroid/content/Intent;->getBundleExtra(Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v0

    .line 96416
    invoke-static {v0}, LX/1be;->A01(Landroid/os/Bundle;)LX/1be;

    move-result-object v39

    .line 96417
    :cond_27
    :goto_7
    new-instance v0, LX/48c;

    move-object/from16 v35, v0

    move-object/from16 v38, v12

    move-object/from16 v40, v11

    move-object/from16 v41, v5

    move-object/from16 v44, v13

    invoke-direct/range {v35 .. v70}, LX/48c;-><init>(Landroid/net/Uri;Landroid/os/Bundle;LX/1VY;LX/1be;LX/0mJ;LX/0l8;LX/0nO;Lcom/whatsapp/jid/UserJid;Ljava/io/File;Ljava/lang/Long;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/ArrayList;Ljava/util/ArrayList;BIIZZZZZZZZZZ)V

    .line 96418
    iget-object v5, v0, LX/48c;->A06:LX/1be;

    iput-object v5, v1, Lcom/soula2/Conversation;->A2A:LX/1be;

    .line 96419
    iget v2, v0, LX/48c;->A02:I

    if-eq v2, v4, :cond_70

    const/16 v24, 0x2

    move/from16 v6, v24

    if-eq v2, v6, :cond_6f

    move/from16 v6, v28

    if-eq v2, v6, :cond_72

    .line 96420
    iget-object v2, v0, LX/48c;->A08:LX/0l8;

    invoke-static {v2}, LX/009;->A05(Ljava/lang/Object;)V

    iput-object v2, v1, Lcom/soula2/Conversation;->A2r:LX/0l8;

    .line 96421
    iget-object v2, v0, LX/48c;->A07:LX/0mJ;

    invoke-static {v2}, LX/009;->A05(Ljava/lang/Object;)V

    iput-object v2, v1, Lcom/soula2/Conversation;->A2b:LX/0mJ;

    .line 96422
    iget-object v7, v1, Lcom/soula2/Conversation;->A0i:LX/2Ds;

    iget-object v6, v1, Lcom/soula2/Conversation;->A2r:LX/0l8;

    .line 96423
    const/16 v26, 0x0

    new-instance v2, LX/2Zn;

    invoke-direct {v2, v1, v7, v6}, LX/2Zn;-><init>(LX/00p;LX/2Ds;LX/0l8;)V

    .line 96424
    new-instance v6, LX/01S;

    invoke-direct {v6, v2, v1}, LX/01S;-><init>(LX/058;LX/00o;)V

    const-class v2, LX/0mf;

    .line 96425
    invoke-virtual {v6, v2}, LX/01S;->A00(Ljava/lang/Class;)LX/011;

    move-result-object v2

    check-cast v2, LX/0mf;

    iput-object v2, v1, Lcom/soula2/Conversation;->A1x:LX/0mf;

    .line 96426
    iget-object v6, v2, LX/0mf;->A0E:LX/012;

    .line 96427
    new-instance v2, Lcom/facebook/redex/IDxObserverShape116S0100000_1_I0;

    move/from16 v7, v21

    invoke-direct {v2, v1, v7}, Lcom/facebook/redex/IDxObserverShape116S0100000_1_I0;-><init>(Ljava/lang/Object;I)V

    .line 96428
    invoke-virtual {v6, v1, v2}, LX/013;->A05(LX/00m;LX/01R;)V

    .line 96429
    iget-object v2, v1, Lcom/soula2/Conversation;->A1x:LX/0mf;

    .line 96430
    iget-object v6, v2, LX/0mf;->A0D:LX/012;

    .line 96431
    new-instance v2, Lcom/facebook/redex/IDxObserverShape116S0100000_1_I0;

    move/from16 v7, v28

    invoke-direct {v2, v1, v7}, Lcom/facebook/redex/IDxObserverShape116S0100000_1_I0;-><init>(Ljava/lang/Object;I)V

    .line 96432
    invoke-virtual {v6, v1, v2}, LX/013;->A05(LX/00m;LX/01R;)V

    .line 96433
    iget-object v2, v1, Lcom/soula2/Conversation;->A1x:LX/0mf;

    .line 96434
    iget-object v7, v2, LX/0mf;->A0Y:LX/1HU;

    .line 96435
    new-instance v6, Lcom/facebook/redex/IDxObserverShape116S0100000_1_I0;

    move/from16 v2, v30

    invoke-direct {v6, v1, v2}, Lcom/facebook/redex/IDxObserverShape116S0100000_1_I0;-><init>(Ljava/lang/Object;I)V

    .line 96436
    invoke-virtual {v7, v1, v6}, LX/013;->A05(LX/00m;LX/01R;)V

    .line 96437
    iget-object v2, v1, Lcom/soula2/Conversation;->A1x:LX/0mf;

    .line 96438
    iget-object v7, v2, LX/0mf;->A0F:LX/012;

    .line 96439
    new-instance v6, Lcom/facebook/redex/IDxObserverShape116S0100000_1_I0;

    move/from16 v2, v20

    invoke-direct {v6, v1, v2}, Lcom/facebook/redex/IDxObserverShape116S0100000_1_I0;-><init>(Ljava/lang/Object;I)V

    invoke-virtual {v7, v1, v6}, LX/013;->A05(LX/00m;LX/01R;)V

    .line 96440
    iget-object v2, v1, Lcom/soula2/Conversation;->A1x:LX/0mf;

    .line 96441
    iget-object v6, v2, LX/0mf;->A0Z:LX/1HU;

    .line 96442
    new-instance v2, Lcom/facebook/redex/IDxObserverShape118S0100000_2_I0;

    move/from16 v7, v24

    invoke-direct {v2, v1, v7}, Lcom/facebook/redex/IDxObserverShape118S0100000_2_I0;-><init>(Ljava/lang/Object;I)V

    .line 96443
    invoke-virtual {v6, v1, v2}, LX/013;->A05(LX/00m;LX/01R;)V

    .line 96444
    iget-object v2, v1, Lcom/soula2/Conversation;->A1x:LX/0mf;

    .line 96445
    iget-object v6, v2, LX/0mf;->A0W:LX/1HU;

    .line 96446
    new-instance v2, Lcom/facebook/redex/IDxObserverShape116S0100000_1_I0;

    invoke-direct {v2, v1, v4}, Lcom/facebook/redex/IDxObserverShape116S0100000_1_I0;-><init>(Ljava/lang/Object;I)V

    .line 96447
    invoke-virtual {v6, v1, v2}, LX/013;->A05(LX/00m;LX/01R;)V

    .line 96448
    iget-object v2, v1, Lcom/soula2/Conversation;->A1x:LX/0mf;

    .line 96449
    iget-object v6, v2, LX/0mf;->A0X:LX/1HU;

    .line 96450
    new-instance v2, Lcom/facebook/redex/IDxObserverShape116S0100000_1_I0;

    invoke-direct {v2, v1, v7}, Lcom/facebook/redex/IDxObserverShape116S0100000_1_I0;-><init>(Ljava/lang/Object;I)V

    invoke-virtual {v6, v1, v2}, LX/013;->A05(LX/00m;LX/01R;)V

    .line 96451
    iget-object v2, v1, Lcom/soula2/Conversation;->A1x:LX/0mf;

    .line 96452
    iget-object v6, v2, LX/0mf;->A0G:LX/012;

    .line 96453
    const/4 v12, 0x7

    new-instance v2, Lcom/facebook/redex/IDxObserverShape118S0100000_2_I0;

    invoke-direct {v2, v1, v12}, Lcom/facebook/redex/IDxObserverShape118S0100000_2_I0;-><init>(Ljava/lang/Object;I)V

    invoke-virtual {v6, v1, v2}, LX/013;->A05(LX/00m;LX/01R;)V

    .line 96454
    iget-boolean v2, v1, Lcom/soula2/Conversation;->A4X:Z

    if-eqz v2, :cond_28

    .line 96455
    new-instance v6, LX/01S;

    invoke-direct {v6, v1}, LX/01S;-><init>(LX/00o;)V

    const-class v2, Lcom/soula2/polls/PollVoterViewModel;

    invoke-virtual {v6, v2}, LX/01S;->A00(Ljava/lang/Class;)LX/011;

    move-result-object v2

    check-cast v2, Lcom/soula2/polls/PollVoterViewModel;

    iput-object v2, v1, Lcom/soula2/Conversation;->A3H:Lcom/soula2/polls/PollVoterViewModel;

    .line 96456
    :cond_28
    iget-object v7, v1, Lcom/soula2/Conversation;->A0u:LX/2Cj;

    iget-object v6, v1, Lcom/soula2/Conversation;->A2r:LX/0l8;

    .line 96457
    new-instance v2, Lcom/facebook/redex/IDxFactoryShape71S0200000_2_I0;

    invoke-direct {v2, v6, v4, v7}, Lcom/facebook/redex/IDxFactoryShape71S0200000_2_I0;-><init>(Ljava/lang/Object;ILjava/lang/Object;)V

    .line 96458
    new-instance v6, LX/01S;

    invoke-direct {v6, v2, v1}, LX/01S;-><init>(LX/058;LX/00o;)V

    const-class v2, LX/2Ck;

    .line 96459
    invoke-virtual {v6, v2}, LX/01S;->A00(Ljava/lang/Class;)LX/011;

    move-result-object v2

    check-cast v2, LX/2Ck;

    .line 96460
    iget-object v7, v2, LX/2Ck;->A04:LX/1HU;

    .line 96461
    new-instance v6, Lcom/facebook/redex/IDxObserverShape118S0100000_2_I0;

    move/from16 v2, v28

    invoke-direct {v6, v1, v2}, Lcom/facebook/redex/IDxObserverShape118S0100000_2_I0;-><init>(Ljava/lang/Object;I)V

    .line 96462
    invoke-virtual {v7, v1, v6}, LX/013;->A05(LX/00m;LX/01R;)V

    .line 96463
    iget-object v11, v1, Lcom/soula2/Conversation;->A0v:LX/2Dv;

    iget-object v10, v1, Lcom/soula2/Conversation;->A2r:LX/0l8;

    iget-object v9, v1, Lcom/soula2/Conversation;->A1y:LX/0l4;

    iget-object v8, v1, Lcom/soula2/Conversation;->A24:Lcom/soula2/countrygating/viewmodel/CountryGatingViewModel;

    iget-object v7, v1, Lcom/soula2/Conversation;->A3i:Lcom/soula2/tosgating/viewmodel/ToSGatingViewModel;

    iget-object v6, v1, Lcom/soula2/Conversation;->A2b:LX/0mJ;

    .line 96464
    new-instance v2, LX/376;

    move-object/from16 v35, v2

    move-object/from16 v36, v11

    move-object/from16 v37, v9

    move-object/from16 v38, v8

    move-object/from16 v39, v6

    move-object/from16 v40, v10

    move-object/from16 v41, v7

    invoke-direct/range {v35 .. v41}, LX/376;-><init>(LX/2Dv;LX/0l4;Lcom/soula2/countrygating/viewmodel/CountryGatingViewModel;LX/0mJ;LX/0l8;Lcom/soula2/tosgating/viewmodel/ToSGatingViewModel;)V

    .line 96465
    new-instance v6, LX/01S;

    invoke-direct {v6, v2, v1}, LX/01S;-><init>(LX/058;LX/00o;)V

    const-class v2, LX/1kT;

    .line 96466
    invoke-virtual {v6, v2}, LX/01S;->A00(Ljava/lang/Class;)LX/011;

    move-result-object v2

    check-cast v2, LX/1kT;

    iput-object v2, v1, Lcom/soula2/Conversation;->A1v:LX/1kT;

    .line 96467
    iget-object v6, v2, LX/1kT;->A0Q:LX/1HU;

    .line 96468
    new-instance v2, Lcom/facebook/redex/IDxObserverShape116S0100000_1_I0;

    invoke-direct {v2, v1, v12}, Lcom/facebook/redex/IDxObserverShape116S0100000_1_I0;-><init>(Ljava/lang/Object;I)V

    .line 96469
    invoke-virtual {v6, v1, v2}, LX/013;->A05(LX/00m;LX/01R;)V

    .line 96470
    iget-object v2, v1, Lcom/soula2/Conversation;->A1v:LX/1kT;

    .line 96471
    iget-object v6, v2, LX/1kT;->A02:LX/012;

    .line 96472
    new-instance v2, Lcom/facebook/redex/IDxObserverShape116S0100000_1_I0;

    invoke-direct {v2, v1, v3}, Lcom/facebook/redex/IDxObserverShape116S0100000_1_I0;-><init>(Ljava/lang/Object;I)V

    invoke-virtual {v6, v1, v2}, LX/013;->A05(LX/00m;LX/01R;)V

    .line 96473
    iget-object v2, v1, Lcom/soula2/Conversation;->A1v:LX/1kT;

    .line 96474
    iget-object v6, v2, LX/1kT;->A0R:LX/1HU;

    .line 96475
    const/16 v15, 0x9

    new-instance v2, Lcom/facebook/redex/IDxObserverShape118S0100000_2_I0;

    invoke-direct {v2, v1, v15}, Lcom/facebook/redex/IDxObserverShape118S0100000_2_I0;-><init>(Ljava/lang/Object;I)V

    .line 96476
    invoke-virtual {v6, v1, v2}, LX/013;->A05(LX/00m;LX/01R;)V

    .line 96477
    iget-object v2, v1, Lcom/soula2/Conversation;->A1v:LX/1kT;

    .line 96478
    iget-object v7, v2, LX/1kT;->A0P:LX/1HU;

    .line 96479
    const/16 v2, 0x8

    new-instance v6, Lcom/facebook/redex/IDxObserverShape116S0100000_1_I0;

    invoke-direct {v6, v1, v2}, Lcom/facebook/redex/IDxObserverShape116S0100000_1_I0;-><init>(Ljava/lang/Object;I)V

    .line 96480
    invoke-virtual {v7, v1, v6}, LX/013;->A05(LX/00m;LX/01R;)V

    .line 96481
    iget-object v8, v1, Lcom/soula2/Conversation;->A0l:LX/2Dp;

    iget-object v7, v1, Lcom/soula2/Conversation;->A2b:LX/0mJ;

    .line 96482
    new-instance v6, LX/370;

    invoke-direct {v6, v8, v7}, LX/370;-><init>(LX/2Dp;LX/0mJ;)V

    .line 96483
    new-instance v7, LX/01S;

    invoke-direct {v7, v6, v1}, LX/01S;-><init>(LX/058;LX/00o;)V

    const-class v6, LX/1kk;

    .line 96484
    invoke-virtual {v7, v6}, LX/01S;->A00(Ljava/lang/Class;)LX/011;

    move-result-object v6

    check-cast v6, LX/1kk;

    .line 96485
    iget-object v7, v6, LX/1kk;->A05:LX/1HU;

    .line 96486
    new-instance v6, Lcom/facebook/redex/IDxObserverShape118S0100000_2_I0;

    invoke-direct {v6, v1, v2}, Lcom/facebook/redex/IDxObserverShape118S0100000_2_I0;-><init>(Ljava/lang/Object;I)V

    .line 96487
    invoke-virtual {v7, v1, v6}, LX/013;->A05(LX/00m;LX/01R;)V

    .line 96488
    iget-object v7, v1, Lcom/soula2/Conversation;->A1t:LX/2Fe;

    iget-object v6, v1, Lcom/soula2/Conversation;->A2b:LX/0mJ;

    invoke-virtual {v7, v6}, LX/2Fe;->A00(LX/0mJ;)V

    .line 96489
    iget-object v10, v1, Lcom/soula2/Conversation;->A1t:LX/2Fe;

    iget-object v9, v1, Lcom/soula2/Conversation;->A2r:LX/0l8;

    .line 96490
    iget-object v6, v10, LX/2Fe;->A07:LX/241;

    .line 96491
    iget-object v8, v6, LX/241;->A01:LX/1OE;

    .line 96492
    iget-object v7, v8, LX/1OE;->A06:LX/1OF;

    .line 96493
    invoke-virtual {v7}, LX/1OF;->A00()Z

    move-result v6

    .line 96494
    if-eqz v6, :cond_29

    const-string v6, "conversation-qpl-annotations/wallpaper"

    .line 96495
    invoke-static {v6}, Lcom/whatsapp/util/Log;->d(Ljava/lang/String;)V

    .line 96496
    new-instance v6, LX/4aX;

    invoke-direct {v6, v1, v10, v9}, LX/4aX;-><init>(Lcom/soula2/Conversation;LX/2Fe;LX/0l8;)V

    .line 96497
    iget-object v8, v8, LX/1OE;->A08:LX/0xf;

    .line 96498
    iget v7, v7, LX/1OF;->A05:I

    .line 96499
    invoke-interface {v8, v6, v7}, LX/0xf;->AJV(LX/1O8;I)V

    .line 96500
    :cond_29
    iget-boolean v6, v0, LX/48c;->A0W:Z

    invoke-static {v6}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v7

    invoke-static {v7}, LX/009;->A05(Ljava/lang/Object;)V

    iput-boolean v6, v1, Lcom/soula2/Conversation;->A4P:Z

    .line 96501
    iget-boolean v6, v0, LX/48c;->A0Y:Z

    invoke-static {v6}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v7

    invoke-static {v7}, LX/009;->A05(Ljava/lang/Object;)V

    iput-boolean v6, v1, Lcom/soula2/Conversation;->A4Q:Z

    .line 96502
    iget-boolean v6, v0, LX/48c;->A0Q:Z

    iput-boolean v6, v1, Lcom/soula2/Conversation;->A4M:Z

    .line 96503
    iget-boolean v6, v0, LX/48c;->A0P:Z

    iput-boolean v6, v1, Lcom/soula2/Conversation;->A4L:Z

    .line 96504
    iget-object v8, v1, Lcom/soula2/Conversation;->A2H:LX/0yO;

    .line 96505
    iget-object v7, v8, LX/0yO;->A02:LX/0li;

    const/16 v13, 0x27

    new-instance v6, Lcom/facebook/redex/RunnableRunnableShape7S0100000_I0_6;

    invoke-direct {v6, v8, v13}, Lcom/facebook/redex/RunnableRunnableShape7S0100000_I0_6;-><init>(Ljava/lang/Object;I)V

    invoke-virtual {v7, v6}, LX/0li;->AZr(Ljava/lang/Runnable;)V

    .line 96506
    iget-boolean v7, v5, LX/1be;->A06:Z

    .line 96507
    iget-object v6, v1, Lcom/soula2/Conversation;->A2G:LX/183;

    .line 96508
    if-nez v7, :cond_2a

    iget-object v6, v6, LX/183;->A00:LX/182;

    .line 96509
    iget-object v7, v6, LX/182;->A00:LX/0kj;

    const/16 v6, 0x3d0

    invoke-virtual {v7, v6}, LX/0kj;->A07(I)Z

    move-result v6

    .line 96510
    if-eqz v6, :cond_34

    .line 96511
    :cond_2a
    iget-object v6, v1, Lcom/soula2/Conversation;->A2G:LX/183;

    .line 96512
    iget-object v7, v6, LX/183;->A01:LX/0zn;

    const-string v6, "20210210"

    invoke-virtual {v7, v6}, LX/0zn;->A00(Ljava/lang/String;)I

    move-result v6

    if-eq v6, v4, :cond_34

    .line 96513
    iget-object v8, v1, Lcom/soula2/Conversation;->A2F:LX/0rK;

    .line 96514
    iget-object v7, v1, Lcom/soula2/Conversation;->A2r:LX/0l8;

    .line 96515
    const/16 v6, 0xa

    .line 96516
    invoke-virtual {v8, v7, v6}, LX/0rK;->A01(LX/0l8;I)V

    .line 96517
    move-object/from16 v6, v26

    iput-object v6, v1, Lcom/soula2/Conversation;->A4G:Ljava/lang/String;

    .line 96518
    iput-object v6, v1, Lcom/soula2/Conversation;->A4H:Ljava/lang/String;

    .line 96519
    iget-object v6, v1, Lcom/soula2/Conversation;->A2r:LX/0l8;

    invoke-static {v6}, Lcom/whatsapp/jid/UserJid;->of(Lcom/whatsapp/jid/Jid;)Lcom/whatsapp/jid/UserJid;

    move-result-object v7

    if-eqz v7, :cond_2b

    .line 96520
    iget-object v6, v1, Lcom/soula2/Conversation;->A28:LX/0yR;

    invoke-virtual {v6, v7}, LX/0yR;->A07(Lcom/whatsapp/jid/UserJid;)LX/1o9;

    move-result-object v7

    if-eqz v7, :cond_2b

    .line 96521
    iget-object v6, v1, Lcom/soula2/Conversation;->A28:LX/0yR;

    .line 96522
    invoke-virtual {v6, v7}, LX/0rH;->A02(LX/1o8;)V

    .line 96523
    :cond_2b
    :goto_8
    iget-object v6, v0, LX/48c;->A0F:Ljava/lang/String;

    iput-object v6, v1, Lcom/soula2/Conversation;->A4J:Ljava/lang/String;

    .line 96524
    iget-object v6, v0, LX/48c;->A0E:Ljava/lang/String;

    iput-object v6, v1, Lcom/soula2/Conversation;->A4I:Ljava/lang/String;

    .line 96525
    iget-object v7, v1, Lcom/soula2/Conversation;->A0t:LX/2Dw;

    iget-object v6, v1, Lcom/soula2/Conversation;->A1y:LX/0l4;

    move-object/from16 v22, v6

    .line 96526
    iget-object v8, v7, LX/2Dw;->A00:LX/2A6;

    .line 96527
    iget-object v6, v8, LX/2A6;->A01:LX/29l;

    .line 96528
    iget-object v7, v6, LX/29l;->A1H:Landroid/app/Activity;

    move-object/from16 v20, v7

    .line 96529
    iget-object v7, v8, LX/2A6;->A03:LX/2fo;

    .line 96530
    iget-object v8, v7, LX/2fo;->A8w:LX/01P;

    .line 96531
    invoke-interface {v8}, LX/01P;->get()Ljava/lang/Object;

    move-result-object v14

    check-cast v14, LX/0li;

    .line 96532
    iget-object v8, v7, LX/2fo;->AKm:LX/01P;

    .line 96533
    invoke-interface {v8}, LX/01P;->get()Ljava/lang/Object;

    move-result-object v12

    check-cast v12, LX/0st;

    .line 96534
    iget-object v8, v7, LX/2fo;->A9i:LX/01P;

    .line 96535
    invoke-interface {v8}, LX/01P;->get()Ljava/lang/Object;

    move-result-object v11

    check-cast v11, LX/18J;

    .line 96536
    iget-object v8, v7, LX/2fo;->A9h:LX/01P;

    .line 96537
    invoke-interface {v8}, LX/01P;->get()Ljava/lang/Object;

    move-result-object v10

    check-cast v10, LX/18A;

    .line 96538
    iget-object v8, v7, LX/2fo;->A3t:LX/01P;

    .line 96539
    invoke-interface {v8}, LX/01P;->get()Ljava/lang/Object;

    move-result-object v9

    check-cast v9, LX/15M;

    .line 96540
    iget-object v6, v6, LX/29l;->A1K:LX/2fo;

    .line 96541
    iget-object v6, v6, LX/2fo;->A04:LX/01P;

    .line 96542
    invoke-interface {v6}, LX/01P;->get()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, LX/0kj;

    new-instance v8, LX/2D2;

    invoke-direct {v8, v6}, LX/2D2;-><init>(LX/0kj;)V

    .line 96543
    iget-object v6, v7, LX/2fo;->A9o:LX/01P;

    .line 96544
    invoke-interface {v6}, LX/01P;->get()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, LX/15n;

    new-instance v6, LX/20a;

    move-object/from16 v35, v6

    move-object/from16 v36, v20

    move-object/from16 v37, v14

    move-object/from16 v38, v12

    move-object/from16 v39, v9

    move-object/from16 v40, v8

    move-object/from16 v41, v22

    move-object/from16 v42, v5

    move-object/from16 v43, v10

    move-object/from16 v44, v11

    move-object/from16 v45, v7

    invoke-direct/range {v35 .. v45}, LX/20a;-><init>(Landroid/app/Activity;LX/0li;LX/0st;LX/15M;LX/2D2;LX/0l4;LX/1be;LX/18A;LX/18J;LX/15n;)V

    .line 96545
    iput-object v6, v1, Lcom/soula2/Conversation;->A2C:LX/20a;

    .line 96546
    iget-object v6, v1, Lcom/soula2/Conversation;->A25:LX/0yU;

    .line 96547
    iget-object v7, v6, LX/0yU;->A00:LX/0kj;

    const/16 v6, 0x337

    invoke-virtual {v7, v6}, LX/0kj;->A07(I)Z

    move-result v6

    .line 96548
    if-nez v6, :cond_2c

    .line 96549
    iget-object v6, v1, Lcom/soula2/Conversation;->A2r:LX/0l8;

    invoke-static {v6}, Lcom/whatsapp/jid/UserJid;->of(Lcom/whatsapp/jid/Jid;)Lcom/whatsapp/jid/UserJid;

    move-result-object v9

    if-eqz v9, :cond_2c

    .line 96550
    iget-object v8, v1, LX/0kJ;->A05:LX/0lO;

    const/16 v7, 0xb

    new-instance v6, Lcom/facebook/redex/RunnableRunnableShape2S0200000_I0;

    invoke-direct {v6, v9, v7, v1}, Lcom/facebook/redex/RunnableRunnableShape2S0200000_I0;-><init>(Ljava/lang/Object;ILjava/lang/Object;)V

    invoke-interface {v8, v6}, LX/0lO;->AZm(Ljava/lang/Runnable;)V

    .line 96551
    :cond_2c
    iget-object v7, v1, LX/0kH;->A0C:LX/0kj;

    const/16 v6, 0x1fc

    .line 96552
    invoke-virtual {v7, v6}, LX/0kj;->A07(I)Z

    move-result v6

    .line 96553
    if-eqz v6, :cond_2d

    iget-object v6, v1, Lcom/soula2/Conversation;->A4J:Ljava/lang/String;

    if-eqz v6, :cond_2d

    .line 96554
    iget-object v6, v1, Lcom/soula2/Conversation;->A2r:LX/0l8;

    invoke-static {v6}, Lcom/whatsapp/jid/UserJid;->of(Lcom/whatsapp/jid/Jid;)Lcom/whatsapp/jid/UserJid;

    move-result-object v8

    if-eqz v8, :cond_2d

    .line 96555
    iget-object v7, v1, LX/0kJ;->A05:LX/0lO;

    new-instance v6, Lcom/facebook/redex/RunnableRunnableShape2S0200000_I0;

    invoke-direct {v6, v8, v15, v1}, Lcom/facebook/redex/RunnableRunnableShape2S0200000_I0;-><init>(Ljava/lang/Object;ILjava/lang/Object;)V

    invoke-interface {v7, v6}, LX/0lO;->AZm(Ljava/lang/Runnable;)V

    .line 96556
    :cond_2d
    iget-object v6, v1, Lcom/soula2/Conversation;->A2b:LX/0mJ;

    invoke-virtual {v6}, LX/0mJ;->A0J()Z

    move-result v6

    iput-boolean v6, v1, Lcom/soula2/Conversation;->A4S:Z

    .line 96557
    iget-object v6, v1, Lcom/soula2/Conversation;->A2b:LX/0mJ;

    .line 96558
    iget-object v6, v6, LX/0mJ;->A0D:Lcom/whatsapp/jid/Jid;

    invoke-static {v6}, LX/0mQ;->A0E(Lcom/whatsapp/jid/Jid;)Z

    move-result v6

    .line 96559
    iput-boolean v6, v1, Lcom/soula2/Conversation;->A4T:Z

    .line 96560
    iget-object v7, v1, Lcom/soula2/Conversation;->A2Q:LX/1Cs;

    move-object/from16 v6, v16

    invoke-virtual {v7, v6}, LX/1Cs;->A00(Landroid/os/Bundle;)V

    .line 96561
    iget-object v8, v1, Lcom/soula2/Conversation;->A2R:LX/19M;

    iget-object v7, v1, Lcom/soula2/Conversation;->A2r:LX/0l8;

    invoke-virtual {v1}, Landroid/app/Activity;->getLocalClassName()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v8, v7, v6}, LX/19M;->A02(LX/0l8;Ljava/lang/String;)V

    .line 96562
    iget-object v6, v1, Lcom/soula2/Conversation;->A2Q:LX/1Cs;

    .line 96563
    iget-wide v6, v6, LX/1Cs;->A00:J

    .line 96564
    iput-wide v6, v1, Lcom/soula2/Conversation;->A03:J

    .line 96565
    iput-boolean v4, v1, Lcom/soula2/Conversation;->A4V:Z

    .line 96566
    iget-object v7, v1, LX/0kH;->A0C:LX/0kj;

    .line 96567
    const/16 v6, 0x548

    .line 96568
    invoke-virtual {v7, v6}, LX/0kj;->A07(I)Z

    move-result v6

    .line 96569
    if-eqz v6, :cond_2e

    .line 96570
    invoke-virtual {v1}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    move-result-object v7

    move-object/from16 v6, v19

    invoke-virtual {v7, v6, v3}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v6

    if-eqz v6, :cond_2e

    .line 96571
    iget-object v7, v1, Lcom/soula2/Conversation;->A2f:LX/2yl;

    const-string/jumbo v6, "total_notification_taps"

    invoke-virtual {v7, v6}, LX/2yl;->A00(Ljava/lang/String;)V

    .line 96572
    iget-object v7, v1, Lcom/soula2/Conversation;->A2f:LX/2yl;

    .line 96573
    iget-boolean v6, v1, Lcom/soula2/Conversation;->A4S:Z

    if-eqz v6, :cond_33

    const-string v6, "group_message_notification_taps"

    .line 96574
    :goto_9
    invoke-virtual {v7, v6}, LX/2yl;->A00(Ljava/lang/String;)V

    .line 96575
    invoke-virtual {v1}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    move-result-object v7

    const-string v6, "last_notification_keep_in_chat"

    .line 96576
    invoke-virtual {v7, v6, v3}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v6

    if-eqz v6, :cond_2e

    .line 96577
    iget-object v7, v1, Lcom/soula2/Conversation;->A2f:LX/2yl;

    const-string v6, "kic_notification_taps"

    invoke-virtual {v7, v6}, LX/2yl;->A00(Ljava/lang/String;)V

    .line 96578
    iget-boolean v6, v1, Lcom/soula2/Conversation;->A4S:Z

    if-eqz v6, :cond_2e

    .line 96579
    iget-object v7, v1, Lcom/soula2/Conversation;->A2f:LX/2yl;

    const-string v6, "kic_group_notification_taps"

    invoke-virtual {v7, v6}, LX/2yl;->A00(Ljava/lang/String;)V

    .line 96580
    :cond_2e
    move-object/from16 v6, v18

    invoke-virtual {v1, v6}, LX/0kK;->A1n(Ljava/lang/String;)V

    .line 96581
    const v6, 0x7f0d0142

    invoke-virtual {v1, v6}, LX/0kH;->setContentView(I)V

    .line 96582
    move-object/from16 v6, v18

    invoke-virtual {v1, v6}, LX/0kK;->A1m(Ljava/lang/String;)V

    .line 96583
    const v6, 0x7f0a127d

    invoke-static {v1, v6}, LX/00S;->A05(Landroid/app/Activity;I)Landroid/view/View;

    move-result-object v6

    check-cast v6, Landroidx/appcompat/widget/Toolbar;

    .line 96584
    invoke-virtual {v6}, Landroidx/appcompat/widget/Toolbar;->A07()V

    .line 96585
    invoke-virtual {v1, v6}, LX/00i;->A1V(Landroidx/appcompat/widget/Toolbar;)V

    .line 96586
    invoke-virtual {v1}, LX/00i;->A1L()LX/03J;

    move-result-object v6

    invoke-static {v6}, LX/009;->A05(Ljava/lang/Object;)V

    .line 96587
    invoke-virtual {v6, v3}, LX/03J;->A0M(Z)V

    .line 96588
    invoke-virtual {v6, v3}, LX/03J;->A0P(Z)V

    .line 96589
    const v6, 0x7f0a0450

    invoke-virtual {v1, v6}, LX/00i;->findViewById(I)Landroid/view/View;

    move-result-object v7

    check-cast v7, Lcom/soula2/KeyboardPopupLayout;

    iput-object v7, v1, Lcom/soula2/Conversation;->A0w:Lcom/soula2/KeyboardPopupLayout;

    .line 96590
    const v6, 0x7f0601eb

    .line 96591
    invoke-static {v1, v6}, LX/00S;->A00(Landroid/content/Context;I)I

    move-result v6

    .line 96592
    invoke-virtual {v7, v6}, Lcom/soula2/KeyboardPopupLayout;->setKeyboardPopupBackgroundColor(I)V

    .line 96593
    iget-object v6, v1, Lcom/soula2/Conversation;->A4A:LX/01K;

    invoke-interface {v6}, LX/01K;->get()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, LX/0xM;

    const-class v9, LX/2Dm;

    invoke-virtual {v6, v9}, LX/0xM;->A00(Ljava/lang/Class;)LX/0zC;

    move-result-object v8

    check-cast v8, LX/2Dm;

    .line 96594
    iget-object v7, v1, Lcom/soula2/Conversation;->A2r:LX/0l8;

    .line 96595
    new-instance v6, LX/1w2;

    invoke-direct {v6, v1}, LX/1w2;-><init>(Landroid/content/Context;)V

    .line 96596
    iput-object v7, v6, LX/1w2;->A0A:LX/0l8;

    .line 96597
    iput-object v6, v8, LX/2Dm;->A00:LX/1w2;

    .line 96598
    iput-object v6, v1, Lcom/soula2/Conversation;->A0F:Landroid/view/View;

    .line 96599
    iget-object v6, v1, Lcom/soula2/Conversation;->A4A:LX/01K;

    invoke-interface {v6}, LX/01K;->get()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, LX/0xM;

    invoke-virtual {v6, v9}, LX/0xM;->A00(Ljava/lang/Class;)LX/0zC;

    move-result-object v6

    check-cast v6, LX/2Dm;

    .line 96600
    new-instance v7, LX/4Xv;

    invoke-direct {v7, v1}, LX/4Xv;-><init>(Lcom/soula2/Conversation;)V

    .line 96601
    iget-object v6, v6, LX/2Dm;->A00:LX/1w2;

    .line 96602
    iput-object v7, v6, LX/1w2;->A00:LX/2K1;

    .line 96603
    const v6, 0x7f0a0453

    invoke-virtual {v1, v6}, LX/00i;->findViewById(I)Landroid/view/View;

    move-result-object v7

    check-cast v7, Landroid/widget/LinearLayout;

    if-eqz v7, :cond_2f

    .line 96604
    iget-object v6, v1, Lcom/soula2/Conversation;->A0F:Landroid/view/View;

    invoke-virtual {v7, v6, v3}, Landroid/view/ViewGroup;->addView(Landroid/view/View;I)V

    .line 96605
    :cond_2f
    const v6, 0x7f0a05bb

    invoke-virtual {v1, v6}, LX/00i;->findViewById(I)Landroid/view/View;

    move-result-object v6

    iput-object v6, v1, Lcom/soula2/Conversation;->A09:Landroid/view/View;

    .line 96606
    const v6, 0x7f0a0e9c

    invoke-virtual {v1, v6}, LX/00i;->findViewById(I)Landroid/view/View;

    move-result-object v6

    check-cast v6, Lcom/soula2/TextEmojiLabel;

    iput-object v6, v1, Lcom/soula2/Conversation;->A14:Lcom/soula2/TextEmojiLabel;

    .line 96607
    const v6, 0x7f0a0e9d

    invoke-virtual {v1, v6}, LX/00i;->findViewById(I)Landroid/view/View;

    move-result-object v6

    check-cast v6, Landroid/widget/ImageView;

    iput-object v6, v1, Lcom/soula2/Conversation;->A0X:Landroid/widget/ImageView;

    .line 96608
    const v6, 0x7f0a0e9e

    .line 96609
    invoke-virtual {v1, v6}, LX/00i;->findViewById(I)Landroid/view/View;

    move-result-object v9

    check-cast v9, Landroid/widget/ImageView;

    iput-object v9, v1, Lcom/soula2/Conversation;->A0W:Landroid/widget/ImageView;

    .line 96610
    iget-object v8, v1, Lcom/soula2/Conversation;->A23:LX/00z;

    const v6, 0x7f080245

    .line 96611
    invoke-static {v1, v6}, LX/00S;->A04(Landroid/content/Context;I)Landroid/graphics/drawable/Drawable;

    move-result-object v7

    new-instance v6, LX/1qq;

    invoke-direct {v6, v7, v8}, LX/1qq;-><init>(Landroid/graphics/drawable/Drawable;LX/00z;)V

    .line 96612
    invoke-virtual {v9, v6}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 96613
    const v6, 0x7f0a0e9f

    invoke-virtual {v1, v6}, LX/00i;->findViewById(I)Landroid/view/View;

    move-result-object v6

    iput-object v6, v1, Lcom/soula2/Conversation;->A0E:Landroid/view/View;

    .line 96614
    const v6, 0x7f0a03e5

    invoke-virtual {v1, v6}, LX/00i;->findViewById(I)Landroid/view/View;

    move-result-object v6

    check-cast v6, Lcom/soula2/TextEmojiLabel;

    iput-object v6, v1, Lcom/soula2/Conversation;->A13:Lcom/soula2/TextEmojiLabel;

    .line 96615
    const v6, 0x7f0a04ae

    .line 96616
    invoke-virtual {v1, v6}, LX/00i;->findViewById(I)Landroid/view/View;

    move-result-object v7

    new-instance v6, LX/0l3;

    invoke-direct {v6, v7}, LX/0l3;-><init>(Landroid/view/View;)V

    iput-object v6, v1, Lcom/soula2/Conversation;->A3s:LX/0l3;

    .line 96617
    const v6, 0x7f0a0177

    invoke-virtual {v1, v6}, LX/00i;->findViewById(I)Landroid/view/View;

    move-result-object v6

    check-cast v6, Landroid/view/ViewGroup;

    iput-object v6, v1, Lcom/soula2/Conversation;->A0L:Landroid/view/ViewGroup;

    .line 96618
    const v6, 0x7f0a141f

    invoke-virtual {v1, v6}, LX/00i;->findViewById(I)Landroid/view/View;

    move-result-object v6

    check-cast v6, Landroid/view/ViewGroup;

    iput-object v6, v1, Lcom/soula2/Conversation;->A0P:Landroid/view/ViewGroup;

    .line 96619
    const v6, 0x7f0a0e6a

    invoke-virtual {v1, v6}, LX/00i;->findViewById(I)Landroid/view/View;

    move-result-object v6

    check-cast v6, Landroid/view/ViewGroup;

    iput-object v6, v1, Lcom/soula2/Conversation;->A0M:Landroid/view/ViewGroup;

    .line 96620
    const v6, 0x7f0a0e6b

    invoke-virtual {v1, v6}, LX/00i;->findViewById(I)Landroid/view/View;

    move-result-object v6

    check-cast v6, Landroid/view/ViewGroup;

    iput-object v6, v1, Lcom/soula2/Conversation;->A0N:Landroid/view/ViewGroup;

    .line 96621
    const v6, 0x7f0a13b5

    invoke-virtual {v1, v6}, LX/00i;->findViewById(I)Landroid/view/View;

    move-result-object v6

    check-cast v6, Landroid/view/ViewGroup;

    iput-object v6, v1, Lcom/soula2/Conversation;->A0O:Landroid/view/ViewGroup;

    .line 96622
    const v6, 0x7f0a0446

    .line 96623
    invoke-static {v1, v6}, LX/00S;->A05(Landroid/app/Activity;I)Landroid/view/View;

    move-result-object v18

    move-object/from16 v6, v18

    check-cast v6, Lcom/soula2/settings/chat/wallpaper/WallPaperView;

    move-object/from16 v18, v6

    .line 96624
    new-instance v6, LX/4ai;

    invoke-direct {v6, v1}, LX/4ai;-><init>(Lcom/soula2/Conversation;)V

    .line 96625
    move-object/from16 v7, v18

    iput-object v6, v7, Lcom/soula2/settings/chat/wallpaper/WallPaperView;->A01:LX/4qx;

    .line 96626
    invoke-virtual {v1}, LX/0kD;->A2V()Landroid/widget/ListView;

    move-result-object v6

    check-cast v6, Lcom/soula2/conversation/ConversationListView;

    iput-object v6, v1, Lcom/soula2/Conversation;->A1c:Lcom/soula2/conversation/ConversationListView;

    .line 96627
    invoke-virtual {v6, v3}, Landroid/widget/ListView;->setDividerHeight(I)V

    .line 96628
    iget-object v6, v1, Lcom/soula2/Conversation;->A1c:Lcom/soula2/conversation/ConversationListView;

    invoke-virtual {v6}, Landroid/view/View;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    move-result-object v7

    iget-object v6, v1, Lcom/soula2/Conversation;->A4m:Landroid/view/ViewTreeObserver$OnPreDrawListener;

    invoke-virtual {v7, v6}, Landroid/view/ViewTreeObserver;->addOnPreDrawListener(Landroid/view/ViewTreeObserver$OnPreDrawListener;)V

    .line 96629
    iget-object v8, v1, Lcom/soula2/Conversation;->A1c:Lcom/soula2/conversation/ConversationListView;

    .line 96630
    iget-boolean v6, v1, Lcom/soula2/Conversation;->A4Y:Z

    if-eqz v6, :cond_30

    .line 96631
    new-instance v7, Lcom/facebook/redex/IDxDCompatShape26S0100000_2_I0;

    move/from16 v6, v24

    invoke-direct {v7, v1, v6}, Lcom/facebook/redex/IDxDCompatShape26S0100000_2_I0;-><init>(Ljava/lang/Object;I)V

    invoke-static {v8, v7}, LX/01U;->A0i(Landroid/view/View;LX/05F;)V

    .line 96632
    :cond_30
    iget-object v9, v1, LX/0kJ;->A02:LX/0kg;

    iget-object v8, v1, Lcom/soula2/Conversation;->A1c:Lcom/soula2/conversation/ConversationListView;

    const/16 v6, 0x20

    new-instance v7, Lcom/facebook/redex/RunnableRunnableShape1S0100000_I0;

    invoke-direct {v7, v1, v6}, Lcom/facebook/redex/RunnableRunnableShape1S0100000_I0;-><init>(Lcom/soula2/Conversation;I)V

    move-object/from16 v6, v17

    move/from16 v10, v24

    invoke-virtual {v9, v8, v7, v6, v10}, LX/0kg;->A04(Landroid/view/View;Ljava/lang/Runnable;Ljava/lang/String;I)V

    .line 96633
    const v6, 0x7f0a102d

    invoke-virtual {v1, v6}, LX/00i;->findViewById(I)Landroid/view/View;

    move-result-object v6

    check-cast v6, Landroid/widget/ImageButton;

    iput-object v6, v1, Lcom/soula2/Conversation;->A0U:Landroid/widget/ImageButton;

    .line 96634
    const v6, 0x7f0a057a

    invoke-virtual {v1, v6}, LX/00i;->findViewById(I)Landroid/view/View;

    move-result-object v17

    move-object/from16 v6, v17

    check-cast v6, Landroid/widget/ImageButton;

    move-object/from16 v17, v6

    .line 96635
    iget-object v10, v1, Lcom/soula2/Conversation;->A0U:Landroid/widget/ImageButton;

    iget-object v9, v1, Lcom/soula2/Conversation;->A23:LX/00z;

    const v8, 0x7f080656

    .line 96636
    invoke-static {v1, v8}, LX/00S;->A04(Landroid/content/Context;I)Landroid/graphics/drawable/Drawable;

    move-result-object v7

    new-instance v6, LX/1qq;

    invoke-direct {v6, v7, v9}, LX/1qq;-><init>(Landroid/graphics/drawable/Drawable;LX/00z;)V

    .line 96637
    invoke-virtual {v10, v6}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 96638
    iget-object v7, v1, Lcom/soula2/Conversation;->A23:LX/00z;

    .line 96639
    invoke-static {v1, v8}, LX/00S;->A04(Landroid/content/Context;I)Landroid/graphics/drawable/Drawable;

    move-result-object v8

    new-instance v6, LX/1qq;

    invoke-direct {v6, v8, v7}, LX/1qq;-><init>(Landroid/graphics/drawable/Drawable;LX/00z;)V

    .line 96640
    move-object/from16 v7, v17

    invoke-virtual {v7, v6}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 96641
    const v6, 0x7f0a08a5

    invoke-static {v1, v6}, LX/00S;->A05(Landroid/app/Activity;I)Landroid/view/View;

    move-result-object v7

    iput-object v7, v1, Lcom/soula2/Conversation;->A0A:Landroid/view/View;

    .line 96642
    const v35, 0x7f080377

    move/from16 v6, v35

    invoke-virtual {v7, v6}, Landroid/view/View;->setBackgroundResource(I)V

    .line 96643
    iget-object v6, v1, Lcom/soula2/Conversation;->A0A:Landroid/view/View;

    invoke-virtual {v6, v3, v3, v3, v3}, Landroid/view/View;->setPadding(IIII)V

    .line 96644
    const v6, 0x7f0a122c

    invoke-static {v1, v6}, LX/00S;->A05(Landroid/app/Activity;I)Landroid/view/View;

    move-result-object v9

    .line 96645
    invoke-virtual {v9}, Landroid/view/View;->getPaddingLeft()I

    move-result v7

    invoke-virtual {v9}, Landroid/view/View;->getPaddingRight()I

    move-result v6

    invoke-static {v7, v6}, Ljava/lang/Math;->max(II)I

    move-result v8

    .line 96646
    invoke-virtual {v9}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v7

    check-cast v7, Landroid/widget/RelativeLayout$LayoutParams;

    .line 96647
    iget-object v6, v1, Lcom/soula2/Conversation;->A23:LX/00z;

    .line 96648
    invoke-virtual {v6}, LX/00z;->A03()LX/1Ks;

    move-result-object v6

    .line 96649
    iget-boolean v6, v6, LX/1Ks;->A06:Z

    .line 96650
    xor-int/lit8 v6, v6, 0x1

    .line 96651
    if-eqz v6, :cond_32

    .line 96652
    iput v8, v7, Landroid/widget/RelativeLayout$LayoutParams;->rightMargin:I

    .line 96653
    :goto_a
    invoke-virtual {v9, v7}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 96654
    sget v25, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v23, 0x15

    move/from16 v7, v25

    move/from16 v6, v23

    if-lt v7, v6, :cond_31

    .line 96655
    new-instance v7, LX/3Kr;

    invoke-direct {v7, v1}, LX/3Kr;-><init>(Lcom/soula2/Conversation;)V

    .line 96656
    iget-object v6, v1, Lcom/soula2/Conversation;->A0U:Landroid/widget/ImageButton;

    invoke-virtual {v6, v7}, Landroid/widget/ImageButton;->setOutlineProvider(Landroid/view/ViewOutlineProvider;)V

    .line 96657
    :cond_31
    const v6, 0x7f0a08a2

    invoke-static {v1, v6}, LX/00S;->A05(Landroid/app/Activity;I)Landroid/view/View;

    move-result-object v6

    check-cast v6, Landroid/widget/ImageButton;

    iput-object v6, v1, Lcom/soula2/Conversation;->A0T:Landroid/widget/ImageButton;

    .line 96658
    invoke-virtual {v6, v3}, Landroid/view/View;->setVisibility(I)V

    .line 96659
    iget-object v7, v1, Lcom/soula2/Conversation;->A0T:Landroid/widget/ImageButton;

    new-instance v6, Lcom/whatsapp/util/ViewOnClickCListenerShape0S0100000_I0;

    move/from16 v8, v24

    invoke-direct {v6, v1, v8}, Lcom/whatsapp/util/ViewOnClickCListenerShape0S0100000_I0;-><init>(Ljava/lang/Object;I)V

    invoke-virtual {v7, v6}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 96660
    iget-object v7, v1, Lcom/soula2/Conversation;->A23:LX/00z;

    iget-object v6, v1, Lcom/soula2/Conversation;->A0w:Lcom/soula2/KeyboardPopupLayout;

    new-instance v8, LX/0mP;

    invoke-direct {v8, v6, v7}, LX/0mP;-><init>(Landroid/view/View;LX/00z;)V

    .line 96661
    new-instance v7, Lcom/facebook/redex/RunnableRunnableShape1S0100000_I0;

    invoke-direct {v7, v8, v13}, Lcom/facebook/redex/RunnableRunnableShape1S0100000_I0;-><init>(Ljava/lang/Object;I)V

    .line 96662
    const v6, 0x7f0a13b0

    invoke-static {v1, v6}, LX/00S;->A05(Landroid/app/Activity;I)Landroid/view/View;

    move-result-object v9

    check-cast v9, Landroid/widget/ImageButton;

    iput-object v9, v1, Lcom/soula2/Conversation;->A0V:Landroid/widget/ImageButton;

    .line 96663
    new-instance v6, Lcom/facebook/redex/IDxTListenerShape28S0300000_2_I0;

    invoke-direct {v6, v8, v7, v1, v3}, Lcom/facebook/redex/IDxTListenerShape28S0300000_2_I0;-><init>(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;I)V

    invoke-virtual {v9, v6}, Landroid/view/View;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 96664
    iget-object v7, v1, Lcom/soula2/Conversation;->A0V:Landroid/widget/ImageButton;

    new-instance v6, Lcom/facebook/redex/IDxDCompatShape26S0100000_2_I0;

    invoke-direct {v6, v1, v4}, Lcom/facebook/redex/IDxDCompatShape26S0100000_2_I0;-><init>(Ljava/lang/Object;I)V

    invoke-static {v7, v6}, LX/01U;->A0i(Landroid/view/View;LX/05F;)V

    .line 96665
    iget-object v6, v1, LX/0kH;->A08:LX/01e;

    .line 96666
    const-string v7, "android.hardware.type.featurephone"

    .line 96667
    iget-object v6, v6, LX/01e;->A0O:LX/01k;

    .line 96668
    iget-object v6, v6, LX/01k;->A00:Landroid/content/Context;

    .line 96669
    invoke-virtual {v6}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v6

    invoke-virtual {v6, v7}, Landroid/content/pm/PackageManager;->hasSystemFeature(Ljava/lang/String;)Z

    move-result v6

    .line 96670
    iput-boolean v6, v1, Lcom/soula2/Conversation;->A4R:Z

    goto :goto_b

    .line 96671
    :cond_32
    iput v8, v7, Landroid/widget/RelativeLayout$LayoutParams;->leftMargin:I

    goto :goto_a

    .line 96672
    :cond_33
    const-string/jumbo v6, "user_message_notification_taps"

    goto/16 :goto_9

    .line 96673
    :cond_34
    iget-object v7, v1, Lcom/soula2/Conversation;->A2F:LX/0rK;

    .line 96674
    iget-object v6, v1, Lcom/soula2/Conversation;->A2r:LX/0l8;

    .line 96675
    invoke-virtual {v7, v6, v15}, LX/0rK;->A01(LX/0l8;I)V

    .line 96676
    iget-object v6, v5, LX/1be;->A02:Ljava/lang/String;

    iput-object v6, v1, Lcom/soula2/Conversation;->A4G:Ljava/lang/String;

    .line 96677
    iget-object v6, v5, LX/1be;->A04:Ljava/lang/String;

    iput-object v6, v1, Lcom/soula2/Conversation;->A4H:Ljava/lang/String;

    goto/16 :goto_8

    .line 96678
    :goto_b
    if-eqz v6, :cond_35

    .line 96679
    iget-object v7, v1, Lcom/soula2/Conversation;->A0V:Landroid/widget/ImageButton;

    new-instance v6, Lcom/facebook/redex/IDxKListenerShape266S0100000_2_I0;

    invoke-direct {v6, v1, v4}, Lcom/facebook/redex/IDxKListenerShape266S0100000_2_I0;-><init>(Ljava/lang/Object;I)V

    invoke-virtual {v7, v6}, Landroid/view/View;->setOnKeyListener(Landroid/view/View$OnKeyListener;)V

    .line 96680
    :cond_35
    const v6, 0x7f0a02d5

    invoke-static {v1, v6}, LX/00S;->A05(Landroid/app/Activity;I)Landroid/view/View;

    move-result-object v7

    check-cast v7, Landroid/widget/ImageButton;

    iput-object v7, v1, Lcom/soula2/Conversation;->A0S:Landroid/widget/ImageButton;

    .line 96681
    new-instance v6, Lcom/facebook/redex/ViewOnClickCListenerShape6S0100000_I0;

    move/from16 v8, v21

    invoke-direct {v6, v1, v8}, Lcom/facebook/redex/ViewOnClickCListenerShape6S0100000_I0;-><init>(Ljava/lang/Object;I)V

    invoke-virtual {v7, v6}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 96682
    iget-object v7, v1, Lcom/soula2/Conversation;->A0S:Landroid/widget/ImageButton;

    new-instance v6, Lcom/facebook/redex/IDxCListenerShape206S0100000_2_I0;

    invoke-direct {v6, v1, v3}, Lcom/facebook/redex/IDxCListenerShape206S0100000_2_I0;-><init>(Ljava/lang/Object;I)V

    invoke-virtual {v7, v6}, Landroid/view/View;->setOnLongClickListener(Landroid/view/View$OnLongClickListener;)V

    .line 96683
    const v6, 0x7f0a01bb

    .line 96684
    invoke-virtual {v1, v6}, LX/00i;->findViewById(I)Landroid/view/View;

    move-result-object v22

    move-object/from16 v6, v22

    check-cast v6, Landroid/view/ViewGroup;

    move-object/from16 v22, v6

    .line 96685
    new-instance v13, LX/0mI;

    invoke-direct {v13, v1}, LX/0mI;-><init>(Lcom/soula2/Conversation;)V

    .line 96686
    iget-object v6, v1, LX/0kJ;->A05:LX/0lO;

    move-object/from16 v38, v6

    iget-object v6, v1, Lcom/soula2/Conversation;->A2N:LX/0uN;

    move-object/from16 v37, v6

    iget-object v6, v1, Lcom/soula2/Conversation;->A2Z:LX/0y5;

    move-object/from16 v36, v6

    iget-object v6, v1, Lcom/soula2/Conversation;->A30:LX/134;

    move-object/from16 v20, v6

    iget-object v6, v1, Lcom/soula2/Conversation;->A1K:LX/0wh;

    move-object/from16 v19, v6

    iget-object v14, v1, Lcom/soula2/Conversation;->A2a:LX/1Ea;

    iget-object v12, v1, Lcom/soula2/Conversation;->A1U:LX/0za;

    iget-object v11, v1, LX/0kH;->A09:LX/0kh;

    iget-object v10, v1, Lcom/soula2/Conversation;->A1P:LX/15M;

    iget-object v9, v1, Lcom/soula2/Conversation;->A2c:LX/0zy;

    iget-object v8, v1, LX/0kB;->A0N:LX/0nR;

    .line 96687
    const/16 v6, 0x29

    new-instance v7, Lcom/facebook/redex/RunnableRunnableShape1S0100000_I0;

    invoke-direct {v7, v13, v6}, Lcom/facebook/redex/RunnableRunnableShape1S0100000_I0;-><init>(Ljava/lang/Object;I)V

    .line 96688
    const/16 v15, 0x2a

    new-instance v6, Lcom/facebook/redex/RunnableRunnableShape1S0100000_I0;

    invoke-direct {v6, v13, v15}, Lcom/facebook/redex/RunnableRunnableShape1S0100000_I0;-><init>(Ljava/lang/Object;I)V

    new-instance v46, LX/2S0;

    move-object/from16 v47, v1

    move-object/from16 v48, v1

    move-object/from16 v49, v19

    move-object/from16 v50, v10

    move-object/from16 v51, v12

    move-object/from16 v52, v11

    move-object/from16 v53, v37

    move-object/from16 v54, v8

    move-object/from16 v55, v36

    move-object/from16 v56, v14

    move-object/from16 v57, v9

    move-object/from16 v58, v20

    move-object/from16 v59, v38

    move-object/from16 v60, v7

    move-object/from16 v61, v6

    invoke-direct/range {v46 .. v61}, LX/2S0;-><init>(LX/00i;LX/0kM;LX/0wh;LX/15M;LX/0za;LX/0kh;LX/0uN;LX/0nR;LX/0y5;LX/1Ea;LX/0zy;LX/134;LX/0lO;Ljava/lang/Runnable;Ljava/lang/Runnable;)V

    .line 96689
    iget-object v7, v1, Lcom/soula2/Conversation;->A0h:LX/2Dt;

    iget-object v6, v1, Lcom/soula2/Conversation;->A2r:LX/0l8;

    move-object/from16 v54, v6

    iget-object v6, v1, Lcom/soula2/Conversation;->A1c:Lcom/soula2/conversation/ConversationListView;

    move-object/from16 v38, v6

    .line 96690
    iget-object v8, v7, LX/2Dt;->A00:LX/2A6;

    .line 96691
    iget-object v6, v8, LX/2A6;->A03:LX/2fo;

    .line 96692
    iget-object v7, v6, LX/2fo;->A04:LX/01P;

    .line 96693
    invoke-interface {v7}, LX/01P;->get()Ljava/lang/Object;

    move-result-object v20

    move-object/from16 v7, v20

    check-cast v7, LX/0kj;

    move-object/from16 v20, v7

    .line 96694
    iget-object v7, v6, LX/2fo;->ABS:LX/01P;

    .line 96695
    invoke-interface {v7}, LX/01P;->get()Ljava/lang/Object;

    move-result-object v15

    check-cast v15, LX/0nN;

    .line 96696
    iget-object v7, v6, LX/2fo;->A3f:LX/01P;

    .line 96697
    invoke-interface {v7}, LX/01P;->get()Ljava/lang/Object;

    move-result-object v14

    check-cast v14, LX/0uN;

    .line 96698
    iget-object v7, v6, LX/2fo;->AJ3:LX/01P;

    .line 96699
    invoke-interface {v7}, LX/01P;->get()Ljava/lang/Object;

    move-result-object v12

    check-cast v12, LX/0nB;

    .line 96700
    iget-object v7, v6, LX/2fo;->AKW:LX/01P;

    .line 96701
    invoke-interface {v7}, LX/01P;->get()Ljava/lang/Object;

    move-result-object v11

    check-cast v11, LX/0y5;

    .line 96702
    iget-object v7, v8, LX/2A6;->A01:LX/29l;

    .line 96703
    invoke-virtual {v7}, LX/29l;->A08()LX/0zB;

    move-result-object v52

    .line 96704
    iget-object v7, v6, LX/2fo;->A4T:LX/01P;

    .line 96705
    invoke-interface {v7}, LX/01P;->get()Ljava/lang/Object;

    move-result-object v10

    check-cast v10, LX/0nL;

    .line 96706
    iget-object v7, v6, LX/2fo;->A1k:LX/01P;

    .line 96707
    invoke-interface {v7}, LX/01P;->get()Ljava/lang/Object;

    move-result-object v9

    check-cast v9, LX/0wh;

    .line 96708
    iget-object v7, v6, LX/2fo;->A9D:LX/01P;

    .line 96709
    invoke-interface {v7}, LX/01P;->get()Ljava/lang/Object;

    move-result-object v8

    check-cast v8, LX/0vp;

    .line 96710
    iget-object v7, v6, LX/2fo;->ANB:LX/01P;

    .line 96711
    invoke-interface {v7}, LX/01P;->get()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, LX/0vO;

    .line 96712
    iget-object v6, v6, LX/2fo;->A2q:LX/01P;

    .line 96713
    invoke-interface {v6}, LX/01P;->get()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, LX/0oZ;

    new-instance v19, LX/1ld;

    move-object/from16 v36, v19

    move-object/from16 v37, v22

    move-object/from16 v39, v1

    move-object/from16 v40, v13

    move-object/from16 v41, v1

    move-object/from16 v42, v15

    move-object/from16 v43, v12

    move-object/from16 v44, v9

    move-object/from16 v45, v10

    move-object/from16 v47, v6

    move-object/from16 v48, v14

    move-object/from16 v49, v11

    move-object/from16 v50, v7

    move-object/from16 v51, v20

    move-object/from16 v53, v8

    invoke-direct/range {v36 .. v54}, LX/1ld;-><init>(Landroid/view/ViewGroup;Landroid/widget/ListView;LX/00i;LX/0mI;LX/0kM;LX/0nN;LX/0nB;LX/0wh;LX/0nL;LX/2S0;LX/0oZ;LX/0uN;LX/0y5;LX/0vO;LX/0kj;LX/0zB;LX/0vp;LX/0l8;)V

    .line 96714
    move-object/from16 v6, v19

    iput-object v6, v1, Lcom/soula2/Conversation;->A1q:LX/1ld;

    .line 96715
    const/16 v22, 0x17

    new-instance v53, LX/48R;

    invoke-direct/range {v53 .. v53}, LX/48R;-><init>()V

    .line 96716
    iget-object v6, v1, LX/0kF;->A05:LX/0lm;

    move-object/from16 v54, v6

    iget-object v6, v1, LX/0kH;->A0C:LX/0kj;

    move-object/from16 v62, v6

    iget-object v6, v1, Lcom/soula2/Conversation;->A15:LX/0yv;

    move-object/from16 v45, v6

    iget-object v6, v1, LX/0kH;->A05:LX/0li;

    move-object/from16 v42, v6

    iget-object v6, v1, LX/0kJ;->A05:LX/0lO;

    move-object/from16 v72, v6

    iget-object v6, v1, Lcom/soula2/Conversation;->A0z:LX/0vi;

    move-object/from16 v43, v6

    iget-object v6, v1, LX/0kB;->A0V:LX/0pG;

    move-object/from16 v64, v6

    iget-object v6, v1, LX/0kH;->A0B:LX/17a;

    move-object/from16 v61, v6

    iget-object v6, v1, LX/0kB;->A03:LX/0oS;

    move-object/from16 v46, v6

    iget-object v6, v1, Lcom/soula2/Conversation;->A2h:LX/0zB;

    move-object/from16 v63, v6

    iget-object v6, v1, LX/0kF;->A00:LX/154;

    move-object/from16 v38, v6

    iget-object v6, v1, LX/0kB;->A07:LX/0nL;

    move-object/from16 v48, v6

    iget-object v6, v1, LX/0kB;->A0T:LX/120;

    move-object/from16 v60, v6

    iget-object v6, v1, LX/0kB;->A0o:LX/15f;

    move-object/from16 v70, v6

    iget-object v6, v1, LX/0kB;->A09:LX/0nS;

    move-object/from16 v49, v6

    iget-object v6, v1, Lcom/soula2/Conversation;->A23:LX/00z;

    move-object/from16 v56, v6

    iget-object v6, v1, Lcom/soula2/Conversation;->A1K:LX/0wh;

    move-object/from16 v47, v6

    iget-object v6, v1, Lcom/soula2/Conversation;->A2U:LX/10g;

    move-object/from16 v59, v6

    iget-object v6, v1, LX/0kB;->A0X:LX/0vp;

    move-object/from16 v66, v6

    iget-object v6, v1, Lcom/soula2/Conversation;->A1k:LX/18D;

    move-object/from16 v51, v6

    iget-object v6, v1, LX/0kB;->A0Y:LX/11J;

    move-object/from16 v67, v6

    iget-object v6, v1, Lcom/soula2/Conversation;->A12:LX/1Fo;

    move-object/from16 v44, v6

    iget-object v6, v1, LX/0kB;->A0W:LX/0uO;

    move-object/from16 v20, v6

    iget-object v6, v1, LX/0kH;->A09:LX/0kh;

    move-object/from16 v19, v6

    iget-object v15, v1, Lcom/soula2/Conversation;->A1j:LX/181;

    iget-object v14, v1, Lcom/soula2/Conversation;->A3m:LX/17x;

    iget-object v13, v1, LX/0kB;->A0N:LX/0nR;

    iget-object v12, v1, Lcom/soula2/Conversation;->A2O:LX/0w1;

    iget-object v11, v1, LX/0kB;->A0d:LX/18e;

    new-instance v10, Lcom/facebook/redex/IDxDListenerShape331S0100000_2_I0;

    invoke-direct {v10, v1, v3}, Lcom/facebook/redex/IDxDListenerShape331S0100000_2_I0;-><init>(Ljava/lang/Object;I)V

    .line 96717
    new-instance v9, LX/3zR;

    invoke-direct {v9, v1}, LX/3zR;-><init>(LX/0kB;)V

    .line 96718
    iget-object v8, v1, Lcom/soula2/Conversation;->A4t:LX/3xf;

    iget-object v7, v1, Lcom/soula2/Conversation;->A2r:LX/0l8;

    new-instance v6, LX/2iK;

    move-object/from16 v36, v6

    move-object/from16 v37, v1

    move-object/from16 v39, v8

    move-object/from16 v40, v10

    move-object/from16 v50, v15

    move-object/from16 v52, v9

    move-object/from16 v55, v19

    move-object/from16 v57, v12

    move-object/from16 v58, v13

    move-object/from16 v65, v20

    move-object/from16 v68, v7

    move-object/from16 v69, v11

    move-object/from16 v71, v14

    invoke-direct/range {v36 .. v72}, LX/2iK;-><init>(LX/00i;LX/154;LX/3xf;LX/1jY;LX/0kM;LX/0li;LX/0vi;LX/1Fo;LX/0yv;LX/0oS;LX/0wh;LX/0nL;LX/0nS;LX/181;LX/18D;LX/3zR;LX/48R;LX/0lm;LX/0kh;LX/00z;LX/0w1;LX/0nR;LX/10g;LX/120;LX/17a;LX/0kj;LX/0zB;LX/0pG;LX/0uO;LX/0vp;LX/11J;LX/0l8;LX/18e;LX/15f;LX/17x;LX/0lO;)V

    iput-object v6, v1, Lcom/soula2/Conversation;->A1p:LX/2iK;

    .line 96719
    iget-object v7, v1, Lcom/soula2/Conversation;->A56:Ljava/util/Set;

    move-object/from16 v115, v7

    invoke-interface {v7, v6}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 96720
    iget-object v7, v1, Lcom/soula2/Conversation;->A2E:LX/0rJ;

    iget-object v6, v1, Lcom/soula2/Conversation;->A2b:LX/0mJ;

    iget-object v8, v1, Lcom/soula2/Conversation;->A2r:LX/0l8;

    invoke-static {v8}, LX/176;->A04(LX/0l8;)Ljava/lang/String;

    move-result-object v11

    .line 96721
    if-eqz v6, :cond_36

    .line 96722
    const-class v8, Lcom/whatsapp/jid/UserJid;

    invoke-virtual {v6, v8}, LX/0mJ;->A0A(Ljava/lang/Class;)Lcom/whatsapp/jid/Jid;

    move-result-object v10

    .line 96723
    iget-object v9, v7, LX/0rJ;->A04:Ljava/util/HashMap;

    invoke-virtual {v9, v10}, Ljava/util/AbstractMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v7

    if-nez v7, :cond_36

    if-eqz v10, :cond_36

    .line 96724
    invoke-virtual {v6}, LX/0mJ;->A0I()Z

    move-result v8

    const/16 v7, 0x1c

    .line 96725
    new-instance v6, LX/204;

    invoke-direct {v6, v7, v11, v8}, LX/204;-><init>(ILjava/lang/String;Z)V

    .line 96726
    invoke-virtual {v9, v10, v6}, Ljava/util/AbstractMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 96727
    :cond_36
    iget-object v11, v0, LX/48c;->A0D:Ljava/lang/String;

    invoke-static {v11}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v6

    if-nez v6, :cond_37

    iget-object v6, v5, LX/1be;->A05:Ljava/lang/String;

    .line 96728
    invoke-static {v6}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v6

    if-nez v6, :cond_37

    goto :goto_c

    .line 96729
    :cond_37
    sget-object v8, Lcom/soula2/Conversation;->A5G:Ljava/util/HashMap;

    iget-object v7, v1, Lcom/soula2/Conversation;->A2b:LX/0mJ;

    const-class v6, LX/0l8;

    .line 96730
    invoke-virtual {v7, v6}, LX/0mJ;->A0A(Ljava/lang/Class;)Lcom/whatsapp/jid/Jid;

    move-result-object v6

    invoke-virtual {v8, v6}, Ljava/util/AbstractMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v6

    const/4 v8, 0x0

    if-eqz v6, :cond_38

    :goto_c
    const/4 v8, 0x1

    .line 96731
    :cond_38
    iget-object v7, v1, Lcom/soula2/Conversation;->A2E:LX/0rJ;

    iget-object v6, v1, Lcom/soula2/Conversation;->A2r:LX/0l8;

    invoke-static {v6}, Lcom/whatsapp/jid/UserJid;->of(Lcom/whatsapp/jid/Jid;)Lcom/whatsapp/jid/UserJid;

    move-result-object v6

    .line 96732
    if-eqz v6, :cond_39

    .line 96733
    invoke-virtual {v7, v6}, LX/0rJ;->A00(Lcom/whatsapp/jid/UserJid;)LX/204;

    move-result-object v6

    .line 96734
    iput-boolean v8, v6, LX/204;->A01:Z

    .line 96735
    :cond_39
    move-object/from16 v6, v27

    invoke-virtual {v1, v6}, LX/0kK;->A1n(Ljava/lang/String;)V

    .line 96736
    const-string v6, "conversation/createconversation"

    new-instance v20, LX/1L4;

    move-object/from16 v7, v20

    invoke-direct {v7, v6}, LX/1L4;-><init>(Ljava/lang/String;)V

    .line 96737
    invoke-virtual {v1}, Lcom/soula2/Conversation;->A2s()V

    .line 96738
    iget-object v14, v1, Lcom/soula2/Conversation;->A2r:LX/0l8;

    iget-object v13, v1, LX/0kB;->A0F:LX/1Mq;

    iget-object v12, v1, LX/0kF;->A05:LX/0lm;

    iget-object v10, v1, LX/0kH;->A0C:LX/0kj;

    iget-object v9, v1, LX/0kH;->A03:LX/0nf;

    iget-object v8, v1, Lcom/soula2/Conversation;->A3M:LX/0v9;

    iget-object v7, v1, LX/0kB;->A0M:LX/0nZ;

    iget-object v6, v1, Lcom/soula2/Conversation;->A2x:LX/14K;

    new-instance v19, LX/0me;

    move-object/from16 v36, v19

    move-object/from16 v38, v9

    move-object/from16 v39, v13

    move-object/from16 v40, v1

    move-object/from16 v41, v12

    move-object/from16 v42, v7

    move-object/from16 v43, v10

    move-object/from16 v44, v14

    move-object/from16 v45, v6

    move-object/from16 v46, v8

    invoke-direct/range {v36 .. v46}, LX/0me;-><init>(LX/00i;LX/0nf;LX/1Mq;LX/0kP;LX/0lm;LX/0nZ;LX/0kj;LX/0l8;LX/14K;LX/0v9;)V

    .line 96739
    iget-object v6, v1, Lcom/soula2/Conversation;->A4f:Landroid/database/DataSetObserver;

    move-object/from16 v7, v19

    invoke-virtual {v7, v6}, Landroid/widget/BaseAdapter;->registerDataSetObserver(Landroid/database/DataSetObserver;)V

    .line 96740
    const v6, 0x7f0a0641

    invoke-virtual {v1, v6}, LX/00i;->findViewById(I)Landroid/view/View;

    move-result-object v8

    check-cast v8, Lcom/soula2/mentions/MentionableEntry;

    iput-object v8, v1, Lcom/soula2/Conversation;->A2v:Lcom/soula2/mentions/MentionableEntry;

    .line 96741
    new-instance v7, LX/31d;

    invoke-direct {v7}, LX/31d;-><init>()V

    .line 96742
    new-instance v6, Lcom/facebook/redex/IDxCListenerShape95S0200000_1_I0;

    invoke-direct {v6, v1, v3, v7}, Lcom/facebook/redex/IDxCListenerShape95S0200000_1_I0;-><init>(Ljava/lang/Object;ILjava/lang/Object;)V

    .line 96743
    iput-object v6, v8, Lcom/soula2/mentions/MentionableEntry;->A0D:LX/4tZ;

    .line 96744
    move/from16 v6, v35

    invoke-virtual {v1, v6}, Lcom/soula2/Conversation;->A2x(I)V

    .line 96745
    iget-boolean v6, v1, Lcom/soula2/Conversation;->A4S:Z

    if-eqz v6, :cond_3a

    iget-object v7, v1, Lcom/soula2/Conversation;->A2N:LX/0uN;

    iget-object v6, v1, Lcom/soula2/Conversation;->A2r:LX/0l8;

    .line 96746
    invoke-static {v6}, LX/0nO;->A02(Lcom/whatsapp/jid/Jid;)LX/0nO;

    move-result-object v6

    invoke-virtual {v7, v6}, LX/0uN;->A02(Lcom/whatsapp/jid/GroupJid;)I

    move-result v7

    move/from16 v6, v28

    if-eq v7, v6, :cond_3a

    .line 96747
    const v6, 0x7f0a0a30

    invoke-static {v1, v6}, LX/00S;->A05(Landroid/app/Activity;I)Landroid/view/View;

    move-result-object v7

    check-cast v7, Landroid/widget/FrameLayout;

    iput-object v7, v1, Lcom/soula2/Conversation;->A0Q:Landroid/widget/FrameLayout;

    .line 96748
    iget-object v6, v1, Lcom/soula2/Conversation;->A2v:Lcom/soula2/mentions/MentionableEntry;

    new-instance v8, Lcom/facebook/redex/IDxCListenerShape401S0100000_2_I0;

    invoke-direct {v8, v1, v3}, Lcom/facebook/redex/IDxCListenerShape401S0100000_2_I0;-><init>(Ljava/lang/Object;I)V

    .line 96749
    iput-object v8, v6, Lcom/soula2/mentions/MentionableEntry;->A0A:LX/1l8;

    .line 96750
    iget-object v8, v1, Lcom/soula2/Conversation;->A2r:LX/0l8;

    invoke-static {v8}, LX/0nO;->A02(Lcom/whatsapp/jid/Jid;)LX/0nO;

    move-result-object v37

    move-object/from16 v35, v6

    move-object/from16 v36, v7

    move/from16 v38, v3

    move/from16 v39, v3

    move/from16 v40, v4

    invoke-virtual/range {v35 .. v40}, Lcom/soula2/mentions/MentionableEntry;->A0E(Landroid/view/ViewGroup;LX/0nO;ZZZ)V

    .line 96751
    :cond_3a
    invoke-virtual {v1}, Lcom/soula2/Conversation;->A2j()V

    .line 96752
    iget-object v9, v1, Lcom/soula2/Conversation;->A2v:Lcom/soula2/mentions/MentionableEntry;

    new-array v8, v4, [Landroid/text/InputFilter;

    iget-object v7, v1, Lcom/soula2/Conversation;->A42:LX/0kz;

    new-instance v6, LX/4Mn;

    invoke-direct {v6, v7}, LX/4Mn;-><init>(LX/0kz;)V

    aput-object v6, v8, v3

    invoke-virtual {v9, v8}, Landroid/widget/TextView;->setFilters([Landroid/text/InputFilter;)V

    .line 96753
    iget-object v7, v1, Lcom/soula2/Conversation;->A2v:Lcom/soula2/mentions/MentionableEntry;

    const v6, 0x7f0a08a5

    invoke-virtual {v1, v6}, LX/00i;->findViewById(I)Landroid/view/View;

    move-result-object v6

    .line 96754
    iput-object v6, v7, Lcom/soula2/mentions/MentionableEntry;->A05:Landroid/view/View;

    .line 96755
    const v6, 0x7f0a0e5f

    invoke-virtual {v1, v6}, LX/00i;->findViewById(I)Landroid/view/View;

    move-result-object v6

    check-cast v6, Landroid/widget/FrameLayout;

    iput-object v6, v1, Lcom/soula2/Conversation;->A0R:Landroid/widget/FrameLayout;

    .line 96756
    invoke-virtual {v6, v2}, Landroid/view/View;->setVisibility(I)V

    .line 96757
    iget-object v6, v1, Lcom/soula2/Conversation;->A1c:Lcom/soula2/conversation/ConversationListView;

    move-object/from16 v7, v19

    invoke-virtual {v6, v7}, Landroid/widget/AbsListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 96758
    iget-boolean v6, v0, LX/48c;->A0V:Z

    .line 96759
    iget-object v7, v1, Lcom/soula2/Conversation;->A32:LX/0uh;

    if-eqz v6, :cond_3d

    .line 96760
    iget-object v6, v1, Lcom/soula2/Conversation;->A2r:LX/0l8;

    invoke-virtual {v7, v6}, LX/0uh;->A09(LX/0l8;)V

    .line 96761
    iput-boolean v4, v1, Lcom/soula2/Conversation;->A4a:Z

    .line 96762
    :goto_d
    iget-boolean v6, v0, LX/48c;->A0U:Z

    if-eqz v6, :cond_3b

    .line 96763
    iget-object v6, v1, Lcom/soula2/Conversation;->A33:LX/0ui;

    invoke-virtual {v6}, LX/0ui;->A02()V

    :cond_3b
    const-string v6, "conversation/createconv/statusbar/cancel"

    .line 96764
    invoke-static {v6}, Lcom/whatsapp/util/Log;->d(Ljava/lang/String;)V

    .line 96765
    iput-boolean v3, v1, Lcom/soula2/Conversation;->A4c:Z

    .line 96766
    iget-boolean v6, v1, Lcom/soula2/Conversation;->A4U:Z

    if-nez v6, :cond_3c

    .line 96767
    iget-object v7, v1, LX/0kB;->A0R:LX/18o;

    iget-object v6, v1, Lcom/soula2/Conversation;->A4w:LX/0t1;

    invoke-virtual {v7, v6}, LX/0pD;->A03(Ljava/lang/Object;)V

    .line 96768
    iput-boolean v4, v1, Lcom/soula2/Conversation;->A4U:Z

    .line 96769
    :cond_3c
    iget-boolean v6, v0, LX/48c;->A0X:Z

    if-eqz v6, :cond_4a

    iget-boolean v6, v1, Lcom/soula2/Conversation;->A4K:Z

    if-nez v6, :cond_4a

    iget-boolean v6, v0, LX/48c;->A0R:Z

    if-eqz v6, :cond_4a

    .line 96770
    iget-object v6, v1, Lcom/soula2/Conversation;->A1e:LX/194;

    .line 96771
    iget-object v7, v6, LX/194;->A00:Ljava/util/HashMap;

    .line 96772
    iget-object v6, v1, Lcom/soula2/Conversation;->A2r:LX/0l8;

    invoke-virtual {v7, v6}, Ljava/util/AbstractMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 96773
    iget-object v6, v1, Lcom/soula2/Conversation;->A1c:Lcom/soula2/conversation/ConversationListView;

    .line 96774
    iput-boolean v4, v6, Lcom/soula2/conversation/ConversationListView;->A0F:Z

    .line 96775
    iget-object v10, v0, LX/48c;->A0N:Ljava/util/ArrayList;

    if-eqz v10, :cond_3e

    goto :goto_e

    .line 96776
    :cond_3d
    invoke-virtual {v7, v4}, LX/0uh;->A0E(Z)V

    goto :goto_d
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_4

    .line 96777
    :goto_e
    :try_start_7
    iget-object v11, v1, Lcom/soula2/Conversation;->A11:LX/0wg;

    iget-object v6, v1, Lcom/soula2/Conversation;->A2r:LX/0l8;

    .line 96778
    invoke-static {v6}, Ljava/util/Collections;->singletonList(Ljava/lang/Object;)Ljava/util/List;

    move-result-object v41

    iget-object v9, v0, LX/48c;->A0G:Ljava/lang/String;

    iget v8, v0, LX/48c;->A01:I

    iget-boolean v7, v0, LX/48c;->A0T:Z

    new-instance v6, LX/4WE;

    invoke-direct {v6, v1}, LX/4WE;-><init>(Lcom/soula2/Conversation;)V

    .line 96779
    move-object/from16 v35, v11

    move-object/from16 v36, v1

    move-object/from16 v37, v1

    move-object/from16 v38, v6

    move-object/from16 v39, v26

    move-object/from16 v40, v9

    move-object/from16 v42, v10

    move/from16 v43, v8

    move/from16 v44, v3

    move/from16 v45, v7

    invoke-virtual/range {v35 .. v45}, LX/0wg;->A00(Landroid/app/Activity;LX/0kM;LX/21R;LX/1VG;Ljava/lang/String;Ljava/util/List;Ljava/util/List;IZZ)V

    goto/16 :goto_13
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_0

    .line 96780
    :catchall_0
    :try_start_8
    move-exception v0

    .line 96781
    throw v0

    .line 96782
    :cond_3e
    iget-byte v7, v0, LX/48c;->A00:B

    move/from16 v6, v21

    if-ne v7, v6, :cond_41

    .line 96783
    iget-object v9, v1, LX/0kB;->A0K:LX/01k;

    iget-object v7, v1, LX/0kB;->A07:LX/0nL;

    iget-object v6, v1, Lcom/soula2/Conversation;->A23:LX/00z;

    new-instance v8, LX/32D;

    invoke-direct {v8, v7, v9, v6}, LX/32D;-><init>(LX/0nL;LX/01k;LX/00z;)V

    iget-object v7, v1, Lcom/soula2/Conversation;->A0f:LX/15c;

    iget-object v10, v0, LX/48c;->A0M:Ljava/lang/String;
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_4

    .line 96784
    :try_start_9
    iget-object v6, v8, LX/32D;->A02:LX/00z;

    new-instance v11, LX/1qP;

    invoke-direct {v11, v7, v6}, LX/1qP;-><init>(LX/15c;LX/00z;)V

    .line 96785
    invoke-virtual {v8, v10}, LX/32D;->A05(Ljava/lang/String;)V

    .line 96786
    iget-object v9, v8, LX/32D;->A03:LX/1YX;

    iget-object v6, v9, LX/1YX;->A05:Ljava/util/List;

    if-eqz v6, :cond_49

    .line 96787
    iget-object v8, v8, LX/32D;->A00:LX/0nL;

    .line 96788
    invoke-interface {v6}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v13

    :cond_3f
    :goto_f
    invoke-interface {v13}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_40

    invoke-interface {v13}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, LX/1YZ;

    .line 96789
    iget-object v6, v7, LX/1YZ;->A02:Ljava/lang/String;

    .line 96790
    invoke-virtual {v8, v6, v4}, LX/0nL;->A0C(Ljava/lang/String;Z)LX/0mJ;

    move-result-object v12

    .line 96791
    if-eqz v12, :cond_3f

    .line 96792
    const-class v6, Lcom/whatsapp/jid/UserJid;

    invoke-virtual {v12, v6}, LX/0mJ;->A0A(Ljava/lang/Class;)Lcom/whatsapp/jid/Jid;

    move-result-object v6

    check-cast v6, Lcom/whatsapp/jid/UserJid;

    iput-object v6, v7, LX/1YZ;->A01:Lcom/whatsapp/jid/UserJid;

    goto :goto_f

    .line 96793
    :cond_40
    invoke-virtual {v11, v9}, LX/1qP;->A00(LX/1YX;)Ljava/lang/String;

    move-result-object v10

    goto/16 :goto_12

    .line 96794
    :cond_41
    const/16 v6, 0xe

    if-ne v7, v6, :cond_42
    :try_end_9
    .catch LX/1qH; {:try_start_9 .. :try_end_9} :catch_3
    .catchall {:try_start_9 .. :try_end_9} :catchall_4

    .line 96795
    :try_start_a
    iget-object v10, v1, LX/0kB;->A03:LX/0oS;

    iget-object v6, v1, Lcom/soula2/Conversation;->A2r:LX/0l8;

    iget-object v9, v0, LX/48c;->A0O:Ljava/util/ArrayList;

    iget-boolean v8, v1, Lcom/soula2/Conversation;->A4P:Z

    .line 96796
    invoke-static {v6}, Ljava/util/Collections;->singletonList(Ljava/lang/Object;)Ljava/util/List;

    move-result-object v7

    .line 96797
    move-object/from16 v6, v26

    invoke-virtual {v10, v6, v7, v9, v8}, LX/0oS;->A0T(LX/0md;Ljava/util/List;Ljava/util/List;Z)V

    goto/16 :goto_13

    .line 96798
    :cond_42
    invoke-static {v11}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v6

    if-nez v6, :cond_46

    iget-object v9, v5, LX/1be;->A05:Ljava/lang/String;

    .line 96799
    invoke-static {v9}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v6

    if-nez v6, :cond_46

    .line 96800
    iget-object v7, v1, Lcom/soula2/Conversation;->A2E:LX/0rJ;

    iget-object v6, v1, Lcom/soula2/Conversation;->A2r:LX/0l8;

    invoke-static {v6}, Lcom/whatsapp/jid/UserJid;->of(Lcom/whatsapp/jid/Jid;)Lcom/whatsapp/jid/UserJid;

    move-result-object v6

    .line 96801
    invoke-virtual {v7, v6, v4}, LX/0rJ;->A01(Lcom/whatsapp/jid/UserJid;I)V

    .line 96802
    iget-object v8, v5, LX/1be;->A03:Ljava/lang/String;

    invoke-static {v8}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v6

    if-nez v6, :cond_45

    .line 96803
    iget-object v7, v1, Lcom/soula2/Conversation;->A2C:LX/20a;

    .line 96804
    iget-boolean v6, v7, LX/20a;->A0S:Z

    .line 96805
    if-eqz v6, :cond_43

    iget-boolean v6, v5, LX/1be;->A07:Z

    if-eqz v6, :cond_43

    .line 96806
    iget v7, v7, LX/20a;->A0I:I

    move/from16 v6, v24

    if-eq v7, v6, :cond_44

    .line 96807
    :cond_43
    sget-object v10, Lcom/soula2/Conversation;->A5I:Ljava/util/HashMap;

    iget-object v7, v1, Lcom/soula2/Conversation;->A2b:LX/0mJ;

    const-class v6, LX/0l8;

    .line 96808
    invoke-virtual {v7, v6}, LX/0mJ;->A0A(Ljava/lang/Class;)Lcom/whatsapp/jid/Jid;

    move-result-object v6

    .line 96809
    invoke-virtual {v10, v6, v8}, Ljava/util/AbstractMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 96810
    :cond_44
    :goto_10
    iget-object v6, v1, Lcom/soula2/Conversation;->A2r:LX/0l8;

    .line 96811
    invoke-virtual {v6}, Lcom/whatsapp/jid/Jid;->getRawString()Ljava/lang/String;

    move-result-object v36

    iget-boolean v8, v5, LX/1be;->A07:Z

    iget-boolean v7, v5, LX/1be;->A08:Z

    new-instance v6, LX/23Q;

    move-object/from16 v35, v6

    move-object/from16 v37, v11

    move-object/from16 v38, v9

    move/from16 v39, v8

    move/from16 v40, v7

    invoke-direct/range {v35 .. v40}, LX/23Q;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZZ)V

    .line 96812
    sget-object v9, Lcom/soula2/Conversation;->A5G:Ljava/util/HashMap;

    iget-object v8, v1, Lcom/soula2/Conversation;->A2b:LX/0mJ;

    const-class v7, LX/0l8;

    invoke-virtual {v8, v7}, LX/0mJ;->A0A(Ljava/lang/Class;)Lcom/whatsapp/jid/Jid;

    move-result-object v7

    invoke-virtual {v9, v7, v6}, Ljava/util/AbstractMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto/16 :goto_13

    .line 96813
    :cond_45
    sget-object v8, Lcom/soula2/Conversation;->A5I:Ljava/util/HashMap;

    iget-object v7, v1, Lcom/soula2/Conversation;->A2b:LX/0mJ;

    const-class v6, LX/0l8;

    invoke-virtual {v7, v6}, LX/0mJ;->A0A(Ljava/lang/Class;)Lcom/whatsapp/jid/Jid;

    move-result-object v6

    invoke-virtual {v8, v6}, Ljava/util/AbstractMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_10

    .line 96814
    :cond_46
    iget-object v10, v0, LX/48c;->A0J:Ljava/lang/String;

    if-eqz v10, :cond_4a

    invoke-virtual {v10}, Ljava/lang/String;->length()I

    move-result v6

    if-lez v6, :cond_4a

    .line 96815
    iget-boolean v6, v0, LX/48c;->A0S:Z

    if-eqz v6, :cond_47

    .line 96816
    sget-object v8, Lcom/soula2/Conversation;->A5I:Ljava/util/HashMap;

    iget-object v7, v1, Lcom/soula2/Conversation;->A2b:LX/0mJ;

    const-class v6, LX/0l8;

    invoke-virtual {v7, v6}, LX/0mJ;->A0A(Ljava/lang/Class;)Lcom/whatsapp/jid/Jid;

    move-result-object v6

    invoke-virtual {v8, v6, v10}, Ljava/util/AbstractMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_13

    .line 96817
    :cond_47
    iget-object v9, v1, LX/0kB;->A03:LX/0oS;

    iget-object v7, v1, Lcom/soula2/Conversation;->A2b:LX/0mJ;

    const-class v6, LX/0l8;

    .line 96818
    invoke-virtual {v7, v6}, LX/0mJ;->A0A(Ljava/lang/Class;)Lcom/whatsapp/jid/Jid;

    move-result-object v6

    invoke-static {v6}, Ljava/util/Collections;->singletonList(Ljava/lang/Object;)Ljava/util/List;

    move-result-object v40

    .line 96819
    invoke-static {v10}, LX/1dI;->A01(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    if-nez v7, :cond_48

    const/4 v8, 0x0

    goto :goto_11

    .line 96820
    :cond_48
    sget-object v6, LX/2EV;->A00:LX/13d;

    invoke-virtual {v6, v7}, Ljava/util/AbstractMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, LX/0l5;

    .line 96821
    :goto_11
    iget-boolean v7, v1, Lcom/soula2/Conversation;->A4P:Z

    iget-boolean v6, v1, Lcom/soula2/Conversation;->A4Q:Z

    .line 96822
    move-object/from16 v35, v9

    move-object/from16 v36, v8

    move-object/from16 v37, v26

    move-object/from16 v38, v26

    move-object/from16 v39, v10

    move-object/from16 v41, v26

    move/from16 v42, v7

    move/from16 v43, v6

    invoke-virtual/range {v35 .. v43}, LX/0oS;->A08(LX/0l5;LX/1VG;LX/0md;Ljava/lang/String;Ljava/util/List;Ljava/util/List;ZZ)V

    .line 96823
    iput-boolean v3, v1, Lcom/soula2/Conversation;->A4Q:Z

    goto :goto_13

    .line 96824
    :catch_3
    move-exception v6

    .line 96825
    new-instance v7, LX/1qI;

    invoke-direct {v7, v6}, LX/1qI;-><init>(Ljava/lang/Throwable;)V

    .line 96826
    const-string v6, "addWaIdsToVCard"

    invoke-static {v6, v7}, Lcom/whatsapp/util/Log;->e(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 96827
    :cond_49
    :goto_12
    iget-object v9, v1, LX/0kB;->A03:LX/0oS;

    iget-object v8, v1, Lcom/soula2/Conversation;->A2r:LX/0l8;

    iget-object v7, v0, LX/48c;->A0L:Ljava/lang/String;

    iget-boolean v6, v1, Lcom/soula2/Conversation;->A4P:Z

    .line 96828
    invoke-static {v8}, Ljava/util/Collections;->singletonList(Ljava/lang/Object;)Ljava/util/List;

    move-result-object v39

    .line 96829
    move-object/from16 v35, v9

    move-object/from16 v36, v26

    move-object/from16 v37, v7

    move-object/from16 v38, v10

    move/from16 v40, v6

    invoke-virtual/range {v35 .. v40}, LX/0oS;->A0S(LX/0md;Ljava/lang/String;Ljava/lang/String;Ljava/util/List;Z)V

    .line 96830
    :cond_4a
    :goto_13
    sget-object v7, Lcom/soula2/Conversation;->A5I:Ljava/util/HashMap;

    iget-object v6, v1, Lcom/soula2/Conversation;->A2b:LX/0mJ;

    const-class v10, LX/0l8;

    .line 96831
    invoke-virtual {v6, v10}, LX/0mJ;->A0A(Ljava/lang/Class;)Lcom/whatsapp/jid/Jid;

    move-result-object v6

    invoke-virtual {v7, v6}, Ljava/util/AbstractMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Ljava/lang/String;

    .line 96832
    invoke-static {v9}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v6

    if-nez v6, :cond_4c

    .line 96833
    sget-object v7, Lcom/soula2/Conversation;->A5H:Ljava/util/HashMap;

    iget-object v6, v1, Lcom/soula2/Conversation;->A2b:LX/0mJ;

    .line 96834
    invoke-virtual {v6, v10}, LX/0mJ;->A0A(Ljava/lang/Class;)Lcom/whatsapp/jid/Jid;

    move-result-object v6

    invoke-virtual {v7, v6}, Ljava/util/AbstractMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/String;

    .line 96835
    invoke-static {v6}, LX/1Xv;->A01(Ljava/lang/String;)Ljava/util/List;

    move-result-object v7

    .line 96836
    iget-object v6, v1, Lcom/soula2/Conversation;->A2v:Lcom/soula2/mentions/MentionableEntry;

    invoke-virtual {v6, v9, v7}, Lcom/soula2/mentions/MentionableEntry;->setMentionableText(Ljava/lang/String;Ljava/util/Collection;)V

    .line 96837
    iget-object v8, v1, LX/0kH;->A0B:LX/17a;

    iget-object v7, v1, LX/0kH;->A08:LX/01e;

    iget-object v6, v1, Lcom/soula2/Conversation;->A3I:LX/0pe;

    iget-object v11, v1, Lcom/soula2/Conversation;->A2v:Lcom/soula2/mentions/MentionableEntry;

    .line 96838
    invoke-virtual {v11}, Landroid/widget/TextView;->getEditableText()Landroid/text/Editable;

    move-result-object v37

    iget-object v11, v1, Lcom/soula2/Conversation;->A2v:Lcom/soula2/mentions/MentionableEntry;

    .line 96839
    invoke-virtual {v11}, Landroid/widget/TextView;->getPaint()Landroid/text/TextPaint;

    move-result-object v36

    .line 96840
    move-object/from16 v35, v1

    move-object/from16 v38, v7

    move-object/from16 v39, v8

    move-object/from16 v40, v6

    invoke-static/range {v35 .. v40}, LX/1us;->A06(Landroid/content/Context;Landroid/graphics/Paint;Landroid/text/Editable;LX/01e;LX/17a;LX/0pe;)V

    .line 96841
    invoke-static {v9}, LX/1dI;->A01(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v12

    .line 96842
    iget-object v6, v1, Lcom/soula2/Conversation;->A1y:LX/0l4;

    invoke-virtual {v6, v12}, LX/0l4;->A07(Ljava/lang/String;)V
    :try_end_a
    .catchall {:try_start_a .. :try_end_a} :catchall_4

    .line 96843
    :try_start_b
    iget-object v11, v1, Lcom/soula2/Conversation;->A1y:LX/0l4;

    .line 96844
    iget-object v8, v1, Lcom/soula2/Conversation;->A1H:LX/1lD;

    if-nez v8, :cond_4b

    .line 96845
    iget-object v7, v1, Lcom/soula2/Conversation;->A1G:LX/15p;

    iget-object v6, v1, Lcom/soula2/Conversation;->A3K:LX/2yh;

    new-instance v8, LX/1lD;

    invoke-direct {v8, v7, v6}, LX/1lD;-><init>(LX/15p;LX/2yh;)V

    iput-object v8, v1, Lcom/soula2/Conversation;->A1H:LX/1lD;

    .line 96846
    :cond_4b
    invoke-virtual {v11, v8, v12}, LX/0l4;->A05(LX/1lD;Ljava/lang/String;)V

    goto :goto_14
    :try_end_b
    .catchall {:try_start_b .. :try_end_b} :catchall_1

    .line 96847
    :catchall_1
    :try_start_c
    move-exception v0

    .line 96848
    throw v0

    .line 96849
    :cond_4c
    iget-object v7, v1, Lcom/soula2/Conversation;->A2v:Lcom/soula2/mentions/MentionableEntry;

    move-object/from16 v6, v26

    invoke-virtual {v7, v6}, Lcom/soula2/mentions/MentionableEntry;->setText(Ljava/lang/String;)V

    .line 96850
    invoke-virtual {v1}, Lcom/soula2/Conversation;->A2k()V

    .line 96851
    :goto_14
    invoke-static {v9}, LX/1HG;->A0D(Ljava/lang/CharSequence;)Z

    move-result v7

    .line 96852
    iget-object v6, v1, Lcom/soula2/Conversation;->A0V:Landroid/widget/ImageButton;

    if-eqz v7, :cond_4e

    .line 96853
    invoke-virtual {v6, v3}, Landroid/view/View;->setVisibility(I)V

    .line 96854
    iget-object v6, v1, Lcom/soula2/Conversation;->A0S:Landroid/widget/ImageButton;

    invoke-virtual {v6, v3}, Landroid/view/View;->setVisibility(I)V

    .line 96855
    iget-object v6, v1, Lcom/soula2/Conversation;->A0U:Landroid/widget/ImageButton;

    invoke-virtual {v6, v2}, Landroid/view/View;->setVisibility(I)V

    .line 96856
    :goto_15
    sget-object v6, Lcom/soula2/Conversation;->A5G:Ljava/util/HashMap;

    iget-object v2, v1, Lcom/soula2/Conversation;->A2b:LX/0mJ;

    .line 96857
    invoke-virtual {v2, v10}, LX/0mJ;->A0A(Ljava/lang/Class;)Lcom/whatsapp/jid/Jid;

    move-result-object v2

    invoke-virtual {v6, v2}, Ljava/util/AbstractMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v11

    check-cast v11, LX/23Q;

    if-eqz v11, :cond_4d

    .line 96858
    iget-object v7, v1, Lcom/soula2/Conversation;->A2E:LX/0rJ;

    iget-object v2, v1, Lcom/soula2/Conversation;->A2r:LX/0l8;

    invoke-static {v2}, Lcom/whatsapp/jid/UserJid;->of(Lcom/whatsapp/jid/Jid;)Lcom/whatsapp/jid/UserJid;

    move-result-object v6

    .line 96859
    move/from16 v2, v24

    invoke-virtual {v7, v6, v2}, LX/0rJ;->A01(Lcom/whatsapp/jid/UserJid;I)V

    .line 96860
    iget-object v6, v1, Lcom/soula2/Conversation;->A1y:LX/0l4;

    iget-object v2, v11, LX/23Q;->A02:Ljava/lang/String;

    invoke-virtual {v6, v2}, LX/0l4;->A07(Ljava/lang/String;)V

    .line 96861
    iget-object v10, v1, Lcom/soula2/Conversation;->A1y:LX/0l4;

    .line 96862
    iget-object v2, v10, LX/0l4;->A0E:LX/0st;

    new-instance v6, LX/2Ig;

    invoke-direct {v6, v2, v11}, LX/2Ig;-><init>(LX/0st;LX/23Q;)V

    iput-object v6, v10, LX/0l4;->A01:LX/0l5;

    .line 96863
    iget-object v2, v10, LX/0l4;->A0A:LX/012;

    invoke-virtual {v2, v6}, LX/013;->A0B(Ljava/lang/Object;)V

    .line 96864
    new-instance v9, LX/1tw;

    invoke-direct {v9, v10, v5, v11}, LX/1tw;-><init>(LX/0l4;LX/1be;LX/23Q;)V

    .line 96865
    iget-object v12, v10, LX/0l4;->A0D:LX/0li;

    iget-object v7, v10, LX/0l4;->A0T:LX/0lO;

    iget-object v6, v10, LX/0l4;->A0R:LX/0qe;

    iget-object v2, v10, LX/0l4;->A0G:LX/0pF;

    new-instance v8, LX/1tc;

    move-object/from16 v35, v8

    move-object/from16 v36, v12

    move-object/from16 v37, v2

    move-object/from16 v38, v9

    move-object/from16 v39, v6

    move-object/from16 v40, v7

    invoke-direct/range {v35 .. v40}, LX/1tc;-><init>(LX/0li;LX/0pF;LX/1tw;LX/0qe;LX/0lO;)V

    .line 96866
    iget-object v7, v8, LX/1tc;->A07:LX/0lO;

    new-instance v6, Lcom/facebook/redex/RunnableRunnableShape9S0200000_I0_7;

    move/from16 v2, v24

    invoke-direct {v6, v8, v2, v11}, Lcom/facebook/redex/RunnableRunnableShape9S0200000_I0_7;-><init>(Ljava/lang/Object;ILjava/lang/Object;)V

    invoke-interface {v7, v6}, LX/0lO;->AZm(Ljava/lang/Runnable;)V

    .line 96867
    iget-object v6, v10, LX/0l4;->A0U:Ljava/util/List;

    new-instance v2, Lcom/facebook/redex/RunnableRunnableShape7S0100000_I0_6;

    move/from16 v7, v21

    invoke-direct {v2, v8, v7}, Lcom/facebook/redex/RunnableRunnableShape7S0100000_I0_6;-><init>(Ljava/lang/Object;I)V

    invoke-interface {v6, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 96868
    iget-object v6, v9, LX/1tw;->A00:LX/012;

    .line 96869
    new-instance v2, Lcom/facebook/redex/IDxObserverShape45S0200000_1_I0;

    invoke-direct {v2, v11, v3, v1}, Lcom/facebook/redex/IDxObserverShape45S0200000_1_I0;-><init>(Ljava/lang/Object;ILjava/lang/Object;)V

    .line 96870
    invoke-virtual {v6, v1, v2}, LX/013;->A05(LX/00m;LX/01R;)V

    .line 96871
    iget-boolean v2, v5, LX/1be;->A0B:Z

    if-eqz v2, :cond_4d

    .line 96872
    iget-object v2, v1, Lcom/soula2/Conversation;->A2v:Lcom/soula2/mentions/MentionableEntry;

    .line 96873
    invoke-virtual {v2, v3}, Lcom/soula2/WaEditText;->A05(Z)V

    .line 96874
    :cond_4d
    iget-boolean v6, v5, LX/1be;->A0A:Z

    .line 96875
    iget-object v5, v1, LX/0kH;->A0C:LX/0kj;

    const/16 v2, 0x372

    invoke-virtual {v5, v2}, LX/0kj;->A07(I)Z

    move-result v2

    if-eqz v2, :cond_4f

    goto :goto_16

    .line 96876
    :cond_4e
    invoke-virtual {v6, v2}, Landroid/view/View;->setVisibility(I)V

    .line 96877
    iget-object v6, v1, Lcom/soula2/Conversation;->A0S:Landroid/widget/ImageButton;

    invoke-virtual {v6, v2}, Landroid/view/View;->setVisibility(I)V

    .line 96878
    iget-object v2, v1, Lcom/soula2/Conversation;->A0U:Landroid/widget/ImageButton;

    invoke-virtual {v2, v3}, Landroid/view/View;->setVisibility(I)V

    goto/16 :goto_15

    .line 96879
    :goto_16
    if-eqz v6, :cond_4f

    .line 96880
    iget-object v2, v1, Lcom/soula2/Conversation;->A2r:LX/0l8;

    invoke-static {v2}, Lcom/whatsapp/jid/UserJid;->of(Lcom/whatsapp/jid/Jid;)Lcom/whatsapp/jid/UserJid;

    move-result-object v7

    if-eqz v7, :cond_4f

    .line 96881
    iget-object v5, v1, LX/00k;->A06:LX/05H;

    .line 96882
    iget-object v2, v1, Lcom/soula2/Conversation;->A29:Lcom/soula2/ctwa/bizpreview/BusinessPreviewInitializer;

    invoke-virtual {v5, v2}, LX/05I;->A00(LX/03O;)V

    .line 96883
    iget-object v2, v1, Lcom/soula2/Conversation;->A29:Lcom/soula2/ctwa/bizpreview/BusinessPreviewInitializer;

    .line 96884
    iget-object v5, v2, Lcom/soula2/ctwa/bizpreview/BusinessPreviewInitializer;->A04:LX/012;

    .line 96885
    new-instance v2, Lcom/facebook/redex/IDxObserverShape47S0200000_2_I0;

    invoke-direct {v2, v7, v3, v1}, Lcom/facebook/redex/IDxObserverShape47S0200000_2_I0;-><init>(Ljava/lang/Object;ILjava/lang/Object;)V

    .line 96886
    invoke-virtual {v5, v1, v2}, LX/013;->A05(LX/00m;LX/01R;)V

    .line 96887
    iget-object v2, v1, Lcom/soula2/Conversation;->A29:Lcom/soula2/ctwa/bizpreview/BusinessPreviewInitializer;

    .line 96888
    iget-object v6, v2, Lcom/soula2/ctwa/bizpreview/BusinessPreviewInitializer;->A04:LX/012;

    sget-object v5, Ljava/lang/Boolean;->FALSE:Ljava/lang/Boolean;

    invoke-virtual {v6, v5}, LX/013;->A0B(Ljava/lang/Object;)V

    .line 96889
    iget-object v8, v2, Lcom/soula2/ctwa/bizpreview/BusinessPreviewInitializer;->A01:LX/20L;

    .line 96890
    iput-object v7, v8, LX/20L;->A04:Lcom/whatsapp/jid/UserJid;

    .line 96891
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v5

    .line 96892
    iput-wide v5, v8, LX/20L;->A00:J

    .line 96893
    iget-object v9, v8, LX/20L;->A01:LX/0lA;

    .line 96894
    iget-object v6, v9, LX/0lA;->A01:LX/0nf;

    move-object/from16 v5, v26

    invoke-virtual {v9, v6, v7, v5}, LX/0lA;->A00(LX/0nf;Lcom/whatsapp/jid/UserJid;Ljava/lang/String;)LX/1Wb;

    move-result-object v7

    .line 96895
    iput-object v8, v7, LX/1Wb;->A00:LX/0kV;

    .line 96896
    new-instance v5, LX/1Tj;

    invoke-direct {v5}, LX/1Tj;-><init>()V

    iput-object v5, v7, LX/1Wb;->A01:LX/1Tj;

    .line 96897
    iget-object v6, v7, LX/1Wb;->A08:LX/0qe;

    invoke-virtual {v6}, LX/0qe;->A01()Ljava/lang/String;

    move-result-object v5

    .line 96898
    iget-object v9, v7, LX/1Wb;->A09:LX/0u6;

    const-string/jumbo v8, "profile_view_tag"

    invoke-virtual {v9, v8}, LX/0u6;->A03(Ljava/lang/String;)V

    .line 96899
    invoke-virtual {v7, v5}, LX/1Wb;->A00(Ljava/lang/String;)LX/1Th;

    move-result-object v8

    const/16 v10, 0x84

    const-wide/16 v11, 0x7d00

    .line 96900
    move-object v9, v5

    invoke-virtual/range {v6 .. v12}, LX/0qe;->A0A(LX/0yu;LX/1Th;Ljava/lang/String;IJ)V

    .line 96901
    const-string/jumbo v6, "sendGetBusinessProfile jid="

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v6, v7, LX/1Wb;->A07:Lcom/whatsapp/jid/UserJid;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v5}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Lcom/whatsapp/util/Log;->i(Ljava/lang/String;)V

    .line 96902
    iget-object v5, v2, Lcom/soula2/ctwa/bizpreview/BusinessPreviewInitializer;->A01:LX/20L;

    .line 96903
    iget-object v7, v5, LX/20L;->A06:LX/012;

    .line 96904
    const/16 v6, 0x6c

    new-instance v5, Lcom/facebook/redex/IDxObserverShape118S0100000_2_I0;

    invoke-direct {v5, v2, v6}, Lcom/facebook/redex/IDxObserverShape118S0100000_2_I0;-><init>(Ljava/lang/Object;I)V

    .line 96905
    invoke-virtual {v7, v5}, LX/013;->A08(LX/01R;)V

    .line 96906
    iget-object v9, v2, Lcom/soula2/ctwa/bizpreview/BusinessPreviewInitializer;->A02:LX/0lO;

    .line 96907
    const/16 v5, 0x22

    new-instance v8, Lcom/facebook/redex/RunnableRunnableShape7S0100000_I0_6;

    invoke-direct {v8, v2, v5}, Lcom/facebook/redex/RunnableRunnableShape7S0100000_I0_6;-><init>(Ljava/lang/Object;I)V

    .line 96908
    const-wide/16 v5, 0x1f4

    const-string v7, "BusinessPreviewInitializer/initiateBusinessPreview"

    .line 96909
    invoke-interface {v9, v8, v7, v5, v6}, LX/0lO;->Aa5(Ljava/lang/Runnable;Ljava/lang/String;J)Ljava/lang/Runnable;

    move-result-object v5

    iput-object v5, v2, Lcom/soula2/ctwa/bizpreview/BusinessPreviewInitializer;->A03:Ljava/lang/Runnable;

    .line 96910
    :cond_4f
    new-instance v21, LX/2Fu;

    move-object/from16 v2, v21

    invoke-direct {v2, v1}, LX/2Fu;-><init>(Landroid/content/Context;)V

    .line 96911
    iget-boolean v2, v1, Lcom/soula2/Conversation;->A4S:Z

    if-eqz v2, :cond_54

    .line 96912
    iget-object v2, v1, Lcom/soula2/Conversation;->A2r:LX/0l8;

    invoke-static {v2}, LX/0nO;->A02(Lcom/whatsapp/jid/Jid;)LX/0nO;

    move-result-object v65

    invoke-static/range {v65 .. v65}, LX/009;->A05(Ljava/lang/Object;)V

    .line 96913
    iget-boolean v2, v1, Lcom/soula2/Conversation;->A4X:Z

    if-eqz v2, :cond_50

    .line 96914
    new-instance v5, LX/2qZ;

    move-object/from16 v6, v19

    move-object/from16 v2, v65

    invoke-direct {v5, v1, v6, v2}, LX/2qZ;-><init>(Lcom/soula2/Conversation;LX/0me;LX/0nO;)V

    iput-object v5, v1, Lcom/soula2/Conversation;->A2p:LX/1cK;

    .line 96915
    iget-object v2, v1, Lcom/soula2/Conversation;->A2q:LX/128;

    invoke-virtual {v2, v5}, LX/0pD;->A03(Ljava/lang/Object;)V

    .line 96916
    :cond_50
    iget-object v5, v1, Lcom/soula2/Conversation;->A0r:LX/2Dj;

    .line 96917
    iget-object v2, v1, LX/0kB;->A0F:LX/1Mq;

    invoke-virtual {v2, v1}, LX/1Mq;->A01(Landroid/content/Context;)LX/1Hc;

    move-result-object v79

    .line 96918
    iget-object v2, v1, Lcom/soula2/Conversation;->A2b:LX/0mJ;

    move-object/from16 v48, v2

    .line 96919
    iget-object v2, v5, LX/2Dj;->A00:LX/2A6;

    .line 96920
    iget-object v2, v2, LX/2A6;->A03:LX/2fo;

    .line 96921
    iget-object v5, v2, LX/2fo;->AMT:LX/01P;

    .line 96922
    invoke-interface {v5}, LX/01P;->get()Ljava/lang/Object;

    move-result-object v47

    move-object/from16 v5, v47

    check-cast v5, LX/0lm;

    move-object/from16 v47, v5

    .line 96923
    iget-object v5, v2, LX/2fo;->A04:LX/01P;

    .line 96924
    invoke-interface {v5}, LX/01P;->get()Ljava/lang/Object;

    move-result-object v46

    move-object/from16 v5, v46

    check-cast v5, LX/0kj;

    move-object/from16 v46, v5

    .line 96925
    iget-object v5, v2, LX/2fo;->A8w:LX/01P;

    .line 96926
    invoke-interface {v5}, LX/01P;->get()Ljava/lang/Object;

    move-result-object v45

    move-object/from16 v5, v45

    check-cast v5, LX/0li;

    move-object/from16 v45, v5

    .line 96927
    iget-object v5, v2, LX/2fo;->AOa:LX/01P;

    .line 96928
    invoke-interface {v5}, LX/01P;->get()Ljava/lang/Object;

    move-result-object v44

    move-object/from16 v5, v44

    check-cast v5, LX/0lO;

    move-object/from16 v44, v5

    .line 96929
    iget-object v5, v2, LX/2fo;->A3f:LX/01P;

    .line 96930
    invoke-interface {v5}, LX/01P;->get()Ljava/lang/Object;

    move-result-object v43

    move-object/from16 v5, v43

    check-cast v5, LX/0uN;

    move-object/from16 v43, v5

    .line 96931
    iget-object v5, v2, LX/2fo;->A6k:LX/01P;

    .line 96932
    invoke-interface {v5}, LX/01P;->get()Ljava/lang/Object;

    move-result-object v42

    move-object/from16 v5, v42

    check-cast v5, LX/17a;

    move-object/from16 v42, v5

    .line 96933
    iget-object v5, v2, LX/2fo;->A4P:LX/01P;

    .line 96934
    invoke-interface {v5}, LX/01P;->get()Ljava/lang/Object;

    move-result-object v41

    move-object/from16 v5, v41

    check-cast v5, LX/18Y;

    move-object/from16 v41, v5

    .line 96935
    iget-object v5, v2, LX/2fo;->A4T:LX/01P;

    .line 96936
    invoke-interface {v5}, LX/01P;->get()Ljava/lang/Object;

    move-result-object v40

    move-object/from16 v5, v40

    check-cast v5, LX/0nL;

    move-object/from16 v40, v5

    .line 96937
    iget-object v5, v2, LX/2fo;->A4a:LX/01P;

    .line 96938
    invoke-interface {v5}, LX/01P;->get()Ljava/lang/Object;

    move-result-object v39

    move-object/from16 v5, v39

    check-cast v5, LX/18I;

    move-object/from16 v39, v5

    .line 96939
    iget-object v5, v2, LX/2fo;->ANW:LX/01P;

    .line 96940
    invoke-interface {v5}, LX/01P;->get()Ljava/lang/Object;

    move-result-object v38

    move-object/from16 v5, v38

    check-cast v5, LX/0nS;

    move-object/from16 v38, v5

    .line 96941
    iget-object v5, v2, LX/2fo;->AOX:LX/01P;

    .line 96942
    invoke-interface {v5}, LX/01P;->get()Ljava/lang/Object;

    move-result-object v37

    move-object/from16 v5, v37

    check-cast v5, LX/00z;

    move-object/from16 v37, v5

    .line 96943
    iget-object v5, v2, LX/2fo;->A4U:LX/01P;

    .line 96944
    invoke-interface {v5}, LX/01P;->get()Ljava/lang/Object;

    move-result-object v36

    move-object/from16 v5, v36

    check-cast v5, LX/10Z;

    move-object/from16 v36, v5

    .line 96945
    iget-object v5, v2, LX/2fo;->A9D:LX/01P;

    .line 96946
    invoke-interface {v5}, LX/01P;->get()Ljava/lang/Object;

    move-result-object v15

    check-cast v15, LX/0vp;

    .line 96947
    iget-object v5, v2, LX/2fo;->AKB:LX/01P;

    .line 96948
    invoke-interface {v5}, LX/01P;->get()Ljava/lang/Object;

    move-result-object v14

    check-cast v14, LX/18Z;

    .line 96949
    iget-object v5, v2, LX/2fo;->ALk:LX/01P;

    .line 96950
    invoke-interface {v5}, LX/01P;->get()Ljava/lang/Object;

    move-result-object v13

    check-cast v13, LX/1DM;

    .line 96951
    iget-object v5, v2, LX/2fo;->A3b:LX/01P;

    .line 96952
    invoke-interface {v5}, LX/01P;->get()Ljava/lang/Object;

    move-result-object v12

    check-cast v12, LX/10b;

    .line 96953
    iget-object v5, v2, LX/2fo;->A3t:LX/01P;

    .line 96954
    invoke-interface {v5}, LX/01P;->get()Ljava/lang/Object;

    move-result-object v11

    check-cast v11, LX/15M;

    .line 96955
    iget-object v5, v2, LX/2fo;->A2k:LX/01P;

    .line 96956
    invoke-interface {v5}, LX/01P;->get()Ljava/lang/Object;

    move-result-object v10

    check-cast v10, LX/0zc;

    .line 96957
    iget-object v5, v2, LX/2fo;->A9F:LX/01P;

    .line 96958
    invoke-interface {v5}, LX/01P;->get()Ljava/lang/Object;

    move-result-object v9

    check-cast v9, LX/11C;

    .line 96959
    iget-object v5, v2, LX/2fo;->A9P:LX/01P;

    .line 96960
    invoke-interface {v5}, LX/01P;->get()Ljava/lang/Object;

    move-result-object v8

    check-cast v8, LX/0nR;

    .line 96961
    iget-object v5, v2, LX/2fo;->A9J:LX/01P;

    .line 96962
    invoke-interface {v5}, LX/01P;->get()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, LX/11F;

    .line 96963
    iget-object v5, v2, LX/2fo;->A4X:LX/01P;

    .line 96964
    invoke-interface {v5}, LX/01P;->get()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, LX/14y;

    .line 96965
    iget-object v5, v2, LX/2fo;->A4u:LX/01P;

    .line 96966
    invoke-interface {v5}, LX/01P;->get()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, LX/0w1;

    .line 96967
    iget-object v2, v2, LX/2fo;->A9Q:LX/01P;

    .line 96968
    invoke-interface {v2}, LX/01P;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/128;

    new-instance v35, LX/1td;

    move-object/from16 v66, v35

    move-object/from16 v67, v1

    move-object/from16 v68, v39

    move-object/from16 v69, v45

    move-object/from16 v70, v21

    move-object/from16 v71, v10

    move-object/from16 v72, v12

    move-object/from16 v73, v11

    move-object/from16 v74, v41

    move-object/from16 v75, v40

    move-object/from16 v76, v36

    move-object/from16 v77, v7

    move-object/from16 v78, v38

    move-object/from16 v80, v6

    move-object/from16 v81, v47

    move-object/from16 v82, v37

    move-object/from16 v83, v43

    move-object/from16 v84, v5

    move-object/from16 v85, v8

    move-object/from16 v86, v48

    move-object/from16 v87, v42

    move-object/from16 v88, v46

    move-object/from16 v89, v15

    move-object/from16 v90, v9

    move-object/from16 v91, v2

    move-object/from16 v92, v13

    move-object/from16 v93, v65

    move-object/from16 v94, v14

    move-object/from16 v95, v44

    invoke-direct/range {v66 .. v95}, LX/1td;-><init>(LX/00i;LX/18I;LX/0li;LX/2Fu;LX/0zc;LX/10b;LX/15M;LX/18Y;LX/0nL;LX/10Z;LX/11F;LX/0nS;LX/1Hc;LX/14y;LX/0lm;LX/00z;LX/0uN;LX/0w1;LX/0nR;LX/0mJ;LX/17a;LX/0kj;LX/0vp;LX/11C;LX/128;LX/1DM;LX/0nO;LX/18Z;LX/0lO;)V

    .line 96969
    move-object/from16 v2, v35

    iput-object v2, v1, Lcom/soula2/Conversation;->A1h:LX/1NR;

    .line 96970
    iget-object v5, v1, Lcom/soula2/Conversation;->A0p:LX/2Dk;

    iget-object v2, v1, Lcom/soula2/Conversation;->A2b:LX/0mJ;

    move-object/from16 v96, v2

    .line 96971
    iget-object v6, v5, LX/2Dk;->A00:LX/2A6;

    .line 96972
    iget-object v2, v6, LX/2A6;->A03:LX/2fo;

    .line 96973
    iget-object v5, v2, LX/2fo;->AMT:LX/01P;

    .line 96974
    invoke-interface {v5}, LX/01P;->get()Ljava/lang/Object;

    move-result-object v64

    move-object/from16 v5, v64

    check-cast v5, LX/0lm;

    move-object/from16 v64, v5

    .line 96975
    iget-object v5, v2, LX/2fo;->A04:LX/01P;

    .line 96976
    invoke-interface {v5}, LX/01P;->get()Ljava/lang/Object;

    move-result-object v62

    move-object/from16 v5, v62

    check-cast v5, LX/0kj;

    move-object/from16 v62, v5

    .line 96977
    iget-object v5, v2, LX/2fo;->A8w:LX/01P;

    .line 96978
    invoke-interface {v5}, LX/01P;->get()Ljava/lang/Object;

    move-result-object v61

    move-object/from16 v5, v61

    check-cast v5, LX/0li;

    move-object/from16 v61, v5

    .line 96979
    iget-object v5, v2, LX/2fo;->A5D:LX/01P;

    .line 96980
    invoke-interface {v5}, LX/01P;->get()Ljava/lang/Object;

    move-result-object v60

    move-object/from16 v5, v60

    check-cast v5, LX/0nf;

    move-object/from16 v60, v5

    .line 96981
    iget-object v5, v2, LX/2fo;->ABS:LX/01P;

    .line 96982
    invoke-interface {v5}, LX/01P;->get()Ljava/lang/Object;

    move-result-object v59

    move-object/from16 v5, v59

    check-cast v5, LX/0nN;

    move-object/from16 v59, v5

    .line 96983
    iget-object v5, v2, LX/2fo;->AOa:LX/01P;

    .line 96984
    invoke-interface {v5}, LX/01P;->get()Ljava/lang/Object;

    move-result-object v58

    move-object/from16 v5, v58

    check-cast v5, LX/0lO;

    move-object/from16 v58, v5

    .line 96985
    iget-object v5, v2, LX/2fo;->A3f:LX/01P;

    .line 96986
    invoke-interface {v5}, LX/01P;->get()Ljava/lang/Object;

    move-result-object v57

    move-object/from16 v5, v57

    check-cast v5, LX/0uN;

    move-object/from16 v57, v5

    .line 96987
    iget-object v5, v2, LX/2fo;->AKS:LX/01P;

    .line 96988
    invoke-interface {v5}, LX/01P;->get()Ljava/lang/Object;

    move-result-object v56

    move-object/from16 v5, v56

    check-cast v5, LX/184;

    move-object/from16 v56, v5

    .line 96989
    iget-object v5, v2, LX/2fo;->AJ3:LX/01P;

    .line 96990
    invoke-interface {v5}, LX/01P;->get()Ljava/lang/Object;

    move-result-object v55

    move-object/from16 v5, v55

    check-cast v5, LX/0nB;

    move-object/from16 v55, v5

    .line 96991
    iget-object v5, v2, LX/2fo;->AMw:LX/01P;

    .line 96992
    invoke-interface {v5}, LX/01P;->get()Ljava/lang/Object;

    move-result-object v54

    move-object/from16 v5, v54

    check-cast v5, LX/0oS;

    move-object/from16 v54, v5

    .line 96993
    iget-object v5, v6, LX/2A6;->A01:LX/29l;

    .line 96994
    invoke-virtual {v5}, LX/29l;->A08()LX/0zB;

    move-result-object v100

    .line 96995
    iget-object v5, v2, LX/2fo;->ALW:LX/01P;

    .line 96996
    invoke-interface {v5}, LX/01P;->get()Ljava/lang/Object;

    move-result-object v53

    move-object/from16 v5, v53

    check-cast v5, LX/0ki;

    move-object/from16 v53, v5

    .line 96997
    iget-object v5, v2, LX/2fo;->ANM:LX/01P;

    .line 96998
    invoke-interface {v5}, LX/01P;->get()Ljava/lang/Object;

    move-result-object v52

    move-object/from16 v5, v52

    check-cast v5, LX/0y8;

    move-object/from16 v52, v5

    .line 96999
    iget-object v5, v2, LX/2fo;->A4T:LX/01P;

    .line 97000
    invoke-interface {v5}, LX/01P;->get()Ljava/lang/Object;

    move-result-object v51

    move-object/from16 v5, v51

    check-cast v5, LX/0nL;

    move-object/from16 v51, v5

    .line 97001
    iget-object v5, v2, LX/2fo;->AOX:LX/01P;

    .line 97002
    invoke-interface {v5}, LX/01P;->get()Ljava/lang/Object;

    move-result-object v50

    move-object/from16 v5, v50

    check-cast v5, LX/00z;

    move-object/from16 v50, v5

    .line 97003
    iget-object v5, v2, LX/2fo;->AGY:LX/01P;

    .line 97004
    invoke-interface {v5}, LX/01P;->get()Ljava/lang/Object;

    move-result-object v49

    move-object/from16 v5, v49

    check-cast v5, LX/10W;

    move-object/from16 v49, v5

    .line 97005
    iget-object v5, v2, LX/2fo;->A2v:LX/01P;

    .line 97006
    invoke-interface {v5}, LX/01P;->get()Ljava/lang/Object;

    move-result-object v48

    move-object/from16 v5, v48

    check-cast v5, LX/1F5;

    move-object/from16 v48, v5

    .line 97007
    iget-object v5, v2, LX/2fo;->A9D:LX/01P;

    .line 97008
    invoke-interface {v5}, LX/01P;->get()Ljava/lang/Object;

    move-result-object v47

    move-object/from16 v5, v47

    check-cast v5, LX/0vp;

    move-object/from16 v47, v5

    .line 97009
    iget-object v5, v2, LX/2fo;->A3Q:LX/01P;

    .line 97010
    invoke-interface {v5}, LX/01P;->get()Ljava/lang/Object;

    move-result-object v46

    move-object/from16 v5, v46

    check-cast v5, LX/11o;

    move-object/from16 v46, v5

    .line 97011
    iget-object v5, v2, LX/2fo;->A4U:LX/01P;

    .line 97012
    invoke-interface {v5}, LX/01P;->get()Ljava/lang/Object;

    move-result-object v45

    move-object/from16 v5, v45

    check-cast v5, LX/10Z;

    move-object/from16 v45, v5

    .line 97013
    iget-object v5, v2, LX/2fo;->AKB:LX/01P;

    .line 97014
    invoke-interface {v5}, LX/01P;->get()Ljava/lang/Object;

    move-result-object v44

    move-object/from16 v5, v44

    check-cast v5, LX/18Z;

    move-object/from16 v44, v5

    .line 97015
    iget-object v5, v2, LX/2fo;->A33:LX/01P;

    .line 97016
    invoke-interface {v5}, LX/01P;->get()Ljava/lang/Object;

    move-result-object v43

    move-object/from16 v5, v43

    check-cast v5, LX/0sl;

    move-object/from16 v43, v5

    .line 97017
    iget-object v5, v2, LX/2fo;->A3a:LX/01P;

    .line 97018
    invoke-interface {v5}, LX/01P;->get()Ljava/lang/Object;

    move-result-object v42

    move-object/from16 v5, v42

    check-cast v5, LX/0ny;

    move-object/from16 v42, v5

    .line 97019
    iget-object v5, v2, LX/2fo;->ABw:LX/01P;

    .line 97020
    invoke-interface {v5}, LX/01P;->get()Ljava/lang/Object;

    move-result-object v41

    move-object/from16 v5, v41

    check-cast v5, LX/0qQ;

    move-object/from16 v41, v5

    .line 97021
    iget-object v5, v2, LX/2fo;->ANz:LX/01P;

    .line 97022
    invoke-interface {v5}, LX/01P;->get()Ljava/lang/Object;

    move-result-object v40

    move-object/from16 v5, v40

    check-cast v5, LX/0sB;

    move-object/from16 v40, v5

    .line 97023
    iget-object v5, v2, LX/2fo;->A3b:LX/01P;

    .line 97024
    invoke-interface {v5}, LX/01P;->get()Ljava/lang/Object;

    move-result-object v39

    move-object/from16 v5, v39

    check-cast v5, LX/10b;

    move-object/from16 v39, v5

    .line 97025
    iget-object v5, v2, LX/2fo;->AMp:LX/01P;

    .line 97026
    invoke-interface {v5}, LX/01P;->get()Ljava/lang/Object;

    move-result-object v38

    move-object/from16 v5, v38

    check-cast v5, LX/1Ea;

    move-object/from16 v38, v5

    .line 97027
    iget-object v5, v2, LX/2fo;->ANF:LX/01P;

    .line 97028
    invoke-interface {v5}, LX/01P;->get()Ljava/lang/Object;

    move-result-object v37

    move-object/from16 v5, v37

    check-cast v5, LX/0za;

    move-object/from16 v37, v5

    .line 97029
    iget-object v5, v2, LX/2fo;->AKG:LX/01P;

    .line 97030
    invoke-interface {v5}, LX/01P;->get()Ljava/lang/Object;

    move-result-object v36

    move-object/from16 v5, v36

    check-cast v5, LX/11W;

    move-object/from16 v36, v5

    .line 97031
    iget-object v5, v2, LX/2fo;->ANy:LX/01P;

    .line 97032
    invoke-interface {v5}, LX/01P;->get()Ljava/lang/Object;

    move-result-object v35

    move-object/from16 v5, v35

    check-cast v5, LX/0kh;

    move-object/from16 v35, v5

    .line 97033
    iget-object v5, v2, LX/2fo;->A3t:LX/01P;

    .line 97034
    invoke-interface {v5}, LX/01P;->get()Ljava/lang/Object;

    move-result-object v15

    check-cast v15, LX/15M;

    .line 97035
    iget-object v5, v2, LX/2fo;->AAj:LX/01P;

    .line 97036
    invoke-interface {v5}, LX/01P;->get()Ljava/lang/Object;

    move-result-object v14

    check-cast v14, LX/13b;

    .line 97037
    iget-object v5, v2, LX/2fo;->A7B:LX/01P;

    .line 97038
    invoke-interface {v5}, LX/01P;->get()Ljava/lang/Object;

    move-result-object v13

    check-cast v13, LX/181;

    .line 97039
    iget-object v5, v2, LX/2fo;->A42:LX/01P;

    .line 97040
    invoke-interface {v5}, LX/01P;->get()Ljava/lang/Object;

    move-result-object v12

    check-cast v12, LX/0zy;

    .line 97041
    iget-object v5, v2, LX/2fo;->A5t:LX/01P;

    .line 97042
    invoke-interface {v5}, LX/01P;->get()Ljava/lang/Object;

    move-result-object v11

    check-cast v11, LX/17x;

    .line 97043
    iget-object v5, v2, LX/2fo;->A9P:LX/01P;

    .line 97044
    invoke-interface {v5}, LX/01P;->get()Ljava/lang/Object;

    move-result-object v10

    check-cast v10, LX/0nR;

    .line 97045
    iget-object v5, v2, LX/2fo;->A3R:LX/01P;

    .line 97046
    invoke-interface {v5}, LX/01P;->get()Ljava/lang/Object;

    move-result-object v9

    check-cast v9, LX/17w;

    .line 97047
    iget-object v5, v2, LX/2fo;->A4u:LX/01P;

    .line 97048
    invoke-interface {v5}, LX/01P;->get()Ljava/lang/Object;

    move-result-object v8

    check-cast v8, LX/0w1;

    .line 97049
    iget-object v5, v2, LX/2fo;->A9Q:LX/01P;

    .line 97050
    invoke-interface {v5}, LX/01P;->get()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, LX/128;

    .line 97051
    iget-object v5, v2, LX/2fo;->AJw:LX/01P;

    .line 97052
    invoke-interface {v5}, LX/01P;->get()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, LX/195;

    .line 97053
    iget-object v5, v2, LX/2fo;->A5J:LX/01P;

    .line 97054
    invoke-interface {v5}, LX/01P;->get()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, LX/17o;

    .line 97055
    iget-object v2, v2, LX/2fo;->ALk:LX/01P;

    .line 97056
    invoke-interface {v2}, LX/01P;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/1DM;

    new-instance v63, LX/2nN;

    move-object/from16 v66, v63

    move-object/from16 v68, v60

    move-object/from16 v69, v1

    move-object/from16 v70, v61

    move-object/from16 v71, v59

    move-object/from16 v72, v55

    move-object/from16 v73, v54

    move-object/from16 v74, v40

    move-object/from16 v75, v21

    move-object/from16 v76, v39

    move-object/from16 v77, v46

    move-object/from16 v78, v15

    move-object/from16 v79, v51

    move-object/from16 v80, v45

    move-object/from16 v81, v37

    move-object/from16 v82, v1

    move-object/from16 v83, v13

    move-object/from16 v84, v5

    move-object/from16 v85, v41

    move-object/from16 v86, v64

    move-object/from16 v87, v35

    move-object/from16 v88, v50

    move-object/from16 v89, v53

    move-object/from16 v90, v43

    move-object/from16 v91, v57

    move-object/from16 v92, v8

    move-object/from16 v93, v10

    move-object/from16 v94, v14

    move-object/from16 v95, v38

    move-object/from16 v97, v12

    move-object/from16 v98, v36

    move-object/from16 v99, v62

    move-object/from16 v101, v47

    move-object/from16 v102, v7

    move-object/from16 v103, v2

    move-object/from16 v104, v65

    move-object/from16 v105, v49

    move-object/from16 v106, v42

    move-object/from16 v107, v6

    move-object/from16 v108, v44

    move-object/from16 v109, v56

    move-object/from16 v110, v9

    move-object/from16 v111, v11

    move-object/from16 v112, v58

    move-object/from16 v113, v48

    move-object/from16 v114, v52

    invoke-direct/range {v66 .. v114}, LX/2nN;-><init>(LX/00i;LX/0nf;LX/0kM;LX/0li;LX/0nN;LX/0nB;LX/0oS;LX/0sB;LX/2Fu;LX/10b;LX/11o;LX/15M;LX/0nL;LX/10Z;LX/0za;LX/0kS;LX/181;LX/17o;LX/0qQ;LX/0lm;LX/0kh;LX/00z;LX/0ki;LX/0sl;LX/0uN;LX/0w1;LX/0nR;LX/13b;LX/1Ea;LX/0mJ;LX/0zy;LX/11W;LX/0kj;LX/0zB;LX/0vp;LX/128;LX/1DM;LX/0nO;LX/10W;LX/0ny;LX/195;LX/18Z;LX/184;LX/17w;LX/17x;LX/0lO;LX/1F5;LX/0y8;)V

    .line 97057
    :goto_17
    iget-object v5, v1, Lcom/soula2/Conversation;->A1h:LX/1NR;

    move-object/from16 v2, v115

    invoke-interface {v2, v5}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 97058
    move-object v5, v2

    move-object/from16 v2, v63

    invoke-interface {v5, v2}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 97059
    iget-object v5, v1, Lcom/soula2/Conversation;->A57:Ljava/util/Set;

    invoke-interface {v5, v2}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 97060
    iget-object v2, v1, Lcom/soula2/Conversation;->A2v:Lcom/soula2/mentions/MentionableEntry;

    invoke-virtual {v2}, Landroid/view/View;->requestFocus()Z

    .line 97061
    new-instance v14, Ljava/util/ArrayList;

    invoke-direct {v14}, Ljava/util/ArrayList;-><init>()V

    .line 97062
    iget-object v13, v1, LX/0kB;->A0q:LX/15r;

    iget-object v12, v1, Lcom/soula2/Conversation;->A2N:LX/0uN;

    iget-object v11, v1, LX/0kH;->A0B:LX/17a;

    iget-object v10, v1, LX/0kH;->A06:LX/0nB;

    iget-object v9, v1, LX/0kH;->A08:LX/01e;

    iget-object v8, v1, Lcom/soula2/Conversation;->A3I:LX/0pe;

    iget-object v7, v1, Lcom/soula2/Conversation;->A2b:LX/0mJ;

    iget-object v6, v1, Lcom/soula2/Conversation;->A0L:Landroid/view/ViewGroup;

    iget-boolean v5, v1, Lcom/soula2/Conversation;->A4S:Z

    new-instance v2, LX/2nK;

    move-object/from16 v35, v2

    move-object/from16 v36, v6

    move-object/from16 v37, v1

    move-object/from16 v38, v10

    move-object/from16 v39, v9

    move-object/from16 v40, v12

    move-object/from16 v41, v7

    move-object/from16 v42, v11

    move-object/from16 v43, v8

    move-object/from16 v44, v13

    move/from16 v45, v5

    invoke-direct/range {v35 .. v45}, LX/2nK;-><init>(Landroid/view/ViewGroup;Lcom/soula2/Conversation;LX/0nB;LX/01e;LX/0uN;LX/0mJ;LX/17a;LX/0pe;LX/15r;Z)V

    invoke-virtual {v14, v2}, Ljava/util/AbstractCollection;->add(Ljava/lang/Object;)Z

    .line 97063
    iget-object v2, v1, Lcom/soula2/Conversation;->A2r:LX/0l8;

    invoke-static {v2}, Lcom/whatsapp/jid/UserJid;->of(Lcom/whatsapp/jid/Jid;)Lcom/whatsapp/jid/UserJid;

    move-result-object v44

    if-eqz v44, :cond_51

    .line 97064
    iget-object v2, v1, LX/0kF;->A01:LX/0nN;

    move-object/from16 v21, v2

    iget-object v15, v1, LX/0kJ;->A05:LX/0lO;

    iget-object v13, v1, LX/0kB;->A07:LX/0nL;

    iget-object v12, v1, LX/0kB;->A09:LX/0nS;

    iget-object v11, v1, Lcom/soula2/Conversation;->A30:LX/134;

    iget-object v10, v1, Lcom/soula2/Conversation;->A23:LX/00z;

    iget-object v9, v1, Lcom/soula2/Conversation;->A2K:LX/1Ds;

    iget-object v8, v1, Lcom/soula2/Conversation;->A2b:LX/0mJ;

    iget-object v7, v1, Lcom/soula2/Conversation;->A0w:Lcom/soula2/KeyboardPopupLayout;

    iget-boolean v6, v1, Lcom/soula2/Conversation;->A4S:Z

    iget-boolean v5, v1, Lcom/soula2/Conversation;->A4T:Z

    new-instance v2, LX/2FU;

    move-object/from16 v35, v2

    move-object/from16 v36, v7

    move-object/from16 v38, v21

    move-object/from16 v39, v13

    move-object/from16 v40, v12

    move-object/from16 v41, v10

    move-object/from16 v42, v9

    move-object/from16 v43, v8

    move-object/from16 v45, v11

    move-object/from16 v46, v15

    move/from16 v47, v6

    move/from16 v48, v5

    invoke-direct/range {v35 .. v48}, LX/2FU;-><init>(Landroid/view/ViewGroup;Lcom/soula2/Conversation;LX/0nN;LX/0nL;LX/0nS;LX/00z;LX/1Ds;LX/0mJ;Lcom/whatsapp/jid/UserJid;LX/134;LX/0lO;ZZ)V

    invoke-virtual {v14, v2}, Ljava/util/AbstractCollection;->add(Ljava/lang/Object;)Z

    .line 97065
    :cond_51
    new-instance v2, LX/2FT;

    invoke-direct {v2, v14}, LX/2FT;-><init>(Ljava/util/List;)V

    iput-object v2, v1, Lcom/soula2/Conversation;->A1b:LX/2FT;

    .line 97066
    invoke-virtual {v1}, Lcom/soula2/Conversation;->A2w()V

    .line 97067
    iget-object v2, v1, Lcom/soula2/Conversation;->A39:LX/17m;

    .line 97068
    move-object/from16 v5, v19

    iget-object v5, v5, LX/0me;->A0U:Ljava/util/List;

    invoke-interface {v5, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 97069
    invoke-virtual/range {v20 .. v20}, LX/1L4;->A01()J

    .line 97070
    move-object/from16 v2, v27

    invoke-virtual {v1, v2}, LX/0kK;->A1m(Ljava/lang/String;)V

    .line 97071
    iget-object v5, v1, Lcom/soula2/Conversation;->A3C:LX/150;

    iget-object v2, v1, Lcom/soula2/Conversation;->A2r:LX/0l8;

    invoke-virtual {v5, v2}, LX/150;->A08(LX/0l8;)I

    move-result v6

    .line 97072
    const v2, 0x7f0a0c6c

    .line 97073
    invoke-static {v1, v2}, LX/00S;->A05(Landroid/app/Activity;I)Landroid/view/View;

    move-result-object v2

    new-instance v5, LX/0l3;

    invoke-direct {v5, v2}, LX/0l3;-><init>(Landroid/view/View;)V

    iput-object v5, v1, Lcom/soula2/Conversation;->A3t:LX/0l3;

    .line 97074
    new-instance v2, LX/3Cl;

    invoke-direct {v2, v1, v6}, LX/3Cl;-><init>(Lcom/soula2/Conversation;I)V

    invoke-virtual {v5, v2}, LX/0l3;->A04(LX/23k;)V

    .line 97075
    iget-object v2, v1, Lcom/soula2/Conversation;->A2v:Lcom/soula2/mentions/MentionableEntry;

    invoke-virtual {v2}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v2

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_52

    invoke-virtual {v1, v6}, Lcom/soula2/Conversation;->A3F(I)Z

    move-result v2

    if-eqz v2, :cond_52

    .line 97076
    invoke-virtual {v1, v6}, Lcom/soula2/Conversation;->A2z(I)V

    .line 97077
    iget-object v2, v1, Lcom/soula2/Conversation;->A3t:LX/0l3;

    invoke-virtual {v2, v3}, LX/0l3;->A02(I)V

    .line 97078
    :cond_52
    new-instance v53, LX/48G;

    invoke-direct/range {v53 .. v53}, LX/48G;-><init>()V

    .line 97079
    iget-object v2, v1, LX/0kH;->A0C:LX/0kj;

    move-object/from16 v61, v2

    iget-object v2, v1, LX/0kH;->A05:LX/0li;

    move-object/from16 v39, v2

    iget-object v2, v1, LX/0kF;->A0B:LX/15n;

    move-object/from16 v71, v2

    iget-object v2, v1, LX/0kF;->A01:LX/0nN;

    move-object/from16 v40, v2

    iget-object v2, v1, Lcom/soula2/Conversation;->A11:LX/0wg;

    move-object/from16 v41, v2

    iget-object v2, v1, LX/0kH;->A04:LX/0lC;

    move-object/from16 v38, v2

    iget-object v2, v1, LX/0kB;->A0V:LX/0pG;

    move-object/from16 v62, v2

    iget-object v2, v1, LX/0kH;->A06:LX/0nB;

    move-object/from16 v42, v2

    iget-object v2, v1, Lcom/soula2/Conversation;->A1F:LX/18U;

    move-object/from16 v47, v2

    iget-object v2, v1, LX/0kB;->A03:LX/0oS;

    move-object/from16 v43, v2

    iget-object v2, v1, Lcom/soula2/Conversation;->A1D:LX/18W;

    move-object/from16 v46, v2

    iget-object v2, v1, LX/0kF;->A06:LX/0ki;

    move-object/from16 v60, v2

    iget-object v2, v1, Lcom/soula2/Conversation;->A2s:LX/1BD;

    move-object/from16 v64, v2

    iget-object v2, v1, Lcom/soula2/Conversation;->A1J:LX/18H;

    move-object/from16 v49, v2

    iget-object v2, v1, Lcom/soula2/Conversation;->A3n:LX/15g;

    move-object/from16 v72, v2

    iget-object v2, v1, LX/0kH;->A08:LX/01e;

    move-object/from16 v57, v2

    iget-object v2, v1, Lcom/soula2/Conversation;->A1K:LX/0wh;

    move-object/from16 v50, v2

    iget-object v2, v1, Lcom/soula2/Conversation;->A3D:LX/0xx;

    move-object/from16 v68, v2

    iget-object v2, v1, Lcom/soula2/Conversation;->A20:LX/0qQ;

    move-object/from16 v56, v2

    iget-object v2, v1, LX/0kB;->A06:LX/118;

    move-object/from16 v27, v2

    iget-object v2, v1, LX/0kB;->A0L:LX/0o2;

    move-object/from16 v21, v2

    iget-object v2, v1, LX/0kH;->A09:LX/0kh;

    move-object/from16 v20, v2

    iget-object v2, v1, Lcom/soula2/Conversation;->A19:LX/187;

    move-object/from16 v19, v2

    iget-object v15, v1, LX/0kB;->A04:LX/0lA;

    iget-object v14, v1, LX/0kF;->A0A:LX/0z3;

    iget-object v13, v1, Lcom/soula2/Conversation;->A3I:LX/0pe;

    iget-object v12, v1, LX/0kB;->A0I:LX/11Q;

    new-instance v11, LX/4Yc;

    invoke-direct {v11, v1}, LX/4Yc;-><init>(LX/00j;)V

    iget-object v10, v1, Lcom/soula2/Conversation;->A2r:LX/0l8;

    iget-boolean v9, v1, Lcom/soula2/Conversation;->A4P:Z

    iget-object v8, v1, Lcom/soula2/Conversation;->A2v:Lcom/soula2/mentions/MentionableEntry;

    iget-object v7, v1, Lcom/soula2/Conversation;->A3A:LX/3MU;

    iget-object v6, v1, Lcom/soula2/Conversation;->A3B:LX/17s;

    iget-object v5, v1, Lcom/soula2/Conversation;->A1I:LX/17i;

    new-instance v2, LX/1tq;

    move-object/from16 v35, v2

    move-object/from16 v36, v1

    move-object/from16 v44, v19

    move-object/from16 v45, v15

    move-object/from16 v48, v5

    move-object/from16 v51, v27

    move-object/from16 v52, v1

    move-object/from16 v54, v12

    move-object/from16 v55, v11

    move-object/from16 v58, v21

    move-object/from16 v59, v20

    move-object/from16 v63, v10

    move-object/from16 v65, v8

    move-object/from16 v66, v7

    move-object/from16 v67, v6

    move-object/from16 v69, v13

    move-object/from16 v70, v14

    move/from16 v73, v9

    invoke-direct/range {v35 .. v73}, LX/1tq;-><init>(LX/00i;LX/0kM;LX/0lC;LX/0li;LX/0nN;LX/0wg;LX/0nB;LX/0oS;LX/187;LX/0lA;LX/18W;LX/18U;LX/17i;LX/18H;LX/0wh;LX/118;LX/0kR;LX/48G;LX/11Q;LX/1bu;LX/0qQ;LX/01e;LX/0o2;LX/0kh;LX/0ki;LX/0kj;LX/0pG;LX/0l8;LX/1BD;Lcom/soula2/mentions/MentionableEntry;LX/3MU;LX/17s;LX/0xx;LX/0pe;LX/0z3;LX/15n;LX/15g;Z)V

    iput-object v2, v1, Lcom/soula2/Conversation;->A1a:LX/1tq;

    .line 97080
    move-object/from16 v5, v115

    invoke-interface {v5, v2}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 97081
    iget-object v14, v1, Lcom/soula2/Conversation;->A55:Ljava/util/Set;

    iget-object v2, v1, Lcom/soula2/Conversation;->A1a:LX/1tq;

    invoke-interface {v14, v2}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 97082
    iget-object v5, v1, Lcom/soula2/Conversation;->A58:Ljava/util/Set;

    iget-object v2, v1, Lcom/soula2/Conversation;->A1a:LX/1tq;

    invoke-interface {v5, v2}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 97083
    const v2, 0x7f0a0585

    invoke-virtual {v1, v2}, LX/00i;->findViewById(I)Landroid/view/View;

    move-result-object v15

    if-eqz v15, :cond_53

    .line 97084
    iget-object v5, v1, LX/0kH;->A0C:LX/0kj;

    const/16 v2, 0x588

    .line 97085
    invoke-virtual {v5, v2}, LX/0kj;->A07(I)Z

    move-result v2

    if-eqz v2, :cond_53

    .line 97086
    iget-object v2, v1, Lcom/soula2/Conversation;->A0j:LX/2Dr;

    iget-object v5, v1, Lcom/soula2/Conversation;->A2v:Lcom/soula2/mentions/MentionableEntry;

    move-object/from16 v19, v5

    iget-object v13, v1, Lcom/soula2/Conversation;->A2r:LX/0l8;

    .line 97087
    iget-object v2, v2, LX/2Dr;->A00:LX/2A6;

    .line 97088
    iget-object v5, v2, LX/2A6;->A01:LX/29l;

    .line 97089
    iget-object v12, v5, LX/29l;->A1H:Landroid/app/Activity;

    .line 97090
    iget-object v2, v2, LX/2A6;->A03:LX/2fo;

    .line 97091
    iget-object v5, v2, LX/2fo;->AIu:LX/01P;

    .line 97092
    invoke-interface {v5}, LX/01P;->get()Ljava/lang/Object;

    move-result-object v11

    check-cast v11, LX/0wg;

    .line 97093
    iget-object v5, v2, LX/2fo;->A8w:LX/01P;

    .line 97094
    invoke-interface {v5}, LX/01P;->get()Ljava/lang/Object;

    move-result-object v10

    check-cast v10, LX/0li;

    .line 97095
    iget-object v5, v2, LX/2fo;->ANw:LX/01P;

    .line 97096
    invoke-interface {v5}, LX/01P;->get()Ljava/lang/Object;

    move-result-object v9

    check-cast v9, LX/0o2;

    .line 97097
    iget-object v5, v2, LX/2fo;->ABh:LX/01P;

    .line 97098
    invoke-interface {v5}, LX/01P;->get()Ljava/lang/Object;

    move-result-object v8

    check-cast v8, LX/15g;

    .line 97099
    iget-object v5, v2, LX/2fo;->A4T:LX/01P;

    .line 97100
    invoke-interface {v5}, LX/01P;->get()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, LX/0nL;

    .line 97101
    iget-object v5, v2, LX/2fo;->ANW:LX/01P;

    .line 97102
    invoke-interface {v5}, LX/01P;->get()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, LX/0nS;

    .line 97103
    iget-object v2, v2, LX/2fo;->A6k:LX/01P;

    .line 97104
    invoke-interface {v2}, LX/01P;->get()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, LX/17a;

    new-instance v2, LX/399;

    move-object/from16 v35, v2

    move-object/from16 v36, v12

    move-object/from16 v37, v10

    move-object/from16 v38, v11

    move-object/from16 v39, v7

    move-object/from16 v40, v6

    move-object/from16 v41, v9

    move-object/from16 v42, v5

    move-object/from16 v43, v13

    move-object/from16 v44, v19

    move-object/from16 v45, v8

    invoke-direct/range {v35 .. v45}, LX/399;-><init>(Landroid/content/Context;LX/0li;LX/0wg;LX/0nL;LX/0nS;LX/0o2;LX/17a;LX/0l8;Lcom/soula2/mentions/MentionableEntry;LX/15g;)V

    .line 97105
    iget-object v5, v1, Lcom/soula2/Conversation;->A0k:LX/2Dq;

    .line 97106
    iget-object v5, v5, LX/2Dq;->A00:LX/2A6;

    .line 97107
    iget-object v5, v5, LX/2A6;->A01:LX/29l;

    .line 97108
    iget-object v6, v5, LX/29l;->A1H:Landroid/app/Activity;

    .line 97109
    new-instance v5, LX/35v;

    invoke-direct {v5, v6, v2}, LX/35v;-><init>(Landroid/content/Context;LX/399;)V

    .line 97110
    invoke-virtual {v15, v5}, Landroid/view/View;->setOnDragListener(Landroid/view/View$OnDragListener;)V

    .line 97111
    invoke-interface {v14, v2}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 97112
    :cond_53
    new-instance v40, LX/429;

    invoke-direct/range {v40 .. v40}, LX/429;-><init>()V

    .line 97113
    iget-object v11, v1, Lcom/soula2/Conversation;->A2r:LX/0l8;

    iget-object v10, v1, LX/0kH;->A05:LX/0li;

    iget-object v9, v1, LX/0kJ;->A05:LX/0lO;

    iget-object v8, v1, LX/0kH;->A08:LX/01e;

    iget-object v7, v1, Lcom/soula2/Conversation;->A3Q:LX/0nw;

    iget-object v5, v1, Lcom/soula2/Conversation;->A1c:Lcom/soula2/conversation/ConversationListView;

    .line 97114
    const/16 v2, 0x28

    new-instance v6, Lcom/facebook/redex/RunnableRunnableShape1S0100000_I0;

    invoke-direct {v6, v5, v2}, Lcom/facebook/redex/RunnableRunnableShape1S0100000_I0;-><init>(Ljava/lang/Object;I)V

    iget-object v5, v1, Lcom/soula2/Conversation;->A0w:Lcom/soula2/KeyboardPopupLayout;

    new-instance v2, LX/2iL;

    move-object/from16 v35, v2

    move-object/from16 v36, v1

    move-object/from16 v37, v5

    move-object/from16 v38, v1

    move-object/from16 v39, v10

    move-object/from16 v41, v8

    move-object/from16 v42, v11

    move-object/from16 v43, v7

    move-object/from16 v44, v18

    move-object/from16 v45, v9

    move-object/from16 v46, v6

    invoke-direct/range {v35 .. v46}, LX/2iL;-><init>(Landroid/app/Activity;Landroid/view/ViewGroup;LX/0kM;LX/0li;LX/429;LX/01e;LX/0l8;LX/0nw;Lcom/soula2/settings/chat/wallpaper/WallPaperView;LX/0lO;Ljava/lang/Runnable;)V

    iput-object v2, v1, Lcom/soula2/Conversation;->A1z:LX/2iL;

    .line 97115
    move-object/from16 v5, v115

    invoke-interface {v5, v2}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 97116
    iget-object v2, v1, Lcom/soula2/Conversation;->A1z:LX/2iL;

    iget-object v2, v2, LX/2iL;->A02:LX/398;

    invoke-interface {v14, v2}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 97117
    move-object/from16 v2, v29

    invoke-virtual {v1, v2}, LX/0kK;->A1n(Ljava/lang/String;)V

    .line 97118
    invoke-interface/range {v115 .. v115}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :goto_18
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_57

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/app/Application$ActivityLifecycleCallbacks;

    .line 97119
    move-object/from16 v5, v16

    invoke-interface {v2, v1, v5}, Landroid/app/Application$ActivityLifecycleCallbacks;->onActivityCreated(Landroid/app/Activity;Landroid/os/Bundle;)V

    goto :goto_18

    .line 97120
    :cond_54
    iget-boolean v2, v1, Lcom/soula2/Conversation;->A4T:Z

    if-eqz v2, :cond_56

    .line 97121
    iget-object v14, v1, Lcom/soula2/Conversation;->A2r:LX/0l8;

    .line 97122
    instance-of v2, v14, LX/1UD;

    if-eqz v2, :cond_55

    .line 97123
    check-cast v14, LX/1UD;

    .line 97124
    :goto_19
    invoke-static {v14}, LX/009;->A05(Ljava/lang/Object;)V

    .line 97125
    iget-object v2, v1, LX/0kH;->A0C:LX/0kj;

    move-object/from16 v53, v2

    iget-object v2, v1, LX/0kH;->A05:LX/0li;

    move-object/from16 v38, v2

    iget-object v2, v1, LX/0kJ;->A05:LX/0lO;

    move-object/from16 v58, v2

    iget-object v2, v1, Lcom/soula2/Conversation;->A2N:LX/0uN;

    move-object/from16 v48, v2

    iget-object v15, v1, LX/0kH;->A0B:LX/17a;

    iget-object v13, v1, Lcom/soula2/Conversation;->A1R:LX/18Y;

    iget-object v12, v1, Lcom/soula2/Conversation;->A0d:LX/18I;

    iget-object v11, v1, LX/0kB;->A09:LX/0nS;

    iget-object v10, v1, Lcom/soula2/Conversation;->A23:LX/00z;

    iget-object v9, v1, Lcom/soula2/Conversation;->A1S:LX/10Z;

    iget-object v8, v1, LX/0kB;->A0X:LX/0vp;

    .line 97126
    iget-object v2, v1, LX/0kB;->A0F:LX/1Mq;

    invoke-virtual {v2, v1}, LX/1Mq;->A01(Landroid/content/Context;)LX/1Hc;

    move-result-object v45

    .line 97127
    iget-object v2, v1, LX/0kB;->A0k:LX/18Z;

    move-object/from16 v57, v2

    iget-object v2, v1, Lcom/soula2/Conversation;->A1N:LX/10b;

    move-object/from16 v59, v2

    iget-object v7, v1, Lcom/soula2/Conversation;->A1A:LX/0zc;

    iget-object v6, v1, LX/0kB;->A0N:LX/0nR;

    iget-object v5, v1, Lcom/soula2/Conversation;->A1W:LX/14y;

    iget-object v2, v1, Lcom/soula2/Conversation;->A2O:LX/0w1;

    move-object/from16 v49, v2

    iget-object v2, v1, Lcom/soula2/Conversation;->A2q:LX/128;

    move-object/from16 v55, v2

    iget-object v2, v1, Lcom/soula2/Conversation;->A2b:LX/0mJ;

    move-object/from16 v51, v2

    new-instance v2, LX/2nO;

    move-object/from16 v35, v2

    move-object/from16 v36, v1

    move-object/from16 v37, v12

    move-object/from16 v39, v21

    move-object/from16 v40, v7

    move-object/from16 v41, v59

    move-object/from16 v42, v13

    move-object/from16 v43, v9

    move-object/from16 v44, v11

    move-object/from16 v46, v5

    move-object/from16 v47, v10

    move-object/from16 v50, v6

    move-object/from16 v52, v15

    move-object/from16 v54, v8

    move-object/from16 v56, v14

    invoke-direct/range {v35 .. v58}, LX/2nO;-><init>(LX/00i;LX/18I;LX/0li;LX/2Fu;LX/0zc;LX/10b;LX/18Y;LX/10Z;LX/0nS;LX/1Hc;LX/14y;LX/00z;LX/0uN;LX/0w1;LX/0nR;LX/0mJ;LX/17a;LX/0kj;LX/0vp;LX/128;LX/1UD;LX/18Z;LX/0lO;)V

    iput-object v2, v1, Lcom/soula2/Conversation;->A1h:LX/1NR;

    .line 97128
    iget-object v2, v1, LX/0kH;->A0C:LX/0kj;

    move-object/from16 v46, v2

    iget-object v2, v1, LX/0kH;->A05:LX/0li;

    move-object/from16 v45, v2

    iget-object v2, v1, LX/0kF;->A01:LX/0nN;

    move-object/from16 v44, v2

    iget-object v2, v1, LX/0kJ;->A05:LX/0lO;

    move-object/from16 v43, v2

    iget-object v2, v1, Lcom/soula2/Conversation;->A2N:LX/0uN;

    move-object/from16 v42, v2

    iget-object v2, v1, Lcom/soula2/Conversation;->A3U:LX/184;

    move-object/from16 v41, v2

    iget-object v2, v1, LX/0kH;->A06:LX/0nB;

    move-object/from16 v40, v2

    iget-object v2, v1, LX/0kB;->A03:LX/0oS;

    move-object/from16 v39, v2

    iget-object v2, v1, Lcom/soula2/Conversation;->A2h:LX/0zB;

    move-object/from16 v38, v2

    iget-object v2, v1, LX/0kF;->A06:LX/0ki;

    move-object/from16 v37, v2

    iget-object v2, v1, Lcom/soula2/Conversation;->A47:LX/0y8;

    move-object/from16 v36, v2

    iget-object v2, v1, Lcom/soula2/Conversation;->A23:LX/00z;

    move-object/from16 v35, v2

    iget-object v15, v1, Lcom/soula2/Conversation;->A1O:LX/11o;

    iget-object v13, v1, Lcom/soula2/Conversation;->A1S:LX/10Z;

    iget-object v12, v1, Lcom/soula2/Conversation;->A3P:LX/0ny;

    iget-object v11, v1, Lcom/soula2/Conversation;->A20:LX/0qQ;

    iget-object v10, v1, Lcom/soula2/Conversation;->A17:LX/0sB;

    iget-object v9, v1, Lcom/soula2/Conversation;->A1U:LX/0za;

    iget-object v8, v1, LX/0kH;->A09:LX/0kh;

    iget-object v7, v1, Lcom/soula2/Conversation;->A1j:LX/181;

    iget-object v6, v1, Lcom/soula2/Conversation;->A2c:LX/0zy;

    iget-object v5, v1, Lcom/soula2/Conversation;->A3m:LX/17x;

    iget-object v2, v1, Lcom/soula2/Conversation;->A1s:LX/17o;

    new-instance v63, LX/2nL;

    move-object/from16 v64, v1

    move-object/from16 v65, v1

    move-object/from16 v66, v45

    move-object/from16 v67, v44

    move-object/from16 v68, v40

    move-object/from16 v69, v39

    move-object/from16 v70, v10

    move-object/from16 v71, v21

    move-object/from16 v72, v59

    move-object/from16 v73, v15

    move-object/from16 v74, v13

    move-object/from16 v75, v9

    move-object/from16 v76, v1

    move-object/from16 v77, v7

    move-object/from16 v78, v2

    move-object/from16 v79, v11

    move-object/from16 v80, v8

    move-object/from16 v81, v35

    move-object/from16 v82, v37

    move-object/from16 v83, v42

    move-object/from16 v84, v49

    move-object/from16 v85, v51

    move-object/from16 v86, v6

    move-object/from16 v87, v46

    move-object/from16 v88, v38

    move-object/from16 v89, v55

    move-object/from16 v90, v14

    move-object/from16 v91, v12

    move-object/from16 v92, v57

    move-object/from16 v93, v41

    move-object/from16 v94, v5

    move-object/from16 v95, v43

    move-object/from16 v96, v36

    invoke-direct/range {v63 .. v96}, LX/2nL;-><init>(LX/00i;LX/0kM;LX/0li;LX/0nN;LX/0nB;LX/0oS;LX/0sB;LX/2Fu;LX/10b;LX/11o;LX/10Z;LX/0za;LX/0kS;LX/181;LX/17o;LX/0qQ;LX/0kh;LX/00z;LX/0ki;LX/0uN;LX/0w1;LX/0mJ;LX/0zy;LX/0kj;LX/0zB;LX/128;LX/1UD;LX/0ny;LX/18Z;LX/184;LX/17x;LX/0lO;LX/0y8;)V

    goto/16 :goto_17

    .line 97129
    :cond_55
    const/4 v14, 0x0

    goto/16 :goto_19

    .line 97130
    :cond_56
    iget-object v5, v1, Lcom/soula2/Conversation;->A0o:LX/2Dl;

    .line 97131
    iget-object v2, v1, LX/0kB;->A0F:LX/1Mq;

    invoke-virtual {v2, v1}, LX/1Mq;->A01(Landroid/content/Context;)LX/1Hc;

    move-result-object v52

    .line 97132
    iget-object v2, v1, Lcom/soula2/Conversation;->A2O:LX/0w1;

    move-object/from16 v58, v2

    iget-object v2, v1, Lcom/soula2/Conversation;->A2r:LX/0l8;

    move-object/from16 v66, v2

    .line 97133
    invoke-static/range {v66 .. v66}, LX/009;->A05(Ljava/lang/Object;)V

    iget-object v2, v1, Lcom/soula2/Conversation;->A2b:LX/0mJ;

    move-object/from16 v60, v2

    .line 97134
    iget-object v5, v5, LX/2Dl;->A00:LX/2A6;

    .line 97135
    iget-object v13, v5, LX/2A6;->A03:LX/2fo;

    .line 97136
    iget-object v2, v13, LX/2fo;->AMT:LX/01P;

    .line 97137
    invoke-interface {v2}, LX/01P;->get()Ljava/lang/Object;

    move-result-object v41

    move-object/from16 v2, v41

    check-cast v2, LX/0lm;

    move-object/from16 v41, v2

    .line 97138
    iget-object v2, v13, LX/2fo;->A04:LX/01P;

    .line 97139
    invoke-interface {v2}, LX/01P;->get()Ljava/lang/Object;

    move-result-object v40

    move-object/from16 v2, v40

    check-cast v2, LX/0kj;

    move-object/from16 v40, v2

    .line 97140
    iget-object v2, v13, LX/2fo;->A8w:LX/01P;

    .line 97141
    invoke-interface {v2}, LX/01P;->get()Ljava/lang/Object;

    move-result-object v39

    move-object/from16 v2, v39

    check-cast v2, LX/0li;

    move-object/from16 v39, v2

    .line 97142
    iget-object v2, v13, LX/2fo;->AOa:LX/01P;

    .line 97143
    invoke-interface {v2}, LX/01P;->get()Ljava/lang/Object;

    move-result-object v38

    move-object/from16 v2, v38

    check-cast v2, LX/0lO;

    move-object/from16 v38, v2

    .line 97144
    iget-object v2, v13, LX/2fo;->A3f:LX/01P;

    .line 97145
    invoke-interface {v2}, LX/01P;->get()Ljava/lang/Object;

    move-result-object v37

    move-object/from16 v2, v37

    check-cast v2, LX/0uN;

    move-object/from16 v37, v2

    .line 97146
    iget-object v2, v13, LX/2fo;->A6k:LX/01P;

    .line 97147
    invoke-interface {v2}, LX/01P;->get()Ljava/lang/Object;

    move-result-object v36

    move-object/from16 v2, v36

    check-cast v2, LX/17a;

    move-object/from16 v36, v2

    .line 97148
    iget-object v2, v5, LX/2A6;->A01:LX/29l;

    .line 97149
    invoke-virtual {v2}, LX/29l;->A08()LX/0zB;

    move-result-object v63

    .line 97150
    iget-object v2, v13, LX/2fo;->A4P:LX/01P;

    .line 97151
    invoke-interface {v2}, LX/01P;->get()Ljava/lang/Object;

    move-result-object v35

    move-object/from16 v2, v35

    check-cast v2, LX/18Y;

    move-object/from16 v35, v2

    .line 97152
    iget-object v2, v13, LX/2fo;->A4a:LX/01P;

    .line 97153
    invoke-interface {v2}, LX/01P;->get()Ljava/lang/Object;

    move-result-object v15

    check-cast v15, LX/18I;

    .line 97154
    iget-object v2, v13, LX/2fo;->ANW:LX/01P;

    .line 97155
    invoke-interface {v2}, LX/01P;->get()Ljava/lang/Object;

    move-result-object v14

    check-cast v14, LX/0nS;

    .line 97156
    iget-object v2, v13, LX/2fo;->AOX:LX/01P;

    .line 97157
    invoke-interface {v2}, LX/01P;->get()Ljava/lang/Object;

    move-result-object v12

    check-cast v12, LX/00z;

    .line 97158
    iget-object v2, v13, LX/2fo;->A4U:LX/01P;

    .line 97159
    invoke-interface {v2}, LX/01P;->get()Ljava/lang/Object;

    move-result-object v11

    check-cast v11, LX/10Z;

    .line 97160
    iget-object v2, v13, LX/2fo;->A9D:LX/01P;

    .line 97161
    invoke-interface {v2}, LX/01P;->get()Ljava/lang/Object;

    move-result-object v10

    check-cast v10, LX/0vp;

    .line 97162
    iget-object v2, v13, LX/2fo;->AKB:LX/01P;

    .line 97163
    invoke-interface {v2}, LX/01P;->get()Ljava/lang/Object;

    move-result-object v9

    check-cast v9, LX/18Z;

    .line 97164
    iget-object v2, v13, LX/2fo;->A3b:LX/01P;

    .line 97165
    invoke-interface {v2}, LX/01P;->get()Ljava/lang/Object;

    move-result-object v8

    check-cast v8, LX/10b;

    .line 97166
    iget-object v2, v13, LX/2fo;->A2k:LX/01P;

    .line 97167
    invoke-interface {v2}, LX/01P;->get()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, LX/0zc;

    .line 97168
    iget-object v2, v13, LX/2fo;->A9P:LX/01P;

    .line 97169
    invoke-interface {v2}, LX/01P;->get()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, LX/0nR;

    .line 97170
    iget-object v2, v13, LX/2fo;->A4X:LX/01P;

    .line 97171
    invoke-interface {v2}, LX/01P;->get()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, LX/14y;

    .line 97172
    iget-object v2, v13, LX/2fo;->A9Q:LX/01P;

    .line 97173
    invoke-interface {v2}, LX/01P;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/128;

    invoke-static {v13}, LX/2fo;->A10(LX/2fo;)LX/0rJ;

    move-result-object v56

    new-instance v13, LX/1NQ;

    move-object/from16 v42, v13

    move-object/from16 v43, v1

    move-object/from16 v44, v15

    move-object/from16 v45, v39

    move-object/from16 v46, v21

    move-object/from16 v47, v7

    move-object/from16 v48, v8

    move-object/from16 v49, v35

    move-object/from16 v50, v11

    move-object/from16 v51, v14

    move-object/from16 v53, v5

    move-object/from16 v54, v41

    move-object/from16 v55, v12

    move-object/from16 v57, v37

    move-object/from16 v59, v6

    move-object/from16 v61, v36

    move-object/from16 v62, v40

    move-object/from16 v64, v10

    move-object/from16 v65, v2

    move-object/from16 v67, v9

    move-object/from16 v68, v38

    invoke-direct/range {v42 .. v68}, LX/1NQ;-><init>(LX/00i;LX/18I;LX/0li;LX/2Fu;LX/0zc;LX/10b;LX/18Y;LX/10Z;LX/0nS;LX/1Hc;LX/14y;LX/0lm;LX/00z;LX/0rJ;LX/0uN;LX/0w1;LX/0nR;LX/0mJ;LX/17a;LX/0kj;LX/0zB;LX/0vp;LX/128;LX/0l8;LX/18Z;LX/0lO;)V

    .line 97174
    iput-object v13, v1, Lcom/soula2/Conversation;->A1h:LX/1NR;

    .line 97175
    iget-object v5, v1, Lcom/soula2/Conversation;->A0g:LX/2Du;

    iget-object v2, v1, Lcom/soula2/Conversation;->A2r:LX/0l8;

    move-object/from16 v102, v2

    iget-object v2, v1, Lcom/soula2/Conversation;->A2b:LX/0mJ;

    move-object/from16 v94, v2

    .line 97176
    iget-object v6, v5, LX/2Du;->A00:LX/2A6;

    .line 97177
    iget-object v2, v6, LX/2A6;->A03:LX/2fo;

    .line 97178
    iget-object v5, v2, LX/2fo;->A04:LX/01P;

    .line 97179
    invoke-interface {v5}, LX/01P;->get()Ljava/lang/Object;

    move-result-object v62

    move-object/from16 v5, v62

    check-cast v5, LX/0kj;

    move-object/from16 v62, v5

    .line 97180
    iget-object v5, v2, LX/2fo;->A8w:LX/01P;

    .line 97181
    invoke-interface {v5}, LX/01P;->get()Ljava/lang/Object;

    move-result-object v61

    move-object/from16 v5, v61

    check-cast v5, LX/0li;

    move-object/from16 v61, v5

    .line 97182
    iget-object v5, v2, LX/2fo;->ABS:LX/01P;

    .line 97183
    invoke-interface {v5}, LX/01P;->get()Ljava/lang/Object;

    move-result-object v60

    move-object/from16 v5, v60

    check-cast v5, LX/0nN;

    move-object/from16 v60, v5

    .line 97184
    iget-object v5, v2, LX/2fo;->AOa:LX/01P;

    .line 97185
    invoke-interface {v5}, LX/01P;->get()Ljava/lang/Object;

    move-result-object v59

    move-object/from16 v5, v59

    check-cast v5, LX/0lO;

    move-object/from16 v59, v5

    .line 97186
    iget-object v5, v2, LX/2fo;->A3f:LX/01P;

    .line 97187
    invoke-interface {v5}, LX/01P;->get()Ljava/lang/Object;

    move-result-object v58

    move-object/from16 v5, v58

    check-cast v5, LX/0uN;

    move-object/from16 v58, v5

    .line 97188
    iget-object v5, v2, LX/2fo;->AKS:LX/01P;

    .line 97189
    invoke-interface {v5}, LX/01P;->get()Ljava/lang/Object;

    move-result-object v57

    move-object/from16 v5, v57

    check-cast v5, LX/184;

    move-object/from16 v57, v5

    .line 97190
    iget-object v5, v2, LX/2fo;->AO9:LX/01P;

    .line 97191
    invoke-interface {v5}, LX/01P;->get()Ljava/lang/Object;

    move-result-object v56

    move-object/from16 v5, v56

    check-cast v5, LX/0pG;

    move-object/from16 v56, v5

    .line 97192
    iget-object v5, v2, LX/2fo;->AJ3:LX/01P;

    .line 97193
    invoke-interface {v5}, LX/01P;->get()Ljava/lang/Object;

    move-result-object v55

    move-object/from16 v5, v55

    check-cast v5, LX/0nB;

    move-object/from16 v55, v5

    .line 97194
    iget-object v5, v2, LX/2fo;->AMw:LX/01P;

    .line 97195
    invoke-interface {v5}, LX/01P;->get()Ljava/lang/Object;

    move-result-object v54

    move-object/from16 v5, v54

    check-cast v5, LX/0oS;

    move-object/from16 v54, v5

    .line 97196
    iget-object v5, v6, LX/2A6;->A01:LX/29l;

    .line 97197
    invoke-virtual {v5}, LX/29l;->A08()LX/0zB;

    move-result-object v99

    .line 97198
    iget-object v5, v2, LX/2fo;->ALW:LX/01P;

    .line 97199
    invoke-interface {v5}, LX/01P;->get()Ljava/lang/Object;

    move-result-object v53

    move-object/from16 v5, v53

    check-cast v5, LX/0ki;

    move-object/from16 v53, v5

    .line 97200
    iget-object v5, v2, LX/2fo;->AB4:LX/01P;

    .line 97201
    invoke-interface {v5}, LX/01P;->get()Ljava/lang/Object;

    move-result-object v52

    move-object/from16 v5, v52

    check-cast v5, LX/15K;

    move-object/from16 v52, v5

    .line 97202
    iget-object v5, v2, LX/2fo;->A0J:LX/01P;

    .line 97203
    invoke-interface {v5}, LX/01P;->get()Ljava/lang/Object;

    move-result-object v51

    move-object/from16 v5, v51

    check-cast v5, LX/154;

    move-object/from16 v51, v5

    .line 97204
    iget-object v5, v2, LX/2fo;->ANW:LX/01P;

    .line 97205
    invoke-interface {v5}, LX/01P;->get()Ljava/lang/Object;

    move-result-object v50

    move-object/from16 v5, v50

    check-cast v5, LX/0nS;

    move-object/from16 v50, v5

    .line 97206
    iget-object v5, v2, LX/2fo;->ANM:LX/01P;

    .line 97207
    invoke-interface {v5}, LX/01P;->get()Ljava/lang/Object;

    move-result-object v49

    move-object/from16 v5, v49

    check-cast v5, LX/0y8;

    move-object/from16 v49, v5

    .line 97208
    iget-object v5, v2, LX/2fo;->AOX:LX/01P;

    .line 97209
    invoke-interface {v5}, LX/01P;->get()Ljava/lang/Object;

    move-result-object v48

    move-object/from16 v5, v48

    check-cast v5, LX/00z;

    move-object/from16 v48, v5

    .line 97210
    iget-object v5, v2, LX/2fo;->AJd:LX/01P;

    .line 97211
    invoke-interface {v5}, LX/01P;->get()Ljava/lang/Object;

    move-result-object v47

    move-object/from16 v5, v47

    check-cast v5, LX/15O;

    move-object/from16 v47, v5

    .line 97212
    iget-object v5, v2, LX/2fo;->A3Q:LX/01P;

    .line 97213
    invoke-interface {v5}, LX/01P;->get()Ljava/lang/Object;

    move-result-object v46

    move-object/from16 v5, v46

    check-cast v5, LX/11o;

    move-object/from16 v46, v5

    .line 97214
    iget-object v5, v2, LX/2fo;->A4U:LX/01P;

    .line 97215
    invoke-interface {v5}, LX/01P;->get()Ljava/lang/Object;

    move-result-object v45

    move-object/from16 v5, v45

    check-cast v5, LX/10Z;

    move-object/from16 v45, v5

    .line 97216
    iget-object v5, v2, LX/2fo;->A1k:LX/01P;

    .line 97217
    invoke-interface {v5}, LX/01P;->get()Ljava/lang/Object;

    move-result-object v44

    move-object/from16 v5, v44

    check-cast v5, LX/0wh;

    move-object/from16 v44, v5

    .line 97218
    iget-object v5, v2, LX/2fo;->AB1:LX/01P;

    .line 97219
    invoke-interface {v5}, LX/01P;->get()Ljava/lang/Object;

    move-result-object v43

    move-object/from16 v5, v43

    check-cast v5, LX/10g;

    move-object/from16 v43, v5

    .line 97220
    iget-object v5, v2, LX/2fo;->AKB:LX/01P;

    .line 97221
    invoke-interface {v5}, LX/01P;->get()Ljava/lang/Object;

    move-result-object v42

    move-object/from16 v5, v42

    check-cast v5, LX/18Z;

    move-object/from16 v42, v5

    .line 97222
    iget-object v5, v2, LX/2fo;->A3a:LX/01P;

    .line 97223
    invoke-interface {v5}, LX/01P;->get()Ljava/lang/Object;

    move-result-object v41

    move-object/from16 v5, v41

    check-cast v5, LX/0ny;

    move-object/from16 v41, v5

    .line 97224
    iget-object v5, v2, LX/2fo;->A8J:LX/01P;

    .line 97225
    invoke-interface {v5}, LX/01P;->get()Ljava/lang/Object;

    move-result-object v40

    move-object/from16 v5, v40

    check-cast v5, LX/147;

    move-object/from16 v40, v5

    .line 97226
    iget-object v5, v2, LX/2fo;->ABw:LX/01P;

    .line 97227
    invoke-interface {v5}, LX/01P;->get()Ljava/lang/Object;

    move-result-object v39

    move-object/from16 v5, v39

    check-cast v5, LX/0qQ;

    move-object/from16 v39, v5

    .line 97228
    iget-object v5, v2, LX/2fo;->ANz:LX/01P;

    .line 97229
    invoke-interface {v5}, LX/01P;->get()Ljava/lang/Object;

    move-result-object v38

    move-object/from16 v5, v38

    check-cast v5, LX/0sB;

    move-object/from16 v38, v5

    .line 97230
    iget-object v5, v2, LX/2fo;->A3b:LX/01P;

    .line 97231
    invoke-interface {v5}, LX/01P;->get()Ljava/lang/Object;

    move-result-object v37

    move-object/from16 v5, v37

    check-cast v5, LX/10b;

    move-object/from16 v37, v5

    .line 97232
    iget-object v5, v2, LX/2fo;->ANF:LX/01P;

    .line 97233
    invoke-interface {v5}, LX/01P;->get()Ljava/lang/Object;

    move-result-object v36

    move-object/from16 v5, v36

    check-cast v5, LX/0za;

    move-object/from16 v36, v5

    .line 97234
    iget-object v5, v2, LX/2fo;->AKG:LX/01P;

    .line 97235
    invoke-interface {v5}, LX/01P;->get()Ljava/lang/Object;

    move-result-object v35

    move-object/from16 v5, v35

    check-cast v5, LX/11W;

    move-object/from16 v35, v5

    .line 97236
    iget-object v5, v2, LX/2fo;->ANy:LX/01P;

    .line 97237
    invoke-interface {v5}, LX/01P;->get()Ljava/lang/Object;

    move-result-object v15

    check-cast v15, LX/0kh;

    .line 97238
    iget-object v5, v2, LX/2fo;->A2i:LX/01P;

    .line 97239
    invoke-interface {v5}, LX/01P;->get()Ljava/lang/Object;

    move-result-object v14

    check-cast v14, LX/187;

    .line 97240
    iget-object v5, v2, LX/2fo;->A7B:LX/01P;

    .line 97241
    invoke-interface {v5}, LX/01P;->get()Ljava/lang/Object;

    move-result-object v13

    check-cast v13, LX/181;

    .line 97242
    iget-object v5, v2, LX/2fo;->A2j:LX/01P;

    .line 97243
    invoke-interface {v5}, LX/01P;->get()Ljava/lang/Object;

    move-result-object v12

    check-cast v12, LX/0lA;

    .line 97244
    iget-object v5, v2, LX/2fo;->A3A:LX/01P;

    .line 97245
    invoke-interface {v5}, LX/01P;->get()Ljava/lang/Object;

    move-result-object v11

    check-cast v11, LX/18V;

    .line 97246
    iget-object v5, v2, LX/2fo;->A42:LX/01P;

    .line 97247
    invoke-interface {v5}, LX/01P;->get()Ljava/lang/Object;

    move-result-object v10

    check-cast v10, LX/0zy;

    .line 97248
    iget-object v5, v2, LX/2fo;->A5t:LX/01P;

    .line 97249
    invoke-interface {v5}, LX/01P;->get()Ljava/lang/Object;

    move-result-object v9

    check-cast v9, LX/17x;

    .line 97250
    iget-object v5, v2, LX/2fo;->A3R:LX/01P;

    .line 97251
    invoke-interface {v5}, LX/01P;->get()Ljava/lang/Object;

    move-result-object v8

    check-cast v8, LX/17w;

    .line 97252
    iget-object v5, v2, LX/2fo;->A4u:LX/01P;

    .line 97253
    invoke-interface {v5}, LX/01P;->get()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, LX/0w1;

    .line 97254
    iget-object v5, v2, LX/2fo;->A9Q:LX/01P;

    .line 97255
    invoke-interface {v5}, LX/01P;->get()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, LX/128;

    invoke-static {v2}, LX/2fo;->A10(LX/2fo;)LX/0rJ;

    move-result-object v90

    .line 97256
    iget-object v5, v2, LX/2fo;->AJw:LX/01P;

    .line 97257
    invoke-interface {v5}, LX/01P;->get()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, LX/195;

    .line 97258
    iget-object v2, v2, LX/2fo;->A5J:LX/01P;

    .line 97259
    invoke-interface {v2}, LX/01P;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/17o;

    new-instance v63, LX/2nM;

    move-object/from16 v64, v1

    move-object/from16 v65, v51

    move-object/from16 v66, v1

    move-object/from16 v67, v61

    move-object/from16 v68, v52

    move-object/from16 v69, v60

    move-object/from16 v70, v55

    move-object/from16 v71, v54

    move-object/from16 v72, v38

    move-object/from16 v73, v21

    move-object/from16 v74, v14

    move-object/from16 v75, v12

    move-object/from16 v76, v11

    move-object/from16 v77, v44

    move-object/from16 v78, v37

    move-object/from16 v79, v46

    move-object/from16 v80, v45

    move-object/from16 v81, v36

    move-object/from16 v82, v50

    move-object/from16 v83, v1

    move-object/from16 v84, v13

    move-object/from16 v85, v2

    move-object/from16 v86, v39

    move-object/from16 v87, v15

    move-object/from16 v88, v48

    move-object/from16 v89, v53

    move-object/from16 v91, v58

    move-object/from16 v92, v7

    move-object/from16 v93, v43

    move-object/from16 v95, v10

    move-object/from16 v96, v35

    move-object/from16 v97, v62

    move-object/from16 v98, v40

    move-object/from16 v100, v56

    move-object/from16 v101, v6

    move-object/from16 v103, v41

    move-object/from16 v104, v5

    move-object/from16 v105, v42

    move-object/from16 v106, v57

    move-object/from16 v107, v8

    move-object/from16 v108, v9

    move-object/from16 v109, v59

    move-object/from16 v110, v49

    move-object/from16 v111, v47

    invoke-direct/range {v63 .. v111}, LX/2nM;-><init>(LX/00i;LX/154;LX/0kM;LX/0li;LX/15K;LX/0nN;LX/0nB;LX/0oS;LX/0sB;LX/2Fu;LX/187;LX/0lA;LX/18V;LX/0wh;LX/10b;LX/11o;LX/10Z;LX/0za;LX/0nS;LX/0kS;LX/181;LX/17o;LX/0qQ;LX/0kh;LX/00z;LX/0ki;LX/0rJ;LX/0uN;LX/0w1;LX/10g;LX/0mJ;LX/0zy;LX/11W;LX/0kj;LX/147;LX/0zB;LX/0pG;LX/128;LX/0l8;LX/0ny;LX/195;LX/18Z;LX/184;LX/17w;LX/17x;LX/0lO;LX/0y8;LX/15O;)V

    goto/16 :goto_17

    .line 97260
    :cond_57
    move-object/from16 v2, v29

    invoke-virtual {v1, v2}, LX/0kK;->A1m(Ljava/lang/String;)V

    .line 97261
    iget-object v2, v1, Lcom/soula2/Conversation;->A1c:Lcom/soula2/conversation/ConversationListView;

    invoke-virtual {v2}, Lcom/soula2/conversation/ConversationListView;->getConversationCursorAdapter()LX/0me;

    move-result-object v7

    .line 97262
    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v2, 0x7f070247

    invoke-virtual {v5, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v6

    .line 97263
    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v2, 0x7f070246

    invoke-virtual {v5, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    shl-int/lit8 v6, v6, 0x1

    add-int/2addr v6, v2

    .line 97264
    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v2, 0x7f0704b6

    invoke-virtual {v5, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    add-int/2addr v6, v2

    .line 97265
    new-instance v5, Landroid/util/DisplayMetrics;

    invoke-direct {v5}, Landroid/util/DisplayMetrics;-><init>()V

    .line 97266
    invoke-virtual {v1}, Landroid/app/Activity;->getWindowManager()Landroid/view/WindowManager;

    move-result-object v2

    invoke-interface {v2}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v2

    invoke-virtual {v2, v5}, Landroid/view/Display;->getMetrics(Landroid/util/DisplayMetrics;)V

    .line 97267
    iget v5, v5, Landroid/util/DisplayMetrics;->widthPixels:I

    const/4 v2, 0x0

    if-le v5, v6, :cond_58

    const/4 v2, 0x1

    .line 97268
    :cond_58
    iput-boolean v2, v7, LX/0me;->A0C:Z

    .line 97269
    iget-object v5, v1, LX/0kH;->A0C:LX/0kj;

    const/16 v2, 0x6e6

    invoke-virtual {v5, v2}, LX/0kj;->A07(I)Z

    move-result v2

    if-eqz v2, :cond_59

    .line 97270
    const v2, 0x7f0a008c

    invoke-static {v1, v2}, LX/00S;->A05(Landroid/app/Activity;I)Landroid/view/View;

    move-result-object v2

    iput-object v2, v1, Lcom/soula2/Conversation;->A08:Landroid/view/View;

    .line 97271
    invoke-static {v2}, LX/20j;->A02(Landroid/view/View;)V

    .line 97272
    iget-object v5, v1, Lcom/soula2/Conversation;->A08:Landroid/view/View;

    const v2, 0x7f1203ac

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v5, v2}, Landroid/view/View;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 97273
    const v2, 0x7f0a008d

    invoke-virtual {v1, v2}, LX/00i;->findViewById(I)Landroid/view/View;

    move-result-object v9

    check-cast v9, Landroid/widget/TextView;

    .line 97274
    iget-object v5, v1, Lcom/soula2/Conversation;->A2b:LX/0mJ;

    const-class v2, Lcom/whatsapp/jid/UserJid;

    invoke-virtual {v5, v2}, LX/0mJ;->A0A(Ljava/lang/Class;)Lcom/whatsapp/jid/Jid;

    move-result-object v8

    check-cast v8, Lcom/whatsapp/jid/UserJid;

    .line 97275
    iget-object v2, v1, Lcom/soula2/Conversation;->A2b:LX/0mJ;

    invoke-virtual {v2}, LX/0mJ;->A0I()Z

    move-result v2

    if-eqz v2, :cond_59

    if-eqz v8, :cond_59

    .line 97276
    iget-object v7, v1, LX/0kJ;->A05:LX/0lO;

    iget-object v6, v1, Lcom/soula2/Conversation;->A1B:LX/0yx;

    iget-object v5, v1, Lcom/soula2/Conversation;->A1E:LX/18V;

    new-instance v2, LX/1qk;

    invoke-direct {v2, v6, v5, v8, v7}, LX/1qk;-><init>(LX/0yx;LX/18V;Lcom/whatsapp/jid/UserJid;LX/0lO;)V

    iget-object v6, v1, Lcom/soula2/Conversation;->A0n:LX/2Dn;

    new-instance v5, LX/4Or;

    invoke-direct {v5, v6, v2}, LX/4Or;-><init>(LX/2Dn;LX/1qk;)V

    new-instance v2, LX/01S;

    invoke-direct {v2, v5, v1}, LX/01S;-><init>(LX/058;LX/00o;)V

    const-class v5, LX/3MC;

    .line 97277
    invoke-virtual {v2, v5}, LX/01S;->A00(Ljava/lang/Class;)LX/011;

    move-result-object v2

    check-cast v2, LX/3MC;

    iput-object v2, v1, Lcom/soula2/Conversation;->A18:LX/3MC;

    .line 97278
    iget-object v5, v2, LX/3MC;->A00:LX/012;

    .line 97279
    new-instance v2, Lcom/facebook/redex/IDxObserverShape20S0300000_1_I0;

    invoke-direct {v2, v9, v1, v8, v3}, Lcom/facebook/redex/IDxObserverShape20S0300000_1_I0;-><init>(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;I)V

    .line 97280
    invoke-virtual {v5, v1, v2}, LX/013;->A05(LX/00m;LX/01R;)V

    .line 97281
    :cond_59
    const v2, 0x7f0a0f7e

    .line 97282
    invoke-static {v1, v2}, LX/00S;->A05(Landroid/app/Activity;I)Landroid/view/View;

    move-result-object v2

    new-instance v5, LX/0l3;

    invoke-direct {v5, v2}, LX/0l3;-><init>(Landroid/view/View;)V

    iput-object v5, v1, Lcom/soula2/Conversation;->A3u:LX/0l3;

    .line 97283
    new-instance v2, Lcom/facebook/redex/IDxIListenerShape270S0100000_2_I0;

    invoke-direct {v2, v1, v4}, Lcom/facebook/redex/IDxIListenerShape270S0100000_2_I0;-><init>(Lcom/soula2/Conversation;I)V

    invoke-virtual {v5, v2}, LX/0l3;->A04(LX/23k;)V

    .line 97284
    invoke-virtual {v1, v4}, Lcom/soula2/Conversation;->A3C(Z)V

    .line 97285
    iget-object v5, v1, Lcom/soula2/Conversation;->A3u:LX/0l3;

    new-instance v2, Lcom/facebook/redex/ViewOnClickCListenerShape6S0100000_I0;

    move/from16 v6, v24

    invoke-direct {v2, v1, v6}, Lcom/facebook/redex/ViewOnClickCListenerShape6S0100000_I0;-><init>(Ljava/lang/Object;I)V

    invoke-virtual {v5, v2}, LX/0l3;->A03(Landroid/view/View$OnClickListener;)V

    .line 97286
    const v2, 0x7f0a0b37

    .line 97287
    invoke-static {v1, v2}, LX/00S;->A05(Landroid/app/Activity;I)Landroid/view/View;

    move-result-object v2

    new-instance v5, LX/0l3;

    invoke-direct {v5, v2}, LX/0l3;-><init>(Landroid/view/View;)V

    iput-object v5, v1, Lcom/soula2/Conversation;->A3v:LX/0l3;

    .line 97288
    new-instance v2, Lcom/facebook/redex/IDxIListenerShape270S0100000_2_I0;

    invoke-direct {v2, v1, v3}, Lcom/facebook/redex/IDxIListenerShape270S0100000_2_I0;-><init>(Lcom/soula2/Conversation;I)V

    invoke-virtual {v5, v2}, LX/0l3;->A04(LX/23k;)V

    .line 97289
    iget-object v5, v1, Lcom/soula2/Conversation;->A3v:LX/0l3;

    new-instance v2, Lcom/facebook/redex/ViewOnClickCListenerShape6S0100000_I0;

    move/from16 v6, v30

    invoke-direct {v2, v1, v6}, Lcom/facebook/redex/ViewOnClickCListenerShape6S0100000_I0;-><init>(Ljava/lang/Object;I)V

    invoke-virtual {v5, v2}, LX/0l3;->A03(Landroid/view/View$OnClickListener;)V

    .line 97290
    iget-object v2, v1, Lcom/soula2/Conversation;->A1c:Lcom/soula2/conversation/ConversationListView;

    invoke-virtual {v2, v4}, Landroid/view/View;->setScrollbarFadingEnabled(Z)V

    .line 97291
    iget-object v5, v1, Lcom/soula2/Conversation;->A1c:Lcom/soula2/conversation/ConversationListView;

    iget-object v2, v1, Lcom/soula2/Conversation;->A4n:Landroid/widget/AbsListView$OnScrollListener;

    invoke-virtual {v5, v2}, Landroid/widget/AbsListView;->setOnScrollListener(Landroid/widget/AbsListView$OnScrollListener;)V

    .line 97292
    iget-object v2, v1, Lcom/soula2/Conversation;->A2v:Lcom/soula2/mentions/MentionableEntry;

    invoke-virtual {v2, v4}, Landroid/view/View;->setScrollbarFadingEnabled(Z)V

    .line 97293
    iget-object v5, v1, Lcom/soula2/Conversation;->A2v:Lcom/soula2/mentions/MentionableEntry;

    iget-object v2, v1, Lcom/soula2/Conversation;->A4k:Landroid/text/TextWatcher;

    invoke-virtual {v5, v2}, Landroid/widget/TextView;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 97294
    iget-object v5, v1, Lcom/soula2/Conversation;->A2v:Lcom/soula2/mentions/MentionableEntry;

    iget-object v2, v1, Lcom/soula2/Conversation;->A4i:Landroid/text/TextWatcher;

    invoke-virtual {v5, v2}, Landroid/widget/TextView;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 97295
    iget-object v5, v1, Lcom/soula2/Conversation;->A2v:Lcom/soula2/mentions/MentionableEntry;

    iget-object v2, v1, Lcom/soula2/Conversation;->A4o:Landroid/widget/TextView$OnEditorActionListener;

    invoke-virtual {v5, v2}, Landroid/widget/TextView;->setOnEditorActionListener(Landroid/widget/TextView$OnEditorActionListener;)V

    .line 97296
    iget-object v5, v1, Lcom/soula2/Conversation;->A2v:Lcom/soula2/mentions/MentionableEntry;

    iget-object v2, v1, Lcom/soula2/Conversation;->A4l:Landroid/view/View$OnClickListener;

    invoke-virtual {v5, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 97297
    iget-object v6, v1, Lcom/soula2/Conversation;->A2v:Lcom/soula2/mentions/MentionableEntry;

    iget-object v2, v1, LX/0kH;->A09:LX/0kh;

    .line 97298
    iget-object v5, v2, LX/0kh;->A00:Landroid/content/SharedPreferences;

    const-string v2, "input_enter_send"

    invoke-interface {v5, v2, v4}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v2

    .line 97299
    invoke-virtual {v6, v2}, LX/1l4;->setInputEnterSend(Z)V

    .line 97300
    iget-object v5, v1, Lcom/soula2/Conversation;->A2v:Lcom/soula2/mentions/MentionableEntry;

    new-instance v2, Lcom/facebook/redex/IDxKListenerShape266S0100000_2_I0;

    invoke-direct {v2, v1, v3}, Lcom/facebook/redex/IDxKListenerShape266S0100000_2_I0;-><init>(Ljava/lang/Object;I)V

    invoke-virtual {v5, v2}, Landroid/view/View;->setOnKeyListener(Landroid/view/View$OnKeyListener;)V

    .line 97301
    iget-object v5, v1, Lcom/soula2/Conversation;->A0U:Landroid/widget/ImageButton;

    new-instance v2, Lcom/facebook/redex/ViewOnClickCListenerShape6S0100000_I0;

    invoke-direct {v2, v1, v4}, Lcom/facebook/redex/ViewOnClickCListenerShape6S0100000_I0;-><init>(Ljava/lang/Object;I)V

    invoke-virtual {v5, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 97302
    new-instance v2, Lcom/facebook/redex/ViewOnClickCListenerShape6S0100000_I0;

    move/from16 v5, v28

    invoke-direct {v2, v1, v5}, Lcom/facebook/redex/ViewOnClickCListenerShape6S0100000_I0;-><init>(Ljava/lang/Object;I)V

    move-object/from16 v5, v17

    invoke-virtual {v5, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 97303
    iget-object v2, v1, Lcom/soula2/Conversation;->A2v:Lcom/soula2/mentions/MentionableEntry;

    invoke-virtual {v2}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v6

    .line 97304
    iget-object v5, v1, Lcom/soula2/Conversation;->A0U:Landroid/widget/ImageButton;

    invoke-static {v6}, LX/1HG;->A0D(Ljava/lang/CharSequence;)Z

    move-result v7

    const/4 v2, 0x0

    if-nez v7, :cond_5a

    const/4 v2, 0x1

    :cond_5a
    invoke-virtual {v5, v2}, Landroid/view/View;->setEnabled(Z)V

    .line 97305
    invoke-static {v6}, LX/1HG;->A0D(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_5b

    .line 97306
    iget-object v5, v1, LX/0kJ;->A04:LX/0xf;

    const v2, 0x29f52e4a

    move-object/from16 v7, v31

    invoke-interface {v5, v2, v7, v4}, LX/0xf;->AJO(ILjava/lang/String;Z)V

    .line 97307
    iget-object v5, v1, LX/0kJ;->A04:LX/0xf;

    const v2, 0x29f511de

    invoke-interface {v5, v2, v7, v4}, LX/0xf;->AJO(ILjava/lang/String;Z)V

    .line 97308
    :cond_5b
    iget-object v2, v1, Lcom/soula2/Conversation;->A3Z:LX/17g;

    invoke-virtual {v2}, LX/17g;->A00()V

    .line 97309
    iget-object v2, v1, Lcom/soula2/Conversation;->A3Z:LX/17g;

    .line 97310
    iget-boolean v2, v2, LX/17g;->A00:Z

    .line 97311
    if-eqz v2, :cond_5c

    .line 97312
    iget-object v5, v1, Lcom/soula2/Conversation;->A3d:LX/17r;

    new-instance v2, LX/4Oq;

    invoke-direct {v2, v5}, LX/4Oq;-><init>(LX/17r;)V

    new-instance v5, LX/01S;

    invoke-direct {v5, v2, v1}, LX/01S;-><init>(LX/058;LX/00o;)V

    const-class v2, LX/1A3;

    .line 97313
    invoke-virtual {v5, v2}, LX/01S;->A00(Ljava/lang/Class;)LX/011;

    move-result-object v7

    check-cast v7, LX/1A3;

    iput-object v7, v1, Lcom/soula2/Conversation;->A3a:LX/1A3;

    .line 97314
    iget-object v2, v1, Lcom/soula2/Conversation;->A3f:LX/1JK;

    .line 97315
    iput-object v7, v2, LX/1JK;->A02:LX/1A3;

    .line 97316
    iget-object v5, v1, LX/0kH;->A05:LX/0li;

    new-instance v2, LX/1KQ;

    invoke-direct {v2, v5, v7}, LX/1KQ;-><init>(LX/0li;LX/1A3;)V

    iput-object v2, v1, Lcom/soula2/Conversation;->A3c:LX/1KQ;

    .line 97317
    iget-object v2, v1, LX/0kB;->A0U:LX/17c;

    invoke-virtual {v2}, LX/17d;->A01()V

    .line 97318
    invoke-static {v6}, LX/1HG;->A0D(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_5c

    iget-object v2, v1, LX/0kB;->A0U:LX/17c;

    .line 97319
    iget-boolean v2, v2, LX/17d;->A02:Z

    .line 97320
    if-eqz v2, :cond_5c

    .line 97321
    iget-object v5, v1, Lcom/soula2/Conversation;->A3c:LX/1KQ;

    const/16 v2, 0x1f4

    invoke-virtual {v5, v6, v2}, LX/1KQ;->A00(Ljava/lang/CharSequence;I)V

    .line 97322
    :cond_5c
    const v2, 0x7f0a05e2

    invoke-static {v1, v2}, LX/00S;->A05(Landroid/app/Activity;I)Landroid/view/View;

    move-result-object v6

    check-cast v6, Landroid/widget/ImageButton;

    const-string v15, "emojiPopup"

    .line 97323
    invoke-virtual {v1, v15}, LX/0kK;->A1n(Ljava/lang/String;)V

    .line 97324
    iget-object v11, v1, Lcom/soula2/Conversation;->A2i:LX/1A2;

    .line 97325
    iput-object v1, v11, LX/1A2;->A00:Landroid/app/Activity;

    .line 97326
    iget-object v5, v1, Lcom/soula2/Conversation;->A0w:Lcom/soula2/KeyboardPopupLayout;

    iget-object v2, v1, Lcom/soula2/Conversation;->A2v:Lcom/soula2/mentions/MentionableEntry;

    .line 97327
    iput-object v5, v11, LX/1A2;->A02:Lcom/soula2/KeyboardPopupLayout;

    .line 97328
    iput-object v6, v11, LX/1A2;->A01:Landroid/widget/ImageButton;

    .line 97329
    iput-object v2, v11, LX/1A2;->A03:Lcom/soula2/WaEditText;

    .line 97330
    iget-object v2, v1, Lcom/soula2/Conversation;->A2I:LX/17h;

    .line 97331
    invoke-virtual {v2}, LX/17h;->A00()LX/47m;

    move-result-object v5

    .line 97332
    iput-object v5, v11, LX/1A2;->A05:LX/47m;

    .line 97333
    iget-object v7, v2, LX/17h;->A0B:LX/17Z;

    iget-object v6, v2, LX/17h;->A0C:LX/17b;

    iget-object v10, v2, LX/17h;->A05:LX/17Y;

    new-instance v5, LX/3Ao;

    invoke-direct {v5, v10, v7, v6}, LX/3Ao;-><init>(LX/17Y;LX/17Z;LX/17b;)V

    .line 97334
    iput-object v5, v11, LX/1A2;->A06:LX/3Ao;

    .line 97335
    iget-object v9, v1, Lcom/soula2/Conversation;->A3a:LX/1A3;

    iget-object v5, v1, Lcom/soula2/Conversation;->A3f:LX/1JK;

    .line 97336
    invoke-virtual {v2, v9, v5}, LX/17h;->A01(LX/1A3;LX/1JK;)LX/20V;

    move-result-object v5

    .line 97337
    iput-object v5, v11, LX/1A2;->A07:LX/20V;

    .line 97338
    iget-object v8, v2, LX/17h;->A0I:LX/13E;

    iget-object v7, v2, LX/17h;->A0G:LX/17e;

    iget-object v6, v2, LX/17h;->A0K:LX/17f;

    iget-object v5, v2, LX/17h;->A0J:LX/17g;

    new-instance v2, LX/214;

    move-object/from16 v40, v9

    move-object/from16 v41, v6

    move-object/from16 v35, v2

    move-object/from16 v36, v10

    move-object/from16 v37, v7

    move-object/from16 v38, v8

    move-object/from16 v39, v5

    invoke-direct/range {v35 .. v41}, LX/214;-><init>(LX/17Y;LX/17e;LX/13E;LX/17g;LX/1A3;LX/17f;)V

    .line 97339
    iput-object v2, v11, LX/1A2;->A04:LX/214;

    .line 97340
    invoke-virtual {v11}, LX/1A2;->A00()LX/0mU;

    move-result-object v5

    iput-object v5, v1, Lcom/soula2/Conversation;->A2j:LX/0mU;

    .line 97341
    iget-object v2, v1, Lcom/soula2/Conversation;->A3l:LX/17w;

    .line 97342
    iput-object v2, v5, LX/0mV;->A0D:LX/17w;

    .line 97343
    iget-object v2, v1, LX/0kB;->A00:Landroid/widget/PopupWindow$OnDismissListener;

    invoke-virtual {v5, v2}, Landroid/widget/PopupWindow;->setOnDismissListener(Landroid/widget/PopupWindow$OnDismissListener;)V

    .line 97344
    iget-object v2, v1, LX/0kH;->A0C:LX/0kj;

    move-object/from16 v20, v2

    iget-object v2, v1, Lcom/soula2/Conversation;->A2l:LX/17Z;

    move-object/from16 v19, v2

    iget-object v2, v1, LX/0kF;->A0B:LX/15n;

    move-object/from16 v18, v2

    iget-object v2, v1, LX/0kB;->A0V:LX/0pG;

    move-object/from16 v17, v2

    iget-object v14, v1, LX/0kH;->A08:LX/01e;

    iget-object v13, v1, Lcom/soula2/Conversation;->A2n:LX/17b;

    const v2, 0x7f0a07a6

    .line 97345
    invoke-virtual {v1, v2}, LX/00i;->findViewById(I)Landroid/view/View;

    move-result-object v12

    check-cast v12, Lcom/soula2/gifsearch/GifSearchContainer;

    iget-object v11, v1, LX/0kH;->A09:LX/0kh;

    const v2, 0x7f0a05e7

    .line 97346
    invoke-virtual {v1, v2}, LX/00i;->findViewById(I)Landroid/view/View;

    move-result-object v10

    check-cast v10, Lcom/soula2/emoji/search/EmojiSearchContainer;

    iget-object v9, v1, Lcom/soula2/Conversation;->A2j:LX/0mU;

    iget-object v8, v1, LX/0kH;->A0B:LX/17a;

    iget-object v7, v1, Lcom/soula2/Conversation;->A2e:LX/10p;

    iget-object v6, v1, Lcom/soula2/Conversation;->A23:LX/00z;

    iget-object v5, v1, Lcom/soula2/Conversation;->A3I:LX/0pe;

    new-instance v2, LX/0ma;

    move-object/from16 v36, v1

    move-object/from16 v37, v14

    move-object/from16 v38, v11

    move-object/from16 v39, v6

    move-object/from16 v40, v8

    move-object/from16 v41, v7

    move-object/from16 v42, v10

    move-object/from16 v43, v20

    move-object/from16 v44, v17

    move-object/from16 v45, v9

    move-object/from16 v46, v19

    move-object/from16 v47, v12

    move-object/from16 v48, v13

    move-object/from16 v49, v5

    move-object/from16 v50, v18

    move-object/from16 v35, v2

    invoke-direct/range {v35 .. v50}, LX/0ma;-><init>(Landroid/app/Activity;LX/01e;LX/0kh;LX/00z;LX/17a;LX/10p;Lcom/soula2/emoji/search/EmojiSearchContainer;LX/0kj;LX/0pG;LX/0mU;LX/17Z;Lcom/soula2/gifsearch/GifSearchContainer;LX/17b;LX/0pe;LX/15n;)V

    iput-object v2, v1, Lcom/soula2/Conversation;->A2k:LX/0ma;

    .line 97347
    iget-object v2, v1, Lcom/soula2/Conversation;->A3G:LX/2FY;

    iget-object v5, v1, Lcom/soula2/Conversation;->A2j:LX/0mU;

    .line 97348
    iput-object v1, v2, LX/2FY;->A02:LX/0kT;

    .line 97349
    iput-object v5, v2, LX/2FY;->A00:LX/0mU;

    .line 97350
    iput-object v2, v5, LX/0mU;->A03:LX/2FY;

    .line 97351
    iget-object v2, v1, Lcom/soula2/Conversation;->A4u:LX/4uu;

    invoke-virtual {v5, v2}, LX/0mV;->A0C(LX/4uu;)V

    .line 97352
    const/16 v6, 0x21

    new-instance v2, Lcom/facebook/redex/RunnableRunnableShape1S0100000_I0;

    invoke-direct {v2, v1, v6}, Lcom/facebook/redex/RunnableRunnableShape1S0100000_I0;-><init>(Lcom/soula2/Conversation;I)V

    .line 97353
    iput-object v2, v5, LX/0mV;->A0E:Ljava/lang/Runnable;

    .line 97354
    new-instance v2, Lcom/facebook/redex/IDxICheckerShape360S0100000_2_I0;

    invoke-direct {v2, v1, v3}, Lcom/facebook/redex/IDxICheckerShape360S0100000_2_I0;-><init>(Ljava/lang/Object;I)V

    .line 97355
    iput-object v2, v5, LX/0mV;->A0A:LX/4tM;

    .line 97356
    iget-object v6, v1, Lcom/soula2/Conversation;->A51:LX/2FZ;

    invoke-virtual {v5, v6}, LX/0mU;->A0K(LX/2FZ;)V

    .line 97357
    invoke-virtual {v1, v15}, LX/0kK;->A1m(Ljava/lang/String;)V

    .line 97358
    iget-object v2, v1, Lcom/soula2/Conversation;->A2k:LX/0ma;

    .line 97359
    iput-object v1, v2, LX/0mb;->A00:LX/0kc;

    .line 97360
    iput-object v1, v2, LX/0ma;->A00:LX/0ka;

    .line 97361
    iget-object v5, v1, Lcom/soula2/Conversation;->A3G:LX/2FY;

    new-instance v2, Lcom/facebook/redex/IDxSListenerShape400S0100000_2_I0;

    invoke-direct {v2, v1, v3}, Lcom/facebook/redex/IDxSListenerShape400S0100000_2_I0;-><init>(Ljava/lang/Object;I)V

    .line 97362
    iput-object v2, v5, LX/2FY;->A01:LX/2Fa;

    .line 97363
    iput-object v6, v5, LX/2FY;->A04:LX/2FZ;

    .line 97364
    iget-object v2, v1, Lcom/soula2/Conversation;->A2v:Lcom/soula2/mentions/MentionableEntry;

    invoke-static {v2}, LX/1GU;->A05(Landroid/widget/EditText;)V

    .line 97365
    iget-object v5, v1, Lcom/soula2/Conversation;->A45:LX/0y6;

    iget-object v2, v1, Lcom/soula2/Conversation;->A53:LX/1Ee;

    invoke-virtual {v5, v2}, LX/0pD;->A03(Ljava/lang/Object;)V

    .line 97366
    iget-object v5, v1, Lcom/soula2/Conversation;->A41:LX/18m;

    iget-object v2, v1, Lcom/soula2/Conversation;->A52:LX/2Ph;

    invoke-virtual {v5, v2}, LX/0pD;->A03(Ljava/lang/Object;)V

    .line 97367
    iget-object v5, v1, Lcom/soula2/Conversation;->A1n:LX/18l;

    iget-object v2, v1, Lcom/soula2/Conversation;->A4p:LX/2M7;

    invoke-virtual {v5, v2}, LX/0pD;->A03(Ljava/lang/Object;)V

    .line 97368
    iget-object v5, v1, Lcom/soula2/Conversation;->A3z:LX/188;

    iget-object v2, v1, Lcom/soula2/Conversation;->A4s:LX/0ky;

    invoke-virtual {v5, v2}, LX/0pD;->A03(Ljava/lang/Object;)V

    .line 97369
    iget-object v5, v1, Lcom/soula2/Conversation;->A36:LX/152;

    iget-object v2, v1, Lcom/soula2/Conversation;->A4y:LX/48s;

    invoke-virtual {v5, v2}, LX/0pD;->A03(Ljava/lang/Object;)V

    .line 97370
    iget-object v5, v1, Lcom/soula2/Conversation;->A1A:LX/0zc;

    iget-object v2, v1, Lcom/soula2/Conversation;->A4v:LX/1W7;

    invoke-virtual {v5, v2}, LX/0pD;->A03(Ljava/lang/Object;)V

    .line 97371
    iget-object v5, v1, Lcom/soula2/Conversation;->A3X:LX/12d;

    iget-object v2, v1, Lcom/soula2/Conversation;->A50:LX/1JJ;

    invoke-virtual {v5, v2}, LX/12d;->A05(LX/1JJ;)V

    .line 97372
    iget-object v2, v1, Lcom/soula2/Conversation;->A3f:LX/1JK;

    .line 97373
    iget-object v5, v2, LX/1JK;->A0B:LX/12d;

    iget-object v2, v2, LX/1JK;->A0A:LX/1JJ;

    invoke-virtual {v5, v2}, LX/12d;->A05(LX/1JJ;)V

    .line 97374
    iget-object v5, v1, Lcom/soula2/Conversation;->A3e:LX/1E8;

    iget-object v2, v1, Lcom/soula2/Conversation;->A4r:LX/213;

    invoke-virtual {v5, v2}, LX/0pD;->A03(Ljava/lang/Object;)V

    .line 97375
    iget-object v5, v1, Lcom/soula2/Conversation;->A38:LX/16K;

    iget-object v2, v1, Lcom/soula2/Conversation;->A4q:LX/1zo;

    invoke-virtual {v5, v2}, LX/0pD;->A03(Ljava/lang/Object;)V

    .line 97376
    iget-object v2, v1, Lcom/soula2/Conversation;->A3i:Lcom/soula2/tosgating/viewmodel/ToSGatingViewModel;

    .line 97377
    iget-object v5, v2, Lcom/soula2/tosgating/viewmodel/ToSGatingViewModel;->A01:LX/012;

    .line 97378
    const/16 v7, 0xa

    new-instance v2, Lcom/facebook/redex/IDxObserverShape118S0100000_2_I0;

    invoke-direct {v2, v1, v7}, Lcom/facebook/redex/IDxObserverShape118S0100000_2_I0;-><init>(Ljava/lang/Object;I)V

    .line 97379
    invoke-virtual {v5, v1, v2}, LX/013;->A05(LX/00m;LX/01R;)V

    .line 97380
    invoke-static {}, LX/0vi;->A00()Z

    move-result v2

    if-eqz v2, :cond_5e

    const-string v2, "conversation/device-not-supported"

    .line 97381
    invoke-static {v2}, Lcom/whatsapp/util/Log;->w(Ljava/lang/String;)V

    .line 97382
    new-instance v2, Lcom/soula2/DisplayExceptionDialogFactory$UnsupportedDeviceDialogFragment;

    invoke-direct {v2}, Lcom/soula2/DisplayExceptionDialogFactory$UnsupportedDeviceDialogFragment;-><init>()V

    invoke-virtual {v1, v2}, LX/0kH;->AcU(Landroidx/fragment/app/DialogFragment;)V

    .line 97383
    :cond_5d
    :goto_1a
    iget-object v2, v1, Lcom/soula2/Conversation;->A1x:LX/0mf;

    .line 97384
    iget-object v5, v0, LX/48c;->A05:LX/1VY;

    .line 97385
    iget-object v6, v2, LX/0mf;->A0J:LX/194;

    .line 97386
    iget-object v8, v6, LX/194;->A00:Ljava/util/HashMap;

    .line 97387
    iget-object v6, v2, LX/0mf;->A0S:LX/0l8;

    invoke-virtual {v8, v6}, Ljava/util/AbstractMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, LX/0md;

    if-eqz v5, :cond_61

    .line 97388
    new-instance v8, LX/0oR;

    invoke-direct {v8}, LX/0oR;-><init>()V

    .line 97389
    iget-object v6, v0, LX/48c;->A0B:Ljava/io/File;

    .line 97390
    iput-object v6, v8, LX/0oR;->A0F:Ljava/io/File;

    .line 97391
    new-instance v6, LX/463;

    invoke-direct {v6, v8, v5, v0, v2}, LX/463;-><init>(LX/0oR;LX/1VY;LX/48c;LX/0mf;)V

    iput-object v6, v2, LX/0mf;->A06:LX/463;

    .line 97392
    iget-object v5, v2, LX/0mf;->A0a:LX/0lO;

    iget-object v9, v2, LX/0mf;->A0V:LX/15g;

    new-instance v2, LX/2uY;

    invoke-direct {v2, v1, v6, v9}, LX/2uY;-><init>(LX/00m;LX/463;LX/15g;)V

    new-array v6, v4, [Ljava/io/File;

    .line 97393
    iget-object v8, v8, LX/0oR;->A0F:Ljava/io/File;

    .line 97394
    aput-object v8, v6, v3

    .line 97395
    invoke-interface {v5, v2, v6}, LX/0lO;->AZj(LX/0ng;[Ljava/lang/Object;)V

    goto/16 :goto_1c

    .line 97396
    :cond_5e
    iget-object v2, v1, Lcom/soula2/Conversation;->A0z:LX/0vi;

    invoke-virtual {v2}, LX/0vi;->A03()Z

    move-result v2

    if-eqz v2, :cond_5f

    const-string v2, "conversation/clock-wrong"

    .line 97397
    invoke-static {v2}, Lcom/whatsapp/util/Log;->w(Ljava/lang/String;)V

    .line 97398
    iget-object v5, v1, Lcom/soula2/Conversation;->A2y:LX/0uB;

    iget-object v2, v1, Lcom/soula2/Conversation;->A32:LX/0uh;

    invoke-static {v1, v5, v2}, LX/1vd;->A01(LX/0kM;LX/0uB;LX/0uh;)Z

    goto :goto_1a

    .line 97399
    :cond_5f
    iget-object v2, v1, Lcom/soula2/Conversation;->A0z:LX/0vi;

    invoke-virtual {v2}, LX/0vi;->A02()Z

    move-result v2

    if-eqz v2, :cond_60

    const-string v2, "conversation/software-expired"

    .line 97400
    invoke-static {v2}, Lcom/whatsapp/util/Log;->w(Ljava/lang/String;)V

    .line 97401
    iget-object v5, v1, Lcom/soula2/Conversation;->A2y:LX/0uB;

    iget-object v2, v1, Lcom/soula2/Conversation;->A32:LX/0uh;

    invoke-static {v1, v5, v2}, LX/1vd;->A02(LX/0kM;LX/0uB;LX/0uh;)Z

    goto :goto_1a

    .line 97402
    :cond_60
    iget-object v5, v1, Lcom/soula2/Conversation;->A12:LX/1Fo;

    iget-object v2, v1, Lcom/soula2/Conversation;->A0z:LX/0vi;

    invoke-virtual {v5, v2}, LX/1Fo;->A02(LX/0vi;)I

    move-result v2

    if-lez v2, :cond_5d

    const-string v2, "conversation/software-about-to-expire"

    .line 97403
    invoke-static {v2}, Lcom/whatsapp/util/Log;->w(Ljava/lang/String;)V

    const/16 v2, 0x73

    .line 97404
    invoke-static {v1, v2}, LX/1ix;->A01(Landroid/app/Activity;I)V

    goto :goto_1a

    .line 97405
    :cond_61
    if-eqz v9, :cond_62

    .line 97406
    iget-wide v5, v9, LX/0md;->A11:J
    :try_end_c
    .catchall {:try_start_c .. :try_end_c} :catchall_4

    .line 97407
    :try_start_d
    iget-object v8, v2, LX/0mf;->A0P:LX/0nZ;

    .line 97408
    iget-object v8, v8, LX/0nZ;->A0K:LX/0oZ;

    invoke-virtual {v8, v5, v6}, LX/0oZ;->A00(J)LX/0md;

    move-result-object v5

    .line 97409
    if-eqz v5, :cond_63

    move-object v9, v5

    goto :goto_1b
    :try_end_d
    .catchall {:try_start_d .. :try_end_d} :catchall_2

    .line 97410
    :catchall_2
    :try_start_e
    move-exception v0

    .line 97411
    throw v0

    .line 97412
    :cond_62
    iget-object v10, v0, LX/48c;->A09:LX/0nO;

    if-eqz v10, :cond_64

    .line 97413
    iget-object v5, v2, LX/0mf;->A0L:LX/0lm;

    .line 97414
    invoke-virtual {v5}, LX/0lm;->A00()J

    move-result-wide v5

    iget-object v9, v0, LX/48c;->A0I:Ljava/lang/String;

    iget-object v8, v0, LX/48c;->A0H:Ljava/lang/String;

    .line 97415
    invoke-static {v10, v9, v8, v5, v6}, LX/0v9;->A00(Lcom/whatsapp/jid/GroupJid;Ljava/lang/String;Ljava/lang/String;J)LX/1Wf;

    move-result-object v9

    const-string v5, ""

    .line 97416
    invoke-virtual {v9, v5}, LX/0md;->A0l(Ljava/lang/String;)V

    .line 97417
    :cond_63
    :goto_1b
    invoke-virtual {v2, v9}, LX/0mf;->A0B(LX/0md;)V

    goto :goto_1c

    .line 97418
    :cond_64
    iget-object v5, v0, LX/48c;->A0C:Ljava/lang/Long;

    if-eqz v5, :cond_65

    .line 97419
    invoke-virtual {v5}, Ljava/lang/Number;->longValue()J

    move-result-wide v5
    :try_end_e
    .catchall {:try_start_e .. :try_end_e} :catchall_4

    .line 97420
    :try_start_f
    iget-object v8, v2, LX/0mf;->A0P:LX/0nZ;

    .line 97421
    iget-object v8, v8, LX/0nZ;->A0K:LX/0oZ;

    invoke-virtual {v8, v5, v6}, LX/0oZ;->A00(J)LX/0md;

    move-result-object v9

    .line 97422
    goto :goto_1b
    :try_end_f
    .catchall {:try_start_f .. :try_end_f} :catchall_3

    :catchall_3
    :try_start_10
    move-exception v0

    .line 97423
    throw v0

    .line 97424
    :cond_65
    :goto_1c
    if-eqz p1, :cond_66

    const-string v5, "keyboard_visible"

    .line 97425
    move-object/from16 v2, v16

    invoke-virtual {v2, v5}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v2

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    iput-object v2, v1, Lcom/soula2/Conversation;->A4E:Ljava/lang/Boolean;

    .line 97426
    :cond_66
    invoke-virtual/range {v33 .. v33}, LX/1L4;->A01()J

    .line 97427
    move/from16 v5, v22

    move/from16 v2, v25

    if-ge v2, v5, :cond_67

    move v5, v2

    move/from16 v2, v23

    if-ne v5, v2, :cond_68

    const-string/jumbo v5, "samsung"

    sget-object v2, Landroid/os/Build;->MANUFACTURER:Ljava/lang/String;

    .line 97428
    invoke-virtual {v5, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_68

    .line 97429
    :cond_67
    iget-object v2, v1, LX/0kH;->A08:LX/01e;

    .line 97430
    invoke-virtual {v2}, LX/01e;->A07()Landroid/app/KeyguardManager;

    move-result-object v2

    invoke-static {v2}, LX/009;->A05(Ljava/lang/Object;)V

    .line 97431
    invoke-virtual {v2}, Landroid/app/KeyguardManager;->isKeyguardLocked()Z

    move-result v2

    if-eqz v2, :cond_68

    const-string v2, "conversation/locked"

    .line 97432
    invoke-static {v2}, Lcom/whatsapp/util/Log;->i(Ljava/lang/String;)V

    .line 97433
    new-instance v6, Lcom/facebook/redex/IDxBReceiverShape7S0100000_1_I0;

    invoke-direct {v6, v1, v3}, Lcom/facebook/redex/IDxBReceiverShape7S0100000_1_I0;-><init>(Ljava/lang/Object;I)V

    iput-object v6, v1, Lcom/soula2/Conversation;->A06:Landroid/content/BroadcastReceiver;

    .line 97434
    const-string v5, "android.intent.action.USER_PRESENT"

    new-instance v2, Landroid/content/IntentFilter;

    invoke-direct {v2, v5}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v6, v2}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 97435
    :cond_68
    new-instance v5, LX/01S;

    invoke-direct {v5, v1}, LX/01S;-><init>(LX/00o;)V

    move-object/from16 v2, v32

    invoke-virtual {v5, v2}, LX/01S;->A00(Ljava/lang/Class;)LX/011;

    move-result-object v2

    check-cast v2, LX/3MU;

    iput-object v2, v1, Lcom/soula2/Conversation;->A3A:LX/3MU;

    .line 97436
    iget-object v2, v1, Lcom/soula2/Conversation;->A1c:Lcom/soula2/conversation/ConversationListView;

    .line 97437
    invoke-virtual {v2}, Lcom/soula2/conversation/ConversationListView;->getConversationCursorAdapter()LX/0me;

    move-result-object v8

    .line 97438
    iget-object v6, v1, LX/0kK;->A00:LX/241;

    .line 97439
    iget-object v5, v6, LX/241;->A00:LX/2Ai;

    .line 97440
    iget-object v2, v1, Lcom/soula2/Conversation;->A1t:LX/2Fe;

    .line 97441
    iput-object v5, v8, LX/0me;->A09:LX/2Ai;

    .line 97442
    iput-object v6, v8, LX/0me;->A08:LX/241;

    .line 97443
    iput-object v2, v8, LX/0me;->A07:LX/2Fe;

    .line 97444
    iget-object v2, v0, LX/48c;->A04:Landroid/os/Bundle;

    if-eqz v2, :cond_69

    .line 97445
    sget-object v0, Ljava/lang/Boolean;->FALSE:Ljava/lang/Boolean;

    iput-object v0, v1, Lcom/soula2/Conversation;->A4E:Ljava/lang/Boolean;

    const/16 v0, 0x35

    .line 97446
    invoke-static {v2, v0}, Lcom/soula2/invites/PromptSendGroupInviteDialogFragment;->A00(Landroid/os/Bundle;I)Lcom/soula2/invites/PromptSendGroupInviteDialogFragment;

    move-result-object v0

    .line 97447
    invoke-virtual {v1, v0}, LX/0kH;->AcU(Landroidx/fragment/app/DialogFragment;)V

    .line 97448
    :cond_69
    iget-object v2, v1, LX/0kB;->A0X:LX/0vp;

    iget-object v0, v1, Lcom/soula2/Conversation;->A2b:LX/0mJ;

    invoke-virtual {v2, v0}, LX/0vp;->A0Z(LX/0mJ;)Z

    move-result v0

    if-eqz v0, :cond_6a

    .line 97449
    iget-object v2, v1, Lcom/soula2/Conversation;->A2b:LX/0mJ;

    const-class v0, LX/0nO;

    invoke-virtual {v2, v0}, LX/0mJ;->A0A(Ljava/lang/Class;)Lcom/whatsapp/jid/Jid;

    move-result-object v5

    check-cast v5, LX/0nO;

    invoke-static {v5}, LX/009;->A05(Ljava/lang/Object;)V

    .line 97450
    iget-object v2, v1, LX/0kB;->A0X:LX/0vp;

    iget-object v0, v1, Lcom/soula2/Conversation;->A2b:LX/0mJ;

    .line 97451
    invoke-virtual {v2, v0}, LX/0vp;->A0a(LX/0mJ;)Z

    move-result v2

    iget-object v0, v1, LX/0kB;->A0N:LX/0nR;

    .line 97452
    invoke-virtual {v0, v5}, LX/0nR;->A0B(Lcom/whatsapp/jid/GroupJid;)Z

    move-result v0

    .line 97453
    invoke-static {v5, v2, v0}, Lcom/soula2/groupsuspend/CreateGroupSuspendDialog;->A00(LX/0nO;ZZ)Lcom/soula2/groupsuspend/CreateGroupSuspendDialog;

    move-result-object v0

    .line 97454
    invoke-virtual {v1, v0}, LX/0kH;->AcU(Landroidx/fragment/app/DialogFragment;)V

    .line 97455
    :cond_6a
    iget-object v5, v1, LX/0kJ;->A05:LX/0lO;

    iget-object v0, v1, Lcom/soula2/Conversation;->A2r:LX/0l8;

    .line 97456
    invoke-static {v0}, Lcom/whatsapp/jid/UserJid;->of(Lcom/whatsapp/jid/Jid;)Lcom/whatsapp/jid/UserJid;

    move-result-object v2

    if-eqz v2, :cond_6b

    .line 97457
    new-instance v0, Lcom/facebook/redex/RunnableRunnableShape2S0200000_I0;

    invoke-direct {v0, v2, v7, v1}, Lcom/facebook/redex/RunnableRunnableShape2S0200000_I0;-><init>(Ljava/lang/Object;ILjava/lang/Object;)V

    invoke-interface {v5, v0}, LX/0lO;->AZm(Ljava/lang/Runnable;)V

    .line 97458
    :cond_6b
    iget-object v5, v1, Lcom/soula2/Conversation;->A2r:LX/0l8;

    if-eqz v5, :cond_6c

    .line 97459
    iget-object v2, v1, Lcom/soula2/Conversation;->A2N:LX/0uN;

    iget-object v0, v1, LX/0kB;->A07:LX/0nL;

    .line 97460
    invoke-static {v0, v2, v5}, LX/1b3;->A00(LX/0nL;LX/0uN;LX/0l8;)I

    move-result v2

    .line 97461
    iget-object v0, v1, Lcom/soula2/Conversation;->A23:LX/00z;

    .line 97462
    invoke-static {v0, v2}, LX/1b3;->A04(LX/00z;I)Ljava/lang/String;

    move-result-object v2

    .line 97463
    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_6c

    .line 97464
    iget-object v0, v1, LX/0kH;->A08:LX/01e;

    invoke-static {v1, v0, v2}, LX/20j;->A00(Landroid/content/Context;LX/01e;Ljava/lang/CharSequence;)V

    .line 97465
    :cond_6c
    iget-object v0, v1, LX/0kH;->A09:LX/0kh;

    .line 97466
    iget-object v2, v0, LX/0kh;->A00:Landroid/content/SharedPreferences;

    const-string/jumbo v0, "ptt_fast_playback_player_state"

    invoke-interface {v2, v0, v3}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v0

    .line 97467
    sput v0, LX/1gq;->A0x:I

    .line 97468
    invoke-virtual {v1}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    move-result-object v5

    .line 97469
    iget-object v0, v1, LX/0kB;->A0f:LX/0oB;

    .line 97470
    invoke-virtual {v0}, LX/0oB;->A02()LX/0oF;

    move-result-object v0

    invoke-interface {v0}, LX/0oF;->ACV()LX/4Cc;

    move-result-object v2

    if-nez v2, :cond_6e

    const-string v0, "PAY: BrazilSMBPaymentActivity/shouldShowUpsell : paymentsMerchantUpsellHelper is null"

    .line 97471
    invoke-static {v0}, Lcom/whatsapp/util/Log;->e(Ljava/lang/String;)V

    .line 97472
    :cond_6d
    :goto_1d
    iget-object v2, v1, Lcom/soula2/Conversation;->A2h:LX/0zB;

    iget-object v0, v1, Lcom/soula2/Conversation;->A2r:LX/0l8;

    invoke-static {v2, v0}, LX/1bj;->A01(LX/0zB;Lcom/whatsapp/jid/Jid;)Z

    move-result v0

    if-eqz v0, :cond_74

    iget-object v0, v1, LX/0kH;->A09:LX/0kh;

    .line 97473
    iget-object v0, v0, LX/0kh;->A00:Landroid/content/SharedPreferences;

    const-string/jumbo v2, "wac_intro_shown"

    invoke-interface {v0, v2, v3}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    .line 97474
    if-nez v0, :cond_74

    .line 97475
    new-instance v0, Lcom/soula2/WAChatIntroBottomSheet;

    invoke-direct {v0}, Lcom/soula2/WAChatIntroBottomSheet;-><init>()V

    invoke-virtual {v1, v0}, LX/0kH;->AcU(Landroidx/fragment/app/DialogFragment;)V

    .line 97476
    iget-object v0, v1, LX/0kH;->A09:LX/0kh;

    .line 97477
    iget-object v0, v0, LX/0kh;->A00:Landroid/content/SharedPreferences;

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 97478
    invoke-interface {v0, v2, v4}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    goto/16 :goto_20

    .line 97479
    :cond_6e
    if-eqz v5, :cond_6d

    const-string v0, "extra_merchant_upsell_enabled"

    .line 97480
    invoke-virtual {v5, v0, v3}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    if-eqz v0, :cond_6d

    .line 97481
    invoke-virtual {v2}, LX/4Cc;->A02()Z

    move-result v0

    if-eqz v0, :cond_6d

    .line 97482
    iget-object v0, v1, LX/0kB;->A0f:LX/0oB;

    .line 97483
    invoke-virtual {v0}, LX/0oB;->A02()LX/0oF;

    move-result-object v0

    invoke-interface {v0}, LX/0oF;->ACV()LX/4Cc;

    move-result-object v7

    .line 97484
    invoke-static {v7}, LX/009;->A05(Ljava/lang/Object;)V

    const-string v8, "chat"

    .line 97485
    iget-object v6, v7, LX/4Cc;->A02:LX/0q8;

    .line 97486
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    const-string v2, "merchant_upsell_prompt"

    .line 97487
    move-object/from16 v0, v26

    invoke-interface {v6, v5, v0, v2, v8}, LX/0q8;->AJ8(Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/String;Ljava/lang/String;)V

    .line 97488
    invoke-virtual {v1}, Landroid/app/Activity;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v5

    .line 97489
    const v2, 0x7f0d03a1

    .line 97490
    invoke-virtual {v5, v2, v0}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v6

    .line 97491
    new-instance v5, LX/2Hs;

    invoke-direct {v5, v1}, LX/2Hs;-><init>(Landroid/content/Context;)V

    .line 97492
    const v2, 0x7f120e25

    .line 97493
    new-instance v0, Lcom/facebook/redex/IDxObserverShape9S1100000_2_I0;

    invoke-direct {v0, v7, v3}, Lcom/facebook/redex/IDxObserverShape9S1100000_2_I0;-><init>(LX/4Cc;I)V

    .line 97494
    invoke-virtual {v5, v1, v0, v2}, LX/2Hs;->A0D(LX/00m;LX/01R;I)V

    .line 97495
    const v2, 0x7f12108c

    .line 97496
    new-instance v0, LX/4Oh;

    invoke-direct {v0, v1, v7}, LX/4Oh;-><init>(Landroid/app/Activity;LX/4Cc;)V

    .line 97497
    invoke-virtual {v5, v1, v0, v2}, LX/2Hs;->A0E(LX/00m;LX/01R;I)V

    new-instance v0, Lcom/facebook/redex/IDxObserverShape9S1100000_2_I0;

    invoke-direct {v0, v7, v4}, Lcom/facebook/redex/IDxObserverShape9S1100000_2_I0;-><init>(LX/4Cc;I)V

    .line 97498
    invoke-virtual {v5, v1, v0}, LX/2Hs;->A0C(LX/00m;LX/01R;)V

    .line 97499
    invoke-virtual {v5, v4}, LX/01Z;->A07(Z)V

    .line 97500
    invoke-virtual {v5, v6}, LX/01Z;->setView(Landroid/view/View;)LX/01Z;

    .line 97501
    invoke-virtual {v5}, LX/01Z;->create()LX/01b;

    move-result-object v0

    .line 97502
    invoke-virtual {v0}, Landroid/app/Dialog;->show()V

    goto/16 :goto_1d

    .line 97503
    :cond_6f
    const-string v2, "fail_display_name"

    .line 97504
    invoke-virtual {v1, v2}, LX/0kK;->A1o(Ljava/lang/String;)V

    .line 97505
    iget-object v2, v1, Lcom/soula2/Conversation;->A1g:LX/0wi;

    iget-object v0, v0, LX/48c;->A08:LX/0l8;

    .line 97506
    invoke-static {v0}, LX/009;->A05(Ljava/lang/Object;)V

    .line 97507
    invoke-virtual {v2, v0, v4, v4}, LX/0wi;->A02(LX/0l8;ZZ)V

    goto :goto_1f

    .line 97508
    :cond_70
    const-string v0, "fail"

    .line 97509
    invoke-virtual {v1, v0}, LX/0kK;->A1o(Ljava/lang/String;)V

    goto :goto_1f

    .line 97510
    :cond_71
    const-string v0, "conversation/create/no-me-or-msgstore-db"

    .line 97511
    invoke-static {v0}, Lcom/whatsapp/util/Log;->i(Ljava/lang/String;)V

    const-string v0, "no_msg_store"

    .line 97512
    invoke-virtual {v1, v0}, LX/0kK;->A1o(Ljava/lang/String;)V

    .line 97513
    invoke-static {v1}, LX/0kk;->A06(Landroid/content/Context;)Landroid/content/Intent;

    move-result-object v3

    goto :goto_1e

    .line 97514
    :cond_72
    const-string v2, "fail_tell_a_friend"

    .line 97515
    invoke-virtual {v1, v2}, LX/0kK;->A1o(Ljava/lang/String;)V

    .line 97516
    const-class v2, Lcom/soula2/conversationslist/SmsDefaultAppWarning;

    new-instance v3, Landroid/content/Intent;

    invoke-direct {v3, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 97517
    iget-object v2, v0, LX/48c;->A0K:Ljava/lang/String;

    .line 97518
    iget-object v0, v0, LX/48c;->A03:Landroid/net/Uri;

    invoke-virtual {v3, v0}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    .line 97519
    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_73

    const-string/jumbo v0, "sms_body"

    .line 97520
    invoke-virtual {v3, v0, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 97521
    :cond_73
    :goto_1e
    invoke-virtual {v1, v3}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    .line 97522
    :goto_1f
    invoke-virtual {v1}, Landroid/app/Activity;->finish()V

    .line 97523
    move/from16 v0, v28

    invoke-virtual {v1, v0}, LX/0kK;->A1q(S)V
    :try_end_10
    .catchall {:try_start_10 .. :try_end_10} :catchall_4

    .line 97524
    :cond_74
    :goto_20
    move-object/from16 v0, v34

    invoke-virtual {v1, v0}, LX/0kK;->A1m(Ljava/lang/String;)V

    .line 97525
    return-void

    :catchall_4
    move-exception v2

    .line 97526
    move-object/from16 v0, v34

    invoke-virtual {v1, v0}, LX/0kK;->A1m(Ljava/lang/String;)V

    .line 97527
    throw v2
.end method

.method public onCreateDialog(I)Landroid/app/Dialog;
    .locals 38

    .line 97528
    move-object/from16 v2, p0

    invoke-virtual {v2}, Lcom/soula2/Conversation;->AGh()Z

    move-result v0

    move/from16 v1, p1

    if-eqz v0, :cond_0

    .line 97529
    iget-object v3, v2, Lcom/soula2/Conversation;->A42:LX/0kz;

    .line 97530
    iget-boolean v0, v3, LX/0kz;->A0W:Z

    .line 97531
    if-nez v0, :cond_0

    .line 97532
    iput v1, v3, LX/0kz;->A05:I

    .line 97533
    const/4 v0, 0x0

    .line 97534
    return-object v0

    .line 97535
    :cond_0
    iget-object v0, v2, Lcom/soula2/Conversation;->A1p:LX/2iK;

    iget-object v5, v2, Lcom/soula2/Conversation;->A2b:LX/0mJ;

    .line 97536
    iget-object v6, v0, LX/2iK;->A0I:LX/48R;

    .line 97537
    const/4 v11, 0x0

    .line 97538
    if-ne v1, v11, :cond_2

    .line 97539
    iget-object v7, v0, LX/2iK;->A02:LX/00i;

    invoke-static {v7, v11}, LX/1ix;->A00(Landroid/app/Activity;I)V

    .line 97540
    iget v3, v6, LX/48R;->A03:I

    .line 97541
    invoke-static {v7, v3}, LX/1ix;->A00(Landroid/app/Activity;I)V

    .line 97542
    new-instance v8, Lcom/facebook/redex/IDxCListenerShape380S0100000_1_I0;

    invoke-direct {v8, v0, v11}, Lcom/facebook/redex/IDxCListenerShape380S0100000_1_I0;-><init>(Ljava/lang/Object;I)V

    .line 97543
    const v3, 0x7f12043f

    invoke-virtual {v7, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v13

    .line 97544
    iget-object v6, v0, LX/2iK;->A0a:LX/17x;

    .line 97545
    const/4 v9, 0x1

    .line 97546
    invoke-virtual {v6}, LX/17x;->A08()Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v10, 0x3

    .line 97547
    move v12, v9

    invoke-virtual/range {v6 .. v12}, LX/17x;->A04(Landroid/content/Context;LX/4vR;IIIZ)LX/01Z;

    move-result-object v0

    .line 97548
    :goto_0
    invoke-virtual {v0}, LX/01Z;->create()LX/01b;

    move-result-object v0

    .line 97549
    invoke-virtual {v0}, Landroid/app/Dialog;->show()V

    .line 97550
    :goto_1
    if-eqz v0, :cond_16

    return-object v0

    .line 97551
    :cond_1
    move-object v10, v6

    move-object v11, v7

    move-object v12, v8

    move v14, v9

    move v15, v9

    invoke-virtual/range {v10 .. v15}, LX/17x;->A05(Landroid/content/Context;LX/4vR;Ljava/lang/String;IZ)LX/01Z;

    move-result-object v0

    goto :goto_0

    .line 97552
    :cond_2
    iget v3, v6, LX/48R;->A03:I

    .line 97553
    if-ne v1, v3, :cond_4

    .line 97554
    iget-object v7, v0, LX/2iK;->A02:LX/00i;

    invoke-static {v7, v11}, LX/1ix;->A00(Landroid/app/Activity;I)V

    .line 97555
    invoke-static {v7, v3}, LX/1ix;->A00(Landroid/app/Activity;I)V

    .line 97556
    const/4 v9, 0x1

    new-instance v4, Lcom/facebook/redex/IDxCListenerShape340S0100000_1_I0;

    invoke-direct {v4, v0, v9}, Lcom/facebook/redex/IDxCListenerShape340S0100000_1_I0;-><init>(Ljava/lang/Object;I)V

    .line 97557
    const v3, 0x7f12043f

    invoke-virtual {v7, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v15

    .line 97558
    iget-object v6, v0, LX/2iK;->A0a:LX/17x;

    .line 97559
    const v16, 0x7f120436

    .line 97560
    invoke-virtual {v6}, LX/17x;->A08()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 97561
    new-instance v8, LX/4bK;

    invoke-direct {v8, v4}, LX/4bK;-><init>(LX/4vQ;)V

    .line 97562
    const/4 v10, 0x3

    .line 97563
    move v12, v11

    invoke-virtual/range {v6 .. v12}, LX/17x;->A04(Landroid/content/Context;LX/4vR;IIIZ)LX/01Z;

    move-result-object v7

    .line 97564
    :goto_2
    invoke-virtual {v7}, LX/01Z;->create()LX/01b;

    move-result-object v0

    goto :goto_1

    .line 97565
    :cond_3
    move-object v12, v6

    move-object v13, v7

    move/from16 v17, v9

    move/from16 v18, v11

    move-object v14, v4

    invoke-virtual/range {v12 .. v18}, LX/17x;->A03(Landroid/content/Context;LX/4vQ;Ljava/lang/String;IIZ)LX/01Z;

    move-result-object v7

    goto :goto_2

    .line 97566
    :cond_4
    iget v7, v6, LX/48R;->A01:I

    .line 97567
    const/4 v4, 0x0

    const/4 v3, 0x1

    if-ne v1, v7, :cond_5

    .line 97568
    new-instance v8, Lcom/facebook/redex/IDxCListenerShape40S0200000_1_I0;

    invoke-direct {v8, v0, v11, v5}, Lcom/facebook/redex/IDxCListenerShape40S0200000_1_I0;-><init>(Ljava/lang/Object;ILjava/lang/Object;)V

    .line 97569
    iget-object v10, v0, LX/2iK;->A02:LX/00i;

    .line 97570
    new-instance v7, LX/2Hs;

    invoke-direct {v7, v10}, LX/2Hs;-><init>(Landroid/content/Context;)V

    .line 97571
    const v9, 0x7f120844

    new-array v6, v3, [Ljava/lang/Object;

    iget-object v3, v0, LX/2iK;->A0E:LX/0nS;

    .line 97572
    invoke-virtual {v3, v5}, LX/0nS;->A05(LX/0mJ;)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v6, v11

    .line 97573
    invoke-virtual {v10, v9, v6}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    iget-object v0, v0, LX/2iK;->A0Q:LX/17a;

    .line 97574
    invoke-static {v10, v0, v3}, LX/2AY;->A05(Landroid/content/Context;LX/17a;Ljava/lang/CharSequence;)Ljava/lang/CharSequence;

    move-result-object v0

    .line 97575
    invoke-virtual {v7, v0}, LX/01Z;->A06(Ljava/lang/CharSequence;)V

    const v0, 0x7f12083d

    .line 97576
    invoke-virtual {v7, v0, v8}, LX/01Z;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)LX/01Z;

    const v0, 0x7f120375

    .line 97577
    invoke-virtual {v7, v0, v4}, LX/01Z;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)LX/01Z;

    goto :goto_2

    .line 97578
    :cond_5
    iget v4, v6, LX/48R;->A02:I

    .line 97579
    if-ne v1, v4, :cond_6

    .line 97580
    const/16 v4, 0x2e

    new-instance v3, Lcom/facebook/redex/IDxCListenerShape129S0100000_2_I0;

    invoke-direct {v3, v0, v4}, Lcom/facebook/redex/IDxCListenerShape129S0100000_2_I0;-><init>(Ljava/lang/Object;I)V

    .line 97581
    iget-object v0, v0, LX/2iK;->A02:LX/00i;

    .line 97582
    new-instance v7, LX/2Hs;

    invoke-direct {v7, v0}, LX/2Hs;-><init>(Landroid/content/Context;)V

    .line 97583
    const v0, 0x7f120a13

    .line 97584
    invoke-virtual {v7, v0}, LX/01Z;->A02(I)V

    const v0, 0x7f120a12

    .line 97585
    invoke-virtual {v7, v0}, LX/01Z;->A01(I)V

    const v0, 0x7f120fba

    .line 97586
    invoke-virtual {v7, v0, v3}, LX/01Z;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)LX/01Z;

    goto :goto_2

    .line 97587
    :cond_6
    const/16 v7, 0x6a

    if-eq v1, v7, :cond_e

    const/16 v4, 0x25b

    if-eq v1, v4, :cond_e

    .line 97588
    iget v4, v6, LX/48R;->A06:I

    .line 97589
    if-ne v1, v4, :cond_7

    const-string v3, "conversation/dialog/oom"

    .line 97590
    invoke-static {v3}, Lcom/whatsapp/util/Log;->w(Ljava/lang/String;)V

    .line 97591
    iget-object v3, v0, LX/2iK;->A02:LX/00i;

    .line 97592
    new-instance v7, LX/2Hs;

    invoke-direct {v7, v3}, LX/2Hs;-><init>(Landroid/content/Context;)V

    .line 97593
    const v3, 0x7f120813

    .line 97594
    invoke-virtual {v7, v3}, LX/01Z;->A01(I)V

    const v5, 0x7f120fba

    const/16 v4, 0x2d

    :goto_3
    new-instance v3, Lcom/facebook/redex/IDxCListenerShape129S0100000_2_I0;

    invoke-direct {v3, v0, v4}, Lcom/facebook/redex/IDxCListenerShape129S0100000_2_I0;-><init>(Ljava/lang/Object;I)V

    .line 97595
    invoke-virtual {v7, v5, v3}, LX/01Z;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)LX/01Z;

    goto/16 :goto_2

    .line 97596
    :cond_7
    iget v4, v6, LX/48R;->A05:I

    .line 97597
    if-ne v1, v4, :cond_8

    const-string v3, "conversation/dialog/not-an-image"

    .line 97598
    invoke-static {v3}, Lcom/whatsapp/util/Log;->e(Ljava/lang/String;)V

    .line 97599
    iget-object v3, v0, LX/2iK;->A02:LX/00i;

    .line 97600
    new-instance v7, LX/2Hs;

    invoke-direct {v7, v3}, LX/2Hs;-><init>(Landroid/content/Context;)V

    .line 97601
    const v3, 0x7f120801

    .line 97602
    invoke-virtual {v7, v3}, LX/01Z;->A01(I)V

    const v5, 0x7f120fba

    const/16 v4, 0x2a

    goto :goto_3

    .line 97603
    :cond_8
    iget v4, v6, LX/48R;->A0C:I

    .line 97604
    if-ne v1, v4, :cond_9

    const-string v3, "conversation/warned-about-call-charges"

    .line 97605
    invoke-static {v3}, Lcom/whatsapp/util/Log;->i(Ljava/lang/String;)V

    .line 97606
    iget-object v3, v0, LX/2iK;->A02:LX/00i;

    .line 97607
    new-instance v7, LX/2Hs;

    invoke-direct {v7, v3}, LX/2Hs;-><init>(Landroid/content/Context;)V

    .line 97608
    const v3, 0x7f12032f

    .line 97609
    invoke-virtual {v7, v3}, LX/01Z;->A01(I)V

    const v5, 0x7f120327

    const/4 v4, 0x7

    new-instance v3, Lcom/facebook/redex/IDxCListenerShape127S0100000_1_I0;

    invoke-direct {v3, v0, v4}, Lcom/facebook/redex/IDxCListenerShape127S0100000_1_I0;-><init>(Ljava/lang/Object;I)V

    .line 97610
    invoke-virtual {v7, v5, v3}, LX/01Z;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)LX/01Z;

    const v5, 0x7f120375

    const/16 v4, 0x2c

    :goto_4
    new-instance v3, Lcom/facebook/redex/IDxCListenerShape129S0100000_2_I0;

    invoke-direct {v3, v0, v4}, Lcom/facebook/redex/IDxCListenerShape129S0100000_2_I0;-><init>(Ljava/lang/Object;I)V

    .line 97611
    invoke-virtual {v7, v5, v3}, LX/01Z;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)LX/01Z;

    goto/16 :goto_2

    .line 97612
    :cond_9
    const/16 v4, 0xa

    if-ne v1, v4, :cond_a

    .line 97613
    iget-object v4, v0, LX/2iK;->A0F:LX/181;

    iget-object v3, v0, LX/2iK;->A02:LX/00i;

    iget-object v0, v0, LX/2iK;->A06:LX/0kM;

    invoke-virtual {v4, v3, v0, v5}, LX/181;->A00(Landroid/app/Activity;LX/0kM;LX/0mJ;)LX/01b;

    move-result-object v0

    goto/16 :goto_1

    :cond_a
    const/16 v4, 0x73

    if-ne v1, v4, :cond_b

    const-string v3, "conversation/dialog software-about-to-expire"

    .line 97614
    invoke-static {v3}, Lcom/whatsapp/util/Log;->i(Ljava/lang/String;)V

    .line 97615
    iget-object v5, v0, LX/2iK;->A09:LX/1Fo;

    iget-object v4, v0, LX/2iK;->A02:LX/00i;

    iget-object v3, v0, LX/2iK;->A0A:LX/0yv;

    iget-object v0, v0, LX/2iK;->A08:LX/0vi;

    invoke-virtual {v5, v4, v0, v3}, LX/1Fo;->A03(Landroid/app/Activity;LX/0vi;LX/0yv;)Landroid/app/Dialog;

    move-result-object v0

    goto/16 :goto_1

    .line 97616
    :cond_b
    iget v4, v6, LX/48R;->A00:I

    .line 97617
    if-ne v1, v4, :cond_c

    const-string v3, "conversation/add existing contact: activity not found, probably tablet"

    .line 97618
    invoke-static {v3}, Lcom/whatsapp/util/Log;->w(Ljava/lang/String;)V

    .line 97619
    iget-object v3, v0, LX/2iK;->A02:LX/00i;

    .line 97620
    new-instance v7, LX/2Hs;

    invoke-direct {v7, v3}, LX/2Hs;-><init>(Landroid/content/Context;)V

    .line 97621
    const v3, 0x7f120092

    .line 97622
    invoke-virtual {v7, v3}, LX/01Z;->A01(I)V

    const v5, 0x7f120fba

    const/16 v4, 0x30

    goto/16 :goto_3

    .line 97623
    :cond_c
    iget v13, v6, LX/48R;->A04:I

    .line 97624
    if-ne v1, v13, :cond_d

    .line 97625
    iget-object v4, v0, LX/2iK;->A04:LX/3xf;

    .line 97626
    iget-object v4, v4, LX/3xf;->A00:Lcom/soula2/Conversation;

    iget-object v6, v4, LX/0kB;->A0J:LX/1tx;

    .line 97627
    if-eqz v6, :cond_15

    .line 97628
    iget-object v4, v6, LX/1tx;->A04:Ljava/util/HashMap;

    invoke-virtual {v4}, Ljava/util/AbstractMap;->isEmpty()Z

    move-result v4

    .line 97629
    if-nez v4, :cond_15

    .line 97630
    const-string v4, "conversation/dialog/delete/"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 97631
    iget-object v6, v6, LX/1tx;->A04:Ljava/util/HashMap;

    invoke-virtual {v6}, Ljava/util/AbstractMap;->size()I

    move-result v4

    .line 97632
    invoke-virtual {v7, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v7}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/whatsapp/util/Log;->i(Ljava/lang/String;)V

    .line 97633
    iget-object v12, v0, LX/2iK;->A02:LX/00i;

    iget-object v4, v0, LX/2iK;->A0J:LX/0lm;

    move-object/from16 v37, v4

    iget-object v4, v0, LX/2iK;->A0R:LX/0kj;

    move-object/from16 v26, v4

    iget-object v4, v0, LX/2iK;->A07:LX/0li;

    move-object/from16 v36, v4

    iget-object v4, v0, LX/2iK;->A0b:LX/0lO;

    move-object/from16 v23, v4

    iget-object v4, v0, LX/2iK;->A0T:LX/0pG;

    move-object/from16 v21, v4

    iget-object v4, v0, LX/2iK;->A0Q:LX/17a;

    move-object/from16 v20, v4

    iget-object v4, v0, LX/2iK;->A0B:LX/0oS;

    move-object/from16 v19, v4

    iget-object v11, v0, LX/2iK;->A0D:LX/0nL;

    iget-object v4, v0, LX/2iK;->A0P:LX/120;

    move-object/from16 v18, v4

    iget-object v10, v0, LX/2iK;->A0E:LX/0nS;

    iget-object v4, v0, LX/2iK;->A0L:LX/00z;

    move-object/from16 v22, v4

    iget-object v4, v0, LX/2iK;->A0V:LX/0vp;

    move-object/from16 v17, v4

    iget-object v4, v0, LX/2iK;->A0W:LX/11J;

    move-object/from16 v16, v4

    iget-object v15, v0, LX/2iK;->A0U:LX/0uO;

    iget-object v9, v0, LX/2iK;->A0K:LX/0kh;

    iget-object v8, v0, LX/2iK;->A0N:LX/0nR;

    iget-object v7, v0, LX/2iK;->A0Y:LX/18e;

    .line 97634
    invoke-virtual {v6}, Ljava/util/AbstractMap;->values()Ljava/util/Collection;

    move-result-object v4

    .line 97635
    new-instance v6, Ljava/util/HashSet;

    invoke-direct {v6, v4}, Ljava/util/HashSet;-><init>(Ljava/util/Collection;)V

    const-class v4, LX/0l8;

    .line 97636
    invoke-virtual {v5, v4}, LX/0mJ;->A0A(Ljava/lang/Class;)Lcom/whatsapp/jid/Jid;

    move-result-object v14

    check-cast v14, LX/0l8;

    .line 97637
    iget-object v5, v0, LX/2iK;->A05:LX/1jY;

    iget-object v4, v0, LX/2iK;->A0H:LX/3zR;

    .line 97638
    new-instance v0, LX/4W3;

    invoke-direct {v0, v12, v13}, LX/4W3;-><init>(Landroid/app/Activity;I)V

    .line 97639
    invoke-static {v12, v11, v10, v14, v6}, LX/33E;->A02(Landroid/content/Context;LX/0nL;LX/0nS;LX/0l8;Ljava/util/Collection;)Ljava/lang/String;

    move-result-object v33

    .line 97640
    move-object/from16 v24, v18

    move-object/from16 v25, v20

    move-object/from16 v27, v21

    move-object/from16 v28, v15

    move-object/from16 v29, v17

    move-object/from16 v30, v16

    move-object/from16 v31, v7

    move-object/from16 v32, v23

    move-object/from16 v34, v6

    move/from16 v35, v3

    move-object v13, v0

    move-object v14, v5

    move-object/from16 v15, v36

    move-object/from16 v16, v19

    move-object/from16 v17, v11

    move-object/from16 v18, v10

    move-object/from16 v19, v4

    move-object/from16 v20, v37

    move-object/from16 v21, v9

    move-object/from16 v23, v8

    invoke-static/range {v12 .. v35}, LX/33E;->A00(Landroid/content/Context;LX/4ut;LX/1jY;LX/0li;LX/0oS;LX/0nL;LX/0nS;LX/3zR;LX/0lm;LX/0kh;LX/00z;LX/0nR;LX/120;LX/17a;LX/0kj;LX/0pG;LX/0uO;LX/0vp;LX/11J;LX/18e;LX/0lO;Ljava/lang/String;Ljava/util/Set;Z)Landroid/app/Dialog;

    move-result-object v0

    goto/16 :goto_1

    .line 97641
    :cond_d
    iget v4, v6, LX/48R;->A0B:I

    .line 97642
    if-ne v1, v4, :cond_11

    .line 97643
    iget-object v6, v0, LX/2iK;->A02:LX/00i;

    .line 97644
    new-instance v7, LX/2Hs;

    invoke-direct {v7, v6}, LX/2Hs;-><init>(Landroid/content/Context;)V

    .line 97645
    const v5, 0x7f12038a

    new-array v4, v3, [Ljava/lang/Object;

    const/high16 v3, 0x10000

    .line 97646
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v4, v11

    .line 97647
    invoke-virtual {v6, v5, v4}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    .line 97648
    invoke-virtual {v7, v3}, LX/01Z;->A06(Ljava/lang/CharSequence;)V

    const v5, 0x7f121599

    const/16 v4, 0x2f

    new-instance v3, Lcom/facebook/redex/IDxCListenerShape129S0100000_2_I0;

    invoke-direct {v3, v0, v4}, Lcom/facebook/redex/IDxCListenerShape129S0100000_2_I0;-><init>(Ljava/lang/Object;I)V

    .line 97649
    invoke-virtual {v7, v5, v3}, LX/01Z;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)LX/01Z;

    const v5, 0x7f120375

    const/16 v4, 0x2b

    goto/16 :goto_4

    .line 97650
    :cond_e
    iget-object v6, v0, LX/2iK;->A0S:LX/0zB;

    iget-object v4, v0, LX/2iK;->A0X:LX/0l8;

    invoke-static {v6, v4}, LX/1bj;->A01(LX/0zB;Lcom/whatsapp/jid/Jid;)Z

    move-result v4

    if-eqz v4, :cond_10

    .line 97651
    const v8, 0x7f121bbb

    .line 97652
    :cond_f
    :goto_5
    iget-object v6, v0, LX/2iK;->A02:LX/00i;

    .line 97653
    new-instance v7, LX/2Hs;

    invoke-direct {v7, v6}, LX/2Hs;-><init>(Landroid/content/Context;)V

    .line 97654
    new-array v4, v3, [Ljava/lang/Object;

    iget-object v3, v0, LX/2iK;->A0E:LX/0nS;

    .line 97655
    invoke-virtual {v3, v5}, LX/0nS;->A05(LX/0mJ;)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v4, v11

    invoke-virtual {v6, v8, v4}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v7, v3}, LX/01Z;->A06(Ljava/lang/CharSequence;)V

    const v4, 0x7f12192d

    new-instance v3, LX/34F;

    invoke-direct {v3, v0, v5, v1}, LX/34F;-><init>(LX/2iK;LX/0mJ;I)V

    .line 97656
    invoke-virtual {v7, v4, v3}, LX/01Z;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)LX/01Z;

    const v5, 0x7f120375

    const/4 v4, 0x2

    new-instance v3, Lcom/facebook/redex/IDxCListenerShape6S0101000_2_I0;

    invoke-direct {v3, v0, v1, v4}, Lcom/facebook/redex/IDxCListenerShape6S0101000_2_I0;-><init>(Ljava/lang/Object;II)V

    .line 97657
    invoke-virtual {v7, v5, v3}, LX/01Z;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)LX/01Z;

    goto/16 :goto_2

    .line 97658
    :cond_10
    const v8, 0x7f1210f6

    if-ne v1, v7, :cond_f

    .line 97659
    const v8, 0x7f120381

    goto :goto_5

    .line 97660
    :cond_11
    iget v4, v6, LX/48R;->A0A:I

    .line 97661
    if-ne v1, v4, :cond_12

    .line 97662
    iget-object v4, v0, LX/2iK;->A02:LX/00i;

    iget-object v7, v0, LX/2iK;->A0R:LX/0kj;

    iget-object v5, v0, LX/2iK;->A03:LX/154;

    iget-object v8, v0, LX/2iK;->A0Z:LX/15f;

    iget-object v6, v0, LX/2iK;->A0K:LX/0kh;

    move v9, v1

    move v10, v3

    invoke-static/range {v4 .. v10}, LX/4HF;->A01(Landroid/app/Activity;LX/154;LX/0kh;LX/0kj;LX/15f;IZ)Landroid/app/Dialog;

    move-result-object v0

    goto/16 :goto_1

    .line 97663
    :cond_12
    iget v4, v6, LX/48R;->A09:I

    .line 97664
    if-ne v1, v4, :cond_13

    .line 97665
    iget-object v5, v0, LX/2iK;->A02:LX/00i;

    iget-object v4, v0, LX/2iK;->A0R:LX/0kj;

    iget-object v6, v0, LX/2iK;->A03:LX/154;

    iget-object v3, v0, LX/2iK;->A0Z:LX/15f;

    iget-object v0, v0, LX/2iK;->A0K:LX/0kh;

    move v10, v1

    move-object v7, v0

    move-object v8, v4

    move-object v9, v3

    invoke-static/range {v5 .. v11}, LX/4HF;->A01(Landroid/app/Activity;LX/154;LX/0kh;LX/0kj;LX/15f;IZ)Landroid/app/Dialog;

    move-result-object v0

    goto/16 :goto_1

    .line 97666
    :cond_13
    iget v4, v6, LX/48R;->A08:I

    .line 97667
    if-ne v1, v4, :cond_14

    .line 97668
    iget-object v4, v0, LX/2iK;->A02:LX/00i;

    iget-object v7, v0, LX/2iK;->A0R:LX/0kj;

    iget-object v5, v0, LX/2iK;->A03:LX/154;

    iget-object v8, v0, LX/2iK;->A0Z:LX/15f;

    iget-object v6, v0, LX/2iK;->A0K:LX/0kh;

    move v9, v1

    move v10, v3

    invoke-static/range {v4 .. v10}, LX/4HF;->A00(Landroid/app/Activity;LX/154;LX/0kh;LX/0kj;LX/15f;IZ)Landroid/app/Dialog;

    move-result-object v0

    goto/16 :goto_1

    .line 97669
    :cond_14
    iget v3, v6, LX/48R;->A07:I

    .line 97670
    if-ne v1, v3, :cond_16

    .line 97671
    iget-object v5, v0, LX/2iK;->A02:LX/00i;

    iget-object v4, v0, LX/2iK;->A0R:LX/0kj;

    iget-object v6, v0, LX/2iK;->A03:LX/154;

    iget-object v3, v0, LX/2iK;->A0Z:LX/15f;

    iget-object v0, v0, LX/2iK;->A0K:LX/0kh;

    move v10, v1

    move-object v7, v0

    move-object v8, v4

    move-object v9, v3

    invoke-static/range {v5 .. v11}, LX/4HF;->A00(Landroid/app/Activity;LX/154;LX/0kh;LX/0kj;LX/15f;IZ)Landroid/app/Dialog;

    move-result-object v0

    goto/16 :goto_1

    .line 97672
    :cond_15
    const-string v0, "conversation/dialog/delete no messages"

    .line 97673
    invoke-static {v0}, Lcom/whatsapp/util/Log;->e(Ljava/lang/String;)V

    .line 97674
    :cond_16
    invoke-super {v2, v1}, LX/0kB;->onCreateDialog(I)Landroid/app/Dialog;

    move-result-object v0

    return-object v0
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;)Z
    .locals 2

    .line 97675
    iget-object v0, p0, Lcom/soula2/Conversation;->A57:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1km;

    .line 97676
    invoke-interface {v0, p1}, LX/1km;->ANF(Landroid/view/Menu;)V

    goto :goto_0

    .line 97677
    :cond_0
    invoke-super {p0, p1}, LX/0kF;->onCreateOptionsMenu(Landroid/view/Menu;)Z

    move-result v0

    return v0
.end method

.method public onDestroy()V
    .locals 6

    .line 97678
    invoke-super {p0}, LX/0kB;->onDestroy()V

    invoke-static {p0}, Lcom/ApxSAMods/core/Izumi;->onDestroy(Lcom/soula2/Conversation;)V

    .line 97679
    iget-object v0, p0, Lcom/soula2/Conversation;->A05:Landroid/animation/ObjectAnimator;

    if-eqz v0, :cond_0

    .line 97680
    invoke-virtual {v0}, Landroid/animation/Animator;->cancel()V

    .line 97681
    :cond_0
    const/4 v3, 0x0

    .line 97682
    iput-object v3, p0, Lcom/soula2/Conversation;->A05:Landroid/animation/ObjectAnimator;

    .line 97683
    iget-object v0, p0, Lcom/soula2/Conversation;->A2j:LX/0mU;

    if-eqz v0, :cond_1

    .line 97684
    invoke-virtual {v0}, LX/0mU;->A0G()V

    .line 97685
    :cond_1
    iget-object v0, p0, Lcom/soula2/Conversation;->A42:LX/0kz;

    if-eqz v0, :cond_2

    .line 97686
    invoke-virtual {v0}, LX/0kz;->A02()V

    .line 97687
    :cond_2
    iget-boolean v0, p0, Lcom/soula2/Conversation;->A4U:Z

    const/4 v4, 0x0

    if-eqz v0, :cond_3

    .line 97688
    iget-object v1, p0, LX/0kB;->A0R:LX/18o;

    iget-object v0, p0, Lcom/soula2/Conversation;->A4w:LX/0t1;

    invoke-virtual {v1, v0}, LX/0pD;->A04(Ljava/lang/Object;)V

    .line 97689
    iput-boolean v4, p0, Lcom/soula2/Conversation;->A4U:Z

    .line 97690
    :cond_3
    invoke-virtual {p0}, Lcom/soula2/Conversation;->A2t()V

    .line 97691
    iget-object v0, p0, Lcom/soula2/Conversation;->A0e:LX/10E;

    .line 97692
    invoke-virtual {v0}, LX/10E;->A00()LX/1hf;

    move-result-object v5

    .line 97693
    invoke-static {v5}, LX/009;->A05(Ljava/lang/Object;)V

    .line 97694
    iget-object v0, v5, LX/1hf;->A00:Lcom/soula2/Conversation;

    if-ne v0, p0, :cond_4

    .line 97695
    const-string v0, "conversation/clearsession/jid "

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1, v0}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/soula2/Conversation;->A2r:LX/0l8;

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/whatsapp/util/Log;->d(Ljava/lang/String;)V

    .line 97696
    iget-object v2, p0, Lcom/soula2/Conversation;->A0e:LX/10E;

    iget-object v0, p0, LX/0kF;->A0A:LX/0z3;

    new-instance v1, LX/1hf;

    invoke-direct {v1, v3, v0, v4}, LX/1hf;-><init>(Lcom/soula2/Conversation;LX/0z3;Z)V

    .line 97697
    iget-object v0, v2, LX/10E;->A00:Ljava/util/concurrent/atomic/AtomicReference;

    invoke-virtual {v0, v5, v1}, Ljava/util/concurrent/atomic/AtomicReference;->compareAndSet(Ljava/lang/Object;Ljava/lang/Object;)Z

    .line 97698
    :cond_4
    invoke-virtual {p0}, Landroid/app/Activity;->isFinishing()Z

    move-result v0

    if-eqz v0, :cond_5

    .line 97699
    iget-object v2, p0, Lcom/soula2/Conversation;->A2R:LX/19M;

    iget-object v1, p0, Lcom/soula2/Conversation;->A2r:LX/0l8;

    invoke-virtual {p0}, Landroid/app/Activity;->getLocalClassName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v1, v0}, LX/19M;->A03(LX/0l8;Ljava/lang/String;)V

    .line 97700
    :cond_5
    iget-object v0, p0, Lcom/soula2/Conversation;->A1f:LX/2w2;

    const/4 v2, 0x1

    if-eqz v0, :cond_6

    .line 97701
    invoke-virtual {v0, v2}, LX/0ng;->A08(Z)V

    .line 97702
    :cond_6
    iget-object v1, p0, Lcom/soula2/Conversation;->A2p:LX/1cK;

    if-eqz v1, :cond_7

    .line 97703
    iget-object v0, p0, Lcom/soula2/Conversation;->A2q:LX/128;

    invoke-virtual {v0, v1}, LX/0pD;->A04(Ljava/lang/Object;)V

    .line 97704
    :cond_7
    iget-object v1, p0, Lcom/soula2/Conversation;->A45:LX/0y6;

    iget-object v0, p0, Lcom/soula2/Conversation;->A53:LX/1Ee;

    invoke-virtual {v1, v0}, LX/0pD;->A04(Ljava/lang/Object;)V

    .line 97705
    iget-object v1, p0, Lcom/soula2/Conversation;->A41:LX/18m;

    iget-object v0, p0, Lcom/soula2/Conversation;->A52:LX/2Ph;

    invoke-virtual {v1, v0}, LX/0pD;->A04(Ljava/lang/Object;)V

    .line 97706
    iget-object v1, p0, Lcom/soula2/Conversation;->A1n:LX/18l;

    iget-object v0, p0, Lcom/soula2/Conversation;->A4p:LX/2M7;

    invoke-virtual {v1, v0}, LX/0pD;->A04(Ljava/lang/Object;)V

    .line 97707
    iget-object v1, p0, Lcom/soula2/Conversation;->A3z:LX/188;

    iget-object v0, p0, Lcom/soula2/Conversation;->A4s:LX/0ky;

    invoke-virtual {v1, v0}, LX/0pD;->A04(Ljava/lang/Object;)V

    .line 97708
    iget-object v1, p0, Lcom/soula2/Conversation;->A36:LX/152;

    iget-object v0, p0, Lcom/soula2/Conversation;->A4y:LX/48s;

    invoke-virtual {v1, v0}, LX/0pD;->A04(Ljava/lang/Object;)V

    .line 97709
    iget-object v1, p0, Lcom/soula2/Conversation;->A1A:LX/0zc;

    iget-object v0, p0, Lcom/soula2/Conversation;->A4v:LX/1W7;

    invoke-virtual {v1, v0}, LX/0pD;->A04(Ljava/lang/Object;)V

    .line 97710
    iget-object v1, p0, Lcom/soula2/Conversation;->A3X:LX/12d;

    iget-object v0, p0, Lcom/soula2/Conversation;->A50:LX/1JJ;

    invoke-virtual {v1, v0}, LX/0pD;->A04(Ljava/lang/Object;)V

    .line 97711
    iget-object v0, p0, Lcom/soula2/Conversation;->A3f:LX/1JK;

    invoke-virtual {v0}, LX/1JK;->A03()V

    .line 97712
    iget-object v1, p0, Lcom/soula2/Conversation;->A3e:LX/1E8;

    iget-object v0, p0, Lcom/soula2/Conversation;->A4r:LX/213;

    invoke-virtual {v1, v0}, LX/0pD;->A04(Ljava/lang/Object;)V

    .line 97713
    iget-object v1, p0, Lcom/soula2/Conversation;->A38:LX/16K;

    iget-object v0, p0, Lcom/soula2/Conversation;->A4q:LX/1zo;

    invoke-virtual {v1, v0}, LX/0pD;->A04(Ljava/lang/Object;)V

    .line 97714
    iget-object v0, p0, Lcom/soula2/Conversation;->A3y:LX/2v5;

    if-eqz v0, :cond_8

    .line 97715
    invoke-virtual {v0, v2}, LX/0ng;->A08(Z)V

    .line 97716
    iput-object v3, p0, Lcom/soula2/Conversation;->A3y:LX/2v5;

    .line 97717
    :cond_8
    iget-object v1, p0, Lcom/soula2/Conversation;->A2v:Lcom/soula2/mentions/MentionableEntry;

    if-eqz v1, :cond_9

    .line 97718
    iget-object v0, p0, Lcom/soula2/Conversation;->A4k:Landroid/text/TextWatcher;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->removeTextChangedListener(Landroid/text/TextWatcher;)V

    .line 97719
    iget-object v1, p0, Lcom/soula2/Conversation;->A2v:Lcom/soula2/mentions/MentionableEntry;

    iget-object v0, p0, Lcom/soula2/Conversation;->A4i:Landroid/text/TextWatcher;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->removeTextChangedListener(Landroid/text/TextWatcher;)V

    .line 97720
    iget-object v1, p0, Lcom/soula2/Conversation;->A2v:Lcom/soula2/mentions/MentionableEntry;

    new-array v0, v4, [Landroid/text/InputFilter;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setFilters([Landroid/text/InputFilter;)V

    .line 97721
    iget-object v0, p0, Lcom/soula2/Conversation;->A2v:Lcom/soula2/mentions/MentionableEntry;

    invoke-virtual {v0}, Lcom/soula2/mentions/MentionableEntry;->A09()V

    .line 97722
    iput-object v3, p0, Lcom/soula2/Conversation;->A2v:Lcom/soula2/mentions/MentionableEntry;

    .line 97723
    :cond_9
    iget-object v0, p0, Lcom/soula2/Conversation;->A3G:LX/2FY;

    invoke-virtual {v0}, LX/2FY;->A00()V

    .line 97724
    iget-object v1, p0, Lcom/soula2/Conversation;->A16:Lcom/soula2/WaEditText;

    if-eqz v1, :cond_a

    .line 97725
    iget-object v0, p0, Lcom/soula2/Conversation;->A4j:Landroid/text/TextWatcher;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->removeTextChangedListener(Landroid/text/TextWatcher;)V

    .line 97726
    :cond_a
    iget-object v0, p0, Lcom/soula2/Conversation;->A06:Landroid/content/BroadcastReceiver;

    if-eqz v0, :cond_b

    .line 97727
    invoke-virtual {p0, v0}, Landroid/content/Context;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 97728
    iput-object v3, p0, Lcom/soula2/Conversation;->A06:Landroid/content/BroadcastReceiver;

    .line 97729
    :cond_b
    iget-boolean v0, p0, Lcom/soula2/Conversation;->A4W:Z

    .line 97730
    iget-object v2, p0, LX/0kB;->A0I:LX/11Q;

    if-eqz v0, :cond_10

    .line 97731
    iget-object v1, p0, LX/0kH;->A09:LX/0kh;

    .line 97732
    iget-object v0, p0, Lcom/soula2/Conversation;->A2r:LX/0l8;

    .line 97733
    invoke-static {v2, v1, v0}, LX/1w1;->A08(LX/11Q;LX/0kh;LX/0l8;)V

    .line 97734
    iget-object v1, p0, LX/0kB;->A0I:LX/11Q;

    iget-object v0, p0, Lcom/soula2/Conversation;->A0B:Landroid/view/View;

    invoke-static {v0, v1}, LX/1w1;->A02(Landroid/view/View;LX/11Q;)V

    .line 97735
    iget-object v0, p0, Lcom/soula2/Conversation;->A1V:LX/1Hc;

    if-eqz v0, :cond_c

    .line 97736
    invoke-virtual {v0}, LX/1Hc;->A00()V

    .line 97737
    iput-object v3, p0, Lcom/soula2/Conversation;->A1V:LX/1Hc;

    .line 97738
    :cond_c
    :goto_0
    iget-boolean v0, p0, Lcom/soula2/Conversation;->A4N:Z

    if-eqz v0, :cond_d

    .line 97739
    iget-object v0, p0, Lcom/soula2/Conversation;->A35:LX/18k;

    invoke-virtual {v0}, LX/18k;->A00()V

    .line 97740
    iput-boolean v4, p0, Lcom/soula2/Conversation;->A4N:Z

    .line 97741
    :cond_d
    iget-object v1, p0, Lcom/soula2/Conversation;->A3x:LX/1mL;

    if-eqz v1, :cond_e

    invoke-interface {v1}, LX/1mL;->ABj()Z

    move-result v0

    if-eqz v0, :cond_e

    .line 97742
    invoke-interface {v1}, LX/1mL;->A5c()V

    .line 97743
    :cond_e
    iget-object v0, p0, Lcom/soula2/Conversation;->A1H:LX/1lD;

    if-eqz v0, :cond_f

    .line 97744
    invoke-virtual {v0}, LX/1lD;->A00()V

    .line 97745
    :cond_f
    iget-object v2, p0, Lcom/soula2/Conversation;->A56:Ljava/util/Set;

    invoke-interface {v2}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_12

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/Application$ActivityLifecycleCallbacks;

    .line 97746
    invoke-interface {v0, p0}, Landroid/app/Application$ActivityLifecycleCallbacks;->onActivityDestroyed(Landroid/app/Activity;)V

    goto :goto_1

    .line 97747
    :cond_10
    invoke-virtual {v2}, LX/11Q;->A00()LX/1gq;

    move-result-object v0

    if-eqz v0, :cond_11

    .line 97748
    iput-object v3, v0, LX/1gq;->A0J:LX/4t9;

    .line 97749
    :cond_11
    iget-object v0, p0, LX/0kB;->A0I:LX/11Q;

    invoke-virtual {v0}, LX/11Q;->A06()V

    .line 97750
    iget-object v0, p0, LX/0kB;->A0I:LX/11Q;

    invoke-virtual {v0}, LX/11Q;->A07()V

    goto :goto_0

    .line 97751
    :cond_12
    invoke-interface {v2}, Ljava/util/Set;->clear()V

    .line 97752
    iget-object v0, p0, Lcom/soula2/Conversation;->A57:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->clear()V

    .line 97753
    iget-object v0, p0, Lcom/soula2/Conversation;->A55:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->clear()V

    .line 97754
    iget-object v0, p0, Lcom/soula2/Conversation;->A58:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->clear()V

    .line 97755
    iput-object v3, p0, Lcom/soula2/Conversation;->A1h:LX/1NR;

    .line 97756
    iget-object v0, p0, Lcom/soula2/Conversation;->A4C:LX/01K;

    invoke-interface {v0}, LX/01K;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/18i;

    invoke-interface {v0}, LX/18i;->Aak()V

    .line 97757
    iget-object v3, p0, Lcom/soula2/Conversation;->A1c:Lcom/soula2/conversation/ConversationListView;

    if-eqz v3, :cond_13

    .line 97758
    iget-object v2, p0, Lcom/soula2/Conversation;->A4m:Landroid/view/ViewTreeObserver$OnPreDrawListener;

    iget-object v1, p0, Lcom/soula2/Conversation;->A4f:Landroid/database/DataSetObserver;

    .line 97759
    invoke-virtual {v3}, Landroid/view/View;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/view/ViewTreeObserver;->removeOnPreDrawListener(Landroid/view/ViewTreeObserver$OnPreDrawListener;)V

    .line 97760
    iget-object v0, v3, Lcom/soula2/conversation/ConversationListView;->A09:Ljava/lang/Runnable;

    invoke-virtual {v3, v0}, Landroid/view/View;->removeCallbacks(Ljava/lang/Runnable;)Z

    .line 97761
    invoke-virtual {v3}, Lcom/soula2/conversation/ConversationListView;->getConversationCursorAdapter()LX/0me;

    move-result-object v0

    if-eqz v0, :cond_13

    .line 97762
    invoke-virtual {v0, v1}, Landroid/widget/BaseAdapter;->unregisterDataSetObserver(Landroid/database/DataSetObserver;)V

    .line 97763
    invoke-virtual {v0}, Landroid/widget/CursorAdapter;->getCursor()Landroid/database/Cursor;

    move-result-object v0

    if-eqz v0, :cond_13

    .line 97764
    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    .line 97765
    :cond_13
    return-void
.end method

.method public onKeyDown(ILandroid/view/KeyEvent;)Z
    .locals 3

    .line 97766
    iget-object v0, p0, Lcom/soula2/Conversation;->A58:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    const/4 v1, 0x1

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    .line 97767
    goto :goto_0

    .line 97768
    :cond_0
    invoke-virtual {p2}, Landroid/view/KeyEvent;->isPrintingKey()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 97769
    iget-object v0, p0, Lcom/soula2/Conversation;->A2v:Lcom/soula2/mentions/MentionableEntry;

    invoke-virtual {v0}, Landroid/view/View;->isShown()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/soula2/Conversation;->A2v:Lcom/soula2/mentions/MentionableEntry;

    .line 97770
    invoke-virtual {v0}, Landroid/view/View;->hasFocus()Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/soula2/Conversation;->A42:LX/0kz;

    if-eqz v0, :cond_2

    .line 97771
    iget-object v0, v0, LX/0kz;->A0P:LX/0l2;

    if-eqz v0, :cond_2

    .line 97772
    :cond_1
    invoke-super {p0, p1, p2}, LX/0kF;->onKeyDown(ILandroid/view/KeyEvent;)Z

    move-result v1

    .line 97773
    return v1

    .line 97774
    :cond_2
    iget-object v0, p0, Lcom/soula2/Conversation;->A2v:Lcom/soula2/mentions/MentionableEntry;

    invoke-virtual {v0}, Landroid/view/View;->requestFocus()Z

    .line 97775
    iget-object v0, p0, Lcom/soula2/Conversation;->A2v:Lcom/soula2/mentions/MentionableEntry;

    invoke-virtual {v0, p2}, Landroid/view/View;->dispatchKeyEvent(Landroid/view/KeyEvent;)Z

    return v1
.end method

.method public onKeyUp(ILandroid/view/KeyEvent;)Z
    .locals 4

    const/4 v3, 0x0

    const/16 v0, 0x52

    if-eq p1, v0, :cond_0

    const/4 v0, 0x4

    if-ne p1, v0, :cond_1

    .line 97776
    :cond_0
    invoke-virtual {p0}, Lcom/soula2/Conversation;->A3E()Z

    move-result v0

    if-eqz v0, :cond_1

    return v3

    .line 97777
    :cond_1
    iget-boolean v0, p0, Lcom/soula2/Conversation;->A4R:Z

    if-eqz v0, :cond_4

    const/16 v0, 0x17

    const/4 v2, 0x1

    if-ne p1, v0, :cond_2

    .line 97778
    invoke-virtual {p2}, Landroid/view/KeyEvent;->getAction()I

    move-result v0

    if-ne v0, v2, :cond_4

    .line 97779
    invoke-virtual {p0}, Lcom/soula2/Conversation;->AGh()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 97780
    iget-object v1, p0, Lcom/soula2/Conversation;->A42:LX/0kz;

    .line 97781
    iget-boolean v0, v1, LX/0kz;->A0W:Z

    .line 97782
    if-nez v0, :cond_4

    .line 97783
    iget-boolean v0, p0, Lcom/soula2/Conversation;->A4P:Z

    invoke-virtual {v1, v2, v0}, LX/0kz;->A0X(ZZ)V

    return v2

    .line 97784
    :cond_2
    invoke-virtual {p0}, Lcom/soula2/Conversation;->AGh()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 97785
    iget-object v1, p0, Lcom/soula2/Conversation;->A42:LX/0kz;

    .line 97786
    iget-boolean v0, v1, LX/0kz;->A0W:Z

    .line 97787
    if-nez v0, :cond_4

    const/16 v0, 0x13

    if-eq p1, v0, :cond_3

    const/16 v0, 0x15

    if-ne p1, v0, :cond_4

    .line 97788
    iget-boolean v0, p0, Lcom/soula2/Conversation;->A4P:Z

    invoke-virtual {v1, v3, v0}, LX/0kz;->A0X(ZZ)V

    return v2

    .line 97789
    :cond_3
    iget-boolean v0, p0, Lcom/soula2/Conversation;->A4P:Z

    invoke-virtual {v1, v0}, LX/0kz;->A0T(Z)V

    return v2

    .line 97790
    :cond_4
    invoke-super {p0, p1, p2}, LX/0kF;->onKeyUp(ILandroid/view/KeyEvent;)Z

    move-result v2

    return v2
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 2

    .line 97791
    iget-object v0, p0, Lcom/soula2/Conversation;->A57:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1km;

    .line 97792
    invoke-interface {v0, p1}, LX/1km;->ARw(Landroid/view/MenuItem;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    return v0

    :cond_1
    const/4 v0, 0x0

    return v0
.end method

.method public onPause()V
    .locals 5

    .line 97793
    invoke-super {p0}, LX/0kB;->onPause()V

    const/4 v4, 0x1

    .line 97794
    iput-boolean v4, p0, Lcom/soula2/Conversation;->A4e:Z

    .line 97795
    invoke-virtual {p0}, Lcom/soula2/Conversation;->A2t()V

    .line 97796
    iget-object v0, p0, LX/0kB;->A0I:LX/11Q;

    invoke-virtual {v0}, LX/11Q;->A0B()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 97797
    iget-object v0, p0, LX/0kB;->A0I:LX/11Q;

    invoke-virtual {v0}, LX/11Q;->A03()V

    .line 97798
    :cond_0
    iget-boolean v0, p0, Lcom/soula2/Conversation;->A4W:Z

    if-eqz v0, :cond_1

    .line 97799
    iget-object v0, p0, LX/0kB;->A0I:LX/11Q;

    invoke-static {v0}, LX/1w1;->A07(LX/11Q;)V

    .line 97800
    iget-object v0, p0, Lcom/soula2/Conversation;->A4B:LX/01K;

    invoke-interface {v0}, LX/01K;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/17t;

    .line 97801
    iget-object v0, p0, LX/0kH;->A00:Landroid/view/View;

    .line 97802
    invoke-static {v0}, LX/17t;->A00(Landroid/view/View;)Z

    move-result v2

    .line 97803
    iget-object v0, v3, LX/17t;->A07:LX/11Q;

    invoke-virtual {v0}, LX/11Q;->A0B()Z

    move-result v1

    const/4 v0, 0x0

    if-nez v2, :cond_8

    if-eqz v1, :cond_7

    .line 97804
    iput-boolean v4, v3, LX/17t;->A05:Z

    .line 97805
    iput-boolean v0, v3, LX/17t;->A04:Z

    .line 97806
    :goto_0
    iput-boolean v4, v3, LX/17t;->A03:Z

    .line 97807
    :cond_1
    :goto_1
    sget-object v2, Lcom/soula2/Conversation;->A5I:Ljava/util/HashMap;

    iget-object v0, p0, Lcom/soula2/Conversation;->A2b:LX/0mJ;

    const-class v3, LX/0l8;

    .line 97808
    invoke-virtual {v0, v3}, LX/0mJ;->A0A(Ljava/lang/Class;)Lcom/whatsapp/jid/Jid;

    move-result-object v1

    iget-object v0, p0, Lcom/soula2/Conversation;->A2v:Lcom/soula2/mentions/MentionableEntry;

    .line 97809
    invoke-virtual {v0}, Lcom/soula2/mentions/MentionableEntry;->getStringText()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/1bk;->A04(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 97810
    invoke-virtual {v2, v1, v0}, Ljava/util/AbstractMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 97811
    sget-object v2, Lcom/soula2/Conversation;->A5H:Ljava/util/HashMap;

    iget-object v0, p0, Lcom/soula2/Conversation;->A2b:LX/0mJ;

    .line 97812
    invoke-virtual {v0, v3}, LX/0mJ;->A0A(Ljava/lang/Class;)Lcom/whatsapp/jid/Jid;

    move-result-object v1

    iget-object v0, p0, Lcom/soula2/Conversation;->A2v:Lcom/soula2/mentions/MentionableEntry;

    invoke-virtual {v0}, Lcom/soula2/mentions/MentionableEntry;->getMentions()Ljava/util/List;

    move-result-object v0

    invoke-static {v0}, LX/1Xv;->A00(Ljava/util/Collection;)Ljava/lang/String;

    move-result-object v0

    .line 97813
    invoke-virtual {v2, v1, v0}, Ljava/util/AbstractMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 97814
    iget-object v0, p0, Lcom/soula2/Conversation;->A1c:Lcom/soula2/conversation/ConversationListView;

    invoke-virtual {v0}, Lcom/soula2/conversation/ConversationListView;->getConversationCursorAdapter()LX/0me;

    move-result-object v2

    .line 97815
    const-string v0, "conversation/pause/appended/count "

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1, v0}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 97816
    iget-object v0, v2, LX/0me;->A0Q:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/AbstractCollection;->size()I

    move-result v0

    .line 97817
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/whatsapp/util/Log;->d(Ljava/lang/String;)V

    .line 97818
    iget-object v0, p0, Lcom/soula2/Conversation;->A07:Landroid/os/Handler;

    if-eqz v0, :cond_2

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/os/Handler;->hasMessages(I)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 97819
    iget-object v0, p0, Lcom/soula2/Conversation;->A07:Landroid/os/Handler;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    .line 97820
    iget-object v0, p0, LX/0kH;->A08:LX/01e;

    invoke-virtual {v0}, LX/01e;->A0J()Landroid/os/PowerManager;

    move-result-object v0

    if-nez v0, :cond_6

    const-string v0, "conversation/pause pm=null"

    .line 97821
    invoke-static {v0}, Lcom/whatsapp/util/Log;->w(Ljava/lang/String;)V

    .line 97822
    :cond_2
    :goto_2
    iget-object v1, p0, Lcom/soula2/Conversation;->A3x:LX/1mL;

    if-eqz v1, :cond_3

    invoke-interface {v1}, LX/1mL;->ABj()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 97823
    invoke-interface {v1}, LX/1mL;->AXg()V

    .line 97824
    :cond_3
    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v0, 0x18

    if-lt v1, v0, :cond_4

    invoke-virtual {p0}, Landroid/app/Activity;->isInMultiWindowMode()Z

    move-result v0

    if-nez v0, :cond_5

    .line 97825
    :cond_4
    invoke-virtual {p0}, Lcom/soula2/Conversation;->AGh()Z

    move-result v0

    if-eqz v0, :cond_5

    .line 97826
    iget-object v0, p0, Lcom/soula2/Conversation;->A42:LX/0kz;

    invoke-static {v0}, LX/009;->A05(Ljava/lang/Object;)V

    invoke-virtual {v0}, LX/0kz;->A03()V

    :cond_5
    return-void

    .line 97827
    :cond_6
    invoke-virtual {v0}, Landroid/os/PowerManager;->isScreenOn()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 97828
    iget-object v0, p0, Lcom/soula2/Conversation;->A07:Landroid/os/Handler;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    goto :goto_2

    .line 97829
    :cond_7
    iput-boolean v0, v3, LX/17t;->A05:Z

    .line 97830
    iput-boolean v0, v3, LX/17t;->A04:Z

    .line 97831
    iput-boolean v0, v3, LX/17t;->A03:Z

    goto/16 :goto_1

    .line 97832
    :cond_8
    iput-boolean v4, v3, LX/17t;->A05:Z

    .line 97833
    iput-boolean v4, v3, LX/17t;->A04:Z

    goto/16 :goto_0
.end method

.method public onPrepareOptionsMenu(Landroid/view/Menu;)Z
    .locals 2

    .line 97834
    iget-object v0, p0, Lcom/soula2/Conversation;->A57:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1km;

    .line 97835
    invoke-interface {v0, p1}, LX/1km;->ASr(Landroid/view/Menu;)Z

    goto :goto_0

    .line 97836
    :cond_0
    invoke-super {p0, p1}, Landroid/app/Activity;->onPrepareOptionsMenu(Landroid/view/Menu;)Z

    move-result v0

    return v0
.end method

.method public onProvideAssistContent(Landroid/app/assist/AssistContent;)V
    .locals 4

    .line 97837
    invoke-super {p0, p1}, Landroid/app/Activity;->onProvideAssistContent(Landroid/app/assist/AssistContent;)V

    .line 97838
    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v0, 0x17

    if-lt v1, v0, :cond_1

    .line 97839
    iget-object v3, p0, Lcom/soula2/Conversation;->A2r:LX/0l8;

    .line 97840
    const/4 v0, 0x1

    invoke-static {v0}, LX/009;->A0E(Z)V

    .line 97841
    const-string v2, "Invalid"

    move-object v1, v2

    if-eqz v3, :cond_0

    .line 97842
    invoke-static {v3}, LX/0mQ;->A0J(Lcom/whatsapp/jid/Jid;)Z

    move-result v0

    if-nez v0, :cond_3

    invoke-static {v3}, LX/0mQ;->A0E(Lcom/whatsapp/jid/Jid;)Z

    move-result v0

    if-nez v0, :cond_3

    .line 97843
    instance-of v0, v3, Lcom/whatsapp/jid/UserJid;

    .line 97844
    if-eqz v0, :cond_0

    const-string v2, "Individual"

    .line 97845
    :cond_0
    :goto_0
    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    const-string v0, "conversation/provide-assist-content/invalid-conversation-id"

    .line 97846
    invoke-static {v0}, Lcom/whatsapp/util/Log;->i(Ljava/lang/String;)V

    .line 97847
    :cond_1
    return-void

    .line 97848
    :cond_2
    :try_start_0
    new-instance v1, Lorg/json/JSONObject;

    invoke-direct {v1}, Lorg/json/JSONObject;-><init>()V

    const-string v0, "conversation_type"

    .line 97849
    invoke-virtual {v1, v0, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    move-result-object v2

    const-string v1, "conversation_id"

    .line 97850
    invoke-static {v3}, LX/009;->A05(Ljava/lang/Object;)V

    invoke-virtual {v3}, Lcom/whatsapp/jid/Jid;->getRawString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v1, v0}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    move-result-object v0

    .line 97851
    if-eqz v0, :cond_1

    goto :goto_1
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 97852
    :cond_3
    const-string v2, "Group"

    goto :goto_0

    .line 97853
    :goto_1
    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/app/assist/AssistContent;->setStructuredData(Ljava/lang/String;)V

    return-void

    .line 97854
    :catch_0
    move-exception v1

    const-string v0, "assistant-utils/get-conversation-structured-data/exception"

    .line 97855
    invoke-static {v0, v1}, Lcom/whatsapp/util/Log;->i(Ljava/lang/String;Ljava/lang/Throwable;)V

    return-void
.end method

.method public onRestart()V
    .locals 5

    .line 97856
    iget-object v4, p0, LX/0kJ;->A02:LX/0kg;

    iget-object v3, p0, Lcom/soula2/Conversation;->A1c:Lcom/soula2/conversation/ConversationListView;

    const/16 v0, 0x1f

    new-instance v2, Lcom/facebook/redex/RunnableRunnableShape1S0100000_I0;

    invoke-direct {v2, p0, v0}, Lcom/facebook/redex/RunnableRunnableShape1S0100000_I0;-><init>(Lcom/soula2/Conversation;I)V

    const-string v1, "Conversation"

    const/4 v0, 0x2

    invoke-virtual {v4, v3, v2, v1, v0}, LX/0kg;->A05(Landroid/view/View;Ljava/lang/Runnable;Ljava/lang/String;I)V

    .line 97857
    invoke-super {p0}, LX/0kH;->onRestart()V

    return-void
.end method

.method public onResume()V
    .locals 33

    const-string v13, "on_resume"

    const/4 v3, 0x1

    .line 97858
    const/4 v12, 0x0

    .line 97859
    :try_start_0
    move-object/from16 v14, p0

    invoke-virtual {v14, v13}, LX/0kK;->A1n(Ljava/lang/String;)V

    .line 97860
    iput-boolean v12, v14, Lcom/soula2/Conversation;->A4e:Z

    .line 97861
    const-string v0, "conversation/resume"

    new-instance v11, LX/1L4;

    invoke-direct {v11, v0}, LX/1L4;-><init>(Ljava/lang/String;)V

    .line 97862
    iget-object v2, v14, Lcom/soula2/Conversation;->A2N:LX/0uN;

    iget-object v1, v14, LX/0kB;->A07:LX/0nL;

    iget-object v0, v14, Lcom/soula2/Conversation;->A2r:LX/0l8;

    invoke-static {v1, v2, v0}, LX/1b3;->A00(LX/0nL;LX/0uN;LX/0l8;)I

    move-result v0

    if-lez v0, :cond_0

    .line 97863
    iget-object v4, v14, LX/0kH;->A0C:LX/0kj;

    iget-object v2, v14, LX/0kH;->A09:LX/0kh;

    .line 97864
    invoke-virtual {v14}, LX/00j;->A0V()LX/01G;

    move-result-object v1

    .line 97865
    iget-object v0, v14, Lcom/soula2/Conversation;->A2r:LX/0l8;

    .line 97866
    invoke-static {v1, v2, v4, v0, v12}, LX/1b3;->A06(LX/01G;LX/0kh;LX/0kj;LX/0l8;Z)V

    .line 97867
    :cond_0
    invoke-static {}, LX/0y8;->A00()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 97868
    iget-object v2, v14, Lcom/soula2/Conversation;->A1M:LX/0wO;

    const-string/jumbo v1, "show_voip_activity"

    new-instance v0, LX/0wq;

    invoke-direct {v0, v1}, LX/0wq;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, LX/0wO;->A00(LX/0wq;)V

    .line 97869
    :cond_1
    invoke-super {v14}, LX/0kF;->onResume()V

    invoke-static {v14}, Lcom/ApxSAMods/core/Izumi;->onResume(Lcom/soula2/Conversation;)V

    .line 97870
    iget-object v0, v14, Lcom/soula2/Conversation;->A2b:LX/0mJ;

    if-nez v0, :cond_2

    .line 97871
    invoke-virtual {v14}, Landroid/app/Activity;->finish()V

    .line 97872
    invoke-virtual {v11}, LX/1L4;->A01()J

    const-string v0, "conversation/resume/no contact"

    .line 97873
    invoke-static {v0}, Lcom/whatsapp/util/Log;->e(Ljava/lang/String;)V

    goto/16 :goto_7

    .line 97874
    :cond_2
    invoke-virtual {v14}, Lcom/soula2/Conversation;->A2s()V

    .line 97875
    iget-object v0, v14, Lcom/soula2/Conversation;->A34:LX/16T;

    .line 97876
    iget-object v0, v0, LX/16T;->A0K:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/AbstractMap;->clear()V

    .line 97877
    iget-object v1, v14, Lcom/soula2/Conversation;->A2O:LX/0w1;

    iget-object v0, v14, Lcom/soula2/Conversation;->A2b:LX/0mJ;

    const-class v2, LX/0l8;

    invoke-virtual {v0, v2}, LX/0mJ;->A0A(Ljava/lang/Class;)Lcom/whatsapp/jid/Jid;

    move-result-object v0

    check-cast v0, LX/0l8;

    invoke-virtual {v1, v0}, LX/0w1;->A01(LX/0l8;)LX/0mJ;

    move-result-object v0

    iput-object v0, v14, Lcom/soula2/Conversation;->A2b:LX/0mJ;

    .line 97878
    invoke-virtual {v14}, Landroid/app/Activity;->hasWindowFocus()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 97879
    iget-object v1, v14, Lcom/soula2/Conversation;->A1g:LX/0wi;

    iget-object v0, v14, Lcom/soula2/Conversation;->A2b:LX/0mJ;

    invoke-virtual {v0, v2}, LX/0mJ;->A0A(Ljava/lang/Class;)Lcom/whatsapp/jid/Jid;

    move-result-object v0

    check-cast v0, LX/0l8;

    invoke-virtual {v1, v0, v3, v3}, LX/0wi;->A02(LX/0l8;ZZ)V

    .line 97880
    :cond_3
    iget-object v0, v14, Lcom/soula2/Conversation;->A1v:LX/1kT;

    invoke-virtual {v0}, LX/1kT;->A04()V

    .line 97881
    iget-object v0, v14, Lcom/soula2/Conversation;->A2b:LX/0mJ;

    invoke-virtual {v0}, LX/0mJ;->A0J()Z

    move-result v0

    if-nez v0, :cond_4

    .line 97882
    iget-object v1, v14, Lcom/soula2/Conversation;->A3J:LX/10W;

    iget-object v0, v14, Lcom/soula2/Conversation;->A2b:LX/0mJ;

    .line 97883
    invoke-virtual {v0, v2}, LX/0mJ;->A0A(Ljava/lang/Class;)Lcom/whatsapp/jid/Jid;

    move-result-object v0

    check-cast v0, LX/0l8;

    invoke-static {v0}, LX/009;->A05(Ljava/lang/Object;)V

    .line 97884
    invoke-virtual {v1, v0}, LX/10W;->A04(LX/0l8;)V

    .line 97885
    :cond_4
    iget-object v1, v14, Lcom/soula2/Conversation;->A1q:LX/1ld;

    .line 97886
    iget-object v0, v1, LX/1ld;->A01:LX/2FR;

    if-nez v0, :cond_5

    iget-object v0, v1, LX/1ld;->A03:LX/2FS;

    if-eqz v0, :cond_6

    .line 97887
    :cond_5
    invoke-virtual {v14}, Lcom/soula2/Conversation;->A2u()V

    .line 97888
    :cond_6
    sget-boolean v0, Lcom/soula2/Conversation;->A5B:Z

    if-eqz v0, :cond_7

    iget-boolean v0, v14, Lcom/soula2/Conversation;->A4V:Z

    if-nez v0, :cond_7

    iget-boolean v0, v14, Lcom/soula2/Conversation;->A4c:Z

    if-nez v0, :cond_7

    .line 97889
    invoke-virtual {v14}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    const-string v0, "fromNotification"

    invoke-virtual {v1, v0, v12}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    if-eqz v0, :cond_8

    .line 97890
    iget-object v1, v14, Lcom/soula2/Conversation;->A32:LX/0uh;

    iget-object v0, v14, Lcom/soula2/Conversation;->A2r:LX/0l8;

    invoke-virtual {v1, v0}, LX/0uh;->A09(LX/0l8;)V

    .line 97891
    iput-boolean v3, v14, Lcom/soula2/Conversation;->A4a:Z

    .line 97892
    :cond_7
    :goto_0
    sput-boolean v12, Lcom/soula2/Conversation;->A5B:Z

    .line 97893
    sget-object v1, Lcom/soula2/Conversation;->A5F:Ljava/util/ArrayList;

    iget-object v0, v14, Lcom/soula2/Conversation;->A2b:LX/0mJ;

    invoke-virtual {v0, v2}, LX/0mJ;->A0A(Ljava/lang/Class;)Lcom/whatsapp/jid/Jid;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/util/AbstractCollection;->remove(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_9

    const-string v0, "conversation/dialog_ask_gps"

    .line 97894
    invoke-static {v0}, Lcom/whatsapp/util/Log;->i(Ljava/lang/String;)V

    const/4 v0, 0x2

    .line 97895
    invoke-static {v14, v0}, LX/1ix;->A01(Landroid/app/Activity;I)V

    goto :goto_1

    .line 97896
    :cond_8
    iget-object v0, v14, Lcom/soula2/Conversation;->A32:LX/0uh;

    invoke-virtual {v0, v3}, LX/0uh;->A0E(Z)V

    goto :goto_0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 97897
    :cond_9
    :goto_1
    :try_start_1
    iget-object v0, v14, Lcom/soula2/Conversation;->A3k:LX/0y9;

    invoke-virtual {v0}, LX/0y9;->A00()V

    goto :goto_2
    :try_end_1
    .catch Ljava/lang/IllegalStateException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :catch_0
    move-exception v1

    :try_start_2
    const-string v0, "conversation/ap/stateerror"

    .line 97898
    invoke-static {v0, v1}, Lcom/whatsapp/util/Log;->e(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 97899
    :goto_2
    iget-boolean v0, v14, Lcom/soula2/Conversation;->A4V:Z

    if-nez v0, :cond_b

    sget-boolean v0, LX/1if;->A00:Z

    if-eqz v0, :cond_b

    .line 97900
    iget-object v4, v14, Lcom/soula2/Conversation;->A1c:Lcom/soula2/conversation/ConversationListView;

    .line 97901
    const/4 v2, 0x0

    .line 97902
    :goto_3
    invoke-virtual {v4}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v0

    if-ge v2, v0, :cond_b

    .line 97903
    invoke-virtual {v4, v2}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    .line 97904
    instance-of v0, v1, LX/1tk;

    if-eqz v0, :cond_a

    .line 97905
    check-cast v1, LX/1tk;

    .line 97906
    invoke-interface {v1}, LX/1tk;->Acr()V

    :cond_a
    add-int/lit8 v2, v2, 0x1

    goto :goto_3

    .line 97907
    :cond_b
    iget-boolean v0, v14, Lcom/soula2/Conversation;->A4V:Z

    if-eqz v0, :cond_11

    .line 97908
    iput-boolean v12, v14, Lcom/soula2/Conversation;->A4V:Z

    .line 97909
    :cond_c
    :goto_4
    iget-object v1, v14, Lcom/soula2/Conversation;->A12:LX/1Fo;

    iget-object v0, v14, Lcom/soula2/Conversation;->A0z:LX/0vi;

    invoke-virtual {v1, v0}, LX/1Fo;->A02(LX/0vi;)I

    move-result v0

    if-lez v0, :cond_d

    const-string v0, "conversation/dialog_software_expire_warning"

    .line 97910
    invoke-static {v0}, Lcom/whatsapp/util/Log;->i(Ljava/lang/String;)V

    const/16 v0, 0x73

    .line 97911
    invoke-static {v14, v0}, LX/1ix;->A01(Landroid/app/Activity;I)V

    .line 97912
    :cond_d
    iget-object v0, v14, Lcom/soula2/Conversation;->A3O:LX/105;

    .line 97913
    iget-boolean v0, v0, LX/105;->A00:Z

    .line 97914
    if-eqz v0, :cond_e

    const-string v0, "conversation/dialog_login_failed"

    .line 97915
    invoke-static {v0}, Lcom/whatsapp/util/Log;->i(Ljava/lang/String;)V

    .line 97916
    iget-object v2, v14, Lcom/soula2/Conversation;->A22:LX/0sE;

    const/16 v1, 0x14

    const-string v0, "ConvPreReg"

    invoke-virtual {v2, v1, v0}, LX/0sE;->A04(ILjava/lang/String;)V

    .line 97917
    invoke-static {v14}, LX/1vd;->A00(LX/0kM;)Z

    .line 97918
    :cond_e
    iget-object v0, v14, LX/0kB;->A0I:LX/11Q;

    invoke-virtual {v0}, LX/11Q;->A0B()Z

    move-result v0

    if-eqz v0, :cond_f

    .line 97919
    iget-object v0, v14, LX/0kB;->A0I:LX/11Q;

    invoke-virtual {v0}, LX/11Q;->A05()V

    .line 97920
    :cond_f
    iget-boolean v0, v14, Lcom/soula2/Conversation;->A4O:Z

    if-eqz v0, :cond_10

    .line 97921
    iget-object v0, v14, Lcom/soula2/Conversation;->A3L:LX/0md;

    if-eqz v0, :cond_10

    .line 97922
    invoke-virtual {v14, v0}, Lcom/soula2/Conversation;->A37(LX/0md;)V

    .line 97923
    :cond_10
    invoke-virtual {v14}, Lcom/soula2/Conversation;->A2j()V

    .line 97924
    iget-object v0, v14, Lcom/soula2/Conversation;->A56:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_5
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_12

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/Application$ActivityLifecycleCallbacks;

    .line 97925
    invoke-interface {v0, v14}, Landroid/app/Application$ActivityLifecycleCallbacks;->onActivityResumed(Landroid/app/Activity;)V

    goto :goto_5

    .line 97926
    :cond_11
    iget-boolean v0, v14, Lcom/soula2/Conversation;->A4c:Z

    if-eqz v0, :cond_c

    const-string v0, "conversation/resume/refreshstatusdelayed"

    .line 97927
    invoke-static {v0}, Lcom/whatsapp/util/Log;->d(Ljava/lang/String;)V

    .line 97928
    new-instance v2, LX/3Js;

    invoke-direct {v2, v14}, LX/3Js;-><init>(Lcom/soula2/Conversation;)V

    iput-object v2, v14, Lcom/soula2/Conversation;->A07:Landroid/os/Handler;

    const-wide/16 v0, 0xbb8

    .line 97929
    invoke-virtual {v2, v12, v0, v1}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    goto :goto_4

    .line 97930
    :cond_12
    iget-object v0, v14, Lcom/soula2/Conversation;->A3F:LX/16j;

    invoke-virtual {v0, v14}, LX/16j;->A02(Landroid/content/Context;)V

    .line 97931
    iget-boolean v0, v14, Lcom/soula2/Conversation;->A4W:Z

    if-eqz v0, :cond_17

    .line 97932
    iget-object v0, v14, Lcom/soula2/Conversation;->A4B:LX/01K;

    invoke-interface {v0}, LX/01K;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/17t;

    .line 97933
    iget-object v1, v0, LX/17t;->A00:LX/1Wm;

    .line 97934
    if-eqz v1, :cond_13

    .line 97935
    iput-object v1, v14, Lcom/soula2/Conversation;->A3L:LX/0md;

    .line 97936
    iput-boolean v3, v14, Lcom/soula2/Conversation;->A4O:Z

    .line 97937
    iget-object v0, v14, Lcom/soula2/Conversation;->A1c:Lcom/soula2/conversation/ConversationListView;

    invoke-virtual {v0}, Lcom/soula2/conversation/ConversationListView;->getConversationCursorAdapter()LX/0me;

    move-result-object v0

    iget-object v1, v1, LX/0md;->A0z:LX/1HF;

    .line 97938
    iget-object v0, v0, LX/0me;->A0V:Ljava/util/Set;

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 97939
    iget-object v0, v14, Lcom/soula2/Conversation;->A4B:LX/01K;

    invoke-interface {v0}, LX/01K;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/17t;

    .line 97940
    const/4 v0, 0x0

    .line 97941
    iput-object v0, v1, LX/17t;->A00:LX/1Wm;

    .line 97942
    :cond_13
    iget-object v0, v14, LX/0kB;->A0I:LX/11Q;

    invoke-virtual {v0}, LX/11Q;->A00()LX/1gq;

    move-result-object v2

    .line 97943
    iget-object v0, v14, LX/0kB;->A0I:LX/11Q;

    .line 97944
    invoke-virtual {v0}, LX/11Q;->A01()LX/1Wm;

    move-result-object v1

    .line 97945
    iget-object v0, v14, Lcom/soula2/Conversation;->A4B:LX/01K;

    invoke-interface {v0}, LX/01K;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/17t;

    .line 97946
    iget-boolean v0, v0, LX/17t;->A02:Z

    .line 97947
    if-eqz v0, :cond_14

    if-eqz v2, :cond_14

    if-eqz v1, :cond_14

    .line 97948
    iput-boolean v3, v14, Lcom/soula2/Conversation;->A4N:Z

    .line 97949
    new-instance v0, LX/4Y7;

    invoke-direct {v0, v14, v2, v1}, LX/4Y7;-><init>(Lcom/soula2/Conversation;LX/1gq;LX/1Wm;)V

    .line 97950
    iput-object v0, v2, LX/1gq;->A0J:LX/4t9;

    .line 97951
    :cond_14
    iget-object v0, v14, Lcom/soula2/Conversation;->A4B:LX/01K;

    invoke-interface {v0}, LX/01K;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/17t;

    .line 97952
    iget-boolean v0, v0, LX/17t;->A04:Z

    .line 97953
    if-eqz v0, :cond_15

    iget-object v0, v14, Lcom/soula2/Conversation;->A4B:LX/01K;

    .line 97954
    invoke-interface {v0}, LX/01K;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/17t;

    .line 97955
    iget-object v1, v14, Lcom/soula2/Conversation;->A2r:LX/0l8;

    .line 97956
    iget-object v0, v0, LX/17t;->A07:LX/11Q;

    .line 97957
    invoke-virtual {v0}, LX/11Q;->A01()LX/1Wm;

    move-result-object v0

    if-eqz v1, :cond_1b

    if-eqz v0, :cond_1b

    .line 97958
    iget-object v0, v0, LX/0md;->A0z:LX/1HF;

    .line 97959
    iget-object v0, v0, LX/1HF;->A00:LX/0l8;

    .line 97960
    if-eqz v0, :cond_1b

    .line 97961
    invoke-virtual {v1, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    .line 97962
    if-eqz v0, :cond_1b

    .line 97963
    :cond_15
    iget-object v0, v14, LX/0kH;->A00:Landroid/view/View;

    .line 97964
    invoke-static {v0}, LX/17t;->A00(Landroid/view/View;)Z

    move-result v0

    if-eqz v0, :cond_1a

    .line 97965
    iget-object v2, v14, LX/0kB;->A0I:LX/11Q;

    iget-object v1, v14, Lcom/soula2/Conversation;->A4B:LX/01K;

    .line 97966
    iget-object v0, v14, LX/0kH;->A00:Landroid/view/View;

    .line 97967
    invoke-static {v0, v2, v1}, LX/1w1;->A04(Landroid/view/View;LX/11Q;LX/01K;)V

    .line 97968
    :cond_16
    :goto_6
    iget-object v0, v14, Lcom/soula2/Conversation;->A4B:LX/01K;

    invoke-interface {v0}, LX/01K;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/17t;

    invoke-virtual {v0}, LX/17t;->A01()V

    .line 97969
    :cond_17
    iget-object v1, v14, Lcom/soula2/Conversation;->A1c:Lcom/soula2/conversation/ConversationListView;

    .line 97970
    iget-boolean v0, v1, Lcom/soula2/conversation/ConversationListView;->A0E:Z

    if-nez v0, :cond_18

    .line 97971
    invoke-virtual {v1, v12}, Landroid/widget/AbsListView;->setTranscriptMode(I)V

    .line 97972
    :cond_18
    iget-object v0, v14, Lcom/soula2/Conversation;->A18:LX/3MC;

    if-eqz v0, :cond_19

    iget-object v1, v14, LX/0kH;->A0C:LX/0kj;

    const/16 v0, 0x6e6

    .line 97973
    invoke-virtual {v1, v0}, LX/0kj;->A07(I)Z

    move-result v0

    if-eqz v0, :cond_19

    .line 97974
    iget-object v0, v14, Lcom/soula2/Conversation;->A18:LX/3MC;

    .line 97975
    iget-object v0, v0, LX/3MC;->A01:LX/1qk;

    invoke-virtual {v0}, LX/1qk;->A00()V

    .line 97976
    :cond_19
    invoke-virtual {v11}, LX/1L4;->A01()J

    goto/16 :goto_7

    .line 97977
    :cond_1a
    iget-object v0, v14, Lcom/soula2/Conversation;->A4B:LX/01K;

    invoke-interface {v0}, LX/01K;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/17t;

    .line 97978
    iget-boolean v0, v2, LX/17t;->A01:Z

    if-eqz v0, :cond_16

    .line 97979
    iget-object v1, v2, LX/17t;->A06:LX/0pA;

    iget-object v0, v2, LX/17t;->A08:LX/4Vy;

    invoke-virtual {v1, v0}, LX/0pD;->A04(Ljava/lang/Object;)V

    .line 97980
    iput-boolean v12, v2, LX/17t;->A01:Z

    goto :goto_6

    .line 97981
    :cond_1b
    iget-object v0, v14, Lcom/soula2/Conversation;->A4B:LX/01K;

    .line 97982
    invoke-interface {v0}, LX/01K;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/17t;

    .line 97983
    iget-object v1, v14, Lcom/soula2/Conversation;->A2r:LX/0l8;

    .line 97984
    iget-object v0, v0, LX/17t;->A07:LX/11Q;

    .line 97985
    invoke-virtual {v0}, LX/11Q;->A01()LX/1Wm;

    move-result-object v0

    if-eqz v1, :cond_15

    if-eqz v0, :cond_15

    .line 97986
    iget-object v0, v0, LX/0md;->A0z:LX/1HF;

    .line 97987
    iget-object v0, v0, LX/1HF;->A00:LX/0l8;

    .line 97988
    if-eqz v0, :cond_15

    .line 97989
    iget-object v0, v14, LX/0kH;->A00:Landroid/view/View;

    move-object/from16 v32, v0

    .line 97990
    iget-object v0, v14, LX/0kH;->A0C:LX/0kj;

    move-object/from16 v18, v0

    iget-object v0, v14, LX/0kH;->A05:LX/0li;

    move-object/from16 v17, v0

    iget-object v0, v14, LX/0kF;->A01:LX/0nN;

    move-object/from16 v16, v0

    iget-object v15, v14, LX/0kJ;->A05:LX/0lO;

    iget-object v10, v14, LX/0kB;->A0B:LX/0y7;

    iget-object v9, v14, LX/0kB;->A07:LX/0nL;

    iget-object v8, v14, LX/0kB;->A09:LX/0nS;

    iget-object v7, v14, Lcom/soula2/Conversation;->A23:LX/00z;

    iget-object v6, v14, LX/0kB;->A0H:LX/18q;

    iget-object v5, v14, LX/0kB;->A0I:LX/11Q;

    iget-object v4, v14, Lcom/soula2/Conversation;->A4B:LX/01K;

    iget-object v3, v14, Lcom/soula2/Conversation;->A4D:LX/01K;

    iget-object v2, v14, Lcom/soula2/Conversation;->A0B:Landroid/view/View;

    iget-object v1, v14, Lcom/soula2/Conversation;->A1V:LX/1Hc;

    iget-object v0, v14, LX/0kH;->A09:LX/0kh;

    const-string v31, "conversation-activity"

    .line 97991
    move-object/from16 v22, v10

    move-object/from16 v23, v6

    move-object/from16 v24, v5

    move-object/from16 v25, v0

    move-object/from16 v26, v7

    move-object/from16 v27, v18

    move-object/from16 v28, v15

    move-object/from16 v29, v4

    move-object/from16 v30, v3

    move-object/from16 v18, v16

    move-object/from16 v19, v9

    move-object/from16 v20, v8

    move-object/from16 v21, v1

    move-object/from16 v15, v32

    move-object/from16 v16, v2

    invoke-static/range {v14 .. v31}, LX/1w1;->A00(Landroid/app/Activity;Landroid/view/View;Landroid/view/View;LX/0li;LX/0nN;LX/0nL;LX/0nS;LX/1Hc;LX/0y7;LX/18q;LX/11Q;LX/0kh;LX/00z;LX/0kj;LX/0lO;LX/01K;LX/01K;Ljava/lang/String;)Landroid/util/Pair;

    move-result-object v1

    .line 97992
    iget-object v0, v1, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v0, Landroid/view/View;

    iput-object v0, v14, Lcom/soula2/Conversation;->A0B:Landroid/view/View;

    .line 97993
    iget-object v0, v1, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v0, LX/1Hc;

    iput-object v0, v14, Lcom/soula2/Conversation;->A1V:LX/1Hc;

    goto/16 :goto_6
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 97994
    :goto_7
    invoke-virtual {v14, v13}, LX/0kK;->A1m(Ljava/lang/String;)V

    .line 97995
    return-void

    :catchall_0
    move-exception v0

    .line 97996
    invoke-virtual {v14, v13}, LX/0kK;->A1m(Ljava/lang/String;)V

    .line 97997
    throw v0
.end method

.method public onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2

    .line 97998
    invoke-super {p0, p1}, LX/0kB;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 97999
    iget-object v0, p0, Lcom/soula2/Conversation;->A09:Landroid/view/View;

    invoke-static {v0}, LX/15n;->A00(Landroid/view/View;)Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lcom/soula2/Conversation;->A4E:Ljava/lang/Boolean;

    .line 98000
    iget-boolean v1, p0, Lcom/soula2/Conversation;->A4M:Z

    const-string v0, "added_by_qr_code"

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 98001
    iget-boolean v1, p0, Lcom/soula2/Conversation;->A4P:Z

    const-string v0, "has_number_from_url"

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 98002
    iget-boolean v1, p0, Lcom/soula2/Conversation;->A4Q:Z

    const-string v0, "has_text_from_url"

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 98003
    iget-object v0, p0, Lcom/soula2/Conversation;->A4E:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    const-string v0, "keyboard_visible"

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 98004
    iget-boolean v1, p0, Lcom/soula2/Conversation;->A4L:Z

    const-string v0, "added_by_number_search"

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 98005
    iget-object v0, p0, Lcom/soula2/Conversation;->A2Q:LX/1Cs;

    invoke-virtual {v0, p1}, LX/1Cs;->A01(Landroid/os/Bundle;)V

    return-void
.end method

.method public onSearchRequested()Z
    .locals 4

    .line 98006
    iget-object v0, p0, Lcom/soula2/Conversation;->A0c:LX/059;

    const/4 v2, 0x0

    if-nez v0, :cond_3

    .line 98007
    invoke-virtual {p0}, LX/0kB;->A2Z()V

    .line 98008
    iget-object v1, p0, Lcom/soula2/Conversation;->A2S:LX/0mR;

    iget-object v0, p0, Lcom/soula2/Conversation;->A2r:LX/0l8;

    invoke-virtual {v1, v0}, LX/0mR;->A09(LX/0l8;)LX/0mS;

    move-result-object v0

    iput-object v0, p0, Lcom/soula2/Conversation;->A2T:LX/0mS;

    .line 98009
    iget-object v3, p0, LX/0kJ;->A05:LX/0lO;

    const/16 v1, 0x24

    new-instance v0, Lcom/facebook/redex/RunnableRunnableShape1S0100000_I0;

    invoke-direct {v0, p0, v1}, Lcom/facebook/redex/RunnableRunnableShape1S0100000_I0;-><init>(Lcom/soula2/Conversation;I)V

    invoke-interface {v3, v0}, LX/0lO;->AZq(Ljava/lang/Runnable;)V

    .line 98010
    iget-object v0, p0, Lcom/soula2/Conversation;->A0b:LX/03w;

    if-nez v0, :cond_0

    .line 98011
    new-instance v0, Lcom/facebook/redex/IDxCallbackShape361S0100000_1_I0;

    invoke-direct {v0, p0, v2}, Lcom/facebook/redex/IDxCallbackShape361S0100000_1_I0;-><init>(Ljava/lang/Object;I)V

    iput-object v0, p0, Lcom/soula2/Conversation;->A0b:LX/03w;

    .line 98012
    :cond_0
    iget-object v1, p0, Lcom/soula2/Conversation;->A42:LX/0kz;

    if-eqz v1, :cond_4

    .line 98013
    iget-object v0, v1, LX/0kz;->A0P:LX/0l2;

    if-nez v0, :cond_1

    .line 98014
    iget-object v0, v1, LX/0kz;->A1G:LX/0l0;

    .line 98015
    iget-object v0, v0, LX/0l0;->A0B:Landroid/view/ViewGroup;

    invoke-virtual {v0}, Landroid/view/View;->getVisibility()I

    move-result v0

    if-nez v0, :cond_4

    .line 98016
    :cond_1
    :goto_0
    iget-object v0, p0, Lcom/soula2/Conversation;->A09:Landroid/view/View;

    invoke-static {v0}, LX/15n;->A00(Landroid/view/View;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 98017
    iget-object v0, p0, LX/0kH;->A08:LX/01e;

    .line 98018
    invoke-virtual {v0}, LX/01e;->A0R()Landroid/view/inputmethod/InputMethodManager;

    move-result-object v0

    invoke-static {v0}, LX/009;->A05(Ljava/lang/Object;)V

    .line 98019
    invoke-virtual {v0, v2, v2}, Landroid/view/inputmethod/InputMethodManager;->toggleSoftInput(II)V

    .line 98020
    :cond_2
    iget-object v0, p0, Lcom/soula2/Conversation;->A0b:LX/03w;

    invoke-virtual {p0, v0}, LX/00i;->A1N(LX/03w;)LX/059;

    move-result-object v0

    iput-object v0, p0, Lcom/soula2/Conversation;->A0c:LX/059;

    .line 98021
    :cond_3
    return v2

    .line 98022
    :cond_4
    iget-object v1, p0, Lcom/soula2/Conversation;->A09:Landroid/view/View;

    const/16 v0, 0x8

    invoke-virtual {v1, v0}, Landroid/view/View;->setVisibility(I)V

    .line 98023
    invoke-virtual {p0}, Lcom/soula2/Conversation;->A3E()Z

    goto :goto_0
.end method

.method public onStart()V
    .locals 35

    const-string v21, "on_start"

    .line 98024
    :try_start_0
    move-object/from16 v4, p0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v0, "conversation/start "

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v0, v4, Lcom/soula2/Conversation;->A2r:LX/0l8;

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/whatsapp/util/Log;->d(Ljava/lang/String;)V

    .line 98025
    move-object/from16 v0, v21

    invoke-virtual {v4, v0}, LX/0kK;->A1n(Ljava/lang/String;)V

    .line 98026
    const-string v1, "conversation/start"

    new-instance v0, LX/1L4;

    invoke-direct {v0, v1}, LX/1L4;-><init>(Ljava/lang/String;)V

    iput-object v0, v4, Lcom/soula2/Conversation;->A3p:LX/1L4;

    .line 98027
    iget-object v0, v4, Lcom/soula2/Conversation;->A1C:LX/19a;

    invoke-virtual {v0, v4}, LX/0pD;->A03(Ljava/lang/Object;)V

    .line 98028
    invoke-super {v4}, LX/0kF;->onStart()V

    invoke-static {v4}, Lcom/ApxSAMods/core/Izumi;->onStart(Lcom/soula2/Conversation;)V

    .line 98029
    invoke-virtual {v4}, Landroid/app/Activity;->isFinishing()Z

    move-result v0

    if-nez v0, :cond_1d

    .line 98030
    iget-object v0, v4, Lcom/soula2/Conversation;->A2r:LX/0l8;

    if-nez v0, :cond_0

    .line 98031
    invoke-virtual {v4}, Landroid/app/Activity;->finish()V

    .line 98032
    iget-object v0, v4, Lcom/soula2/Conversation;->A3p:LX/1L4;

    invoke-virtual {v0}, LX/1L4;->A01()J

    const-string v0, "conversation/start/no jid"

    .line 98033
    invoke-static {v0}, Lcom/whatsapp/util/Log;->e(Ljava/lang/String;)V

    goto/16 :goto_10

    .line 98034
    :cond_0
    iget-object v0, v4, LX/0kH;->A09:LX/0kh;

    .line 98035
    iget-object v2, v0, LX/0kh;->A00:Landroid/content/SharedPreferences;

    const-string v1, "interface_font_size"

    const-string v0, "0"

    invoke-interface {v2, v1, v0}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const/4 v6, 0x0

    invoke-static {v0, v6}, LX/1M4;->A00(Ljava/lang/String;I)I

    move-result v0

    .line 98036
    sput v0, Lcom/soula2/preference/WaFontListPreference;->A00:I

    .line 98037
    invoke-virtual {v4}, Lcom/soula2/Conversation;->A2s()V

    .line 98038
    invoke-virtual {v4}, LX/0kK;->A1j()V

    .line 98039
    iget-object v0, v4, Lcom/soula2/Conversation;->A1q:LX/1ld;

    iget-object v1, v0, LX/1ld;->A00:Landroid/view/View;

    const/16 v20, 0x8

    move/from16 v0, v20

    invoke-virtual {v1, v0}, Landroid/view/View;->setVisibility(I)V

    .line 98040
    iget-object v5, v4, Lcom/soula2/Conversation;->A1x:LX/0mf;

    .line 98041
    invoke-virtual {v4}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    move-result-object v9

    .line 98042
    invoke-virtual {v4}, Lcom/soula2/Conversation;->A2f()I

    move-result v19

    iget-object v13, v4, Lcom/soula2/Conversation;->A1t:LX/2Fe;

    iget-wide v14, v4, Lcom/soula2/Conversation;->A03:J

    iget-object v0, v4, Lcom/soula2/Conversation;->A1c:Lcom/soula2/conversation/ConversationListView;

    .line 98043
    invoke-virtual {v0}, Lcom/soula2/conversation/ConversationListView;->getConversationCursorAdapter()LX/0me;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/CursorAdapter;->getCursor()Landroid/database/Cursor;

    move-result-object v0

    const/16 v32, 0x0

    if-nez v0, :cond_1

    const/16 v32, 0x1

    :cond_1
    iget-boolean v0, v4, Lcom/soula2/Conversation;->A4S:Z

    move/from16 v18, v0

    .line 98044
    iget-object v0, v5, LX/0mf;->A0O:LX/0uN;

    move-object/from16 v23, v0

    iget-object v8, v5, LX/0mf;->A0S:LX/0l8;

    invoke-virtual {v0, v8}, LX/0uN;->A07(LX/0l8;)LX/1SS;

    move-result-object v0

    .line 98045
    iget v7, v0, LX/1SS;->A00:I

    iput v7, v5, LX/0mf;->A01:I

    .line 98046
    iget v3, v0, LX/1SS;->A01:I

    iput v3, v5, LX/0mf;->A02:I

    if-gtz v7, :cond_2

    if-gtz v3, :cond_2

    const/4 v2, 0x0

    goto :goto_0

    .line 98047
    :cond_2
    iget v2, v0, LX/1SS;->A02:I

    :goto_0
    iput v2, v5, LX/0mf;->A03:I

    .line 98048
    const-string v0, "conversation/start/unseen "

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1, v0}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v0, "/"

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/whatsapp/util/Log;->i(Ljava/lang/String;)V

    .line 98049
    invoke-virtual {v5}, LX/0mf;->A04()V

    .line 98050
    const-string/jumbo v12, "row_id"

    const-wide/16 v2, 0x0

    .line 98051
    invoke-virtual {v9, v12, v2, v3}, Landroid/content/Intent;->getLongExtra(Ljava/lang/String;J)J

    move-result-wide v0

    const-string/jumbo v11, "sort_id"

    .line 98052
    invoke-virtual {v9, v11, v2, v3}, Landroid/content/Intent;->getLongExtra(Ljava/lang/String;J)J

    move-result-wide v16

    cmp-long v7, v0, v2

    if-lez v7, :cond_3

    .line 98053
    iget-object v7, v13, LX/2Fe;->A07:LX/241;

    .line 98054
    iget-object v10, v7, LX/241;->A01:LX/1OE;

    .line 98055
    iget-object v7, v10, LX/1OE;->A06:LX/1OF;

    move-object/from16 v22, v7

    .line 98056
    invoke-virtual/range {v22 .. v22}, LX/1OF;->A00()Z

    move-result v7

    .line 98057
    if-eqz v7, :cond_4

    const-string v7, "conversation-qpl-annotations/logIsStarred"

    .line 98058
    invoke-static {v7}, Lcom/whatsapp/util/Log;->d(Ljava/lang/String;)V

    .line 98059
    new-instance v7, LX/3C8;

    invoke-direct {v7, v13, v0, v1}, LX/3C8;-><init>(LX/2Fe;J)V

    .line 98060
    iget-object v13, v10, LX/1OE;->A07:LX/0xg;

    .line 98061
    move-object/from16 v10, v22

    iget v10, v10, LX/1OF;->A05:I

    .line 98062
    invoke-virtual {v13, v7, v10}, LX/0xg;->AJV(LX/1O8;I)V

    goto :goto_1

    .line 98063
    :cond_3
    iget-object v7, v13, LX/2Fe;->A07:LX/241;

    .line 98064
    iget-object v10, v7, LX/241;->A01:LX/1OE;

    .line 98065
    iget-object v7, v10, LX/1OE;->A06:LX/1OF;

    .line 98066
    invoke-virtual {v7}, LX/1OF;->A00()Z

    move-result v7

    .line 98067
    if-eqz v7, :cond_4

    const-string v7, "conversation-qpl-annotations/is_starred"

    .line 98068
    invoke-static {v7}, Lcom/whatsapp/util/Log;->d(Ljava/lang/String;)V

    .line 98069
    const-string v7, "is_starred"

    .line 98070
    invoke-virtual {v10, v7, v6, v6}, LX/1OE;->A0B(Ljava/lang/String;ZZ)V

    .line 98071
    :cond_4
    :goto_1
    const/4 v10, -0x1

    const/4 v13, 0x0

    cmp-long v7, v0, v2

    if-lez v7, :cond_8

    .line 98072
    invoke-virtual {v9, v12, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    .line 98073
    invoke-virtual {v9, v11, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    .line 98074
    invoke-static {v9}, LX/1hb;->A02(Landroid/content/Intent;)LX/1HF;

    move-result-object v27

    const-string/jumbo v7, "query"

    .line 98075
    invoke-virtual {v9, v7}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    iput-object v10, v5, LX/0mf;->A09:Ljava/lang/String;

    .line 98076
    iget-object v7, v5, LX/0mf;->A0M:LX/00z;

    invoke-static {v7, v10}, LX/1bl;->A02(LX/00z;Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v7

    iput-object v7, v5, LX/0mf;->A0B:Ljava/util/ArrayList;

    .line 98077
    iget-object v7, v5, LX/0mf;->A09:Ljava/lang/String;

    iput-object v7, v5, LX/0mf;->A0A:Ljava/lang/String;

    .line 98078
    const/16 v30, 0x33
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_4

    .line 98079
    :try_start_1
    iget-object v7, v5, LX/0mf;->A0P:LX/0nZ;

    .line 98080
    move-object/from16 v28, v7

    move-object/from16 v29, v8

    move-wide/from16 v31, v0

    move-wide/from16 v33, v14

    invoke-virtual/range {v28 .. v34}, LX/0nZ;->A0A(LX/0l8;IJJ)LX/1Ve;

    move-result-object v7

    .line 98081
    iget-object v10, v7, LX/1Ve;->A00:Landroid/database/Cursor;

    invoke-interface {v10}, Landroid/database/Cursor;->getCount()I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 98082
    :try_start_2
    const-string v10, "conversation/start/count"

    .line 98083
    invoke-static {v10}, Lcom/whatsapp/util/Log;->i(Ljava/lang/String;)V

    .line 98084
    iget-wide v10, v7, LX/1Ve;->A02:J

    cmp-long v12, v16, v2

    if-lez v12, :cond_5

    move-wide/from16 v0, v16
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_4

    .line 98085
    :cond_5
    :try_start_3
    iget-object v2, v5, LX/0mf;->A0Q:LX/13P;

    move-object/from16 v28, v2

    move-wide/from16 v30, v10

    move-wide/from16 v32, v0

    invoke-virtual/range {v28 .. v33}, LX/13P;->A02(LX/0l8;JJ)I

    move-result v10
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 98086
    :try_start_4
    invoke-virtual/range {v23 .. v23}, LX/0uN;->A0B()Ljava/util/concurrent/ConcurrentHashMap;

    move-result-object v0

    invoke-virtual {v0, v8}, Ljava/util/AbstractMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1Ni;

    if-nez v0, :cond_6

    goto :goto_2

    .line 98087
    :cond_6
    iget-wide v0, v0, LX/1Ni;->A0P:J

    goto :goto_3

    .line 98088
    :goto_2
    const-wide/16 v0, 0x1

    .line 98089
    :goto_3
    cmp-long v2, v16, v0

    if-lez v2, :cond_7

    add-int/lit8 v10, v10, 0x1

    .line 98090
    :cond_7
    const-string v1, "conversation/start/count/result "

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/whatsapp/util/Log;->i(Ljava/lang/String;)V

    move-object v12, v13

    const/16 v32, 0x1

    goto/16 :goto_7

    .line 98091
    :catchall_0
    move-exception v0

    .line 98092
    throw v0

    .line 98093
    :catchall_1
    move-exception v0

    .line 98094
    throw v0

    .line 98095
    :cond_8
    iget-object v0, v5, LX/0mf;->A0K:LX/0z4;

    .line 98096
    invoke-static {v8}, LX/009;->A05(Ljava/lang/Object;)V

    .line 98097
    iget-object v0, v0, LX/0z4;->A00:Ljava/util/HashMap;

    invoke-virtual {v0, v8}, Ljava/util/AbstractMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/45z;

    .line 98098
    if-eqz v32, :cond_9

    .line 98099
    iget v0, v5, LX/0mf;->A03:I

    if-nez v0, :cond_9

    if-eqz v2, :cond_9

    const-string v0, "from_chats_list"

    .line 98100
    invoke-virtual {v9, v0, v6}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    if-eqz v0, :cond_9

    .line 98101
    iget-wide v0, v2, LX/45z;->A02:J

    invoke-virtual {v5, v0, v1}, LX/0mf;->A08(J)V

    .line 98102
    iget-wide v0, v2, LX/45z;->A03:J

    invoke-virtual {v5, v0, v1}, LX/0mf;->A08(J)V

    .line 98103
    iget v1, v2, LX/45z;->A01:I

    if-gez v1, :cond_a

    .line 98104
    sget-object v0, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    .line 98105
    new-instance v3, LX/1ll;

    invoke-direct {v3, v13, v0}, LX/1ll;-><init>(LX/0md;Ljava/lang/Boolean;)V

    .line 98106
    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v0

    new-instance v2, Landroid/os/Handler;

    invoke-direct {v2, v0}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    .line 98107
    const/4 v1, 0x7

    new-instance v0, Lcom/facebook/redex/RunnableRunnableShape5S0200000_I0_3;

    invoke-direct {v0, v5, v1, v3}, Lcom/facebook/redex/RunnableRunnableShape5S0200000_I0_3;-><init>(Ljava/lang/Object;ILjava/lang/Object;)V

    invoke-virtual {v2, v0}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 98108
    move-object v12, v13

    goto :goto_4

    .line 98109
    :cond_9
    move-object v12, v13

    if-eqz v32, :cond_b

    goto :goto_5

    .line 98110
    :cond_a
    iget v0, v2, LX/45z;->A00:I

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v12

    move v10, v1

    .line 98111
    :goto_4
    const-string v0, "conversation/start/scroll-state pos:"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2, v0}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v0, " offset:"

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v0, " startRef:"

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 98112
    iget-wide v0, v5, LX/0mf;->A04:J

    .line 98113
    invoke-virtual {v2, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    const-string v0, " startSortRef:"

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 98114
    iget-wide v0, v5, LX/0mf;->A05:J

    .line 98115
    invoke-virtual {v2, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    .line 98116
    invoke-static {v0}, Lcom/whatsapp/util/Log;->d(Ljava/lang/String;)V

    .line 98117
    :goto_5
    iget v1, v5, LX/0mf;->A03:I

    const/16 v0, 0x5a

    if-le v1, v0, :cond_b

    add-int/lit8 v0, v1, 0xa

    .line 98118
    invoke-virtual {v5, v0, v14, v15}, LX/0mf;->A03(IJ)LX/1Ve;

    move-result-object v7

    goto :goto_6

    .line 98119
    :cond_b
    move/from16 v0, v19

    invoke-virtual {v5, v0, v14, v15}, LX/0mf;->A03(IJ)LX/1Ve;

    move-result-object v7

    :goto_6
    move-object/from16 v27, v13

    .line 98120
    :goto_7
    iget-object v2, v7, LX/1Ve;->A00:Landroid/database/Cursor;

    if-eqz v2, :cond_1c

    invoke-interface {v2}, Landroid/database/Cursor;->isClosed()Z

    move-result v0

    if-nez v0, :cond_1c

    if-eqz v32, :cond_c

    .line 98121
    iget v0, v5, LX/0mf;->A03:I

    if-lez v0, :cond_c

    .line 98122
    invoke-virtual/range {v23 .. v23}, LX/0uN;->A0B()Ljava/util/concurrent/ConcurrentHashMap;

    move-result-object v0

    invoke-virtual {v0, v8}, Ljava/util/AbstractMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1Ni;

    if-eqz v0, :cond_c

    .line 98123
    iget-wide v0, v0, LX/1Ni;->A0K:J

    .line 98124
    const-wide/16 v14, 0x1

    cmp-long v3, v0, v14

    if-lez v3, :cond_c

    .line 98125
    iget-object v11, v5, LX/0mf;->A0a:LX/0lO;

    iget-object v0, v5, LX/0mf;->A0D:LX/012;

    .line 98126
    new-instance v3, LX/28L;

    invoke-direct {v3, v0}, LX/28L;-><init>(LX/012;)V

    new-instance v1, Lcom/facebook/redex/RunnableRunnableShape5S0200000_I0_3;

    move/from16 v0, v20

    invoke-direct {v1, v3, v0, v5}, Lcom/facebook/redex/RunnableRunnableShape5S0200000_I0_3;-><init>(Ljava/lang/Object;ILjava/lang/Object;)V

    .line 98127
    invoke-interface {v11, v1}, LX/0lO;->AZq(Ljava/lang/Runnable;)V

    .line 98128
    :cond_c
    iget v1, v5, LX/0mf;->A03:I

    if-lez v1, :cond_d

    invoke-interface {v2}, Landroid/database/Cursor;->getCount()I

    move-result v0

    if-le v1, v0, :cond_d

    .line 98129
    const-string v0, "conversation/start/unseen greater than cursor count "

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1, v0}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 98130
    invoke-interface {v2}, Landroid/database/Cursor;->getCount()I

    move-result v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    .line 98131
    invoke-static {v0}, Lcom/whatsapp/util/Log;->i(Ljava/lang/String;)V

    .line 98132
    iput v6, v5, LX/0mf;->A03:I

    .line 98133
    iput v6, v5, LX/0mf;->A01:I

    .line 98134
    iput v6, v5, LX/0mf;->A02:I

    .line 98135
    :cond_d
    iget-wide v0, v7, LX/1Ve;->A01:J

    invoke-virtual {v5, v0, v1}, LX/0mf;->A08(J)V

    .line 98136
    iget-wide v0, v7, LX/1Ve;->A02:J

    invoke-virtual {v5, v0, v1}, LX/0mf;->A09(J)V

    .line 98137
    if-eqz v32, :cond_e

    .line 98138
    invoke-interface {v2}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_e
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_4

    .line 98139
    :try_start_5
    iget-object v0, v5, LX/0mf;->A0P:LX/0nZ;

    .line 98140
    iget-object v0, v0, LX/0nZ;->A0K:LX/0oZ;

    invoke-virtual {v0, v2, v8}, LX/0oZ;->A02(Landroid/database/Cursor;LX/0l8;)LX/0md;

    move-result-object v13

    .line 98141
    goto :goto_8
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_2

    :catchall_2
    :try_start_6
    move-exception v0

    .line 98142
    throw v0

    .line 98143
    :cond_e
    :goto_8
    iget v1, v5, LX/0mf;->A03:I

    if-lez v1, :cond_f

    invoke-interface {v2}, Landroid/database/Cursor;->getCount()I

    move-result v0

    if-ne v1, v0, :cond_f

    const-string v0, "conversation/start/scrolltotop/true"

    .line 98144
    invoke-static {v0}, Lcom/whatsapp/util/Log;->d(Ljava/lang/String;)V

    .line 98145
    iput v6, v5, LX/0mf;->A03:I

    .line 98146
    iput v6, v5, LX/0mf;->A01:I

    .line 98147
    iput v6, v5, LX/0mf;->A02:I

    .line 98148
    :cond_f
    iget v0, v5, LX/0mf;->A03:I

    if-lez v0, :cond_10

    if-eqz v18, :cond_10

    .line 98149
    invoke-interface {v2}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_12

    goto :goto_b

    .line 98150
    :goto_9
    iget-object v1, v0, LX/0md;->A0z:LX/1HF;

    iget-boolean v1, v1, LX/1HF;->A02:Z

    if-eqz v1, :cond_10

    .line 98151
    iget v1, v0, LX/0md;->A0C:I

    .line 98152
    const/4 v0, 0x6

    if-eq v1, v0, :cond_11

    .line 98153
    :cond_10
    :goto_a
    iget v1, v5, LX/0mf;->A03:I

    if-lez v1, :cond_14

    invoke-interface {v2}, Landroid/database/Cursor;->getCount()I

    move-result v0

    const/16 v33, 0x1

    if-eq v1, v0, :cond_15

    goto :goto_c

    .line 98154
    :cond_11
    invoke-interface {v2}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-nez v0, :cond_13

    .line 98155
    :cond_12
    const-string v0, "conversation/start/reset-unseen"

    .line 98156
    invoke-static {v0}, Lcom/whatsapp/util/Log;->d(Ljava/lang/String;)V

    .line 98157
    iput v6, v5, LX/0mf;->A03:I

    .line 98158
    iput v6, v5, LX/0mf;->A01:I

    .line 98159
    iput v6, v5, LX/0mf;->A02:I

    goto :goto_a
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_4

    .line 98160
    :cond_13
    :goto_b
    :try_start_7
    iget-object v0, v5, LX/0mf;->A0P:LX/0nZ;

    .line 98161
    iget-object v0, v0, LX/0nZ;->A0K:LX/0oZ;

    invoke-virtual {v0, v2, v8}, LX/0oZ;->A02(Landroid/database/Cursor;LX/0l8;)LX/0md;

    move-result-object v0

    .line 98162
    if-eqz v0, :cond_11

    goto :goto_9

    .line 98163
    :cond_14
    :goto_c
    const/16 v33, 0x0
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_3

    .line 98164
    :cond_15
    :try_start_8
    iget v0, v5, LX/0mf;->A03:I

    const/16 v34, 0x0

    if-lez v0, :cond_16

    const/16 v34, 0x1

    .line 98165
    :cond_16
    invoke-interface {v2}, Landroid/database/Cursor;->getCount()I

    move-result v3

    const/4 v1, 0x0

    move/from16 v0, v19

    if-lt v3, v0, :cond_17

    const/4 v1, 0x1

    :cond_17
    iput-boolean v1, v5, LX/0mf;->A0C:Z

    .line 98166
    invoke-virtual/range {v23 .. v23}, LX/0uN;->A0B()Ljava/util/concurrent/ConcurrentHashMap;

    move-result-object v0

    invoke-virtual {v0, v8}, Ljava/util/AbstractMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1Ni;

    if-nez v0, :cond_18

    const/4 v0, 0x0

    goto :goto_d

    .line 98167
    :cond_18
    iget v0, v0, LX/1Ni;->A02:I

    .line 98168
    :goto_d
    iput v0, v5, LX/0mf;->A00:I

    .line 98169
    const-string v0, "conversation/start/has-earlier "

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1, v0}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-boolean v0, v5, LX/0mf;->A0C:Z

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/whatsapp/util/Log;->d(Ljava/lang/String;)V

    .line 98170
    iget-boolean v0, v5, LX/0mf;->A0C:Z

    const/16 v30, 0x8

    if-eqz v0, :cond_19

    const/16 v30, 0x0

    .line 98171
    :cond_19
    invoke-virtual/range {v23 .. v23}, LX/0uN;->A0B()Ljava/util/concurrent/ConcurrentHashMap;

    move-result-object v0

    invoke-virtual {v0, v8}, Ljava/util/AbstractMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1Ni;

    if-nez v0, :cond_1a

    const/4 v11, 0x0

    goto :goto_e

    .line 98172
    :cond_1a
    iget v11, v0, LX/1Ni;->A02:I

    .line 98173
    :goto_e
    invoke-virtual {v5, v7}, LX/0mf;->A0A(LX/1Ve;)V

    .line 98174
    iget-object v6, v5, LX/0mf;->A0X:LX/1HU;

    .line 98175
    iget v8, v5, LX/0mf;->A01:I

    iget v3, v5, LX/0mf;->A02:I

    iget v1, v5, LX/0mf;->A03:I

    new-instance v0, LX/2B6;

    invoke-direct {v0, v8, v3, v1}, LX/2B6;-><init>(III)V

    .line 98176
    sget-object v3, Lcom/soula2/Conversation;->A5E:Ljava/lang/String;

    invoke-virtual {v9}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v3, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1b

    invoke-interface {v2}, Landroid/database/Cursor;->getCount()I

    move-result v1

    if-lez v1, :cond_1b

    .line 98177
    invoke-static {v9}, LX/1hb;->A02(Landroid/content/Intent;)LX/1HF;

    move-result-object v2

    if-eqz v2, :cond_1b

    .line 98178
    iget-object v1, v5, LX/0mf;->A0P:LX/0nZ;

    .line 98179
    iget-object v1, v1, LX/0nZ;->A0K:LX/0oZ;

    invoke-virtual {v1, v2}, LX/0oZ;->A04(LX/1HF;)LX/0md;

    move-result-object v26

    .line 98180
    :goto_f
    new-instance v1, LX/2FX;

    move-object/from16 v22, v1

    move-object/from16 v23, v0

    move-object/from16 v24, v7

    move-object/from16 v25, v13

    move-object/from16 v28, v12

    move/from16 v29, v10

    move/from16 v31, v11

    invoke-direct/range {v22 .. v34}, LX/2FX;-><init>(LX/2B6;LX/1Ve;LX/0md;LX/0md;LX/1HF;Ljava/lang/Integer;IIIZZZ)V

    .line 98181
    invoke-virtual {v6, v1}, LX/013;->A0B(Ljava/lang/Object;)V

    goto :goto_10

    .line 98182
    :cond_1b
    const/16 v26, 0x0

    goto :goto_f

    .line 98183
    :catchall_3
    move-exception v0

    .line 98184
    throw v0

    .line 98185
    :cond_1c
    const-string v0, "conversation/start/cursor is null"

    .line 98186
    invoke-static {v0}, Lcom/whatsapp/util/Log;->w(Ljava/lang/String;)V
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_4

    .line 98187
    :cond_1d
    :goto_10
    move-object/from16 v0, v21

    invoke-virtual {v4, v0}, LX/0kK;->A1m(Ljava/lang/String;)V

    .line 98188
    return-void

    :catchall_4
    move-exception v1

    .line 98189
    move-object/from16 v0, v21

    invoke-virtual {v4, v0}, LX/0kK;->A1m(Ljava/lang/String;)V

    .line 98190
    throw v1
.end method

.method public onStop()V
    .locals 12

    .line 98191
    invoke-super {p0}, LX/00i;->onStop()V

    .line 98192
    iget-object v0, p0, Lcom/soula2/Conversation;->A1C:LX/19a;

    invoke-virtual {v0, p0}, LX/0pD;->A04(Ljava/lang/Object;)V

    .line 98193
    iget-object v0, p0, Lcom/soula2/Conversation;->A09:Landroid/view/View;

    invoke-static {v0}, LX/15n;->A00(Landroid/view/View;)Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lcom/soula2/Conversation;->A4E:Ljava/lang/Boolean;

    .line 98194
    invoke-virtual {p0}, Lcom/soula2/Conversation;->AGh()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 98195
    iget-object v0, p0, Lcom/soula2/Conversation;->A42:LX/0kz;

    invoke-static {v0}, LX/009;->A05(Ljava/lang/Object;)V

    invoke-virtual {v0}, LX/0kz;->A03()V

    .line 98196
    :cond_0
    invoke-virtual {p0}, Lcom/soula2/Conversation;->A2t()V

    .line 98197
    iget-object v1, p0, Lcom/soula2/Conversation;->A3x:LX/1mL;

    if-eqz v1, :cond_1

    invoke-interface {v1}, LX/1mL;->ABj()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 98198
    invoke-interface {v1}, LX/1mL;->AXg()V

    .line 98199
    :cond_1
    iget-object v1, p0, Lcom/soula2/Conversation;->A39:LX/17m;

    iget-object v0, p0, Lcom/soula2/Conversation;->A4z:LX/2Fd;

    invoke-virtual {v1, v0}, LX/17m;->A01(LX/2Fd;)V

    .line 98200
    iget-object v0, p0, Lcom/soula2/Conversation;->A2v:Lcom/soula2/mentions/MentionableEntry;

    invoke-virtual {v0}, Lcom/soula2/WaEditText;->A04()V

    .line 98201
    iget-object v0, p0, Lcom/soula2/Conversation;->A29:Lcom/soula2/ctwa/bizpreview/BusinessPreviewInitializer;

    .line 98202
    iget-object v0, v0, Lcom/soula2/ctwa/bizpreview/BusinessPreviewInitializer;->A04:LX/012;

    .line 98203
    invoke-virtual {v0, p0}, LX/013;->A04(LX/00m;)V

    .line 98204
    iget-object v1, p0, LX/00k;->A06:LX/05H;

    .line 98205
    iget-object v0, p0, Lcom/soula2/Conversation;->A29:Lcom/soula2/ctwa/bizpreview/BusinessPreviewInitializer;

    invoke-virtual {v1, v0}, LX/05I;->A01(LX/03O;)V

    .line 98206
    const-string v0, "conversation/stop/release "

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1, v0}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {}, Landroid/database/sqlite/SQLiteDatabase;->releaseMemory()I

    move-result v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v0, " jid="

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v0, p0, Lcom/soula2/Conversation;->A2r:LX/0l8;

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/whatsapp/util/Log;->i(Ljava/lang/String;)V

    .line 98207
    iget-object v1, p0, Lcom/soula2/Conversation;->A1c:Lcom/soula2/conversation/ConversationListView;

    iget-object v4, p0, Lcom/soula2/Conversation;->A2r:LX/0l8;

    iget-object v0, p0, Lcom/soula2/Conversation;->A1x:LX/0mf;

    .line 98208
    iget-wide v8, v0, LX/0mf;->A04:J

    .line 98209
    iget-wide v10, v0, LX/0mf;->A05:J

    .line 98210
    invoke-virtual {v1}, Landroid/view/View;->getHeight()I

    move-result v0

    if-eqz v0, :cond_2

    .line 98211
    invoke-virtual {v1}, Lcom/soula2/conversation/ConversationListView;->A0A()Z

    move-result v0

    if-eqz v0, :cond_4

    const/4 v0, 0x1

    .line 98212
    iput-boolean v0, v1, Lcom/soula2/conversation/ConversationListView;->A0E:Z

    const-string v0, "conversation/dbgscroll/resumescrolltobottom/true"

    .line 98213
    invoke-static {v0}, Lcom/whatsapp/util/Log;->d(Ljava/lang/String;)V

    .line 98214
    iget-object v0, v1, Lcom/soula2/conversation/ConversationListView;->A04:LX/0z4;

    invoke-static {v4}, LX/009;->A05(Ljava/lang/Object;)V

    .line 98215
    iget-object v0, v0, LX/0z4;->A00:Ljava/util/HashMap;

    invoke-virtual {v0, v4}, Ljava/util/AbstractMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 98216
    :cond_2
    :goto_0
    invoke-virtual {v1}, Lcom/soula2/conversation/ConversationListView;->getConversationCursorAdapter()LX/0me;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/CursorAdapter;->getCursor()Landroid/database/Cursor;

    move-result-object v0

    if-eqz v0, :cond_3

    .line 98217
    invoke-interface {v0}, Landroid/database/Cursor;->deactivate()V

    .line 98218
    :cond_3
    return-void

    .line 98219
    :cond_4
    invoke-virtual {v1}, Landroid/widget/AdapterView;->getFirstVisiblePosition()I

    move-result v3

    iput v3, v1, Lcom/soula2/conversation/ConversationListView;->A01:I

    .line 98220
    const-string v2, "conversation/dbgscroll/resumescrolltopos/"

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/whatsapp/util/Log;->d(Ljava/lang/String;)V

    const/4 v2, 0x0

    .line 98221
    invoke-virtual {v1, v2}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    .line 98222
    iput v2, v1, Lcom/soula2/conversation/ConversationListView;->A02:I

    if-eqz v0, :cond_5

    .line 98223
    invoke-virtual {v0}, Landroid/view/View;->getTop()I

    move-result v0

    iput v0, v1, Lcom/soula2/conversation/ConversationListView;->A02:I

    .line 98224
    :cond_5
    iget-object v3, v1, Lcom/soula2/conversation/ConversationListView;->A04:LX/0z4;

    .line 98225
    invoke-static {v4}, LX/009;->A05(Ljava/lang/Object;)V

    iget v6, v1, Lcom/soula2/conversation/ConversationListView;->A01:I

    .line 98226
    invoke-virtual {v1}, Landroid/widget/ListView;->getHeaderViewsCount()I

    move-result v0

    sub-int/2addr v6, v0

    iget v7, v1, Lcom/soula2/conversation/ConversationListView;->A02:I

    new-instance v5, LX/45z;

    invoke-direct/range {v5 .. v11}, LX/45z;-><init>(IIJJ)V

    .line 98227
    iget-object v0, v3, LX/0z4;->A00:Ljava/util/HashMap;

    invoke-virtual {v0, v4, v5}, Ljava/util/AbstractMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 98228
    const-string v0, "conversation/dbgscroll/scroll-state pos:"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3, v0}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v0, v1, Lcom/soula2/conversation/ConversationListView;->A01:I

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v0, " offset:"

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v0, v1, Lcom/soula2/conversation/ConversationListView;->A02:I

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v0, " startRef:"

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3, v8, v9}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    const-string v0, " startSortRef:"

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3, v10, v11}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    const-string v0, " headerCount:"

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 98229
    invoke-virtual {v1}, Landroid/widget/ListView;->getHeaderViewsCount()I

    move-result v0

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    .line 98230
    invoke-static {v0}, Lcom/whatsapp/util/Log;->d(Ljava/lang/String;)V

    .line 98231
    iput-boolean v2, v1, Lcom/soula2/conversation/ConversationListView;->A0E:Z

    const-string v0, "conversation/dbgscroll/resumescrolltobottom/false"

    .line 98232
    invoke-static {v0}, Lcom/whatsapp/util/Log;->d(Ljava/lang/String;)V

    goto/16 :goto_0
.end method

.method public onWindowFocusChanged(Z)V
    .locals 3

    .line 98233
    const-string v0, "conversation/window-focus-changed "

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1, v0}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/soula2/Conversation;->A2b:LX/0mJ;

    .line 98234
    iget-object v0, v0, LX/0mJ;->A0D:Lcom/whatsapp/jid/Jid;

    .line 98235
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v0, " "

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/whatsapp/util/Log;->i(Ljava/lang/String;)V

    .line 98236
    invoke-super {p0, p1}, Landroid/app/Activity;->onWindowFocusChanged(Z)V

    if-eqz p1, :cond_0

    .line 98237
    iget-object v2, p0, Lcom/soula2/Conversation;->A1g:LX/0wi;

    iget-object v1, p0, Lcom/soula2/Conversation;->A2b:LX/0mJ;

    const-class v0, LX/0l8;

    invoke-virtual {v1, v0}, LX/0mJ;->A0A(Ljava/lang/Class;)Lcom/whatsapp/jid/Jid;

    move-result-object v1

    check-cast v1, LX/0l8;

    const/4 v0, 0x1

    invoke-virtual {v2, v1, v0, v0}, LX/0wi;->A02(LX/0l8;ZZ)V

    .line 98238
    iget-boolean v0, p0, Lcom/soula2/Conversation;->A4a:Z

    if-eqz v0, :cond_0

    .line 98239
    iget-object v0, p0, Lcom/soula2/Conversation;->A32:LX/0uh;

    invoke-virtual {v0}, LX/0uh;->A08()V

    const/4 v0, 0x0

    .line 98240
    iput-boolean v0, p0, Lcom/soula2/Conversation;->A4a:Z

    :cond_0
    return-void
.end method
